/**************************************************
 *
 * Declarations for segment initialization.
 *
 * Copyright 1999-2005 IAR Systems. All rights reserved.
 *
 * $Revision: 1.3 $
 *
 **************************************************/

#ifndef __SEGMENT_INIT_H_INCLUDED
#define __SEGMENT_INIT_H_INCLUDED


#ifdef __cplusplus
extern "C" {
#endif

#pragma language=extended

__interwork void __segment_init(void);



/* Structure of an element in the segment initialization table
 * in segment INITTAB. Used for both zero-initialization and copying. */
typedef struct
{
  long   Size;                  /* Number of bytes to initialize */
  char*  Dst;                   /* Destination. If Dst==Src, then init to 0 */
  char*  Src;                   /* Source address. Usually in ROM. */
} InitBlock_Type;

#ifdef __cplusplus
}
#endif



#endif /* __SEGMENT_INIT_H_INCLUDED */
