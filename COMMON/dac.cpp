#include "BoardAdd.h"
#include "defkb.h"
#include "keyboard.h"
#include "PWM.h"
#include "WatchDog.h"
#include "LCD.h"
#include "dac.h"
#include "spi.h"
#include <stdlib.h>


#if defined(__TTMon)
#include "ind.h"
#endif


#ifndef __emulator
//------------------------------------------------------------------------------
//  ������������� ��� � �������� ����������
//------------------------------------------------------------------------------
void SetDAC(char NumDAC,unsigned int Value)
{

DisableLCD(0xFF);
SPI_Prepare(SPIFREQ_8000,2);
if ((NumDAC==1)||(NumDAC==3)||(NumDAC==5)) Value|=0x4000;
   else Value|=0xC000;

#if defined(__VV)
if ((NumDAC==1)||(NumDAC==2)) SPI_SetCS(SPI_CS_DAC1);
else if ((NumDAC==3)||(NumDAC==4)) SPI_SetCS(SPI_CS_DAC2);
#endif
#if defined(__TTMon)
else if ((NumDAC==5)||(NumDAC==6)) SPI_SetCS(SPI_CS_DAC3);
#endif
#if defined(__R1500_6)
   SPI_SetCS(SPI_CS_DAC2);
#endif
#if defined(__R1500)
   SPI_SetCS(SPI_CS_DAC2);
#endif

SPI_WriteRead(Value>>8);
SPI_WriteRead(Value&0xFF);
SPI_ClearCS();
EnableLCD();
}


//------------------------------------------------------------------------------
// ��������� ������ ���, ���������� �� ��������� �� ��� ���� 3 ������
//------------------------------------------------------------------------------
/*
static void SetDACFirst(void)
{
SetDAC(1,0xFFF);
SetDAC(2,0xFFF);
SetDAC(3,0xFFF);
SetDAC(4,0xFFF);
#if defined(__TTMon)
SetDAC(5,0xFFF);
SetDAC(6,0xFFF);
#endif
SetDACRefresh();
}
*/

void SetDACRefresh(void)
{
    DisableLCD(0xFF);
    SPI_Prepare(SPIFREQ_16000,0);
    SPI_SetCS(SPI_CS_ALTERA);
    SPI_WriteRead(0xF0);
    SPI_WriteRead(1<<1);
    SPI_ClearCS();
    AT91F_PIO_ClearOutput(AT91C_BASE_PIOA, AT91C_PIO_PA8);
    AT91F_PIO_SetOutput(AT91C_BASE_PIOA, AT91C_PIO_PA8);
    SPI_SetCS(SPI_CS_ALTERA);
    SPI_WriteRead(0xF0);
    SPI_WriteRead(0<<1);
    SPI_ClearCS();
    EnableLCD();
}

#endif
//------------------------------------------------------------------------------
/*
      ��� �����-������ ������� ��� ������ � ����������

*/
//------------------------------------------------------------------------------
void TestDAC(void)
{
#if defined(__TTMon)
KEY key;
char CurPos;
unsigned int CurVolt;

  IndData[0]=cht;
  IndData[1]=chE;
  IndData[2]=chS;
  IndData[3]=cht&chPoint;
  IndData[4]=chd;
  IndData[5]=chA;
  IndData[6]=chC;
  Delay(1000);

  IndData[0]=chd;
  IndData[1]=chA;
  IndData[2]=chC;
  IndData[3]=ch1;
  IndData[4]=chEmpty;
  IndData[5]=ch3&chPoint;
  IndData[6]=ch0;
  CurPos=1;
  CurVolt=0xFFF;

#ifndef __emulator
                    SetDACFirst();
#endif

while(1){

#ifndef __emulator
    DisableLCD(0xFF);
    SPI_Prepare(SPIFREQ_16000,0);
    SPI_SetCS(SPI_CS_ALTERA);
    SPI_WriteRead(0xF0);
    SPI_WriteRead(1<<1);
    SPI_ClearCS();
    AT91F_PIO_ClearOutput(AT91C_BASE_PIOA, AT91C_PIO_PA8);
    AT91F_PIO_SetOutput(AT91C_BASE_PIOA, AT91C_PIO_PA8);
    SPI_SetCS(SPI_CS_ALTERA);
    SPI_WriteRead(0xF0);
    SPI_WriteRead(0<<1);
    SPI_ClearCS();
    EnableLCD();
#endif


   key=KB_KEY&WaitReadKey();
   if(key==KB_ESC) break;

   if(key==KB_RIGHT){
       if(CurPos==1) {IndData[3]=ch2; CurPos++;}
       else
         if(CurPos==2) {IndData[3]=ch3; CurPos++;}
         else
           if(CurPos==3) {IndData[3]=ch4; CurPos++;}
           else
             if(CurPos==4) {IndData[3]=ch5; CurPos++;}
             else
               if(CurPos==5) {IndData[3]=ch6; CurPos++;}
               else  {IndData[3]=ch1; CurPos=1;}
   }

   if(key==KB_LEFT){
       if(CurPos==1) {IndData[3]=ch6; CurPos=6;}
       else
         if(CurPos==2) {IndData[3]=ch1; CurPos--;}
         else
           if(CurPos==3) {IndData[3]=ch2; CurPos--;}
           else
             if(CurPos==4) {IndData[3]=ch3; CurPos--;}
             else
               if(CurPos==5) {IndData[3]=ch4; CurPos--;}
               else  {IndData[3]=ch5; CurPos--;}
    }

    if(key==KB_UP){
       if (CurVolt==0xFFF) { IndData[5]= ch0&chPoint; IndData[6]=ch0; CurVolt=0x0;}
       else
         if (CurVolt==0xD54) { IndData[5]= ch3&chPoint; IndData[6]=ch0; CurVolt=0xFFF;}
         else
           if (CurVolt==0xAA9) { IndData[5]= ch2&chPoint; IndData[6]=ch5; CurVolt=0xD54;}
           else
             if (CurVolt==0x7FF) { IndData[5]= ch2&chPoint; IndData[6]=ch0; CurVolt=0xAA9;}
             else
               if (CurVolt==0x555) { IndData[5]= ch1&chPoint; IndData[6]=ch5; CurVolt=0x7FF;}
               else
                 if (CurVolt==0x2AA)  { IndData[5]= ch1&chPoint; IndData[6]=ch0; CurVolt=0x555;}
                 else
                   if (CurVolt==0x0)  { IndData[5]= ch0&chPoint; IndData[6]=ch5; CurVolt=0x2AA;}
    }

    if(key==KB_DOWN){
       if (CurVolt==0xFFF) { IndData[5]= ch2&chPoint; IndData[6]=ch5; CurVolt=0xD54;}
       else
         if (CurVolt==0xD54) { IndData[5]= ch2&chPoint; IndData[6]=ch0; CurVolt=0xAA9;}
         else
           if (CurVolt==0xAA9) { IndData[5]= ch1&chPoint; IndData[6]=ch5; CurVolt=0x7FF;}
           else
             if (CurVolt==0x7FF) { IndData[5]= ch1&chPoint; IndData[6]=ch0; CurVolt=0x555;}
             else
               if (CurVolt==0x555) { IndData[5]= ch0&chPoint; IndData[6]=ch5; CurVolt=0x2AA;}
               else
                 if (CurVolt==0x2AA)  { IndData[5]= ch0&chPoint; IndData[6]=ch0; CurVolt=0x0;}
                 else
                   if (CurVolt==0x0)  { IndData[5]= ch3&chPoint; IndData[6]=ch0; CurVolt=0xFFF;}
    }
#ifndef __emulator

       SetDAC(CurPos,CurVolt);
#endif
}
#endif

#if defined(__PDMon)

SetDACFirst();
WaitReadKey();

SetDAC(1,0x7FF);
SetDAC(2,0x7FF);
SetDAC(3,0x7FF);
SetDAC(4,0x7FF);
SetDACRefresh();
WaitReadKey();

SetDAC(1,0);
SetDAC(2,0);
SetDAC(3,0);
SetDAC(4,0);
SetDACRefresh();
WaitReadKey();

while (1) ;
#endif

}



