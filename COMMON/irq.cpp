//*----------------------------------------------------------------------------
//*         ATMEL Microcontroller Software Support  -  ROUSSET  -
//*----------------------------------------------------------------------------
//* The software is delivered "AS IS" without warranty or condition of any
//* kind, either express, implied or statutory. This includes without
//* limitation any warranty or condition with respect to merchantability or
//* fitness for any particular purpose, or against the infringements of
//* intellectual property rights of others.
//*----------------------------------------------------------------------------
//* File Name           : main.c
//* Object              : main application written in C
//* Creation            : JPP   16/Jun/2004
//*----------------------------------------------------------------------------

// Include Standard files
#include "intrinsic.h"

#include "BoardAdd.h"
#include "LCD.h"
#include "Timer.h"





void at91_SW3_handler(void)
{

  unsigned int dummy;

AT91F_PIO_ClearOutput( AT91C_BASE_PIOA, LED3);

    //* enable the next PIO IRQ
    dummy =AT91F_PIO_GetInterruptStatus(AT91C_BASE_PIOA);
    //* suppress the compilation warning
    dummy =dummy;

//__enable_interrupt();

while ( (AT91F_PIO_GetInput(AT91C_BASE_PIOA) & SW3_MASK) == 0 ) {};
AT91F_PIO_SetOutput( AT91C_BASE_PIOA, LED3);


//AT91F_AIC_AcknowledgeIt(AT91C_BASE_AIC);
}





//*------------------------- Interrupt Function -------------------------------

volatile char TimerCycle0=0;
volatile char TimerCycle1=0;

void timer0_c_irq_handler(void)
{
	AT91PS_TC TC_pt = AT91C_BASE_TC0;
    unsigned int dummy;
    //* Acknowledge interrupt status
    dummy = TC_pt->TC_SR;
    //* Suppress warning variable "dummy" was set but never used
    dummy = dummy;


//__enable_interrupt();

TimerCycle0=~TimerCycle0;
if (TimerCycle0) AT91F_PIO_ClearOutput( AT91C_BASE_PIOA, LED2);
  else AT91F_PIO_SetOutput( AT91C_BASE_PIOA, LED2);

//AT91F_AIC_AcknowledgeIt(AT91C_BASE_AIC);
}


void timer1_c_irq_handler(void)
{
	AT91PS_TC TC_pt = AT91C_BASE_TC1;
    unsigned int dummy;
    //* Acknowledge interrupt status
    dummy = TC_pt->TC_SR;
    //* Suppress warning variable "dummy" was set but never used
    dummy = dummy;


//__enable_interrupt();

TimerCycle1=~TimerCycle1;
if (TimerCycle1) AT91F_PIO_ClearOutput( AT91C_BASE_PIOA, LED4);
  else AT91F_PIO_SetOutput( AT91C_BASE_PIOA, LED4);

//AT91F_AIC_AcknowledgeIt(AT91C_BASE_AIC);
}




void timer_init ( void )
//* Begin
{
float Freq;

Freq=1.0;
PrepareWaveTimer(0,0,&Freq);

//* Open Timer 0 interrupt
AT91F_AIC_ConfigureIt ( AT91C_BASE_AIC, AT91C_ID_TC0, 2,AT91C_AIC_SRCTYPE_INT_LEVEL_SENSITIVE, (void(*)(void))timer0_c_irq_handler);
AT91F_TC_InterruptEnable(AT91C_BASE_TC0,AT91C_TC_CPCS);  //  IRQ enable CPC
AT91F_AIC_EnableIt (AT91C_BASE_AIC, AT91C_ID_TC0);

StartWaveTimer(0);


Freq=3.0;
PrepareWaveTimer(1,0,&Freq);

//* Open Timer 1 interrupt
AT91F_AIC_ConfigureIt ( AT91C_BASE_AIC, AT91C_ID_TC1, 4,AT91C_AIC_SRCTYPE_INT_LEVEL_SENSITIVE, (void(*)(void))timer1_c_irq_handler);
AT91F_TC_InterruptEnable(AT91C_BASE_TC1,AT91C_TC_CPCS);  //  IRQ enable CPC
AT91F_AIC_EnableIt (AT91C_BASE_AIC, AT91C_ID_TC1);

StartWaveTimer(1);

}



void TestIRQ(void)
{

timer_init ();

AT91F_PIO_CfgOutput( AT91C_BASE_PIOA, LED1 | LED2 | LED3 | LED4 );
AT91F_PIO_SetOutput( AT91C_BASE_PIOA, LED1 | LED2 | LED3 | LED4);

AT91F_PIO_CfgInput(AT91C_BASE_PIOA, SW1_MASK | SW2_MASK | SW3_MASK | SW4_MASK);
AT91F_PIO_InterruptEnable(AT91C_BASE_PIOA,SW3_MASK);

AT91F_AIC_ConfigureIt (AT91C_BASE_AIC,AT91C_ID_PIOA,3,AT91C_AIC_SRCTYPE_EXT_LOW_LEVEL,(void(*)(void))at91_SW3_handler);
AT91F_AIC_EnableIt(AT91C_BASE_AIC,AT91C_ID_PIOA);

/*
    // open external IRQ interrupt
    	AT91F_PIO_CfgPeriph(AT91C_BASE_PIOA,SW2_MASK,0);
   	// open external IRQ0 interrupt
        AT91F_AIC_ConfigureIt ( pAic, AT91C_ID_IRQ0, IRQ0_INTERRUPT_LEVEL,AT91C_AIC_SRCTYPE_INT_EDGE_TRIGGERED, at91_IRQ0_handler);
        AT91F_AIC_EnableIt (pAic, AT91C_ID_IRQ0);
*/


  while (1) {

    if ( (AT91F_PIO_GetInput(AT91C_BASE_PIOA) & SW1_MASK) == 0 )
      AT91F_PIO_ClearOutput( AT91C_BASE_PIOA, LED1) ;
    if ( (AT91F_PIO_GetInput(AT91C_BASE_PIOA) & SW1_MASK) != 0 )
      AT91F_PIO_SetOutput( AT91C_BASE_PIOA, LED1) ;

  }
}




