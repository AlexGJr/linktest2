#ifndef _EFC_h
#define _EFC_h

#include "TypesDef.h"

// ������ ����-������ �� ���������� FLASH ����������
// �� ���� ��� �������/�������� �� ������ EFC_PAGE_SIZE

#if defined(__KIT) || defined(__VV) || defined(__Vibrometer)

// AT91SAM7S64
// � 0x00100000 �� 0x00110000
#define EFC_PAGE_SIZE 128
#define EFC_PAGE_COUNT 512
#define EFC_PagesInTheLockRegion 32

#else

// #AT91SAM7S256
// � 0x00100000 �� 0x00140000
#define EFC_PAGE_SIZE 256
#define EFC_PAGE_COUNT 1024
#define EFC_PagesInTheLockRegion 64
#endif

#define EFC_PAGE_SIZE_UINT (EFC_PAGE_SIZE/4) // ���������� unsigned int


void EFC_Init(void);

// adr ���������� � 0x00100000 � ������ ���� �������� �� EFC_PAGE_SIZE
// pbuf �.�. �������� EFC_PAGE_SIZE_UINT int
// ������� ����� �� ������, ��� ��� ������ - 10000 �������
// ������� �� ��������� �����. ���� ���� �������, ���������� LOCK
// � ������ FLASH �� ������, ��� ��� ��� ����� ��������� :-)
// ��� ������ �� ������ ���������� ����������, ��� ��� ��� ����������� �� FLASH
// �� ���� ��� ����, ��������� AT91F_disable_interrupt();EFC_WritePage();AT91F_enable_interrupt();
__ramfunc u32 EFC_WritePage(u32 adr, u32 *pbuf);

// ������ ������� ���� �, ���� ����, Reset
__ramfunc char EFCWrite(u32 adr, u8 *pbuf, u32 Len, u32 NeedReset);


// EFC_ReadPage ����� �� ������, � ����� ������ ��������� �� ������ �� ������
// (��. TestEFC() pbuf3[] )
u32 EFC_ReadPage(u32 adr, u32 *pbuf);

void TestEFC(void);

#endif

