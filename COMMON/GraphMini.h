#ifndef _GraphMini_h
#define _GraphMini_h

// Mega-��������� �������
#define tmpStrLen 20

void OutUInt(char Pos,unsigned int Value, char Digits,char LeftZero);
void OutLong(char Pos,long Value, char Digits,char LeftZero);
void OutFloat(char Pos,float Value, char Digits, char DigitsInt,char LeftZero);
long InpLong(char Pos,long Value, char Digits,char LeftZero);
long InpLongSequrity(char Pos,long Value, char Digits,char LeftZero);
float InpFloat(char Pos,float Value, char Digits, char DigitsInt,char LeftZero);
void InpTimeHM(char *H,char *M);
char InpDateDM(char *D,char *M);
void OutTime(void);
void OutDate(void);
void InpTime(void);
void InpDate(void);
long InpLongWithMinus(char Pos,long Value, char Digits,char LeftZero);
char* LongToStr(s32 l);
char* IntToStr(int i);
char* FloatToStr(float f);
char* FloatToStrSpec(float f, char Digits);
int StrToInt(char* str);
float StrToFloat(char* str);
int StrToInt(char* str);
long StrToLong(char* str);

#endif


