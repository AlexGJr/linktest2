#include "ScrlMngr.h"
#include "defkb.h"
#include "Graph.h"
//#include "Graph2.h"
#include "LCD.h"
#include "Keyboard.h"
//#include "StrUtil.h"
#include <string.h>

void InitScroller(struct ScrollerMngr *Scrl,char X, char Y,char Font)
{
 (*Scrl).CurStr=1;
 (*Scrl).NumberStr=0;
 (*Scrl).Font=Font;
 (*Scrl).LeftKey=KB_LEFT;
 (*Scrl).RightKey=KB_RIGHT;
 (*Scrl).ActionKey1=KB_UP;
 (*Scrl).ActionKey2=KB_DOWN;
 (*Scrl).X=X;
 (*Scrl).Y=Y;
 (*Scrl).Len=0;
 (*Scrl).IsInv=0;
 (*Scrl).ShortDraw=0;
}

void ClearScroller(struct ScrollerMngr *Scrl)
{
 (*Scrl).NumberStr=0;
}

char AddScrollerString(struct ScrollerMngr *Scrl,char *ScrlString)
{
char len;
if ((*Scrl).NumberStr<MaxScrlStrs-1) {
   (*Scrl).String[(*Scrl).NumberStr]=ScrlString;
   (*Scrl).NumberStr++;
   SetFont(Scrl->Font);
   len=CurFontWidth*strlen(ScrlString);
   if ( Scrl->Len < len ) Scrl->Len = len;
   return  (*Scrl).NumberStr;
} else return  0;
}

void RedrawScrlBoxArrow(u16 x, u16 y, u16 Len)
{
  DrawLine(x+CurFontWidth,y,x+CurFontWidth,y+CurFontHeight);
  DrawLine(x+CurFontWidth*2,y,x+CurFontWidth*2,y+CurFontHeight);

  DrawLine(x+2,y+CurFontHeight/2,x+CurFontWidth-2,y+2);
  DrawLine(x+CurFontWidth-2,y+2,x+CurFontWidth-2,y+CurFontHeight-2);
  DrawLine(x+CurFontWidth-2,y+CurFontHeight-2,x+2,y+CurFontHeight/2);

  DrawLine(x+CurFontWidth*2-2,y+CurFontHeight/2,x+CurFontWidth+2,y+2);
  DrawLine(x+CurFontWidth+2,y+2,x+CurFontWidth+2,y+CurFontHeight-2);
  DrawLine(x+CurFontWidth+2,y+CurFontHeight-2,x+CurFontWidth*2-2,y+CurFontHeight/2);

  DrawRect(x,y,x+CurFontWidth*2+Len+10,y+CurFontHeight);
}

void RedrawScrl(struct ScrollerMngr *Scrl)
{

char *Str;

  Str=(*Scrl).String[(*Scrl).CurStr-1];
  if (Str==0) return;
  SetFont((*Scrl).Font);
//  if (Scrl->IsInv)
//    OutStringInv((*Scrl).X,(*Scrl).Y,Str);
  if (Scrl->ShortDraw)
     {
     ClearRect(Scrl->X,Scrl->Y,Scrl->X+Scrl->Len-1,Scrl->Y+CurFontHeight-1);
     OutString(Scrl->X,Scrl->Y,Str);
     }
  else
     {
     ClearRect(Scrl->X,Scrl->Y,Scrl->X+Scrl->Len+CurFontWidth*2+10,Scrl->Y+CurFontHeight-1);
     OutString(Scrl->X+CurFontWidth*2+8,Scrl->Y,Str);
     RedrawScrlBoxArrow(Scrl->X,Scrl->Y, Scrl->Len);
     }

//  Redraw();
}

void DoLeftKeySc(struct ScrollerMngr *Scrl)
{
 if ((*Scrl).NumberStr)
    if ((*Scrl).CurStr>1)
       {
       (*Scrl).CurStr--;
       RedrawScrl(Scrl);
       }
    else
       {
       (*Scrl).CurStr=(*Scrl).NumberStr;
       RedrawScrl(Scrl);
       }
}

void DoRightKeySc(struct ScrollerMngr *Scrl)
{
 if ((*Scrl).NumberStr)
 if ((*Scrl).CurStr<(*Scrl).NumberStr) {
         (*Scrl).CurStr++;
         RedrawScrl(Scrl);
 } else {
 (*Scrl).CurStr=1;
 RedrawScrl(Scrl);
 }
}

char DoScrlNoWait(struct ScrollerMngr *Scrl, unsigned int ch)
{
  //RedrawScrl(Scrl);
  if (ch==(*Scrl).LeftKey)
     {DoLeftKeySc(Scrl); return 1;}
  if (ch==(*Scrl).RightKey)
     {DoRightKeySc(Scrl); return 1;}
  return 0;
}

unsigned int DoScrl(struct ScrollerMngr *Scrl)
{ unsigned int ch;
  RedrawScrl(Scrl);
  ch=WaitReadKey();
  if (ch==(*Scrl).LeftKey)  {DoLeftKeySc(Scrl);}
  if (ch==(*Scrl).RightKey) {DoRightKeySc(Scrl);}
  if (ch==KB_ESC) { ch=KB_ESC;}
  return ch;
}


char GetCurScrl(struct ScrollerMngr *Scrl)
{
return (*Scrl).CurStr;
}
char SetCurScrl(struct ScrollerMngr *Scrl,char N)
{
if ((N<=(*Scrl).NumberStr)&(N>0)) (*Scrl).CurStr=N;
return (*Scrl).CurStr;
}
