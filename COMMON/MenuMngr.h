#ifndef _menumngr_h
#define _menumngr_h

#include "Protocol.h"
#include "sysutil.h"
#include "defkb.h"

#define MaxCursors 13


#define NilMenuElement           0
#define ScrlMenuElement          1
#define InpIntMenuElement        2
#define DateTimeMenuElement      3

#define smrNone                  204
#define smrRedrawScrl            203
#define smrRedrawCursor          202
#define smrLeftCircledMenu       201
#define smrRightCircledMenu      200


struct CursorInfo{
           char ElementInfo;
           void *Element;
           char *Cursor;
           char *Message;
           u16 X,Y;
           KEY FKey;
};
typedef struct CursorInfo TCursorInfo;


typedef struct MenuMngr{
           signed char NumberCurCursor,NumberCursors;
           KEY LeftKey;
           KEY RightKey;
           KEY ActionKey;
           char MessageX,MessageY;
           char MessageFont;
           char DrawLine;
           u16 LineX1,LineY1,LineX2,LineY2;
           struct CursorInfo Cursors[MaxCursors];
}TMenuMngr;

void InitMenuMngr(struct MenuMngr *Menu,KEY LefKEY,KEY RighKEY,KEY ActionKey );
void AddCursor(struct MenuMngr *Menu,u16 X, u16 Y, char *Cursor, char *Msg,char EInfo, void *El,KEY FKey);
void AddCursorMenu(struct MenuMngr *Menu,u16 X, char *Msg,KEY FKey);
void SetMessage(struct MenuMngr *Menu,u16 X,u16 Y,char Font);
void LineAfterCursor(struct MenuMngr *Menu,u16 X1,u16 Y1,u16 X2,u16 Y2);
u16 ScanMenu(struct MenuMngr *Menu);
u16 ScanMenuNoWait(struct MenuMngr *Menu,KEY *Key);

void ClearCursor(struct MenuMngr *Menu);
void RedrawCursor(struct MenuMngr *Menu);

char DoLefKEY(struct MenuMngr *Menu);
char DoRighKEY(struct MenuMngr *Menu);

void DrawStr(char *Str);
void DrawDown(char *Str);

void DrawFrame(int x1,int y1,int x2,int y2);


void DrawButton(int x,int y, int len,char *Text);

void DrawPageHeader(char *Title);
void DrawPageTemplate(char *Title, char *Bottom);

void SetMenuItem(struct MenuMngr *Menu, int aItem);



void DrawLUCorner(int x,int y);
void DrawLDCorner(int x,int y);
void DrawRUCorner(int x,int y);
void DrawRDCorner(int x,int y);



#endif
