#ifndef pwm_h
#define pwm_h

#define PWMC_MCK      0x00
#define PWMC_MCK8     0x03
#define PWMC_MCK64    0x06
#define PWMC_MCK1024  0x0A
#define PWMC_MCKCLKA  0x0B
#define PWMC_MCKCLKB  0x0C


void PreparePMW(int Channel, unsigned int LineOut, float *Freq, char Period, char Duty);
void StartPWM(int Channel);
void StopPWM(int Channel);
void SetDutyPMW(int Channel, int Duty);

void TestPMW(void);

#endif
