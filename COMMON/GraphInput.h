#ifndef GraphIO_h
#define GraphIO_h

#include "sysutil.h"

int   InputInt(unsigned int X,unsigned int Y,int IntValue,unsigned char DigitCount);
void  InputTime(unsigned int X,unsigned int Y,char* Hour,char* Min,char* Sec);
int   InputIntUpDown(unsigned int X,unsigned int Y,int IntValue,unsigned char DigitCount);

char  InputTimeUpDown(unsigned int X,unsigned int Y,u16 *Hour,u16 *Min,u16 *Sec);
char  InputDateUpDown(unsigned int X,unsigned int Y,u16 *Day,u16 *Month,u16 *Year);

char  InputString(unsigned int X,unsigned int Y,char *Str,char StrLength,char Font,char FullTable);
float InputFloat(unsigned int X,unsigned int Y,float Num,int r,int I);

#endif

