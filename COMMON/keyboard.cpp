/*
Date of last change: 16.11.2005 11:31
*/

#include "Board.h"
#include "BoardAdd.h"
#include "defkb.h"
#include "keyboard.h"
#include "WatchDog.h"
#include "LCD.h"
#include "TypesDef.h"

#include "defs.h"	// acm, FUTURE change, added for move LED code from Timer1 IRQ to PIT below
#include "error.h"
#include "uart.h"

#ifndef AT91RM9200
//#include "debug.h"
#if defined(__TTMon)
#include "lcdTTMon.h"
#endif

#if defined(__TTMon) || defined(__VVTester) || defined(TDM) || defined(__TIM3)
#include "Link.h"
#endif

#else

#define SetLightOff()
#define SetLightOn()
volatile char RedrawEnabled=1;

#endif

#if defined(__R1500_6)
#define MaxWatchCounter (unsigned long)9600 //~5min
extern unsigned long WatchCounter;
extern u32 RunWatch;
#endif

extern u16 OneSec; 
extern u16 SwWd;


#include <stdlib.h>

// If many time no activity, call IdleFunc
#define IdleBound 1e4
int IdleCount;

// May assign 1, o suppress calling Help, for example in comms
char HelpActive;

SLICE Slice=NULL;
POWER Power=NULL;

KEY LastKey = KB_NO; // Last pressed key
KEY LastKey2 = KB_NO; // Key pressed externally

unsigned char IsAuto=0;
unsigned char IsPowerOff=1;
unsigned int  CurPribPower=0xFFFF;
unsigned int  CurLightPower=0xFFFF;
unsigned int  MaxPribPower=0xFFFF;
unsigned int  MaxLightPower=0xFFFF;
char LightOn=1;
char LightMode=1;

//must be multiple of 32
#if defined(__TTMon)
#define KeybFreq 320
#else
#define KeybFreq 32
#endif


// How many ticks to skip to allow redraw
#define SkipKeybCount (KeybFreq/32)
volatile char SkipKeyb=SkipKeybCount;

// 32 times a second counter. may stoe some time
#ifndef __emulator
__no_init volatile unsigned int Tic32 @ "USBDATA";

// for safe read Tic32
unsigned int GetTic32(void) @ "USBCODE"
#else
volatile unsigned int Tic32;

unsigned int GetTic32(void)
#endif
{
  unsigned int i;
  i=Tic32;
  while (i!=Tic32)
    i=Tic32;
  return i;
}


void PutKey(KEY ch) // Set the key in quine
{
  LastKey2 = ch;
}


//                                   -       1              3              5              10              15
const unsigned int PribPower[6]={0xFFFF, 1*60*KeybFreq, 3*60*KeybFreq, 5*60*KeybFreq, 10*60*KeybFreq, 15*60*KeybFreq};
//                                   -       5�          10�          20c          40c          60c
const unsigned int LightPower[6]={0xFFFF, 5*KeybFreq, 10*KeybFreq, 20*KeybFreq, 40*KeybFreq, 60*KeybFreq};

volatile char FlagNeedRedraw=0;

#ifndef __emulator


//Keybord bufer in Bytes
#define KBBufSize       60
//Keyboard buffer in keys
#define KBBufKSize      30


char cKBBufEnd, cKBBufBegin;
KEY KeyBuf[KBBufKSize];

#ifdef __KIT
#define KeysCount 4
#endif

#ifdef __VV
#define KeysCount 5
#endif

#if defined(__TTMon) || defined(__PDMon) || defined(__R1500_6) || defined(__TIM3)
#define KeysCount 9
#endif

#if defined(__Korsar)
#define KeysCount 10
#endif

#if defined(__VVTester)
#define KeysCount 12
#endif

#if defined(__Vibrometer) || defined(__KIT9)
#define KeysCount 1
#endif


volatile char KeysState[KeysCount];
volatile char KeysPressed[KeysCount];
volatile unsigned short KeysPressedTik[KeysCount];
volatile char KeysByUp[KeysCount];
volatile char IsShift[KeysCount];
volatile KEY ShiftMask=0;
// What kind of mask these shift produce. Recommended use indexes 0,1 for shifts
const KEY ShiftMasks[2]={KB_MEM_MASK,KB_MOD_MASK};

// State Automat
// 4 -> Put key2 -> 0
// 8 -> Put key1 -> 3
// 12 -> Put key -> 13
// 5 - (IsAuto==0) -> 3
//                              0 1 2 3 4 5 6 7  8  9 0 1  2 3   0 1 2 3 4 5 6  7  8  9  0  1  2  3
const char KeyAutomate[2][14]={{0,0,4,2,0,2,2,2, 3, 0,2,2,13,0},{1,8,3,5,0,6,7,10, 3, 9,11,12,13,11}};


#pragma diag_suppress=Pa082
static void CheckKeyPressed(unsigned int aLine, unsigned int aKey)
{
#ifdef __TIM3  
  KeysPressed[aKey]=(AT91F_PIO_IsInputSet(AT91C_BASE_PIOB,aLine))?0:1;
#else
  KeysPressed[aKey]=(AT91F_PIO_IsInputSet(AT91C_BASE_PIOA,aLine))?0:1;
#endif
}

static void CheckKey(unsigned int aKey)
{
  int CurKeysState;
  
  CurKeysState=KeysPressed[aKey];
  
  if (CurKeysState==0)
    KeysPressedTik[aKey]=0;
  if (KeysPressedTik[aKey]<0x7FFF)
    KeysPressedTik[aKey]++;
  
  CurKeysState=KeyAutomate[CurKeysState][KeysState[aKey]];
  
  if ((CurKeysState==5)&&(IsAuto==0))
    CurKeysState=3;
  
  KeysState[aKey]=CurKeysState;
}

static KEY CheckKeyPut(unsigned int aKey, int PutKey)
{
  int CurKeysState;
  KEY res;
  
  CurKeysState=KeysState[aKey];
  
  res=KB_NO;
  if (((!KeysByUp[aKey])&&(CurKeysState==8)) ||
      ((KeysByUp[aKey])&&(CurKeysState==4)) ||
        (CurKeysState==12)) {
          res=aKey | ShiftMask | (KeysPressedTik[aKey]<<16);
          if (KeysPressed[aKey])
            res|=KB_PRESSED;
          if (PutKey) {
            if ((KeyBuf[cKBBufEnd] & KB_KEY)==KB_NO) {
              KeyBuf[cKBBufEnd]=res;
              cKBBufEnd++;
              if (cKBBufEnd>=KBBufKSize)
                cKBBufEnd=0;
            }
          }
          KeysState[aKey]=KeyAutomate[0][CurKeysState];
        }
  return res;
}

#ifdef __VV
static void FillKeyPressedVV(void)
{
  AT91F_PIO_CfgInput(AT91C_BASE_PIOA, AT91C_PIO_PA6 | AT91C_PIO_PA7);
  AT91F_PIO_CfgOutput(AT91C_BASE_PIOA, AT91C_PIO_PA19 | AT91C_PIO_PA20);
  
  AT91F_PIO_SetOutput(AT91C_BASE_PIOA,AT91C_PIO_PA20);
  AT91F_PIO_ClearOutput(AT91C_BASE_PIOA,AT91C_PIO_PA19);
  DelayMks(10);
  
  CheckKeyPressed(AT91C_PIO_PA6, KB_SET);
  CheckKeyPressed(AT91C_PIO_PA7, KB_LIGHT);
  
  AT91F_PIO_SetOutput(AT91C_BASE_PIOA,AT91C_PIO_PA19);
  AT91F_PIO_ClearOutput(AT91C_BASE_PIOA,AT91C_PIO_PA20);
  DelayMks(10);
  
  CheckKeyPressed(AT91C_PIO_PA6, KB_HOLD);
  //CheckKeyPressed(AT91C_PIO_PA7, KB_ON);
  
  AT91F_PIO_SetOutput(AT91C_BASE_PIOA,AT91C_PIO_PA20);
  DelayMks(10);
  
  // Bad processing SET+HOLD
  KeysPressed[KB_SET_HOLD]=(KeysPressed[KB_SET]&&KeysPressed[KB_HOLD])?1:0;
  
  //Bad processing of the key "On"
  //KeysPressed[KB_POWER]=(AT91F_PIO_IsInputSet(AT91C_BASE_PIOA,AT91C_PIO_PA2))?1:0;
}
#endif


#if defined(__Korsar)
static void FillKeyPressedKorsar(void)
{
  AT91F_PIO_CfgInput(AT91C_BASE_PIOA, AT91C_PIO_PA7 | AT91C_PIO_PA8);
  AT91F_PIO_CfgOutput(AT91C_BASE_PIOA, AT91C_PIO_PA19 | AT91C_PIO_PA20 | AT91C_PIO_PA21 | AT91C_PIO_PA22);
  
  AT91F_PIO_SetOutput(AT91C_BASE_PIOA,AT91C_PIO_PA20 | AT91C_PIO_PA21 | AT91C_PIO_PA22);
  AT91F_PIO_ClearOutput(AT91C_BASE_PIOA,AT91C_PIO_PA19);
  DelayMks(10);
  CheckKeyPressed(AT91C_PIO_PA7, KB_MEM);
  CheckKeyPressed(AT91C_PIO_PA8, KB_DOWN);
  
  AT91F_PIO_SetOutput(AT91C_BASE_PIOA,AT91C_PIO_PA19 | AT91C_PIO_PA21 | AT91C_PIO_PA22);
  AT91F_PIO_ClearOutput(AT91C_BASE_PIOA,AT91C_PIO_PA20);
  DelayMks(10);
  CheckKeyPressed(AT91C_PIO_PA7, KB_ENTER/*KB_MEM*/);
  CheckKeyPressed(AT91C_PIO_PA8, KB_LEFT/*KB_SET*/);
  
  AT91F_PIO_SetOutput(AT91C_BASE_PIOA,AT91C_PIO_PA19 | AT91C_PIO_PA20 | AT91C_PIO_PA22);
  AT91F_PIO_ClearOutput(AT91C_BASE_PIOA,AT91C_PIO_PA21);
  DelayMks(10);
  CheckKeyPressed(AT91C_PIO_PA7, KB_ESC/*KB_ESC*/);
  CheckKeyPressed(AT91C_PIO_PA8, KB_RIGHT/*KB_LEFT*/);
  
  AT91F_PIO_SetOutput(AT91C_BASE_PIOA,AT91C_PIO_PA19 | AT91C_PIO_PA20 | AT91C_PIO_PA21);
  AT91F_PIO_ClearOutput(AT91C_BASE_PIOA,AT91C_PIO_PA22);
  DelayMks(10);
  CheckKeyPressed(AT91C_PIO_PA7, KB_SET/*KB_DOWN*/);
  CheckKeyPressed(AT91C_PIO_PA8, KB_UP);
  
  AT91F_PIO_SetOutput(AT91C_BASE_PIOA,AT91C_PIO_PA22);
  DelayMks(10);
  
  KeysPressed[KB_POWER]=(AT91F_PIO_IsInputSet(AT91C_BASE_PIOA,AT91C_PIO_PA2))?1:0;
}
#endif


#if defined(__TTMon) || defined(__PDMon) || defined(__R1500_6)
static void FillKeyPressedTTMon(void)
{
  if (LCDConnected) {
    
    AT91F_PIO_CfgInput(AT91C_BASE_PIOA, AT91C_PIO_PA27 | AT91C_PIO_PA28 | AT91C_PIO_PA29);
    AT91F_PIO_CfgOutput(AT91C_BASE_PIOA, AT91C_PIO_PA24 | AT91C_PIO_PA25 | AT91C_PIO_PA26);
    
    AT91F_PIO_SetOutput(AT91C_BASE_PIOA,AT91C_PIO_PA25 | AT91C_PIO_PA26);
    AT91F_PIO_ClearOutput(AT91C_BASE_PIOA,AT91C_PIO_PA24);
    DelayMks(10);
    CheckKeyPressed(AT91C_PIO_PA27, KB_ENTER);
    CheckKeyPressed(AT91C_PIO_PA28, KB_MEM);
    CheckKeyPressed(AT91C_PIO_PA29, KB_RIGHT);
    AT91F_PIO_SetOutput(AT91C_BASE_PIOA,AT91C_PIO_PA24);
    
    AT91F_PIO_ClearOutput(AT91C_BASE_PIOA,AT91C_PIO_PA25);
    DelayMks(10);
    CheckKeyPressed(AT91C_PIO_PA27, KB_MOD);
    CheckKeyPressed(AT91C_PIO_PA28, KB_UP);
    CheckKeyPressed(AT91C_PIO_PA29, KB_DOWN);
    AT91F_PIO_SetOutput(AT91C_BASE_PIOA,AT91C_PIO_PA25);
    
    AT91F_PIO_ClearOutput(AT91C_BASE_PIOA,AT91C_PIO_PA26);
    DelayMks(10);
    //CheckKeyPressedTic(AT91C_PIO_PA27, 6);
    CheckKeyPressed(AT91C_PIO_PA28, KB_ESC);
    CheckKeyPressed(AT91C_PIO_PA29, KB_LEFT);
    AT91F_PIO_SetOutput(AT91C_BASE_PIOA,AT91C_PIO_PA26);
    
  } else {
    ClearKeyboard();
  }
  
  //???
  ResetWatchDog();
  
}
#endif

#if defined(__TIM3)
static void FillKeyPressedTTMon(void)
{
  if (LCDConnected) {
    AT91F_PIO_SetOutput(AT91C_BASE_PIOB,AT91C_PIO_PB30 | AT91C_PIO_PB22);
    AT91F_PIO_ClearOutput(AT91C_BASE_PIOB, AT91C_PIO_PB24);
    DelayMks(10);
    CheckKeyPressed(AT91C_PIO_PB19, KB_ENTER);
    CheckKeyPressed(AT91C_PIO_PB27, KB_MEM);
    CheckKeyPressed(AT91C_PIO_PB20, KB_RIGHT);
    AT91F_PIO_SetOutput(AT91C_BASE_PIOB, AT91C_PIO_PB24);
    
    AT91F_PIO_ClearOutput(AT91C_BASE_PIOB, AT91C_PIO_PB30);
    DelayMks(10);
    CheckKeyPressed(AT91C_PIO_PB19, KB_MOD);
    CheckKeyPressed(AT91C_PIO_PB27, KB_UP);
    CheckKeyPressed(AT91C_PIO_PB20, KB_DOWN);
    AT91F_PIO_SetOutput(AT91C_BASE_PIOB, AT91C_PIO_PB30);
    
    AT91F_PIO_ClearOutput(AT91C_BASE_PIOB,AT91C_PIO_PB22);
    DelayMks(10);
    //CheckKeyPressedTic(AT91C_PIO_PA27, 6);
    CheckKeyPressed(AT91C_PIO_PB27, KB_ESC);
    CheckKeyPressed(AT91C_PIO_PB20, KB_LEFT);
    AT91F_PIO_SetOutput(AT91C_BASE_PIOB,AT91C_PIO_PB22);
    
  } else {
    ClearKeyboard();
  }
}
#endif


#ifdef __KIT
static void FillKeyPressedKIT(void)
{
  KeysPressed[0]=((AT91F_PIO_GetInput(AT91C_BASE_PIOA) & SW1_MASK) == 0 )?1:0;
  KeysPressed[1]=((AT91F_PIO_GetInput(AT91C_BASE_PIOA) & SW2_MASK) == 0 )?1:0;
  KeysPressed[2]=((AT91F_PIO_GetInput(AT91C_BASE_PIOA) & SW3_MASK) == 0 )?1:0;
  KeysPressed[3]=((AT91F_PIO_GetInput(AT91C_BASE_PIOA) & SW4_MASK) == 0 )?1:0;
}
#endif


#ifdef __VVTester
static void FillKeyPressedVVTester(void)
{
  AT91F_PIO_CfgInput(AT91C_BASE_PIOA, AT91C_PIO_PA28 | AT91C_PIO_PA29 | AT91C_PIO_PA30);
  AT91F_PIO_CfgOutput(AT91C_BASE_PIOA, AT91C_PIO_PA24 | AT91C_PIO_PA25 | AT91C_PIO_PA26 | AT91C_PIO_PA27);
  AT91F_PIO_SetOutput(AT91C_BASE_PIOA,AT91C_PIO_PA25 | AT91C_PIO_PA26);
  
  AT91F_PIO_ClearOutput(AT91C_BASE_PIOA,AT91C_PIO_PA24);
  DelayMks(10);
  CheckKeyPressed(AT91C_PIO_PA28, KB_F1);
  CheckKeyPressed(AT91C_PIO_PA29, KB_ESC);
  CheckKeyPressed(AT91C_PIO_PA30, KB_ENTER);
  AT91F_PIO_SetOutput(AT91C_BASE_PIOA,AT91C_PIO_PA24);
  
  AT91F_PIO_ClearOutput(AT91C_BASE_PIOA,AT91C_PIO_PA25);
  DelayMks(10);
  CheckKeyPressed(AT91C_PIO_PA28, KB_F2);
  CheckKeyPressed(AT91C_PIO_PA29, KB_LEFT);
  CheckKeyPressed(AT91C_PIO_PA30, KB_DOWN);
  AT91F_PIO_SetOutput(AT91C_BASE_PIOA,AT91C_PIO_PA25);
  
  AT91F_PIO_ClearOutput(AT91C_BASE_PIOA,AT91C_PIO_PA26);
  DelayMks(10);
  CheckKeyPressed(AT91C_PIO_PA28, KB_F3);
  CheckKeyPressed(AT91C_PIO_PA29, KB_UP);
  CheckKeyPressed(AT91C_PIO_PA30, KB_RIGHT);
  AT91F_PIO_SetOutput(AT91C_BASE_PIOA,AT91C_PIO_PA26);
  
  AT91F_PIO_ClearOutput(AT91C_BASE_PIOA,AT91C_PIO_PA27);
  DelayMks(10);
  CheckKeyPressed(AT91C_PIO_PA28, KB_F4);
  CheckKeyPressed(AT91C_PIO_PA29, KB_LIGHT);
  AT91F_PIO_SetOutput(AT91C_BASE_PIOA,AT91C_PIO_PA27);
  
  KeysPressed[KB_POWER]=(AT91F_PIO_IsInputSet(AT91C_BASE_PIOA,AT91C_PIO_PA2))?1:0;
}
#endif



static KEY KeyboardHandlerInline(void)
{
  int i,fl,t;
  KEY res,kb;
  
#ifdef __VV
  FillKeyPressedVV();
#endif
  
#if defined(__Korsar)
  FillKeyPressedKorsar();
#endif
  
#if defined(__TTMon) || defined(__PDMon) || defined(__R1500_6) || defined(__TIM3)
  FillKeyPressedTTMon();
#endif
  
#ifdef __KIT
  FillKeyPressedKIT();
#endif
  
#ifdef __VVTester
  FillKeyPressedVVTester();
#endif
  
  for (i=0;i<KeysCount;i++)
    CheckKey(i);
  
  fl=0; // Do we have SHIFT ?
  for (i=0;i<KeysCount;i++)
    if (IsShift[i] && KeysPressed[i]) {
      fl++;
    }
  
  // Other key pressed bisedes shift ?
  if (fl) {
    if (fl==1) {
      // Shift pressed only once
      fl=0;
      for (i=0;i<KeysCount;i++)
        if (!IsShift[i])
          if (KeysPressed[i]) {
            fl=1;
            break;
          }
    }
    if (fl) {
      // somehing pressed
      for (i=0;i<KeysCount;i++)
        if (IsShift[i] && KeysPressed[i]) {
          // Mask
          ShiftMask|=ShiftMasks[i];
          // Shift setting in state 9 - wating for release
          KeysState[i]=9;
        }
    }
  } else
    ShiftMask=0;
  
  // Released shift - remove it from mask
  for (i=0;i<KeysCount;i++)
    if (IsShift[i] && (!KeysPressed[i])) {
      // Mask
      ShiftMask&=~ShiftMasks[i];
    }
  
  if (IsAuto) {
    //  autorepeat only last pressed key
    // selecting the last pressed from not Shift keys
    // others -> to the state 13
    fl=-1;
    t=0x10000;
    for (i=0;i<KeysCount;i++)
      if ((!IsShift[i]) && KeysPressed[i])
        if (t>KeysPressedTik[i]) {
          t=KeysPressedTik[i];
          fl=i;
        }
    if (fl>=0) {
      for (i=0;i<KeysCount;i++)
        if ((i!=fl)&&(!IsShift[i]) && (KeysPressed[i]))
          KeysState[i]=13;
    }
  }
  
  res=kb=KB_NO;
  fl=0;
  for (i=0;i<KeysCount;i++) {
    kb=CheckKeyPut(i, (i!=KB_LIGHT));
    if ((kb&KB_KEY)==KB_LIGHT)
      fl=1;
    if ((kb&KB_KEY)!=KB_NO)
      res=kb;
  }
  // Returning KB_LIGHT, KB_NO od th code of pressed key
  if (fl) return KB_LIGHT;
  else return res;
}

static u32 AcknowledgeInterruptStatus(void)
{
  unsigned int dummy;
#ifndef AT91RM9200
  dummy = AT91C_BASE_PITC->PITC_PISR;
  if (dummy & AT91C_PITC_PITS) {
    dummy = AT91C_BASE_PITC->PITC_PIVR;
    return 1;
  }  else return 0;
#else
  dummy = AT91C_BASE_ST->ST_SR;
  return (dummy & AT91C_ST_PITS);
#endif
}

#ifdef __arm
//acm, comment out until LED blink move to PIT
//#define MaxLEDBlinkCounter 12000/250		// acm, scale from 8khz IRQ routine to 32 hz
//#define MaxLEDBlinkCounterOnError 3000/250
#endif

/*acm, uncomment out if move LED blink from main Timer1 to PIT
int LEDBlinkCounter=0;
int CurLEDBlinkState=0;*/

// Interrupt frequency is 32 Hz
void PITInterrupt(void)
{
  register KEY keys;
  //int c,CurMaxLEDBlinkCounter;;  //acm
  
  if (!AcknowledgeInterruptStatus())
    return;
  
  if(OneSec++ >= 32){ //ag >=
    OneSec = 0;
    if(SwWd++ >= 3600){
      Restart(); //2.06 Reset (reboot);
    }
  }
  
  SkipKeyb--;
  if (SkipKeyb==0) {
    
    Tic32++;
    SkipKeyb=SkipKeybCount;
    
    keys=KB_NO;
    
    keys=KeyboardHandlerInline();
    
    if ((keys&KB_KEY)==KB_NO) {
      keys=LastKey2;
      LastKey2=KB_NO;
    }
    
    // hold off for now
    //acm, v1.79 comment out, doesn't apply to us...
    if (keys==KB_LIGHT){
      //Light On/Off
      if (LightOn) SetLightOff();
      else SetLightOn();
      
      LightOn=!LightOn;
      CurLightPower=MaxLightPower;
      CurPribPower=MaxPribPower;
      keys=KB_NO;
    }
    
    if((keys&KB_KEY)!=KB_NO){
      CurPribPower=MaxPribPower;
      CurLightPower=MaxLightPower;
      if (LightMode) {
        SetLightOn();
        LightOn=1;
      }
    }else{
      if ((IsPowerOff)&&(MaxPribPower<0xFFFF)) CurPribPower--;
      if ((CurPribPower==0)&&(Power)) {
        CurPribPower=MaxPribPower;
        Power();
      }
      if (MaxLightPower<0xFFFF) CurLightPower--;
      if (CurLightPower==0) {
        //Light Off
        CurLightPower=MaxLightPower;
        SetLightOff();
        LightOn=0;
      }
    }//acm*/
  }
  
  if (RedrawEnabled) {
    
#if defined(__VV) || defined(__PDMon) || defined(__Korsar) || defined(__VVTester)
    if (FlagNeedRedraw) {
      RedrawInline();
      FlagNeedRedraw=0;
    }
#endif
#ifdef __TTMon
    RedrawInline();
    if(++BlinkCntr>200){
      BlinkCntr=0;
      blink=(blink)?0:1;
    }
#endif
  }
  
#if defined(__R1500_6)
  if (++WatchCounter>=MaxWatchCounter) {
    //5 min before next start
    RunWatch=1;
    WatchCounter=0;
  }
#endif
  
  /* decided to hold off this change for now...
  acm, 3-24 tested, move from main.c, 8khz TIO1 to 32hz PIT, measurably more efficient (5min38sec vs 6min9sec balance time)
  c=Led; // acm, Led == variable set in LED macro that drives LED to CPLD
}*/
  
}



#if defined(__VV) || defined(__PDMon) || defined(__Korsar) || defined(__VVTester)
void NeedRedraw(void)
{
  FlagNeedRedraw=1;
}


int RedrawProcessed(void)
{
  char i;
  i=FlagNeedRedraw;
  while (i!=FlagNeedRedraw)
    i=FlagNeedRedraw;
  return i;
}


void Redraw(void)
{
  NeedRedraw();
  while (RedrawProcessed()) ;
}
#endif

#if defined(__R1500_6) || defined(__TIM3)
void NeedRedraw(void)
{
  if (RedrawEnabled) RedrawInline();
}


int RedrawProcessed(void)
{
  return 1;
}

#endif


// Sets and start of interrupt
// Using PIT=32Hz
static void InitKeyboardInline(void)
{
#ifndef AT91RM9200
  unsigned int Period;
#endif
  
  StopKeyboard();
  
  ResetWatchDog();
  
#ifndef AT91RM9200
  
  Period=MCK/16/KeybFreq-1;
  AT91C_BASE_PITC->PITC_PIMR=Period | AT91C_PITC_PITEN | AT91C_PITC_PITIEN;
  //* Open interrupt
  AT91F_AIC_ConfigureIt ( AT91C_BASE_AIC, AT91C_ID_SYS, KEYBOARD_PRIORITY, AT91C_AIC_SRCTYPE_INT_LEVEL_SENSITIVE, PITInterrupt);
  
#ifdef __KIT
  AT91F_PIO_CfgInput(AT91C_BASE_PIOA, SW3 | SW4);
#endif
  
#else
  
  AT91C_BASE_ST->ST_PIMR=SLOWCLOCK/KeybFreq;
  
  //* Open interrupt
  AT91F_AIC_ConfigureIt ( AT91C_BASE_AIC, AT91C_ID_SYS, KEYBOARD_PRIORITY, AT91C_AIC_SRCTYPE_INT_LEVEL_SENSITIVE, PITInterrupt);
  AT91C_BASE_ST->ST_IER=AT91C_ST_PITS;
  
#endif
  
  MaxLightPower=LightPower[0];
  
  AutoRepeatEnable();
  StartKeyboard();
}

static void ClearKeyboardInline(void)
{
  register int i;
  for (i=0;i<KeysCount;i++) {
    KeysState[i]=0;
    KeysPressed[i]=0;
  }
  
  cKBBufEnd=0;
  cKBBufBegin=0;
  for (i=0;i<KBBufKSize;i++)
    KeyBuf[i]=KB_NO;
  
}

void StartKeyboard(void)
{
  //Allows interrupt
  AT91F_AIC_EnableIt (AT91C_BASE_AIC, AT91C_ID_SYS);
}


void StopKeyboard(void)
{
  // Stopps interrupt
  AT91F_AIC_DisableIt (AT91C_BASE_AIC, AT91C_ID_SYS);
}


//**********************************************
// RETURNS CODE OF KEY AND CLEARS THE BUFFER
//----------------------------------------------
//  Result - KEY CODE
//  		 KB_NO - NO
//**********************************************
KEY ReadKey2(void)
{
  KEY c;
  
  ResetWatchDog();
  c=KeyBuf[cKBBufBegin];
  if ((c&KB_KEY)==KB_NO) c=KB_NO;
  else {
    KeyBuf[cKBBufBegin]=KB_NO;
    cKBBufBegin++;
    
    if (cKBBufBegin>=KBBufKSize)
      cKBBufBegin=0;
  }
  
  if (((c&KB_KEY)==KB_NO)&&(Slice))
    Slice();
  
  if (((c&KB_KEY)==KB_POWER)&&(Power))
    Power();
  if (c!=KB_NO) {
    LastKey=c;
    IdleCount=0;
  }
  
  ResetWatchDog();
  
#if defined(__TTMon) || defined(__VVTester)
  ScanMessages();
  ResetWatchDog();
#endif
  
  return c;
}


char KeyPressed(void)
{
  ResetWatchDog();
  
#pragma diag_suppress=Pa082
  
#ifdef __VV
  return (KeysPressed[0] | KeysPressed[2]);
#endif
  
#ifdef __Korsar
  return (KeysPressed[0] |
          KeysPressed[1] |
            KeysPressed[2] |
              KeysPressed[3] |
                KeysPressed[4] |
                  KeysPressed[5] |
                    KeysPressed[6] |
                      KeysPressed[7]);
#endif
  
#if defined(__TTMon) || defined(__PDMon) || defined(__R1500_6) || defined(__VVTester) || defined(__KIT9)|| defined(__TIM3)
  return 0;
#endif
  
#ifdef __KIT
  return ((AT91F_PIO_GetInput(AT91C_BASE_PIOA) & SW_MASK) != SW_MASK);
#endif
}


#else

//#include <vcl\forms.hpp>

//**********************************************
// RETURNS KEY CODE AND CLEARS THE BUFFER
//----------------------------------------------
//  Result - KEY CODE
//  		 KB_NO - NO
//**********************************************
KEY ReadKey2(void)
{
  KEY c;
  
  //Application->ProcessMessages();
  c=LastKey;
  LastKey=KB_NO;
  if (TryClose)
    return KB_ESC;
  if ((c==KB_NO)&&(Slice))
    Slice();
  if ((c&KB_KEY)!=KB_NO)
    IdleCount=0;
  return c;
}


char KeyPressed(void)
{
  return (LastKey!=KB_NO);
}

static void ClearKeyboardInline(void)
{
}

static void InitKeyboardInline(void)
{
}

#if !defined(__R1500_6) && !defined(__TIM3)

void Redraw(void)
{
  extern volatile char FlagNeedRedraw;
  FlagNeedRedraw=1;
}
#endif

#endif

void ClearKeyboard(void)
{
  ClearKeyboardInline();
  IdleCount=0;
  LastKey=KB_NO;
}


void InitKeyboard(void)
{
  ClearKeyboard();
  Tic32=0;
  InitKeyboardInline();
}

KEY ReadKey(void)
{
  return ReadKey2()&KB_KEY;
}


//**********************************************
// WAITS A KEY PRESSED AND CLEARS BUFFER
//----------------------------------------------
//  Result - KEY CODE
//
//**********************************************
KEY WaitReadKey(void)
{
  
  KEY c;
  c=KB_NO;
  while (c==KB_NO) c=ReadKey();
  IdleCount=0;
  return c;
  
}


KEY WaitReadKey2(void)
{
  
  KEY c;
  c=KB_NO;
  while ((c&KB_KEY)==KB_NO) c=ReadKey2();
  IdleCount=0;
  return c;
  
}

KEY WaitReadKeyWithDelay(float DelaySec,KEY KeyAtTime)
{
  unsigned int First=GetTic32(), Now;
  unsigned int DelayTic=(float)DelaySec*32;
  
  KEY c;
  c=KB_NO;
  while (c==KB_NO) {
    c=ReadKey();
    Now=GetTic32();
    if((DelaySec>0.09)&&((Now<First)||(DelayTic<=(Now-First)))){
      c=KeyAtTime;
      break;
    }
    if (ScanMessages()) {
      Redraw();
    }  
  }
  IdleCount=0;
  return c;
}

// If did not have a key press for some time, call IdleFunc
// Returns KB_IDLE, if did something good
KEY ReadKeyIdle(IDLEFUNC IdleFunc)
{
  KEY c;
  
  c=ReadKey();
  if (c==KB_NO) {
#ifdef __emulator
    if (TryClose)
      return KB_ESC;
#endif
    if (IdleCount>IdleBound) {
      // Waiting
      //      IdleCount=0;
      if (IdleFunc)
        if (IdleFunc())
          return KB_IDLE;
    } else {
      IdleCount++;
    }
  } else
    IdleCount=0;
  return c;
}



void AutoRepeatEnable(void)
{IsAuto=1;}

void AutoRepeatDisable(void)
{IsAuto=0;}

void PowerOffEnable(void)
{IsPowerOff=1;}

void PowerOffDisable(void)
{IsPowerOff=0;}

void BeepEnable(void)
{}

void BeepDisable(void)
{}

void SetLightOffVal(int Val)
{
  if ((Val<0)||(Val>5))
    Val=0;
  CurLightPower=MaxLightPower=LightPower[Val];
}

// 0 - turn off and wait; 1 - turn on by any key pressed
void SetLightOnMode(int Val)
{
  LightMode=Val;
}

void SetPowerOffVal(int Val)
{
  if ((Val<0)||(Val>5))
    Val=0;
  CurPribPower=MaxPribPower=PribPower[Val];
}

void TestKeyboard(void)
{
  
#ifdef __KIT
  AT91F_PIO_CfgOutput( AT91C_BASE_PIOA, LED_MASK );
#endif

  while (1) {
    
#ifndef __emulator

#endif
    
    WaitReadKey2();
    
#ifdef __emulator
    if (TryClose)
      break;
#endif
    
  }  
}


