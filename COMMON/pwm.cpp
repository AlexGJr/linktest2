// Include Standard files
#include "Board.h"
#include "pwm.h"
#include <math.h>


// Channel=0..1, 5MHz-1Hz
// 0 = DIVA
// 1 = DIVB
// 2,3 = ���� ����� ������������ �����-������ MCKx
// � LineOut ��������� !!!
// ��������� LineOut:
// PWM0 AT91C_PA0_PWM0 AT91C_PA11_PWM0 AT91C_PA23_PWM0
// PWM1 AT91C_PA1_PWM1 AT91C_PA12_PWM1 AT91C_PA24_PWM1
// PWM2 PA2A PA13B PA25B
// PWM3 PA7B PA14B
void PreparePMW(int Channel,unsigned int LineOut, float *Freq, char Period, char Duty)
{
float sFreq;
unsigned int sMCK,sDIV;

StopPWM(Channel);

AT91F_PMC_EnablePeriphClock ( AT91C_BASE_PMC, 1 << AT91C_ID_PWMC ) ;

sFreq=(*Freq)*(float)Period;
if (sFreq>200000) {
    sMCK=PWMC_MCK;
    sDIV=floor((float)MCK/sFreq);
    *Freq=(float)MCK/(float)sDIV/(float)Period;
} else
if (sFreq>30000) {
    sMCK=PWMC_MCK8;
    sDIV=floor((float)MCK/8.0/sFreq);
    *Freq=(float)MCK/8.0/(float)sDIV/(float)Period;
} else
if (sFreq>4000) {
    sMCK=PWMC_MCK64;
    sDIV=floor((float)MCK/64.0/sFreq);
    *Freq=(float)MCK/64.0/(float)sDIV/(float)Period;
} else
if (sFreq>190) {
    sMCK=PWMC_MCK1024;
    sDIV=floor((float)MCK/1024.0/sFreq);
    *Freq=(float)MCK/1024.0/(float)sDIV/(float)Period;
} else {
    sMCK=PWMC_MCK1024;
    sDIV=255;
    *Freq=0;
}


if (Channel==0) {
    AT91C_BASE_PWMC->PWMC_MR = (sDIV | (sMCK<<8));
    AT91C_BASE_PWMC->PWMC_CH[Channel].PWMC_CMR = PWMC_MCKCLKA;
} else if (Channel==1) {
    AT91C_BASE_PWMC->PWMC_MR = (sDIV<<16 | (sMCK<<24));
    AT91C_BASE_PWMC->PWMC_CH[Channel].PWMC_CMR = PWMC_MCKCLKB;
}

AT91C_BASE_PWMC->PWMC_CH[Channel].PWMC_CPRDR = Period;
AT91C_BASE_PWMC->PWMC_CH[Channel].PWMC_CDTYR = Duty;

if (LineOut) {
  if (Channel==0) {
    if (LineOut==AT91C_PA0_PWM0) AT91F_PIO_CfgPeriph(AT91C_BASE_PIOA,AT91C_PA0_PWM0,0);
    else if (LineOut==AT91C_PA11_PWM0) AT91F_PIO_CfgPeriph(AT91C_BASE_PIOA,0,AT91C_PA11_PWM0);
    else if (LineOut==AT91C_PA23_PWM0) AT91F_PIO_CfgPeriph(AT91C_BASE_PIOA,0,AT91C_PA23_PWM0);
  } else
  if (Channel==1) {
    if (LineOut==AT91C_PA1_PWM1) AT91F_PIO_CfgPeriph(AT91C_BASE_PIOA,AT91C_PA1_PWM1,0);
    else if (LineOut==AT91C_PA12_PWM1) AT91F_PIO_CfgPeriph(AT91C_BASE_PIOA,0,AT91C_PA12_PWM1);
    else if (LineOut==AT91C_PA24_PWM1) AT91F_PIO_CfgPeriph(AT91C_BASE_PIOA,0,AT91C_PA24_PWM1);
  }
}

}


void StartPWM(int Channel)
{
AT91F_PWMC_StartChannel(AT91C_BASE_PWMC,1<<Channel);
}


void StopPWM(int Channel)
{
AT91F_PWMC_StopChannel(AT91C_BASE_PWMC,1<<Channel);
}


void SetDutyPMW(int Channel, int Duty)
{
AT91C_BASE_PWMC->PWMC_CH[Channel].PWMC_CUPDR = Duty;
}


void TestPMW(void)
{
float Freq;

Freq=1.0;
PreparePMW(0, AT91C_PA0_PWM0, &Freq, 200, 20);
//AT91F_PIO_CfgPeriph(AT91C_BASE_PIOA,LED1,0);
StartPWM(0);

}




