#ifndef _scrlmhgr_h
#define _scrlmhgr_h

#include "protocol.h"
#include "sysutil.h"
#define MaxScrlStrs 10

typedef struct ScrollerMngr{
           char X,Y,Len;
           char Font;
           char ShortDraw;
           signed char CurStr,NumberStr;
           unsigned int LeftKey;
           unsigned int RightKey;
           unsigned int ActionKey1;
           unsigned int ActionKey2;
           char IsInv;
           char *String[MaxScrlStrs];
}TScrollerMngr;

void InitScroller(struct ScrollerMngr *Scrl,char X, char Y,char Font);
char AddScrollerString(struct ScrollerMngr *Scrl,char *ScrlString);
void RedrawScrlBoxArrow(u16 x, u16 y, u16 Len);
//void RedrawScrlString(u16 x, u16 y,char ROM * Str);
void RedrawScrl(struct ScrollerMngr *Scrl);
char DoScrlNoWait(struct ScrollerMngr *Scrl, unsigned int ch);
unsigned int DoScrl(struct ScrollerMngr *Scrl);
char GetCurScrl(struct ScrollerMngr *Scrl);
char SetCurScrl(struct ScrollerMngr *Scrl,char N);
void ClearScroller(struct ScrollerMngr *Scrl);
void DoLeftKeySc(struct ScrollerMngr *Scrl);
void DoRightKeySc(struct ScrollerMngr *Scrl);

#endif

