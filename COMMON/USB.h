#ifndef USB_H
#define USB_H

#define USBDataBlock 1024

// 0 to stop, 1 - to start
extern volatile u8 USBStarted;


// line to check USB connected
// If does not exist d not #define
#if defined (__KIT)
#define USB_DETECT_LINE AT91C_PIO_PA13
#endif

#if defined (__Korsar)
#define USB_DETECT_LINE AT91C_PIO_PA16
#endif

#if defined (__TTMon) || defined (__R1500_6) ||defined (__R1500) || defined (__VVTester)
#define USB_DETECT_LINE AT91C_PIO_PA23  // acm, 2-18-09 we define __R1500 and our schematic matches PA23
#endif

#if defined (__R505)
#define USB_DETECT_LINE AT91C_PIO_PA30
#endif

#if defined (__LocMon)
#define USB_DETECT_LINE AT91C_PIO_PB23
#endif

#if defined (__AR200)
#define USB_DETECT_LINE AT91C_PIO_PB18
#endif

#if defined (__KIT9)
// �.�. AT91C_PIO_PCx
#define USB_DETECT_LINE AT91C_PIO_PC7
#endif

#if defined (__TIM3)
#define USB_DETECT_LINE AT91C_PIO_PA31
#endif


// if time between packets > USB_RX_TIMEOUT Tics32 - old packet deleted
// if no need to delete, do not #define
#define USB_RX_TIMEOUT 16 // 16*32 ticks=0.5seconds



// If need to align packet to 64 byte (for compatibiliy with old)
#if defined (__KIT)
#else
#define USB_ALIGN64
#endif




// Turns on USB, starts interrupt and sets for receive
// Do somwhere in Init right after keypad
//void USBInit(void);
// If line does exist, waiting for intialization. Not more than 100mS
//void USBWaitInit(void);

// Do on device intit. my not do at all.
//void USBClose(void);

//void SOEUart_irq_handler(void); //agonly in PDM
// start-stop USB using stop-start of interrupt
// Shall not do at early stage- need time to init USB
void USBDisable(void);
void USBEnable(void);


// 1 - USB present, initialized, USBStarted==1
u32 IsUSBPresent(void);
void USBMemMove(u8 *Dst, u8 *Src, u32 Len);
void USBMemSet(void *s, int c, u32 n);  //2.10 new to avoid failure in __segment_init
void USB_Usart_Init(void);    //2.10 new instead of void Usart_init(void). Hopping on more stability

// start writing without a delay
void USBStartWrite(u8* Buf, u32 Len);
// waiting for finishing
u32 USBWrited(void);
// start and wait - does not process keyboard !!!
u32 USBWrite(u8* Buf, u32 Len);

// Read what has arrived
// even with alligning (USB_ALIGN64) fills only Len bytes, all rest should be throuwn out USBFlushRX()
u32 USBRead(u8* Buf, u32 Len);
// How much data is in input buffer or 0
u32 USBReaded(void);
// Clear unneeded data
void USBFlushRX(void);

void USBShift(u32 Len);

void TestUSB(void);


#endif



