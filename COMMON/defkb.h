#ifndef _defkb_h
#define _defkb_h

// ������� ���� - ��� �������
// ������� ���� - ������� ����� (1/16 ���) ��� ���� ������ (0..127)
// ���� KB_TIME<0x80000, �� ��� ����������� �������, ���� >=0x80000 - �� ����������
// ������� ��� - 1=������ 0=���������
typedef unsigned int KEY;
#define        KB_KEY                 ((KEY)0x0000FFFF)
#define        KB_TIME                ((KEY)0x7FFF0000)
#define        KB_PRESSED             ((KEY)0x80000000)

#define        KB_NO                  ((KEY)0x0000FFFF)
#define        KB_IDLE                ((KEY)0x0000003F)

#define        KB_MEM_MASK            ((KEY)0x00000040)
#define        KB_MOD_MASK            ((KEY)0x00000080)


#ifdef __VV
#define        KB_SET                 ((KEY)0)
#define        KB_LIGHT               ((KEY)1)
#define        KB_HOLD                ((KEY)2)
#define        KB_POWER               ((KEY)3)
#define        KB_SET_HOLD            ((KEY)4)
#define        KB_ESC                 ((KEY)5)

#define        KB_ENTER               KB_SET

#endif


#if defined(__TTMon) || defined(__PDMon) || defined(__R1500_6) || defined(__TIM3)

#define        KB_ESC                 ((KEY)7)
#define        KB_ENTER               ((KEY)0)
#define        KB_MEM                 ((KEY)1)
#define        KB_MOD                 ((KEY)3)

#define        KB_UP                  ((KEY)4)
#define        KB_DOWN                ((KEY)5)
#define        KB_LEFT                ((KEY)8)
#define        KB_RIGHT               ((KEY)2)

#define        KB_LIGHT               ((KEY)9)

#define        KB_MOD_UP              ((KEY)0x84)
#define        KB_MOD_DOWN            ((KEY)0x85)
#define        KB_POWER               ((KEY)10)
#define        KB_TIMEOUT             ((KEY)11)

#endif


#if defined(__Korsar)

#define        KB_SET_MASK            KB_MOD_MASK

#define        KB_MEM                 ((KEY)0)
#define        KB_SET                 ((KEY)1)
#define        KB_POWER               ((KEY)2)

#define        KB_ENTER               ((KEY)9)
#define        KB_RIGHT               ((KEY)8)
#define        KB_UP                  ((KEY)3)

#define        KB_ESC                 ((KEY)4)
#define        KB_LEFT                ((KEY)5)
#define        KB_DOWN                ((KEY)6)
#define        KB_LIGHT               ((KEY)7)

#endif


#ifndef KB_LIGHT
#define KB_LIGHT ((KEY)20)
#endif

#ifndef KB_POWER
#define KB_POWER ((KEY)21)
#endif


#endif

