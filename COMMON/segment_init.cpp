/**************************************************
 *
 * Segment initialization that must be
 * performed before main is called.
 *
 * Since this function is called from cstartup,
 * it is located in the same segment, ICODE.
 *
 * Copyright 1999-2004 IAR Systems. All rights reserved.
 *
 * $Revision: 1.9 $
 *
 **************************************************/

#include "segment_init.h"
#include "TypesDef.h"
#include "usb.h"
//#include <string.h>

#pragma language=extended
#pragma segment="INITTAB"

#pragma location="ICODE"
__interwork void __segment_init(void)
{
  InitBlock_Type const * const initTableBegin = __sfb( "INITTAB" );
  InitBlock_Type const * const initTableEnd = __sfe( "INITTAB" );
  InitBlock_Type const * initTableP;

  /* Loop over all elements in the initialization table. */
  for (initTableP=initTableBegin; initTableP<initTableEnd; initTableP++)
  {
    /* If src=dest then we should clear a memory
     * block, otherwise it's a copy operation. */
    if (initTableP->Src == initTableP->Dst)
    {
      USBMemSet(initTableP->Dst, 0, (u32)initTableP->Size);
    }
    else
    {
      USBMemMove(initTableP->Dst, initTableP->Src, (u32)initTableP->Size);
    }
  }
}

#pragma language=default
