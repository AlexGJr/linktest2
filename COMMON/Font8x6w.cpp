const char Fnt6x8[256][8]={      
      0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,  /* 00 */
      0x78,0x84,0xCC,0x84,0xB4,0x84,0x84,0x78,  /* 01 */
      0x78,0xFC,0xB4,0xFC,0xCC,0xFC,0xFC,0x78,  /* 02 */
      0x48,0xFC,0xFC,0xFC,0x78,0x30,0x00,0x00,  /* 03 */
      0x00,0x20,0x70,0xF8,0x70,0x20,0x00,0x00,  /* 04 */
      0x20,0x70,0x20,0xF8,0xA8,0x20,0x70,0x00,  /* 05 */
      0x00,0x20,0x70,0xF8,0xF8,0x20,0x70,0x00,  /* 06 */
      0x00,0x00,0x30,0x78,0x78,0x30,0x00,0x00,  /* 07 */
      0xFC,0xFC,0xCC,0x84,0x84,0xCC,0xFC,0xFC,  /* 08 */
      0x00,0x78,0xCC,0x84,0x84,0xCC,0x78,0x00,  /* 09 */
      0xFC,0xCC,0x84,0xB4,0xB4,0x84,0xCC,0xFC,  /* 0A */
      0x00,0x0C,0x0C,0x70,0x88,0x88,0x88,0x70,  /* 0B */
      0x70,0x88,0x88,0x88,0x70,0x20,0x70,0x20,  /* 0C */
      0x38,0x28,0x38,0x20,0x20,0x20,0xE0,0xE0,  /* 0D */
      0x7C,0x44,0x7C,0x44,0x44,0x4C,0xCC,0xC0,  /* 0E */
      0x00,0xA8,0x70,0x88,0x88,0x70,0xA8,0x00,  /* 0F */
      0x80,0xE0,0xF8,0xFC,0xF8,0xE0,0x80,0x00,  /* 10 */
      0x04,0x1C,0x7C,0xFC,0x7C,0x1C,0x04,0x00,  /* 11 */
      0x30,0x78,0xFC,0x30,0x30,0xFC,0x78,0x30,  /* 12 */
      0x48,0x48,0x48,0x48,0x48,0x00,0x48,0x00,  /* 13 */
      0x78,0xA8,0xA8,0x68,0x28,0x28,0x28,0x00,  /* 14 */
      0x70,0x80,0x60,0x90,0x90,0x60,0x10,0xE0,  /* 15 */
      0x00,0x00,0x00,0x00,0x00,0xF8,0xF8,0x00,  /* 16 */
      0x20,0x70,0xF8,0x20,0xF8,0x70,0x20,0xF8,  /* 17 */
      0x20,0x70,0xF8,0x20,0x20,0x20,0x20,0x00,  /* 18 */
      0x20,0x20,0x20,0x20,0xF8,0x70,0x20,0x00,  /* 19 */
      0x00,0x10,0x08,0xFC,0x08,0x10,0x00,0x00,  /* 1A */
      0x00,0x20,0x40,0xFC,0x40,0x20,0x00,0x00,  /* 1B */
      0x00,0x00,0x00,0x80,0x80,0xFC,0x00,0x00,  /* 1C */
      0x00,0x00,0x48,0xFC,0x48,0x00,0x00,0x00,  /* 1D */
      0x00,0x00,0x30,0x78,0xFC,0xFC,0x00,0x00,  /* 1E */
      0x00,0xFC,0xFC,0x78,0x30,0x00,0x00,0x00,  /* 1F */
      0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,  /* 20 */
      0x20,0x70,0x70,0x20,0x20,0x00,0x20,0x00,  /* 21 */
      0x50,0x50,0x00,0x00,0x00,0x00,0x00,0x00,  /* 22 */
      0x50,0x50,0xF8,0x50,0xF8,0x50,0x50,0x00,  /* 23 */
      0x10,0x78,0xC0,0x78,0x0C,0x78,0x10,0x00,  /* 24 */
      0x00,0x84,0x08,0x10,0x20,0x40,0x84,0x00,  /* 25 */
      0x30,0x48,0x30,0x5C,0x88,0x88,0x74,0x00,  /* 26 */
      0x10,0x10,0x20,0x00,0x00,0x00,0x00,0x00,  /* 27 */
      0x08,0x10,0x20,0x20,0x20,0x10,0x08,0x00,  /* 28 */
      0x40,0x20,0x10,0x10,0x10,0x20,0x40,0x00,  /* 29 */
      0x00,0xCC,0x30,0xFC,0x30,0xCC,0x00,0x00,  /* 2A */
      0x00,0x20,0x20,0xF8,0x20,0x20,0x00,0x00,  /* 2B */
      0x00,0x00,0x00,0x00,0x00,0x20,0x20,0x40,  /* 2C */
      0x00,0x00,0x00,0x78,0x00,0x00,0x00,0x00,  /* 2D */
      0x00,0x00,0x00,0x00,0x00,0x00,0x20,0x00,  /* 2E */
      0x00,0x04,0x08,0x10,0x20,0x40,0x80,0x00,  /* 2F */
      0x70,0x88,0x98,0xA8,0xC8,0x88,0x70,0x00,  /* 30 */
      0x20,0x20,0x60,0x20,0x20,0x20,0x70,0x00,  /* 31 */
      0x70,0x88,0x08,0x10,0x20,0x48,0xF8,0x00,  /* 32 */
      0x70,0x88,0x08,0x30,0x08,0x88,0x70,0x00,  /* 33 */
      0x10,0x30,0x50,0x90,0xF8,0x10,0x38,0x00,  /* 34 */
      0xF8,0x80,0x80,0xF0,0x08,0x08,0xF0,0x00,  /* 35 */
      0x30,0x40,0x80,0xF0,0x88,0x88,0x70,0x00,  /* 36 */
      0xF8,0x88,0x10,0x20,0x40,0x40,0x40,0x00,  /* 37 */
      0x70,0x88,0x88,0x70,0x88,0x88,0x70,0x00,  /* 38 */
      0x70,0x88,0x88,0x78,0x08,0x08,0x70,0x00,  /* 39 */
      0x00,0x20,0x20,0x00,0x00,0x20,0x20,0x00,  /* 3A */
      0x00,0x20,0x20,0x00,0x00,0x20,0x20,0x40,  /* 3B */
      0x08,0x10,0x20,0x40,0x20,0x10,0x08,0x00,  /* 3C */
      0x00,0x00,0x78,0x00,0x00,0x78,0x00,0x00,  /* 3D */
      0x40,0x20,0x10,0x08,0x10,0x20,0x40,0x00,  /* 3E */
      0x70,0x88,0x10,0x20,0x20,0x00,0x20,0x00,  /* 3F */
      0x70,0x88,0xB8,0xA8,0xB8,0x80,0x70,0x00,  /* 40 */
      0x70,0x88,0x88,0xF8,0x88,0x88,0x88,0x00,  /* 41 */
      0xF0,0x48,0x48,0x70,0x48,0x48,0xF0,0x00,  /* 42 */
      0x70,0x88,0x80,0x80,0x80,0x88,0x70,0x00,  /* 43 */
      0xF0,0x48,0x48,0x48,0x48,0x48,0xF0,0x00,  /* 44 */
      0xF8,0x48,0x40,0x60,0x40,0x48,0xF8,0x00,  /* 45 */
      0xF8,0x48,0x40,0x60,0x40,0x40,0xE0,0x00,  /* 46 */
      0x70,0x88,0x80,0x80,0x98,0x88,0x70,0x00,  /* 47 */
      0x88,0x88,0x88,0xF8,0x88,0x88,0x88,0x00,  /* 48 */
      0x70,0x20,0x20,0x20,0x20,0x20,0x70,0x00,  /* 49 */
      0x38,0x10,0x10,0x10,0x90,0x90,0x60,0x00,  /* 4A */
      0x88,0x88,0x90,0xE0,0x90,0x88,0x88,0x00,  /* 4B */
      0xE0,0x40,0x40,0x40,0x40,0x48,0xF8,0x00,  /* 4C */
      0x88,0xD8,0xA8,0x88,0x88,0x88,0x88,0x00,  /* 4D */
      0x88,0x88,0xC8,0xA8,0x98,0x88,0x88,0x00,  /* 4E */
      0x70,0x88,0x88,0x88,0x88,0x88,0x70,0x00,  /* 4F */
      0xF0,0x48,0x48,0x70,0x40,0x40,0xE0,0x00,  /* 50 */
      0x70,0x88,0x88,0x88,0x88,0x98,0x78,0x04,  /* 51 */
      0xF0,0x88,0x88,0xF0,0xA0,0x90,0x88,0x00,  /* 52 */
      0x70,0x88,0x80,0x70,0x08,0x88,0x70,0x00,  /* 53 */
      0xF8,0xA8,0x20,0x20,0x20,0x20,0x70,0x00,  /* 54 */
      0x88,0x88,0x88,0x88,0x88,0x88,0x70,0x00,  /* 55 */
      0x88,0x88,0x88,0x88,0x88,0x50,0x20,0x00,  /* 56 */
      0x88,0x88,0x88,0x88,0xA8,0xA8,0x50,0x00,  /* 57 */
      0x88,0x88,0x50,0x20,0x50,0x88,0x88,0x00,  /* 58 */
      0x88,0x88,0x88,0x70,0x20,0x20,0x70,0x00,  /* 59 */
      0xF8,0x88,0x10,0x20,0x40,0x88,0xF8,0x00,  /* 5A */
      0x30,0x20,0x20,0x20,0x20,0x20,0x30,0x00,  /* 5B */
      0x00,0x80,0x40,0x20,0x10,0x08,0x04,0x00,  /* 5C */
      0x30,0x10,0x10,0x10,0x10,0x10,0x30,0x00,  /* 5D */
      0x20,0x50,0x88,0x00,0x00,0x00,0x00,0x00,  /* 5E */
      0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xFC,  /* 5F */
      0x40,0x20,0x00,0x00,0x00,0x00,0x00,0x00,  /* 60 */
      0x00,0x00,0x70,0x08,0x78,0x88,0x74,0x00,  /* 61 */
      0x80,0x80,0xF0,0x88,0x88,0x88,0xF0,0x00,  /* 62 */
      0x00,0x00,0x70,0x88,0x80,0x88,0x70,0x00,  /* 63 */
      0x08,0x08,0x78,0x88,0x88,0x88,0x74,0x00,  /* 64 */
      0x00,0x00,0x70,0x88,0xF8,0x80,0x70,0x00,  /* 65 */
      0x30,0x48,0x40,0xF0,0x40,0x40,0xE0,0x00,  /* 66 */
      0x00,0x00,0x74,0x88,0x88,0x78,0x08,0x70,  /* 67 */
      0xC0,0x40,0x50,0x68,0x48,0x48,0xC8,0x00,  /* 68 */
      0x20,0x00,0x60,0x20,0x20,0x20,0x70,0x00,  /* 69 */
      0x08,0x00,0x08,0x08,0x08,0x48,0x48,0x30,  /* 6A */
      0xC0,0x40,0x48,0x50,0x60,0x50,0xC8,0x00,  /* 6B */
      0x60,0x20,0x20,0x20,0x20,0x20,0x70,0x00,  /* 6C */
      0x00,0x00,0xD0,0xA8,0xA8,0xA8,0xA8,0x00,  /* 6D */
      0x00,0x00,0xD0,0x68,0x48,0x48,0x48,0x00,  /* 6E */
      0x00,0x00,0x70,0x88,0x88,0x88,0x70,0x00,  /* 6F */
      0x00,0x00,0xF0,0x48,0x48,0x70,0x40,0xE0,  /* 70 */
      0x00,0x00,0x68,0x90,0x90,0x70,0x10,0x38,  /* 71 */
      0x00,0x00,0xD0,0x68,0x40,0x40,0xE0,0x00,  /* 72 */
      0x00,0x00,0x78,0x80,0x70,0x08,0xF0,0x00,  /* 73 */
      0x20,0x20,0x70,0x20,0x20,0x28,0x10,0x00,  /* 74 */
      0x00,0x00,0x90,0x90,0x90,0x90,0x68,0x00,  /* 75 */
      0x00,0x00,0x88,0x88,0x88,0x50,0x20,0x00,  /* 76 */
      0x00,0x00,0x88,0x88,0xA8,0xA8,0x50,0x00,  /* 77 */
      0x00,0x00,0x88,0x50,0x20,0x50,0x88,0x00,  /* 78 */
      0x00,0x00,0x88,0x88,0x88,0x78,0x08,0x70,  /* 79 */
      0x00,0x00,0xF8,0x90,0x20,0x48,0xF8,0x00,  /* 7A */
      0x18,0x20,0x20,0x40,0x20,0x20,0x18,0x00,  /* 7B */
      0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x00,  /* 7C */
      0x60,0x10,0x10,0x08,0x10,0x10,0x60,0x00,  /* 7D */
      0x64,0x98,0x00,0x00,0x00,0x00,0x00,0x00,  /* 7E */
      0x00,0x00,0x20,0x50,0x88,0x88,0xF8,0x00,  /* 7F */
      0x10,0x44,0x10,0x44,0x10,0x44,0x10,0x44,  /* B0 */
      0x54,0xA8,0x54,0xA8,0x54,0xA8,0x54,0xA8,  /* B1 */
      0xDC,0x74,0xDC,0x74,0xDC,0x74,0xDC,0x74,  /* B2 */
      0x10,0x10,0x10,0x10,0x10,0x10,0x10,0x10,  /* B3 */
      0x10,0x10,0x10,0xF0,0x10,0x10,0x10,0x10,  /* B4 */
      0x10,0xF0,0x10,0xF0,0x10,0x10,0x10,0x10,  /* B5 */
      0x28,0x28,0x28,0xE8,0x28,0x28,0x28,0x28,  /* B6 */
      0x00,0x00,0x00,0xF8,0x28,0x28,0x28,0x28,  /* B7 */
      0x00,0xF0,0x10,0xF0,0x10,0x10,0x10,0x10,  /* B8 */
      0x28,0xE8,0x08,0xE8,0x28,0x28,0x28,0x28,  /* B9 */
      0x28,0x28,0x28,0x28,0x28,0x28,0x28,0x28,  /* BA */
      0x00,0xF8,0x08,0xE8,0x28,0x28,0x28,0x28,  /* BB */
      0x28,0xE8,0x08,0xF8,0x00,0x00,0x00,0x00,  /* BC */
      0x28,0x28,0x28,0xF8,0x00,0x00,0x00,0x00,  /* BD */
      0x10,0xF0,0x10,0xF0,0x00,0x00,0x00,0x00,  /* BE */
      0x00,0x00,0x00,0xF0,0x10,0x10,0x10,0x10,  /* BF */
      0x10,0x10,0x10,0x1F,0x00,0x00,0x00,0x00,  /* C0 */
      0x10,0x10,0x10,0xFE,0x00,0x00,0x00,0x00,  /* C1 */
      0x00,0x00,0x00,0xFE,0x10,0x10,0x10,0x10,  /* C2 */
      0x10,0x10,0x10,0x1E,0x10,0x10,0x10,0x10,  /* C3 */
      0x00,0x00,0x00,0xFE,0x00,0x00,0x00,0x00,  /* C4 */
      0x10,0x10,0x10,0xFE,0x10,0x10,0x10,0x10,  /* C5 */
      0x10,0x1E,0x10,0x1E,0x10,0x10,0x10,0x10,  /* C6 */
      0x28,0x28,0x28,0x2C,0x28,0x28,0x28,0x28,  /* C7 */
      0x28,0x2C,0x20,0x3C,0x00,0x00,0x00,0x00,  /* C8 */
      0x00,0x3C,0x20,0x2C,0x28,0x28,0x28,0x28,  /* C9 */
      0x28,0xEC,0x00,0xFC,0x00,0x00,0x00,0x00,  /* CA */
      0x00,0xFC,0x00,0xEC,0x28,0x28,0x28,0x28,  /* CB */
      0x28,0x2C,0x20,0x2C,0x28,0x28,0x28,0x28,  /* CC */
      0x00,0xFC,0x00,0xFC,0x00,0x00,0x00,0x00,  /* CD */
      0x28,0xEC,0x00,0xEC,0x28,0x28,0x28,0x28,  /* CE */
      0x10,0xFC,0x00,0xFC,0x00,0x00,0x00,0x00,  /* CF */
      0x28,0x28,0x28,0xFC,0x00,0x00,0x00,0x00,  /* D0 */
      0x00,0xFC,0x00,0xFC,0x10,0x10,0x10,0x10,  /* D1 */
      0x00,0x00,0x00,0xFC,0x28,0x28,0x28,0x28,  /* D2 */
      0x28,0x28,0x28,0x3C,0x00,0x00,0x00,0x00,  /* D3 */
      0x10,0x1C,0x10,0x1C,0x00,0x00,0x00,0x00,  /* D4 */
      0x00,0x1C,0x10,0x1C,0x10,0x10,0x10,0x10,  /* D5 */
      0x00,0x00,0x00,0x3C,0x28,0x28,0x28,0x28,  /* D6 */
      0x28,0x28,0x28,0xFC,0x28,0x28,0x28,0x28,  /* D7 */
      0x10,0xFC,0x10,0xFC,0x10,0x10,0x10,0x10,  /* D8 */
      0x10,0x10,0x10,0xF0,0x00,0x00,0x00,0x00,  /* D9 */
      0x00,0x00,0x00,0x0E,0x08,0x08,0x08,0x08,  /* DA */
      0xFC,0xFC,0xFC,0xFC,0xFC,0xFC,0xFC,0xFC,  /* DB */
      0x00,0x00,0x00,0xFC,0xFC,0xFC,0xFC,0xFC,  /* DC */
      0xE0,0xE0,0xE0,0xE0,0xE0,0xE0,0xE0,0xE0,  /* DD */
      0x1C,0x1C,0x1C,0x1C,0x1C,0x1C,0x1C,0x1C,  /* DE */
      0xFC,0xFC,0xFC,0xFC,0x00,0x00,0x00,0x00,  /* DF */
      0x50,0xF8,0x48,0x60,0x40,0x48,0xF8,0x00,  /* F0 */
      0x50,0x00,0x70,0x88,0xF8,0x80,0x70,0x00,  /* F1 */
      0x70,0x88,0x80,0xE0,0x80,0x88,0x70,0x00,  /* F2 */
      0x00,0x00,0x70,0x88,0xE0,0x88,0x70,0x00,  /* F3 */
      0x50,0x70,0x20,0x20,0x20,0x20,0x70,0x00,  /* F4 */
      0x50,0x00,0x20,0x20,0x20,0x20,0x70,0x00,  /* F5 */
      0x70,0x88,0x88,0x78,0x08,0x88,0x70,0x00,  /* F6 */
      0x60,0x00,0x90,0x90,0x90,0x70,0x10,0xE0,  /* F7 */
      0x30,0x48,0x48,0x30,0x00,0x00,0x00,0x00,  /* F8 */
      0x00,0x00,0x00,0x30,0x30,0x00,0x00,0x00,  /* F9 */
      0x00,0x00,0x00,0x00,0x30,0x00,0x00,0x00,  /* FA */
      0x18,0x10,0x10,0x10,0xD0,0x50,0x30,0x00,  /* FB */
      0x9C,0x94,0x9C,0xD0,0xB0,0x90,0x9C,0x00,  /* FC */
      0x00,0xD8,0x70,0x88,0x88,0x70,0xD8,0x00,  /* FD */
      0x00,0x00,0x78,0x78,0x78,0x78,0x00,0x00,  /* FE */
      0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,  /* FF */
      0x38,0x48,0x88,0x88,0xF8,0x88,0x88,0x00,  /* 80 */
      0xF8,0x48,0x40,0x70,0x48,0x48,0xF0,0x00,  /* 81 */
      0xF0,0x48,0x48,0x70,0x48,0x48,0xF0,0x00,  /* 82 */
      0xF8,0x48,0x40,0x40,0x40,0x40,0xE0,0x00,  /* 83 */
      0x18,0x28,0x48,0x48,0x48,0x48,0xFC,0x84,  /* 84 */
      0xF8,0x48,0x40,0x60,0x40,0x48,0xF8,0x00,  /* 85 */
      0xA8,0xA8,0xA8,0x70,0xA8,0xA8,0xA8,0x00,  /* 86 */
      0x70,0x88,0x08,0x30,0x08,0x88,0x70,0x00,  /* 87 */
      0x88,0x88,0x98,0xA8,0xC8,0x88,0x88,0x00,  /* 88 */
      0x70,0x88,0x98,0xA8,0xC8,0x88,0x88,0x00,  /* 89 */
      0xC8,0x48,0x50,0x60,0x50,0x48,0xC8,0x00,  /* 8A */
      0x18,0x28,0x48,0x48,0x48,0x48,0x88,0x00,  /* 8B */
      0x88,0xD8,0xA8,0x88,0x88,0x88,0x88,0x00,  /* 8C */
      0x88,0x88,0x88,0xF8,0x88,0x88,0x88,0x00,  /* 8D */
      0x70,0x88,0x88,0x88,0x88,0x88,0x70,0x00,  /* 8E */
      0xF8,0x88,0x88,0x88,0x88,0x88,0x88,0x00,  /* 8F */
      0xF0,0x48,0x48,0x48,0x70,0x40,0xE0,0x00,  /* 90 */
      0x70,0x88,0x80,0x80,0x80,0x88,0x70,0x00,  /* 91 */
      0xF8,0xA8,0x20,0x20,0x20,0x20,0x70,0x00,  /* 92 */
      0x88,0x88,0x88,0x78,0x08,0x88,0x70,0x00,  /* 93 */
      0x70,0xA8,0xA8,0xA8,0x70,0x20,0x70,0x00,  /* 94 */
      0x88,0x88,0x50,0x20,0x50,0x88,0x88,0x00,  /* 95 */
      0x90,0x90,0x90,0x90,0x90,0x90,0xF8,0x08,  /* 96 */
      0x88,0x88,0x88,0x78,0x08,0x08,0x08,0x00,  /* 97 */
      0xA8,0xA8,0xA8,0xA8,0xA8,0xA8,0xF8,0x00,  /* 98 */
      0xA8,0xA8,0xA8,0xA8,0xA8,0xA8,0xFC,0x04,  /* 99 */
      0xC0,0xC0,0x40,0x70,0x48,0x48,0x70,0x00,  /* 9A */
      0x88,0x88,0x88,0xE8,0x98,0x98,0xE8,0x00,  /* 9B */
      0x40,0x40,0x40,0x70,0x48,0x48,0x70,0x00,  /* 9C */
      0x70,0x88,0x08,0x38,0x08,0x88,0x70,0x00,  /* 9D */
      0x90,0xA8,0xA8,0xE8,0xA8,0xA8,0x90,0x00,  /* 9E */
      0x78,0x88,0x88,0x88,0x78,0x48,0x88,0x00,  /* 9F */
      0x00,0x00,0x60,0x10,0x70,0x90,0x68,0x00,  /* A0 */
      0x08,0x70,0x80,0x70,0x88,0x88,0x70,0x00,  /* A1 */
      0x00,0x00,0xF0,0x48,0x70,0x48,0xF0,0x00,  /* A2 */
      0x00,0x00,0xF8,0x48,0x40,0x40,0xE0,0x00,  /* A3 */
      0x00,0x00,0x30,0x50,0x50,0x50,0xF8,0x88,  /* A4 */
      0x00,0x00,0x70,0x88,0xF8,0x80,0x70,0x00,  /* A5 */
      0x00,0x00,0xA8,0x70,0x20,0x70,0xA8,0x00,  /* A6 */
      0x00,0x00,0x70,0x88,0x10,0x88,0x70,0x00,  /* A7 */
      0x00,0x00,0x88,0x98,0xA8,0xC8,0x88,0x00,  /* A8 */
      0x00,0x70,0x88,0x98,0xA8,0xC8,0x88,0x00,  /* A9 */
      0x00,0x00,0xC8,0x50,0x60,0x50,0xC8,0x00,  /* AA */
      0x00,0x00,0x38,0x48,0x48,0x48,0x88,0x00,  /* AB */
      0x00,0x00,0x88,0xD8,0xA8,0x88,0x88,0x00,  /* AC */
      0x00,0x00,0x88,0x88,0xF8,0x88,0x88,0x00,  /* AD */
      0x00,0x00,0x70,0x88,0x88,0x88,0x70,0x00,  /* AE */
      0x00,0x00,0xF8,0x88,0x88,0x88,0x88,0x00,  /* AF */
      0x00,0x00,0xF0,0x48,0x48,0x70,0x40,0xE0,  /* E0 */
      0x00,0x00,0x70,0x88,0x80,0x88,0x70,0x00,  /* E1 */
      0x00,0x00,0xF8,0xA8,0x20,0x20,0x70,0x00,  /* E2 */
      0x00,0x00,0x88,0x88,0x88,0x78,0x08,0x70,  /* E3 */
      0x00,0x00,0x70,0xA8,0xA8,0x70,0x20,0x70,  /* E4 */
      0x00,0x00,0x88,0x50,0x20,0x50,0x88,0x00,  /* E5 */
      0x00,0x00,0x90,0x90,0x90,0x90,0xF8,0x08,  /* E6 */
      0x00,0x00,0x88,0x88,0x78,0x08,0x08,0x00,  /* E7 */
      0x00,0x00,0xA8,0xA8,0xA8,0xA8,0xF8,0x00,  /* E8 */
      0x00,0x00,0xA8,0xA8,0xA8,0xA8,0xFC,0x04,  /* E9 */
      0x00,0x00,0xC0,0xC0,0x70,0x48,0x70,0x00,  /* EA */
      0x00,0x00,0x88,0x88,0xE8,0x98,0xE8,0x00,  /* EB */
      0x00,0x00,0x40,0x40,0x70,0x48,0x70,0x00,  /* EC */
      0x00,0x00,0x70,0x88,0x38,0x88,0x70,0x00,  /* ED */
      0x00,0x00,0x90,0xA8,0xE8,0xA8,0x90,0x00,  /* EE */
      0x00,0x00,0x78,0x88,0x78,0x48,0x88,0x00  /* EF */
};

