#include "font.h"
#include "LCD.h"

#include <string.h>

#include "FONT6X5W.cpp"
#include "FONT8X6W.cpp"
#include "FONT8X8W.cpp"


char CurFontWidth    = 8,        //������ �������� �����
     CurFontHeight   = 8;        //������  �������� �����
char CurFontPtrNumer = Font8x8;  //��������� �������� �����

//---
char CurFontPtr(u16 i1,u16 i2)
{

    char b;
    switch (CurFontPtrNumer)
       {
       case Font6x5   : { b=Fnt6x5[i1][i2];   return b;}
       case Font8x6   : { b=Fnt6x8[i1][i2];   return b;}
       case Font8x8   : { b=Fnt8x8[i1][i2];   return b;}
       //case Font8x16  : { b=Fnt8x16[i1][i2];  return b;}
       //case Font12x16 : { b=Fnt12x16[i1][i2]; return b;}
       //case Font16x16 : { b=Fnt16x16[i1][i2]; return b;}
       default:         { b=Fnt8x8[i1][i2];   return b;}
       }
}
//---
void SetFont(char FontType)
{
    switch (FontType)
       {
       case Font6x5:   {CurFontWidth=5;  CurFontHeight=6;   CurFontPtrNumer=Font6x5;  break; }
       case Font8x6:   {CurFontWidth=6;  CurFontHeight=8;   CurFontPtrNumer=Font8x6;  break; }
       case Font8x8:   {CurFontWidth=8;  CurFontHeight=8;   CurFontPtrNumer=Font8x8;  break; }
       //case Font8x16:  {CurFontWidth=8;  CurFontHeight=16;  CurFontPtrNumer=Font8x16; break; }
       //case Font12x16: {CurFontWidth=12; CurFontHeight=16;  CurFontPtrNumer=Font12x16;break; }
       //case Font16x16: {CurFontWidth=16; CurFontHeight=16;  CurFontPtrNumer=Font16x16;break; }
       default:        {CurFontWidth=8;  CurFontHeight=8;   CurFontPtrNumer=Font8x8;  break; }
       }
}
//---
void OutCharAND(u16 X,u16 Y,char S,u16 Mask)
{
u16 i, j, b;
for (j=0;j<CurFontHeight;j++)
    {
    if (CurFontWidth>8) {b = ((CurFontPtr(S,j*2)^Mask)<<8) | CurFontPtr(S,j*2+1)^Mask;}
    else                {b = (CurFontPtr(S,j)^Mask)<<8;}
    for (i=0;i<CurFontWidth;i++)
        {OutPoint(X+i,Y+j,b & (0x8000>>i));}
    }
}

u16 strlenwidth(char *Str)
{
return strlen(Str)*CurFontWidth;
}
