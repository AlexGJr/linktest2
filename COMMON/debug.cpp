#include "lcd.h"

#if defined(__VV) || defined(__PDMon) || defined(__Korsar) || defined(__KIT)

#include "Graph.h"

#include "debug.h"
#include <stdlib.h>
#include <string.h>

#ifdef __KIT
#include <stdio.h>
#endif

/*
int DebugStr=64;


int PrintDebugInt(int Val)
{
#ifdef EnableDebugMessages
char SavedFont;

	if (Val>99999) {
		PrintDebug(IntToStr(Val/100000,5));
        SavedFont=CurFontPtrNumer;
	    SetFont(Font8x8);
		OutString(ScreenWidth-120+CurFontWidth*6,DebugStr,"00000");
		OutString(ScreenWidth-120+CurFontWidth*6,DebugStr,IntToStr(Val%100000,5));
        SetFont(SavedFont);
	}	else {
		PrintDebug(IntToStr(Val,5));
	}
#endif
return Val;
}


void PrintDebugHex(unsigned int Val)
{
#ifdef EnableDebugMessages
char s[20], i;
char HEX[16]={'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};

strcpy(s,"0x00000000");
for (i=0;i<8;i++)
	s[9-i]=HEX[(Val>>(4*i)) & 0xF];
PrintDebug(s);
#endif
}


void PrintDebug(char* String)
{
#ifdef EnableDebugMessages
    char SavedFont;

    SavedFont=CurFontPtrNumer;
	SetFont(Font8x8);
	OutString(ScreenWidth-120,DebugStr," ");
	DebugStr+=CurFontHeight;
	if (DebugStr>ScreenHeight-60)
		DebugStr=64;
	OutString(ScreenWidth-120,DebugStr,">");
	OutString(ScreenWidth-120+CurFontWidth,DebugStr,"              ");
	OutString(ScreenWidth-120+CurFontWidth,DebugStr,String);
    SetFont(SavedFont);
#endif
}


*/


#if defined(__VV) || defined(__Korsar)

int DebugX=200;
int DebugY=200;

void PrintDebug(char* String)
{
SetFont(Font6x5);
if (DebugX<110)
  OutString(DebugX,DebugY," ");
DebugX+=30;
if (DebugX>110) {
  DebugX=0;
  DebugY+=6;
}
if (DebugY>26) {
  DebugY=0;
  DebugX=0;
}
OutString(DebugX,DebugY,">");
OutString(DebugX+6,DebugY,"     ");
OutString(DebugX+6,DebugY,String);
}


int PrintDebugInt(int Val)
{
PrintDebug("---");
return Val;
}
#endif


#if defined(__PDMon)
void PrintDebug(char* String)
{
OutString( 0,8,"--");
OutString(16,8,String);
}
#endif



void PrintDebugHex(unsigned int Val)
{
char s[5];
char HEX[16]={'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};

s[0]=HEX[(Val>>12) & 0xF];
s[1]=HEX[(Val>> 8) & 0xF];
s[2]=HEX[(Val>> 4) & 0xF];
s[3]=HEX[(Val>> 0) & 0xF];
s[4]=0;
#ifdef __KIT
puts(s);
#else
PrintDebug(s);
#endif
}
#endif


#ifdef __TTMon
void PrintDebugHex(unsigned int Val)
{
char HEX[16]={ch0,ch1,ch2,ch3,ch4,ch5,ch6,ch7,ch8,ch9,chA,chb,chC,chd,chE,chF};

IndData[0]=chMinus;
IndData[1]=chMinus;
IndData[2]=chEmpty;
IndData[3]=HEX[(Val>>12) & 0xF];
IndData[4]=HEX[(Val>> 8) & 0xF];
IndData[5]=HEX[(Val>> 4) & 0xF];
IndData[6]=HEX[(Val>> 0) & 0xF];
}
#endif


