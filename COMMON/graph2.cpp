#include <string.h>
#include <math.h>
#include "graph.h"
#include "graph2.h"
#include "LCD.h"
#include "Keyboard.h"
#include "DefKB.h"
#include "Defs.h"
#include "PicRes.h"
#include "setup.h"
#include "strconst.h"
//#include "inpframe.h"
//#include "utils.h"

char fStrLen(char ROM *s)
{
char i;
if (s[0]==0) return 0;
i=1;
while (s[i]) i++;
return i;
}

void fStrCpy(char *s1, char ROM *s2){
char i=0;

  do{
    s1[i]=s2[i];
  } while(s2[i++]);
}

void fStrCat(char *s1, char ROM *s2){
char i=strlen(s1), i1=0;

 if (fStrLen(s2))
  do{
    s1[i+i1]=s2[i1];
  } while(s2[i1++]);
}

void OutProgressBar(unsigned int X,unsigned int Y,unsigned int W,unsigned int H,char Percent){
unsigned int i,X2;
char Str[10];

if(Percent>100)Percent=100;
X2=X+W*Percent/100;
DrawRect(X,Y,X+W-1,Y+H-1);
ClearRect(X+1,Y+1,H-2,W-2);

for(i=X;i<=X2;i++)
  DrawLine(i,Y,i,Y+H-1);

strcpy(Str, IntToStr(Percent));
strcat(Str, " %");
OutString1XOR(X+(W-strlen(Str)*CurFontWidth)/2,Y+(H+1-CurFontHeight)/2,Str);
}

char NextWord(char *Mes, char index)
{
char MaxWidth;
char Col=0,j=0,i;

MaxWidth = scrWidth/CurFontWidth;

i=index;
while (1)
  {
  j++;
  if ((Mes[i]==' ')||(Mes[i]=='~')||(Mes[i]==0))
    {
    if ((Col+j<=MaxWidth) && (Mes[i]!=0) && (Mes[i]!='~'))
      {
      Col+=j;
      }
    else
      {
      if ((Mes[i]==0) && (Col+j<=MaxWidth)) Col+=j;
      if ((Mes[i]=='~') && (Col+j<=MaxWidth)) Col+=j;
      return index+Col-1;
      }
    j=0;
    }
  i++;
  }
}

void InternalMessage(char *Mes)
{
char Row,index;
char i,k;
char y,x;
char X[100];

i=0; Row=0; index=0;
while (i<strlen(Mes)+1)
  {
  i=NextWord(Mes, index);
  Row++;
  i++;
  index=i;
  }
y= (scrHeight-Row*CurFontHeight)/2;

i=0; Row=0; index=0;
while (i<strlen(Mes)+1)
  {
  i=NextWord(Mes, index);
  x=(scrWidth-(i-index)*CurFontWidth)/2;
//  for (k=index;k<i;k++)
//    OutChar(x+CurFontWidth*(k-index),y+Row*CurFontHeight,Mes[k],0x00);
  X[i-index]=0;
  for (k=index;k<i;k++)
    X[k-index]=Mes[k];
  OutString1(x,y+Row*CurFontHeight,X);
  i++;
  index=i;
  Row++;
  }
}

static void MsgProc(char ROM *Cap, char ROM *Mes, char ROM *Stat,char Rect){
char X[100];

if(Rect){
  if (scrHeight>32)DrawFrame(0,0,scrWidth-1,scrHeight-1);
    else DrawRect(0,0,scrWidth-1,scrHeight-1);
}
if(Cap){
  OutStringCenter(0,1,scrWidth,Cap);
  DrawFrame(0, 0, scrWidth - 1, 2+CurFontHeight);
}
if(Stat){
  OutStringCenter(0, scrHeight - 1 - CurFontHeight, scrWidth, Stat);
  DrawFrame(0, scrHeight - 3 - CurFontHeight, scrWidth - 1, scrHeight-1);
}
if (Mes){
//  OutInt(0,0,3,1);
///  Redraw();
  fStrCpy(X, Mes);
//  OutString1(0,0,X);
//  OutInt(0,0,4,1);
//  Redraw();
  InternalMessage(X);
//  OutInt(0,0,5,1);
//  Redraw();
}
Redraw();
}

void Message(char ROM *Cap, char ROM *Mes, char ROM *Stat, char Rect){
ClearLCD();
MsgProc(Cap, Mes, Stat, Rect);
}

unsigned int MessageBox3(char ROM *Cap, char ROM *Mes, char ROM *Stat, char Font)
{
unsigned int key,prevfont=CurFontPtrNumer;

ClearLCD();

#if defined(__VVTester)
SetFont(Font8x16);
if(Font&0x80){
  DrawLongButton(34, 214, EntBtn);
  DrawLongButton(177, 214, CnclBtn);
} else {
  DrawLongButton(105, 214, EntBtn);
}
#endif

SetFont(Font&0x7F);
MsgProc(Cap, Mes, Stat, 1);


key=KB_NO;
while ((key!=KB_ENTER)&&(key!=KB_MEM)&&(key!=KB_ESC)&&(key!=KB_TIMEOUT)) key=WaitReadKeyWithDelay(BREAKDELAY, KB_TIMEOUT);
SetFont(prevfont);
return key;
}


static void int2mystr(int Value, char *aStr){

  Value=Value%10000;
  aStr[0]=Value/1000+48;
  aStr[1]=(Value%1000)/100+48;
  aStr[2]=(Value%100)/10+48;
  aStr[3]=Value%10+48;
}

static int mystr2int(char *aStr){
 int Result=0;

  Result=(aStr[0]-48)*1000+(aStr[1]-48)*100+(aStr[2]-48)*10+(aStr[3]-48);

  return Result;
}


void OutMyString(int X, int Y, char * aStr, char Pos){
 char String[5]={' ',' ',' ',' ','\x00'};
 unsigned int i;

 for(i=0;i<4;i++)
   if(Pos==i)String[i]=aStr[i]; else String[i]=7;

  OutString1(X, Y, String);
}

int MyInputInt(unsigned int X, unsigned int Y, int IntValue, char *CurPos, unsigned int *ExitKey)
{
#define DigitCount 4 //��� ��������� ����� ����� ����������
                     //��� ������� ����

   char EndInp;
   int ReturnValue;
   unsigned int ch;
   char aStr[DigitCount];

   int2mystr(IntValue, aStr);
   EndInp=0;
   while (1>0)
     {
       OutMyString(X,Y,aStr,(*CurPos));
       DrawLine(X+(*CurPos)*CurFontWidth,Y+CurFontHeight-1,X+((*CurPos)+1)*CurFontWidth-1,Y+CurFontHeight-1);
       Redraw();
       ch=WaitReadKeyWithDelay(BREAKDELAY,KB_ESC);
       switch (ch) {
       case KB_ESC   : { *ExitKey=ch; EndInp=1; ReturnValue=IntValue; break;}
       case KB_ENTER : { *ExitKey=ch; EndInp=1; ReturnValue=mystr2int(aStr); break;}
       case KB_RIGHT : {if ((*CurPos)<DigitCount-1) (*CurPos)++; else (*CurPos)=0; break;}
       case KB_LEFT  : {if ((*CurPos)>0) (*CurPos)--; else (*CurPos)=DigitCount-1; break;}
       case KB_UP    : {if (aStr[*CurPos]<57) aStr[*CurPos]++;
                          else aStr[*CurPos]=48;
                        break;}
       case KB_DOWN  : {if (aStr[*CurPos]>48) aStr[*CurPos]--;
                          else aStr[*CurPos]=57;
                        break;}
       }
       if (EndInp) break;
   }

   OutMyString(X, Y, aStr, DigitCount);
//   DrawLine(X+(*CurPos)*CurFontWidth,Y+CurFontHeight-1,X+((*CurPos)+1)*CurFontWidth-1,Y+CurFontHeight-1);
   Redraw();

   return ReturnValue;
}

static char* NoiseShift2Str(signed char NoiseValue){
  extern char OutStr[];

  if((NoiseValue<stchNCHShiftMin)||
     (NoiseValue>stchNCHShiftMax)) NoiseValue=stchNCHShiftDefault;

  IntToStrFormat(NoiseValue-stchNCHShiftZero, 2);
  if(NoiseValue-stchNCHShiftZero>0) OutStr[0]='+';

  return OutStr;
}

void OutNoiseShift(unsigned int X, unsigned int Y, signed char aValue){
  OutString1(X, Y, NoiseShift2Str(aValue));
}

char InputNoiseShift(unsigned int X,unsigned int Y,signed char Value, unsigned int *ExitKey){
   char EndInp;
   signed char EdValue, ReturnValue;
   unsigned int ch;

   EndInp=0;
   EdValue=Value;
  if((Value<stchNCHShiftMin)||
     (Value>stchNCHShiftMax)) Value=stchNCHShiftDefault;
   *ExitKey=KB_NO;

   while (1>0)
     {
       ClearRect(X,Y,CurFontHeight,CurFontWidth*2);
       OutNoiseShift(X,Y,EdValue);
       DrawLine(X+CurFontWidth,Y+CurFontHeight-1,X+2*CurFontWidth-2,Y+CurFontHeight-1);
       Redraw();
       ch=WaitReadKeyWithDelay(BREAKDELAY, KB_TIMEOUT);
       switch (ch) {
       case KB_TIMEOUT:
       case KB_ESC    : { *ExitKey=ch; EndInp=1; ReturnValue=Value; break;}
       case KB_ENTER  : { *ExitKey=ch; EndInp=1; ReturnValue=EdValue; break;}
       case KB_DOWN   : {if (EdValue>stchNCHShiftMin) EdValue--; else
                           EdValue=stchNCHShiftMax;
                         break;}
       case KB_UP     : {if (EdValue<stchNCHShiftMax) EdValue++; else
                           EdValue=stchNCHShiftMin;
                         break;}
       }
       if (EndInp) break;
   }
   OutNoiseShift(X,Y,ReturnValue);
   return ReturnValue;
}

#define fak 0.001047197551  //Pi / 3000
#ifndef ccm1
  #define npXk 8
  #define npYk 5
#else
  #define npXk 9
  #define npYk 9
#endif

static void DrawArrow(unsigned int x, unsigned int y, float angle, float len){
int nUrca;

    nUrca = ((float)angle * 100) * 30 / 6;

    DrawLine(x, y,
      x+((float)(npXk*len+1)*sin(nUrca * fak)),y-((float)(npYk*len + 1) * cos(nUrca * fak)));
}

void OutTimeAnalog(unsigned int x, unsigned int y){
extern struct StDateTime DateTime;
float Min, Hour;

  Min=((float)DateTime.Min/5);
  Hour=((float)(DateTime.Hour%12))+((float)DateTime.Min/60);

  OutPicture(x, y, AnalogIco);
  x+=GetPicWidth(AnalogIco)/2;
  y+=GetPicHeight(AnalogIco)/2;
  DrawArrow(x,y,Min,1);
  DrawArrow(x,y,Hour,0.5);
}


void GetStartAndLen(char ROM *Str, char *Start, char *End){
char i=0;
  *Start=fStrLen(Str)-1;
  *End=0;
  while(Str[i]==0x20)i++;
  if(Str[i]!=0){
    *Start=i;
    i=fStrLen(Str);
    while((i>0)&&(Str[i-1]==0x20))i--;
    if(i==0)*End=*Start; else
     *End=i-1;
  }
}

void InpOnOffValue(unsigned int x, unsigned int y,char *CurValue,char ROM* Str1,char ROM* Str2, unsigned int *ExitKey)
{
  char Cur=*CurValue, w=max(fStrLen(Str1),fStrLen(Str2))*CurFontWidth, Start, End;
  char ROM *Var;

  while (1) {
    ClearRect(x,y,CurFontHeight,w);
    if(Cur) Var=Str2;
      else Var=Str1;
    GetStartAndLen(Var, &Start, &End);
    OutStringCenter(x,y,w,Var);
    DrawLine(x+(w-CurFontWidth*fStrLen(Var))/2+Start*CurFontWidth,y+CurFontHeight-1,
             x+(w-CurFontWidth*fStrLen(Var))/2+(End+1)*CurFontWidth-1,y+CurFontHeight-1);
    Redraw();
    *ExitKey = WaitReadKeyWithDelay(BREAKDELAY, KB_TIMEOUT);
    if ((*ExitKey==KB_UP)||(*ExitKey==KB_DOWN))
      if (Cur) Cur=0; else Cur=1;

    if ((*ExitKey==KB_ESC)||(*ExitKey==KB_ENTER)||(*ExitKey==KB_TIMEOUT))break;
  }

  if (*ExitKey==KB_ENTER) *CurValue=Cur;
}

void InputScrl(struct ScrollerMngr *Scrl, KEY *ExitKey){
KEY key=KB_NO;
char State=GetCurScrl(Scrl);
char w=0,i,f=CurFontPtrNumer, Start, End;
char ROM *Var;
 
 SetFont(Scrl->Font);
 for(i=0;i<Scrl->NumberStr-1;i++)
   w=max(w,fStrLen(Scrl->String[i])); 
 w*=CurFontWidth;

 while(1){
   ClearRect(Scrl->X,Scrl->Y,CurFontHeight,w);
   Var=Scrl->String[GetCurScrl(Scrl)-1];
   GetStartAndLen(Var, &Start, &End);
   OutStringCenter(Scrl->X,Scrl->Y,w,Var);
   DrawLine(Scrl->X+(w-CurFontWidth*fStrLen(Var))/2+Start*CurFontWidth,Scrl->Y+CurFontHeight-1,
            Scrl->X+(w-CurFontWidth*fStrLen(Var))/2+(End+1)*CurFontWidth-1,Scrl->Y+CurFontHeight-1);
   Redraw();

   key=WaitReadKeyWithDelay(BREAKDELAY, KB_TIMEOUT);
   DoScrlNoWait(Scrl, key);

   if((key==KB_TIMEOUT)||(key==KB_ENTER)||(key==KB_ESC)) {*ExitKey=key;break;}
 }

 if(key!=KB_ENTER) {
   SetCurScrl(Scrl,State);
   RedrawScrl(Scrl);
 }
 SetFont(f);
}


void Battery(unsigned int x,unsigned int y,float Val)
{
int i,j;

j=Round((float)Val/10);

DrawRect(x,y,x+22,y+10);
DrawRect(x+23,y+2,x+24,y+8);
for (i=0;i<j;i++)
    DrawLine(x+2+i*2,y+2,x+2+i*2,y+8);
if (j<10)
   ClearRect(x+2+j*2,y+2,7,20-j*2);
}


static void DrawLUCorner(int x,int y)
{
DrawPoint(x+1,y+4);
DrawPoint(x+1,y+3);
DrawPoint(x+2,y+2);
DrawPoint(x+3,y+1);
DrawPoint(x+4,y+1);
}

static void DrawLDCorner(int x,int y)
{
DrawPoint(x+1,y-4);
DrawPoint(x+1,y-3);
DrawPoint(x+2,y-2);
DrawPoint(x+3,y-1);
DrawPoint(x+4,y-1);
}

static void DrawRDCorner(int x,int y)
{
DrawPoint(x-1,y-4);
DrawPoint(x-1,y-3);
DrawPoint(x-2,y-2);
DrawPoint(x-3,y-1);
DrawPoint(x-4,y-1);
}

static void DrawRUCorner(int x,int y)
{
DrawPoint(x-1,y+4);
DrawPoint(x-1,y+3);
DrawPoint(x-2,y+2);
DrawPoint(x-3,y+1);
DrawPoint(x-4,y+1);
}

void DrawButton(int x,int y, char ROM *Text)
{
if (Text[0]!=0) {
	OutString(x+28-fStrLen(Text)*CurFontWidth/2,y,Text);
	DrawLine(x+2,y-2,x+56,y-2);
	DrawLine(x-3,y+15,x+51,y+15);
	DrawLine(x-3,y+3,x-3,y+15);
	DrawLine(x+56,y-2,x+56,y+10);
	DrawLUCorner(x-3,y-2);
	DrawRDCorner(x+56,y+15);

	DrawLine(x-1,y+17,x+53,y+17);
	DrawLine(x+58,y-0,x+58,y+12);
	DrawRDCorner(x+58,y+17);
	DrawPoint(x-1,y+16);
	DrawPoint(x+57,y-0);
}
}


void DrawFrame(int x1,int y1,int x2,int y2)
{
DrawLine(x1+5,y1,x2-5,y1);
DrawLine(x1+5,y2,x2-5,y2);
DrawLine(x1,y1+5,x1,y2-5);
DrawLine(x2,y1+5,x2,y2-5);
DrawLUCorner(x1,y1);
DrawRUCorner(x2,y1);
DrawLDCorner(x1,y2);
DrawRDCorner(x2,y2);
}

void DrawLongButton(int x, int y, char ROM *Text)
{
if (Text[0]!=0) {
	OutString(x+28+24-fStrLen(Text)*4,y,Text);
	DrawLine(x+2,y-2,x+56+48,y-2);
	DrawLine(x-3,y+15,x+51+48,y+15);
	DrawLine(x-3,y+3,x-3,y+15);
	DrawLine(x+56+48,y-2,x+56+48,y+10);
	DrawLUCorner(x-3,y-2);
	DrawRDCorner(x+56+48,y+15);

	DrawLine(x-1,y+17,x+53+48,y+17);
	DrawLine(x+58+48,y-0,x+58+48,y+12);
	DrawRDCorner(x+58+48,y+17);
	DrawPoint(x-1,y+16);
	DrawPoint(x+57+48,y-0);
}
}



char InpString(unsigned int X,unsigned int Y,char *Str,char StrLength,char Font,char FullTable)
{
#define MaxStrInp 30
char CurPos,Length,TmpStr[MaxStrInp+1],i,n;
KEY ch;
struct InputFrame IF;

   if (StrLength>MaxStrInp) Length=MaxStrInp;
   else Length=StrLength;
   for (i=0;i<Length;i++) TmpStr[i]=32;
   for (i=0;i<Length;i++)
       if (Str[i]==0) break;
       else TmpStr[i]=Str[i];
   TmpStr[Length]=0;

   CurPos=Length-1;
   SetFont(Font);
   n=TmpStr[0];
   while (1){
       OutString1(X,Y,TmpStr);
       DrawLine(X+(Length-CurPos-1)*CurFontWidth,Y+CurFontHeight-1,X+(Length-CurPos)*CurFontWidth-1,Y+CurFontHeight-1);
       Redraw();
       ch=WaitReadKeyWithDelay(BREAKDELAY, KB_TIMEOUT);

       switch (ch) {
       case KB_TIMEOUT:
       case KB_ESC:{ return 0;}
       case KB_ENTER : {// for (i=Length;i>0;i--)
                        //     if (TmpStr[i-1]==32) TmpStr[i-1]=0;
                        //     else break;
                         trim(TmpStr);
                         for (i=0;i<Length;i++)
                             Str[i]=TmpStr[i];
                         Str[Length]=0;
                         return 1;
                       }
       case KB_LEFT  : {CurPos++;
                        if (CurPos==Length) CurPos=0;
                        n=TmpStr[Length-1-CurPos];
                        break;}
       case KB_RIGHT : {
                        if (CurPos==0) {CurPos=Length-1;} else CurPos--;
                        n=TmpStr[Length-1-CurPos];
                        break;}
       case  KB_UP    : {
                         if (FullTable) {
                            if (n==255) n=1;
                            else n++;
                         } else {
                            if (n==255) {n=32; }
                            else {
                                n++;
                                if (n==33) n=48;
                                if (n==58) n=65;
                                if (n==91) n=97;
                                if (n==123) n=192;
                            }
                         }
                         TmpStr[Length-1-CurPos]=n;
                         break;
                        }
       case  KB_DOWN  : {
                         if (FullTable) {
                            if (n==1) n=255;
                            else n--;
                         } else {
                             n--;
                             if (n==31) n=255;
                             if (n==47) n=32;
                             if (n==64) n=57;
                             if (n==96) n=90;
                             if (n==191) n=122;
                           }
                         TmpStr[Length-1-CurPos]=n;
                         break;
                        }
       case  KB_F1  : {
                         InitInputFrame(&IF,24,48,frTypeDigitsAndARSymbols, 0,TmpStr,Length,Font,X,Y);
                         RunInputFrame(&IF);
                         break;
                       }
     }
   }
}



char InputName(char ROM* Title, char MaxLength, char* String)
{
int x1,x2,l;
char r;

#ifdef __saverestorelcd
SaveLCD();
#endif

l=fStrLen(Title);
if (MaxLength>l) l=MaxLength;
if (l<14) l=14;
x1=160-l*4-20;
x2=160+l*4+20;


SetFont(Font8x16);
ClearRect(x1,100-2,48,x2-x1+1);
DrawFrame(x1,100-2,x2,144+2);
DrawFrame(x1+2,100,x2-2,144);
OutString(160-fStrLen(Title)*4,102,Title);
DrawFrame(x1+2,100,x2-2,118);

r=InpString(160-MaxLength*4,124,String,MaxLength,Font8x16,0);

#ifdef __saverestorelcd
RestoreLCD();
#endif

return r;
}

char* TruncStr(char *Str,unsigned int Width){
unsigned int W;
char s[tmpStrLen],s1[tmpStrLen];
extern unsigned char CurFontWidth;

  strcpy(s, Str);
  W = strlen(s)*CurFontWidth;

     if ((W > Width)&&(s[0] > 0)) {

       do{
         s[strlen(s)-1]=0;
         strcpy(s1, s);
         strcat(s1, "...");

         W = strlen(s1)*CurFontWidth;
         if (s[0] == 0) {strcpy(OutStr, s);return OutStr;}
         if (W <= Width) {strcpy(OutStr, s1);return OutStr;}
         if (strlen(s) == 1) break;
       }while(1);

       strcat(s,"...");
       do{
         s[strlen(s)-1]=0;
         W = strlen(s)*CurFontWidth;

         if ((s[0] == 0) ||
            (W <= Width)) break;
       }while(1);
     }

     strcpy(OutStr, s);
     return OutStr;
}


unsigned int SmallMessageBox(char ROM *Caption, char ROM * Text1, char ROM * Text2,char ROM * Text3, char Btn1,char Btn2)
{
int x1,x2,key;
unsigned int l,top,yh,y;

char d=RedrawEnabled;

if (!d)EnableLCD();

l=fStrLen(Text1)>fStrLen(Text2) ? fStrLen(Text1) : fStrLen(Text2);
l=l>fStrLen(Text3) ? l : fStrLen(Text3);
l=l>fStrLen(Caption) ? l : fStrLen(Caption);

yh=50;
if (Caption[0] ==0 ) yh-=18;
if ((Btn1==0) && (Btn2==0)) yh-=22;
if (Text1[0]   !=0 ) yh+=18;
if (Text2[0]   !=0 ) yh+=18;
if (Text3[0]   !=0 ) yh+=18;
top=(240-yh)/2;

if (l<30) l=30;
x1=160-l*4-20;
x2=160+l*4+20;

SetFont(Font8x16);
ClearRect(x1,top-2,yh+4,x2-x1+1);
DrawFrame(x1,top-2,x2,top+yh+2);
DrawFrame(x1+2,top,x2-2,top+yh);

y=top+6;
if (Caption[0]!=0)
  {
  OutStringCenter(x1, top+2, (x2-x1), Caption);
  DrawFrame(x1+2,top,x2-2,top+18);
  y+=18;
  }

if (Text1[0]!=0) {OutString(160-fStrLen(Text1)*4,y,Text1);y+=18;}
if (Text2[0]!=0) {OutString(160-fStrLen(Text2)*4,y,Text2);y+=18;}
if (Text3[0]!=0) {OutString(160-fStrLen(Text3)*4,y,Text3);y+=18;}

if ((Btn1!=0) && (Btn2!=0))
  {
  DrawLongButton(50, y+4, EntBtn);
  DrawLongButton(170,y+4, CnclBtn);
  }
else if ((Btn1!=0) || (Btn2!=0))
  {
  if (Btn1!=0) DrawLongButton(110,y+4, EntBtn);
  if (Btn2!=0) DrawLongButton(110,y+4, CnclBtn);
  }
Redraw();

while (1) {
	key=WaitReadKeyWithDelay(BREAKDELAY, KB_TIMEOUT);

        if ((key==KB_TIMEOUT)||(key==KB_ENTER)||(key==KB_ESC))break;
}
if(!d)DisableLCD(chMinus);

return key;
}

char* trim(char*s){
char i=strlen(s);

  if(i){
    while((i)&&(s[--i]==32))s[i]=0;

    while(s[0]==32)memmove(s,s+1,strlen(s));
  }

  return s;
}

