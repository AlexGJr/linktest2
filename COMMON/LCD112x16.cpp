#include "Graph.h"
#include "BoardAdd.h"
#include "KeyBoard.h"
#include "DefKb.h"
#include "LCD.h"
#ifdef SAM7SE
#include "SPI_SE.h"
#else
#include "SPI.h"
#endif


#define SPIRegime 3

volatile char RedrawEnabled;

unsigned char Video_Ram[VideoRamLen];
char LCDConnected=1;

#ifndef __emulator


void SetLightOff(void)
{
}


void SetLightOn(void)
{
}


//***************************************
static char SPI_Rotate(char data)
{

int i,c;

c=0;
for (i=0;i<8;i++)
  if (data&(1<<i))
      c|=1<<(7-i);
return c;

}



static unsigned int SPI_WriteRead2(unsigned int data)
{
int c=0;

#ifdef __TIM3
 while (AT91F_PIO_IsInputSet(AT91C_BASE_PIOB,AT91C_PIO_PB21)) {
#else   
 while (AT91F_PIO_IsInputSet(AT91C_BASE_PIOA,AT91C_PIO_PA31)) {
#endif   
     DelayMks(1);
     c++;
     if (c>800) {
        LCDConnected=0;
        return 0;
     }
  }
DelayMks(5);
return SPI_WriteRead(data);
}


char WinToDOS(char c)
{
  if ((c>=192)&&(c<=239)) return c-64;
  else if ((c>=240)&&(c<=255)) return c-16;
       else return c;
}


//  ������������� LCD
void InitLCD(void)
{
#define InitLCDDeldy 5000

#ifdef TDM
  AT91F_PIO_CfgInput(AT91C_BASE_PIOA, AT91C_PIO_PA8);
  if (AT91F_PIO_IsInputSet(AT91C_BASE_PIOA,AT91C_PIO_PA8)) return;
  else LCDConnected=1;
#endif

#ifdef __TIM3
  if (AT91F_PIO_IsInputSet(AT91C_BASE_PIOB,AT91C_PIO_PB25)) return;
  else LCDConnected=1;
#endif

SPI_Prepare(SPIFREQ_1000,SPIRegime);

SPI_SetCS(SPI_CS_MOSI);

//while (1) {
//Initialize Display
DelayMks(InitLCDDeldy);
SPI_WriteRead2(SPI_Rotate(0x1B));
DelayMks(InitLCDDeldy);
SPI_WriteRead2(SPI_Rotate(0x40));
DelayMks(InitLCDDeldy);

//Display Clear
SPI_WriteRead(SPI_Rotate(0x0C));
DelayMks(InitLCDDeldy);
SPI_WriteRead(SPI_Rotate(0x0C));
DelayMks(InitLCDDeldy);

//Cursor set to 0,0
SPI_WriteRead2(SPI_Rotate(0x1F));
SPI_WriteRead2(SPI_Rotate(0x24));
SPI_WriteRead2(0x00);
SPI_WriteRead2(0x00);
SPI_WriteRead2(0x00);
SPI_WriteRead2(0x00);

//Font Magnifiend Display
SPI_WriteRead2(SPI_Rotate(0x1F));
SPI_WriteRead2(SPI_Rotate(0x28));
SPI_WriteRead2(SPI_Rotate(0x67));
SPI_WriteRead2(SPI_Rotate(0x40));
SPI_WriteRead2(SPI_Rotate(0x01));
SPI_WriteRead2(SPI_Rotate(0x01));

//Brightness Level Set (1-8)
SPI_WriteRead2(SPI_Rotate(0x1F));
SPI_WriteRead2(SPI_Rotate(0x58));
SPI_WriteRead2(SPI_Rotate(0x03));

//Set international Font
SPI_WriteRead2(SPI_Rotate(0x1B));
SPI_WriteRead2(SPI_Rotate(0x52));
SPI_WriteRead2(SPI_Rotate(0x01));

//Set Code Page 866
SPI_WriteRead2(SPI_Rotate(0x1B));
SPI_WriteRead2(SPI_Rotate(0x74));
SPI_WriteRead2(SPI_Rotate(0x11));

SPI_ClearCS();
RedrawEnabled=1;

}



// ----------------------------------------
void RedrawInline(void)
{
int i;

#ifdef TDM
  if (AT91F_PIO_IsInputSet(AT91C_BASE_PIOA,AT91C_PIO_PA8)) LCDConnected=0;
  else if(!LCDConnected) InitLCD();
#endif
#ifdef __TIM3
  if (AT91F_PIO_IsInputSet(AT91C_BASE_PIOB,AT91C_PIO_PB25)) LCDConnected=0;
  else if(!LCDConnected) InitLCD();
#endif

if (LCDConnected) {
   RedrawEnabled=0;
   SPI_Prepare(SPIFREQ_1000,SPIRegime);
   SPI_SetCS(SPI_CS_MOSI);
   SPI_WriteRead2(SPI_Rotate(0x01));

   //Cursor set to 0,0
   SPI_WriteRead2(SPI_Rotate(0x1F));
   SPI_WriteRead2(SPI_Rotate(0x24));
   SPI_WriteRead2(0x00);
   SPI_WriteRead2(0x00);
   SPI_WriteRead2(0x00);
   SPI_WriteRead2(0x00);

   SPI_WriteRead2(SPI_Rotate(0x1F));
   SPI_WriteRead2(SPI_Rotate(0x28));
   SPI_WriteRead2(SPI_Rotate(0x66));
   SPI_WriteRead2(SPI_Rotate(0x11));
   //���� �� �
   SPI_WriteRead2(SPI_Rotate(ScreenWidth));
   SPI_WriteRead2(0x00);
   //����� �� Y
   SPI_WriteRead2(SPI_Rotate(0x02));
   SPI_WriteRead2(0x00);
   //����� ����������� ��� ��� ���
   SPI_WriteRead2(SPI_Rotate(0x01));

   for (i=0;i<VideoRamLen/2;i++) {
      SPI_WriteRead2(Video_Ram[i]);
      SPI_WriteRead2(Video_Ram[VideoRamLen/2+i]);
   }


   SPI_ClearCS();
   RedrawEnabled=1;
}

}
//************************


#else
unsigned int VideoBuf;

void InitLCD(void)
{
int i;
for (i=0; i<VideoRamLen; i++) {Video_Ram[i]=0;}
VideoBuf=(unsigned int)&Video_Ram[0];
}
/*
void Redraw(void)
{
extern int NeeedRedraw;
NeeedRedraw=1;
}
*/
#endif


/*
inline void DisableLCD(char Symbol)
{
RedrawEnabled=0;
}


inline void EnableLCD()
{
RedrawEnabled=1;
}
*/

void ClearLCD(void)
{
unsigned int i;
for (i=0; i<VideoRamLenDiv4; i++) {
  ((unsigned int *)Video_Ram)[i]=0;
}
}







//----------------------------------------------------
//������� ��������
//���������� X(����� ������� ����)
//���������� Y(����� ������� ����)
//*Picture  - ��������� �� ��������
//----------------------------------------------------
void OutPicture(unsigned int X,unsigned int Y, char const *ptr)
{
     char PicWidth,PicHeight, CountLines, FinishMask;
     char Yshift, i, j, b, b1, b2, sh1, sh2;
     int SkipByte, CurByte;
     int SkipByte1;

     PicHeight=ptr[0];
     PicWidth=ptr[1];

     CurByte=2;

     FinishMask=(PicHeight&0x07);
     CountLines=PicHeight>>3;
     Yshift=Y&0x07;
   if (FinishMask) { if (CountLines)CountLines++;
        if (Yshift+FinishMask>8) { b1=0x00; b2=0xFF<<(FinishMask+Yshift)-8;}
       else {b1=0xFF<<FinishMask+Yshift; b2=0xFF;}
   } else { b1=0; b2=0;}
   if (Yshift) {
     sh1=(0xFF>>(8-Yshift));
     sh2=(0xFF<<(Yshift));}
   else {
     sh1=0;
     sh2=0xFF;}
     if ((CountLines==0)&(FinishMask))
   {
    CountLines++;
    if ((Yshift+FinishMask)>8) { sh2=0xFF<<((Yshift+FinishMask)-8);}
    else {sh1|=0xFF<<(FinishMask+Yshift);
          sh2=0xFF;}
    b2=0x00; b1=0x00;//FF>>FinishMask;
   }
//     SkipByte=((Y&0xF8)<<4)+X;
     SkipByte=((Y&0xF8)*(unsigned int)ScreenWidthDiv8)+X;
     SkipByte1=SkipByte;
     for (i=CountLines; i>0; i--)
     {
          for (j=PicWidth; j>0; j--)
        {
         b=ptr[CurByte++];
           if (i==1) {
             Video_Ram[SkipByte]=(Video_Ram[SkipByte]&( sh1|b1 ))|(b<<Yshift);
             if (SkipByte+ScreenWidth<VideoRamLen)
                Video_Ram[SkipByte+ScreenWidth]=(Video_Ram[SkipByte+ScreenWidth]&( sh2|b2 ))|(b>>(8-Yshift));
           }  else {
             Video_Ram[SkipByte]=(Video_Ram[SkipByte]&( sh1 ))|(b<<Yshift);
             if (SkipByte+ScreenWidth<VideoRamLen)
                Video_Ram[SkipByte+ScreenWidth]=(Video_Ram[SkipByte+ScreenWidth]&( sh2 ))|(b>>(8-Yshift));
           }
           SkipByte++;
           }
    SkipByte=SkipByte1+ScreenWidth;
    SkipByte1=SkipByte;
     }
}

void PutPixel(unsigned int X, unsigned int Y)
{
Video_Ram[((Y&0xF8)*(unsigned int)ScreenWidthDiv8)+X] |= (1<<(Y&0x07));
}



void OutChar(unsigned int X,unsigned int Y,char S,char Mask)
{
int SkipByte;
unsigned char j, Yshift, b, msk;

   SkipByte=((Y&0xF8)*(unsigned int)ScreenWidthDiv8)+X;
   if (SkipByte>=VideoRamLen) return;

   Yshift=Y&0x07;

   msk=0xFF>>(8-CurFontHeight);
   Mask &= msk;

   for (j=0; j<CurFontWidth; j++)
     {
     b=CurFontPtr(S,j);
     if (Yshift)
        {
        if (SkipByte<VideoRamLen)
           Video_Ram[SkipByte]=(Video_Ram[SkipByte] ) ^ ((b^Mask)<<Yshift);
        if (SkipByte+ScreenWidth<VideoRamLen)
           Video_Ram[SkipByte+ScreenWidth]=(Video_Ram[SkipByte+ScreenWidth] ) ^ ((b^Mask)>>(8-Yshift));
        }
     else
        { if (SkipByte<VideoRamLen) Video_Ram[SkipByte]=(Video_Ram[SkipByte] ) ^ (b^Mask);}
     SkipByte+=1;
     }
}


void OutStringMask(unsigned int X,unsigned int Y,char const *String, char Mask)
{
char i=0;
while (String[i]!=0) {i++;}
ClearRect(X,Y,CurFontHeight,i*CurFontWidth);
i=0;
while (String[i]!=0) {OutChar(X+CurFontWidth*i,Y,String[i],Mask); i++;}
}



//---------- ----------
void ClearRect(unsigned int X,unsigned int Y,unsigned int PicHeight,unsigned int PicWidth)
{
unsigned int i0,j0;
int SkipByte;

for (i0=0;i0<PicHeight;i0++)
  for (j0=0;j0<PicWidth;j0++)
    {
    SkipByte=(((Y+i0)&0xF8)*(unsigned int)ScreenWidthDiv8)+(X+j0);
    if (SkipByte<VideoRamLen)  Video_Ram[SkipByte]&=~(1<<((Y+i0)&0x07));
    }
}




void TestLCD(void)
{

ClearLCD();
SetFont(Font8x8);
OutString(0,0,"Test");
SetFont(Font8x6);
OutString(40,0,"Test");
SetFont(Font6x5);
OutString(80,2,"Test");
Redraw();
WaitReadKey();
}
