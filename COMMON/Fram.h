#ifndef fram_h
#define fram_h

// FM25CL64
// FM25L256

extern short Buf[512];

void InitFram(void);
unsigned int WriteFram(unsigned int addr, void *Buf, unsigned int Count);
unsigned int ReadFram(unsigned int addr, void *Buf, unsigned int Count);


void TestFram(void);

#endif
