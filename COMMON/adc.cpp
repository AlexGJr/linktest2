// Include Standard files
#include "BoardAdd.h"
#include "adc.h"




void StartReadADC(char Channels)
{

AT91F_ADC_SoftReset (AT91C_BASE_ADC);										// acm, AT91SAM7S256 internal ADC

// 1��� Clock, 80 ��� Start, 8��� Hold
// 1 MHz Clock, 80 ms Start, 8mks Hold
AT91F_ADC_CfgModeReg (AT91C_BASE_ADC, (23<<8) | (9<<16) | (7<<24));
AT91F_ADC_EnableChannel (AT91C_BASE_ADC,Channels);

AT91F_ADC_StartConversion (AT91C_BASE_ADC);

}


int IsADCReaded(char Channels, unsigned short* Data)
{
unsigned int i;
AT91_REG* DataReg;

if ((AT91F_ADC_GetStatus(AT91C_BASE_ADC) & Channels)!=Channels)
  return 0;

// ������ - �������� � ������
i=1;
DataReg=AT91C_ADC_CDR0;
while (i<=0x80) {
  if (Channels & i) {
    *Data=(unsigned short)(*DataReg);
    Data++;
  }
  i<<=1;
  DataReg++;
}
return 1;
}



// ������ ������ ��� 0..7. ���������� � Data ������ ��, ��� �������� � ����� Channels
// reads the ADC channels 0 .. 7. Adds the Data only those that are included in the mask Channels
void ReadADC(char Channels, unsigned short* Data)
{

StartReadADC(Channels);

// acm, delay 8 us guarantee conversion done, polling SR may trigger errata problems?
DelayMks(8);

while (!IsADCReaded(Channels,Data)) {};

}



void TestADC(void)
{
unsigned short Data[8];

while (1) {
  ReadADC(0x8F , Data);
  DelayMks(100);
}

}




