#include "BoardAdd.h"
#include "spi.h"
#include "LCD.h"
#include "keyboard.h"
#include "DefKB.h"
#include <string.h>
#include <stdlib.h>


#ifndef __emulator

volatile unsigned int SPIBusy=0;

void SPI_SetCS(unsigned int CS)
{
if (SPIBusy) SPI_ClearCS();
SPIBusy=1;
//Clear All CS
AT91F_PIO_SetOutput(AT91C_BASE_PIOA, SPI_CSx(SPI_CS0,SPI_CS1,SPI_CS2,SPI_CS3));  // acm, a little confusing, single register write configures 4 outputs
//Set CS
AT91F_PIO_CfgOutput(AT91C_BASE_PIOA, CS);
AT91F_PIO_ClearOutput(AT91C_BASE_PIOA, CS);
}


void SPI_ClearCS(void)
{
SPIBusy=0;
AT91F_PIO_CfgOutput(AT91C_BASE_PIOA, SPI_CSx(SPI_CS0,SPI_CS1,SPI_CS2,SPI_CS3));
AT91F_PIO_SetOutput(AT91C_BASE_PIOA, SPI_CSx(SPI_CS0,SPI_CS1,SPI_CS2,SPI_CS3));
}


//#define CS0Value (0x0E<<16)
#define CS0Value 0
void SPI_Prepare(unsigned int kHz, unsigned int Mode)
{
char v;
unsigned int aMode;

if ((Mode==2) || (Mode==3)) aMode=AT91C_SPI_CPOL;
   else aMode=0;
if ((Mode==0) || (Mode==2)) aMode|=AT91C_SPI_NCPHA;

AT91F_SPI_Disable(AT91C_BASE_SPI);
AT91F_SPI_Reset(AT91C_BASE_SPI);
DelayMks(0x01);  // allen did this 10-21-08, Atmel told us to do double reset to fix rev. B problem, SPI gone crazy
AT91F_SPI_Reset(AT91C_BASE_SPI);

AT91F_SPI_CfgMode(AT91C_BASE_SPI,AT91C_SPI_MSTR | AT91C_SPI_MODFDIS | CS0Value);
AT91F_SPI_CfgCs(AT91C_BASE_SPI,0,AT91C_SPI_CSAAT | aMode | kHz);
v=SPI_Read();
v=v;
AT91F_SPI_Enable(AT91C_BASE_SPI);
}


char SPI_Read(void)
{
return (AT91C_BASE_SPI->SPI_RDR);
}


void SPI_Write(char Value)
{
AT91C_BASE_SPI->SPI_TDR=Value | CS0Value;
while(!(AT91C_BASE_SPI->SPI_SR & AT91C_SPI_TDRE));
}


char SPI_WriteRead(char Value)
{

SPI_Read();
SPI_Write(Value);
while(!(AT91C_BASE_SPI->SPI_SR & AT91C_SPI_RDRF));
//DelayMks(SPITimeOut);
return SPI_Read();
}




void TestSPI(void)
{

 DisableLCD(0xFF);
 SPI_Prepare(SPIFREQ_2000,0);

 //while (1)
 {

 SPI_SetCS(SPI_CS_ALTERA);
 SPI_WriteRead(0x60);
 SPI_WriteRead(0x01);
// SPI_ClearCS();
 }

 while (1) {
 SPI_SetCS(SPI_CS_ALTERA);
 SPI_WriteRead(0x70);
 SPI_WriteRead(0x00);
 SPI_WriteRead(0x80);
 SPI_WriteRead(0x26);
 SPI_ClearCS();
 }
}

#endif


