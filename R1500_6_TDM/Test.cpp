
#ifndef TestOFF

#include "LCD.h"
#include "Graph.h"
#include "KeyBoard.h"
#include "Defs.h"
#include "DefKB.h"
#include "RAM.h"
#include "Error.h"
#include "DrvFlash.h"
#include "Flash.h"
#include "RTC.h"
#include "DAC.h"
#include "Utils.h"
#include "ADCDevice.h"
#include "CalcParam.h"
#include "SetChannels.h"
#include <string.h>
#include "DateTime.h"
#include "BoardAdd.h"
#include "SysUtil.h"
#ifndef __emulator
#include "ADC.h"
#include "Timer.h"
#endif
#include "DACDevice.h"
#include "ADCDevice.h"
#include "FRAM.h"
#include "CalcParam.h"	// acm
#include "Complex.h"	// acm
extern void ImpPhaseCalc(s16 *a, s16 *x, int NPoints, float *Freq,float *Ampl, float *Faza, float *Ampl1,char Page0,char Page1);
extern float AmplCoeff[MaxTransformerSide][3];
extern void ReSetNullLines(int aReadCount, char Side);

#define scrWidth  ScreenWidth
#define scrHalf   (char)ScreenWidth/2
#define scrHeight ScreenHeight

//����� ������� �����
#define  ww          20
//���� ���� err
#define  err         80
//���� ���� ��
#define  ok          40

ROM char StrLCD[]=   "TEST LCD";

ROM char StrKBD[]=   "TEST KBD";
ROM char KB_NOStr[]=  "NO   ";
ROM char KB_ESCStr[]=  "ESC  ";
ROM char KB_UPStr[]=   "UP   ";
ROM char KB_LEFTStr[]= "LEFT ";
ROM char KB_RIGHTStr[]="RIGHT";
ROM char KB_DOWNStr[]= "DOWN ";
ROM char KB_ENTERStr[]="ENTER";
ROM char KB_MEMStr[]=  "MEM  ";
ROM char KB_MODStr[]=  "MOD  ";


ROM char StrRAM[]=   "TEST RAM";
ROM char StrPages[]="Page";
ROM char StrFrom[]="from";
ROM char StrRead[]= "Reading... ";
ROM char StrWrite[]="Writing... ";
ROM char StrBadPage[]="Last bad page: ";
ROM char StrBadAddr[]="Last bad addr: ";
ROM char StrBadValu[]="Bad value:     ";
ROM char StrOK[]="OK";
ROM char StrERR[]="ERROR!";
ROM char StrSKIP[]="Skip";

ROM char StrTESTCOEFF[]="TEST Ampl Coeff";
ROM char StrTESTCOEFFRead[]="Reading...";
ROM char StrTESTCOEFFOk[]="-OK";
ROM char StrTESTCOEFFFail[]="-ER";


ROM char StrFLASH[]= "TEST FLASH";
ROM char TestFlashStr5[]="0xAA  ...";
ROM char TestFlashStr6[]="0x55  ...";
ROM char TestFlashStr7[]="CLDS  ...";
ROM char TestFlashStr8[]="good";
ROM char TestFlashStr9[]="Error in page";

ROM char StrRTC[]=   "TEST RTC";
ROM char StrDAC[]=   "TEST CALIBR DAC";
ROM char StrTEMPTEST[]=  "TEMPERATURE TEST";
ROM char StrINTEMPTEST[]="BOARD TEMPERATURE";
ROM char StrHUMTEST[]=   "HUMIDITY TEST";
ROM char StrANALOGTEST[]=  "INPUT TEST";
ROM char StrANALOGTEST1[]=  "Gain";

ROM char LEDStr[]=       "TEST LED and RELAY";
ROM char LED1Str[]=      "LED1";
ROM char LED2Str[]=      "LED2";
ROM char LEDGreenStr[]=  "Green";
ROM char LEDRedStr[]=    "Red";
ROM char LEDYellowStr[]= "Yellow";
ROM char Relay1Str[]=    "Relay1";
ROM char Relay2Str[]=    "Relay2";
ROM char Relay3Str[]=    "Relay3";
ROM char Relay4Str[]=    "Relay4";
ROM char RelayTStr[]=    "RelayT";
ROM char RelayOnStr[]=   "On";
ROM char RelayOffStr[]=  "Off";

ROM char StrTESTFRAM1[]=  "FRAM Test";
ROM char StrTESTFRAM2[]=  "FRAM Test Ok";
ROM char StrTESTFRAM3[]=  "FRAM Test Error";

ROM char GammaChStr[]=   "GAMMA CHANNEL";
ROM char GammaChStr1[]=  "OUT-";
ROM char GammaChStr2[]=  "PhaseA";
ROM char GammaChStr3[]=  "PhaseB";
ROM char GammaChStr4[]=  "PhaseC";
ROM char GammaChStr5[]=  "Unn   ";
ROM char GammaChStr6[]=  "Gain-";
ROM char GammaChStr7[]=  "1 ";
ROM char GammaChStr8[]=  "4 ";
ROM char GammaChStr9[]=  "16";
ROM char GammaChStr10[]= "64";
ROM char GammaChStr11[]= "HV";
ROM char GammaChStr12[]= "LV";

static char Wait2(int b){
int i;
ReadKey();
for (i=0;i<b;i++)
  {
#ifndef __emulator
    Delay(100);
#endif
  if (ReadKey()==KB_ESC) return 1;
  }
return 0;;
}

/*
`
  LCD TEST

*/
static char LCD_TEST(void){
int j,k;

SetFont(Font6x8);
ClearLCD();
Redraw();

OutString(0,0,StrLCD);
Redraw();
if (Wait2(ww)==1)
  {
  ClearLCD();
  Redraw();
  return 2;
  }

for (j=0;j<112;j++){
  DrawLine(j,0,j,15);
  Redraw();
  if (ReadKey()!=KB_NO){return 2;}
  }
if (Wait2(20)==1) return 2;
ClearLCD();
Redraw();

for (j=0;j<16;j++){
  DrawLine(0,j,111,j);
  Redraw();
  if (ReadKey()!=KB_NO){return 2;}
  }

if (Wait2(20)==1) return 2;
ClearLCD();
Redraw();

for (k=1;k<=2;k++){
  ClearLCD();
  Redraw();
  for (j=0;j<16;j+=8){
    SetFont(k);
    OutInt(j*2,j,j,2);
    Redraw();
    if (ReadKey()!=KB_NO){return 2;}
    }
  if (Wait2(10)==1) {return 2;}
  }

ClearLCD();
Redraw();
for (j=0;j<112;j+=2){
  DrawLine(j,0,j,15);
  Redraw();
  if (ReadKey()!=KB_NO){return 2;}
  }

if (Wait2(10)==1) {return 2;}
ClearLCD();
Redraw();
for (j=0;j<16;j+=2){
  DrawLine(0,j,111,j);
  Redraw();
  if (ReadKey()!=KB_NO){return 2;}
  }
if (Wait2(10)==1) {return 2;}
ClearLCD();
Redraw();
for (j=0;j<111;j+=2){
  DrawLine(j,0,j,15);
  Redraw();
  if (ReadKey()!=KB_NO){return 2;}
  }
for (j=0;j<16;j+=2){
  DrawLine(0,j,111,j);
  Redraw();
  if (ReadKey()!=KB_NO){return 2;}
  }

if (Wait2(10)==1) {return 2;}
SetFont(Font6x8);
ClearLCD();
Redraw();
OutString(0,0,StrLCD);
OutString(9*6,0,StrOK);
Redraw();

Wait2(ok);
return 1;
}



static void RunKeyBoardTest(void)
{

KEY ch=KB_NO;

SetFont(Font6x8);
ClearLCD();
OutString(0,0,StrKBD);
SetFont(Font8x8);
while (ch!=KB_ESC) {
   switch (ch) {
   case KB_NO     :{OutString(0,8,KB_NOStr); break;}
   case KB_ESC    :{OutString(0,8,KB_ESCStr); break;}
   case KB_UP     :{OutString(0,8,KB_UPStr); break;}
   case KB_LEFT   :{OutString(0,8,KB_LEFTStr); break;}
   case KB_RIGHT  :{OutString(0,8,KB_RIGHTStr); break;}
   case KB_DOWN   :{OutString(0,8,KB_DOWNStr); break;}
   case KB_ENTER  :{OutString(0,8,KB_ENTERStr); break;}
   case KB_MEM    :{OutString(0,8,KB_MEMStr); break;}
   case KB_MOD    :{OutString(0,8,KB_MODStr); break;}
   }
   Redraw();
   ch=WaitReadKey();
}
}




/*
char RAM_TEST(void){

#define KBDivisor 0x03FF
char n, Ok;
int i;
int *p;


Ok=1;
p=((int*)0x8000);


ClearLCD();
SetFont(Font6x8);
OutString(0,0,StrRAM);
Redraw();
if (Wait2(ww)==1) return 2;

ClearLCD();
OutString(0,0,StrPages);
OutString(8*6,0,StrFrom);
OutInt(12*6,0,RAMPages,3);

OutString(0,8,StrWrite);
for (n=0;n<RAMPages;n++) {
  SetPage(n);
  OutInt(11*6,8,n,2);
  OutInt(5*6,0,GetPage()+1,2);
  Redraw();

  for (i=0;i<0x7FFF;i++)
  {
    *((char*)p+i)=n;
    if ((i&KBDivisor==0)&&(ReadKey()!=KB_NO)){
      ClearLCD();
      Redraw();
      return 2;
    }
  }

}

OutString(0,8,StrRead);
//OutInt(11*6,8,0,1);
//Redraw();
for (n=0;n<RAMPages;n++){
  SetPage(n);
  OutInt(11*6,8,n,2);
  OutInt(5*6,0,GetPage()+1,2);
  Redraw();
  for (i=0;i<0x7FFF;i++){
    if ((i&KBDivisor==0)&&(ReadKey()!=KB_NO)){
      ClearLCD();
      Redraw();
      return 2;
      }
    if (*((char*)p+i)!=n){
      Ok=0;
      OutString(1,0,StrBadPage);
      OutInt(15*6,0,n,2);
      OutString(1,8,StrBadAddr);
      OutInt(15*6,8,i,5);
//      OutString(1,40,StrBadValu);
//      OutInt(15*6,40,*((char*)p+i),3);
//      OutString(9*6,1,StrERR);
      Redraw();
      Wait2(err);
      ClearLCD();
      Redraw();
      return Ok;
      }
    }
  }

OutString(0,8,StrWrite);
OutInt(11*6,8,0xAA,3);
//Redraw();
for (n=0;n<RAMPages;n++){
  SetPage(n);
  OutInt(5*6,0,GetPage()+1,2);
  Redraw();
  for (i=0;i<0x7FFF;i++){
    if ((i&KBDivisor==0)&&(ReadKey()!=KB_NO)){
      ClearLCD();
      Redraw();
      return 2;
      }
    *((char*)p+i)=0xAA;
    }
  }

OutString(0,8,StrRead);
OutInt(11*6,8,0xAA,3);
//Redraw();
for (n=0;n<RAMPages;n++){
  SetPage(n);
  OutInt(5*6,0,GetPage()+1,2);
  Redraw();
  for (i=0;i<0x7FFF;i++){
    if ((i&KBDivisor==0)&&(ReadKey()!=KB_NO)) {
      ClearLCD();
      Redraw();
      return 2;
      }
    if (*((char*)p+i)!=0xAA){
      Ok=0;
      OutString(1,0,StrBadPage);
      OutInt(15*6,0,n,2);
      OutString(1,8,StrBadAddr);
      OutInt(15*6,8,i,5);
//      OutString(1,40,StrBadValu);
//      OutInt(15*6,40,*((char*)p+i),3);
//      OutString(9*6,1,StrERR);
      Redraw();
      Wait2(err);
      ClearLCD();
      Redraw();
      return Ok;
      }
    }
  }

OutString(0,8,StrWrite);
OutInt(11*6,8,0x55,3);
//Redraw();
for (n=0;n<RAMPages;n++){
  SetPage(n);
  OutInt(5*6,0,GetPage()+1,2);
  Redraw();
  for (i=0;i<0x7FFF;i++){
    if ((i&KBDivisor==0)&&(ReadKey()!=KB_NO)){
      ClearLCD();
      Redraw();
      return 2;
      }
    *((char*)p+i)=0x55;
    }
  }

OutString(0,8,StrRead);
OutInt(11*6,8,0x55,3);
//Redraw();
for (n=0;n<RAMPages;n++){
  SetPage(n);
  OutInt(5*6,0,GetPage()+1,2);
  Redraw();
  for (i=0;i<0x7FFF;i++){
    if ((i&KBDivisor==0)&&(ReadKey()!=KB_NO)){
      ClearLCD();
      Redraw();
      return 2;
      }
    if (*((char*)p+i)!=0x55){
      Ok=0;
      OutString(1,0,StrBadPage);
      OutInt(15*6,0,n,2);
      OutString(1,8,StrBadAddr);
      OutInt(15*6,8,i,5);
//      OutString(1,40,StrBadValu);
//      OutInt(15*6,40,*((char*)p+i),3);
//      OutString(9*6,1,StrERR);
      Redraw();
      Wait2(err);
      ClearLCD();
      Redraw();
      return Ok;
      }
    }
  }

OutString(0,8,StrWrite);
OutInt(10*6,8,0,1);
OutInt(12*6,8,0x7FFF,5);
//Redraw();
for (n=0;n<RAMPages;n++){
  SetPage(n);
  OutInt(5*6,0,GetPage()+1,2);
  Redraw();
  for (i=0;i<0x3FFF;i++){
    if ((i&KBDivisor==0)&&(ReadKey()!=KB_NO)){
      ClearLCD();
      Redraw();
      return 2;
      }
    *((int*)p+i)=i;
    }
  }

OutString(0,8,StrRead);
OutInt(10*6,8,0,1);
OutInt(12*6,8,0x7FFF,5);
//Redraw();
for (n=0;n<RAMPages;n++){
  SetPage(n);
  OutInt(5*6,0,GetPage()+1,2);
  Redraw();
  for (i=0;i<0x3FFF;i++){
    if ((i&KBDivisor==0)&&(ReadKey()!=KB_NO)){
      ClearLCD();
      Redraw();
      return 2;
      }
    if (*((int*)p+i)!=i){
      Ok=0;
      OutString(1,0,StrBadPage);
      OutInt(15*6,0,n,2);
      OutString(1,8,StrBadAddr);
      OutInt(15*6,8,i,5);
//      OutString(1,40,StrBadValu);
//      OutInt(15*6,40,*((int*)p+i),5);
//      OutString(9*6,1,StrERR);
      Redraw();
      Wait2(err);
      ClearLCD();
      Redraw();
      return Ok;
      }
    }
  }

ClearLCD();
OutString(0,0,StrRAM);
if (Ok) OutString(6*9,0,StrOK);
else OutString(6*9,0,StrERR);
Redraw();
Wait2(ok);
return Ok;
}
*/

static char RTC_TEST(void)
{

SetFont(Font6x8);

ClearLCD();
Redraw();

OutString(0,0,StrRTC);
Redraw();
if (Wait2(ww)==1) return 2;

DateTime.Hour=23;
DateTime.Min=59;
DateTime.Sec=55;
//DateTime.DSec=0;
DateTime.Day=31;
DateTime.Month=12;
DateTime.Year=3;
//TimeoutTest=0;
SET_TIME_FROMBUF();
while ((DateTime.Sec>=55)||(DateTime.Sec<=5)){
  if (ReadKey()!=KB_NO){
    ClearLCD();
    Redraw();
    return 2;
    }
  OutDate(0,8);
  OutTime(56,8);
  Redraw();
  GET_TIME_INBUF();
  }

OutString(9*6,0,StrOK);
Redraw();
Wait2(ok);
ClearLCD();
Redraw();
return 1;
}



/*

  FLASH TEST

*/
/*
ROM char FLASH_TEST_WRITE_STR[] = "Wr(00000/00000)";
ROM char FLASH_TEST_READ_STR[]  = "Rd";
unsigned int FLASH_TEST_SYM(char x,char y,char *buf,char Sym)
{
extern unsigned int equByteClasterFat;
unsigned int i,j,equCount,equByte;
unsigned int w=0,r=0,c=0;

for (i=0;i<FatBlockCount/FatBlocksInClaster;i++)
  FlashClearBlock(i,0);

equCount = FatBlockCount;
equByte  = FatBlockLen;

if ((x!=0xFF) && (y!=0xFF))
  {
  OutString(x,y,FLASH_TEST_WRITE_STR);
  OutInt(x+CurFontWidth*9,y,equCount-1,5);
  }
memset(buf,Sym,equByte);
for (j=0;j<equCount;j++)
  {
  if (FlashWriteBlock(j,buf,0,equByte)!=equByte){
//   OutString1(x+CurFontWidth*6,0,"WrErr!");
//   Redraw();
//   WaitReadKey();
//   OutString1(x+CurFontWidth*6,0,"      ");
//   Redraw();
   OutInt(0,0,++w,3);
   Redraw();
  }
  if ((x!=0xFF) && (y!=0xFF))
    { OutInt(x+CurFontWidth*3,y,j,5); Redraw();}
  if (ReadKey()==KB_ESC) return 0xFFFF;
  }

if ((x!=0xFF) && (y!=0xFF))
  {
  OutString(x,y,FLASH_TEST_READ_STR);
  }
for (j=0;j<equCount;j++)
  {
  memset(buf,0,equByte);
  if (FlashReadBlock(j,buf,0,equByte)!=equByte){
//   OutString1(x+CurFontWidth*6,0,"RdErr!");
//   Redraw();
//   WaitReadKey();
//   OutString1(x+CurFontWidth*6,0,"      ");
//   Redraw();
   OutInt(4*CurFontWidth,0,++r,3);
   Redraw();
  } else {
  if ((x!=0xFF) && (y!=0xFF))
     {OutInt(x+CurFontWidth*3,y,j,5); Redraw();}
  for (i=0;i<equByte;i++) {
    if (buf[i]!=Sym)
    {
//      OutString1(x+CurFontWidth*6,0,"C.Err!");
//      Redraw();
//      WaitReadKey();
//      OutString1(x+CurFontWidth*6,0,"      ");
//      Redraw();
      OutInt(8*CurFontWidth,0,++c,3);
      Redraw();
      break;
      }
  }
  }
  if (ReadKey()==KB_ESC) return 0xFFFF;
  }
return 0xEFFF;
}


unsigned int FLASH_TEST_KALEIDOSCOPE(char x,char y,char *buf)
{
extern unsigned int equByteClasterFat;
unsigned int i,j,equCount,equByte;

for (i=0;i<FatBlockCount/FatBlocksInClaster;i++)
  FlashClearBlock(i,0);

equCount = FatBlockCount;
equByte  = FatBlockLen;


if ((x!=0xFF) && (y!=0xFF))
  {
  OutString(x,y,FLASH_TEST_WRITE_STR);
  OutInt(x+CurFontWidth*9,y,equCount-1,5);
  }
for (j=0;j<equCount;j++)
  {
  for (i=0;i<equByte;i++) {buf[i]=(char)(j+i);}
  FlashWriteBlock(j,buf,0,equByte);
  if ((x!=0xFF) && (y!=0xFF))
     {if (j%100==0) {OutInt(x+CurFontWidth*3,y,j,5); Redraw();}}
  if (ReadKey()==KB_ESC) return 0xFFFF;
  }
  OutInt(x+CurFontWidth*3,y,equCount-1,5);
  Redraw();
if ((x!=0xFF) && (y!=0xFF))
  {
  OutString(x,y,FLASH_TEST_READ_STR);
  }
for (j=0;j<equCount;j++)
  {
  for (i=0;i<equByte;i++) {buf[i]=0;}

  FlashReadBlock(j,buf,0,equByte);

  for (i=0;i<equByte;i++)
    {
    if (buf[i]!=(char)(j+i)) {
       return j;
    }
    }
  if ((x!=0xFF) && (y!=0xFF))
     {
     if (j%100==0) { OutInt(x+CurFontWidth*3,y,j,5); Redraw();}
     }
  if (ReadKey()==KB_ESC) return 0xFFFF;
  }
OutInt(x+CurFontWidth*3,y,equCount-1,5); Redraw();


return 0xEFFF;
}

ROM char BadBlocks[] = "Bad blocks =";

char TestFlash(void)
{
char flag=1;
int page;

SetFont(Font6x8);
ClearLCD();
OutString(0,0,StrFLASH);
Redraw();
if (Wait2(ww)==1) return 2;

OutString(0,8,BadBlocks);
OutInt(fStrLen(BadBlocks)*CurFontWidth,8,FlashBadBlockCount(),4);
Redraw();
WaitReadKeyWithDelay(3,KB_NO);

if ((Error&&((unsigned long)1<<erFlashReadFail))||(Error&&((unsigned long)1<<erFlashWriteFail))) {OutString1(0,0,"Fat Error!"); Redraw(); WaitReadKey(); return 0;}
  {
  SetPage(1);
  if (flag==1)
    {
    OutString(0,8,TestFlashStr7); Redraw();
    page=FLASH_TEST_KALEIDOSCOPE(4*6,8,(char *)0x8000);

    if (page==0xFFFF) return 2;
    else
    if (page!=0xEFFF) flag=0;
    }

  }

ClearLCD();
OutString(0,0,StrFLASH);
if (flag==1) OutString(11*6,0,StrOK);
else { OutString(11*6,0,StrERR); OutInt(0,16,page,6);}
Redraw();
//Wait2(ok);

WaitReadKey();

return flag;
}
*/

ROM char FLASH_TEST_WRITE_STR[] = "Wr(00000/00000)";
ROM char FLASH_TEST_READ_STR[]  = "Rd";
ROM char FLASH_TEST_ERR_STR[] = " 0/ 0/ 0";

static unsigned int FLASH_TEST_KALEIDOSCOPE(char x,char y,char *buf)
{
extern unsigned int equByteClasterFat;
unsigned int i,j,equCount,equByte;
static unsigned int we=0,re=0,ce=0;

for (i=0;i<FatBlockCount/FatBlocksInClaster;i++)
  FlashClearBlock(i,1);
//  FlashClearBlock(i,0);

equCount = FatBlockCount;
equByte  = FatBlockLen;


if ((x!=0xFF) && (y!=0xFF))
  {
  OutString(x,y,FLASH_TEST_WRITE_STR);
  OutString(scrWidth-fStrLen(FLASH_TEST_ERR_STR)*CurFontWidth,0,FLASH_TEST_ERR_STR);
  OutInt(x+CurFontWidth*9,y,equCount-1,5);
  }
for (j=0;j<equCount;j++)
  {
  for (i=0;i<equByte;i++) {buf[i]=(char)(j+i);}
  if (FlashWriteBlock(j,buf,0,equByte)!=equByte)we++;

  if (j%100==0) {
     if ((x!=0xFF) && (y!=0xFF))
        {OutInt(x+CurFontWidth*3,y,j,5); OutInt(scrWidth-fStrLen(FLASH_TEST_ERR_STR)*CurFontWidth,0,we,2); Redraw();}
     if (ReadKey()==KB_ESC) return 0xFFFF;
  }
  }
if ((x!=0xFF) && (y!=0xFF))
  {
  OutString(x,y,FLASH_TEST_READ_STR);
  }
for (j=0;j<equCount;j++)
{
  for (i=0;i<equByte;i++) {buf[i]=0;}

  if(FlashReadBlock(j,buf,0,equByte)!=equByte)re++; else {

    for (i=0;i<equByte;i++)
      {
      if (buf[i]!=(char)(j+i)) 
      {
        ce++;
        break;
      }
      }
  }

  if (j%100==0) {
  if ((x!=0xFF) && (y!=0xFF))
     {
     OutInt(x+CurFontWidth*3,y,j,5);
     OutInt(scrWidth-(fStrLen(FLASH_TEST_ERR_STR)-3)*CurFontWidth,0,re,2);
//     OutInt(scrWidth-(fStrLen(FLASH_TEST_ERR_STR)-6)*CurFontWidth,0,ce,2);
     Redraw();
     }
  }
  if (ReadKey()==KB_ESC) return 0xFFFF;
}


return 0xEFFF;
}

ROM char BadBlocks[] = "Bad blocks =";

static char TestFlash(void)
{
char flag=1;
int page;

SetFont(Font6x8);
ClearLCD();
OutString(0,0,StrFLASH);
Redraw();
if (Wait2(ww)==1) return 2;

OutString(0,8,BadBlocks);
OutInt(fStrLen(BadBlocks)*CurFontWidth,8,FlashBadBlockCount(),4);
Redraw();
WaitReadKeyWithDelay(3,KB_NO);

if ((Error&&((unsigned long)1<<erFlashReadFail))||(Error&&((unsigned long)1<<erFlashWriteFail))) {OutString1(0,0,"Fat Error!"); Redraw(); WaitReadKey(); return 0;}
  {
  SetPage(0);
/*
  if (flag==1)
    {
    OutString(0,8,TestFlashStr5);  Redraw();
    page=FLASH_TEST_SYM(4*6,8,(char *)0x8000,0xAA);
    if (page==0xFFFF) return 2;
    else
    if (page!=0xEFFF) flag=0;
    }
  if (flag==1)
    {
    OutString(0,8,TestFlashStr6); Redraw();
    page=FLASH_TEST_SYM(4*6,8,(char *)0x8000,0x55);
    if (page==0xFFFF) return 2;
    else
    if (page!=0xEFFF) flag=0;
    }
*/
  if (flag==1)
    {
    OutString(0,8,TestFlashStr7); Redraw();
    page=FLASH_TEST_KALEIDOSCOPE(4*6,8,(char *)BaseAddrSig1[0]);

    if (page==0xFFFF) return 2;
    else
    if (page!=0xEFFF) flag=0;
    }

  }

ClearLCD();
OutString(0,0,StrFLASH);
if (flag==1) OutString(11*6,0,StrOK);
else OutString(11*6,0,StrERR);
Redraw();
Wait2(ok);
return flag;
}


#define Relay1On
#define Relay1Off
#define Relay2On  SetRelayWarning
#define Relay2Off SetRelayNormal
#define Relay3On  SetRelayAlarm
#define Relay3Off SetRelayNormal
#define Relay4On
#define Relay4Off
#define RelayTOn
#define RelayTOff

static char LED_RELAY_TEST(void)
{ unsigned int c,n=0,l1=0,r=0;

AllLEDOff;
RelayAllOff;
SetFont(Font6x8);
ClearLCD();
OutString(0,0,LEDStr);
Redraw();
if (Wait2(ww)==1) return 2;


c=ReadKey();
while (c!=KB_ESC) {
   ClearLCD();
   if (n==0) {
      OutString(0,0,LED1Str);
      switch (l1) {
         case 0:
           OutString(30,0,RelayOffStr);
           AllLEDOff;
           break;
         case 1:
           OutString(30,0,LEDRedStr);
           LED1Red;
           break;
         case 2:
           OutString(30,0,LEDGreenStr);
           LED1Green;
           break;
         case 3:
           OutString(30,0,LEDYellowStr);
           LED1Yellow;
           break;
      }
   }
/*
   else
   if (n==1) {
      OutString(0,0,LED2Str);
      switch (l2) {
         case 0:
           OutString(30,0,RelayOffStr);
           LED2Off;
           break;
         case 1:
           OutString(30,0,LEDRedStr);
           LED2Red;
           break;
         case 2:
           OutString(30,0,LEDGreenStr);
           LED2Green;
           break;
         case 3:
           OutString(30,0,LEDYellowStr);
           LED2Yellow;
           break;
      }
   } else
*/
   if (n==1) {
      OutString(0,0,Relay1Str);
      if (r&(1<<0)) {
         OutString(42,0,RelayOnStr);
         Relay1On;
      } else {
         OutString(42,0,RelayOffStr);
         Relay1Off;
      }
   } else
   if (n==2) {
      OutString(0,0,Relay2Str);
      if (r&(1<<1)) {
         OutString(42,0,RelayOnStr);
         Relay2On;
      } else {
         OutString(42,0,RelayOffStr);
         Relay2Off;
      }
   } else
   if (n==3) {
      OutString(0,0,Relay3Str);
      if (r&(1<<2)) {
         OutString(42,0,RelayOnStr);
         Relay3On;
      } else {
         OutString(42,0,RelayOffStr);
         Relay3Off;
      }
   } else
/*
   if (n==3) {
      OutString(0,0,Relay4Str);
      if (r&(1<<3)) {
         OutString(42,0,RelayOnStr);
         Relay4On;
      } else {
         OutString(42,0,RelayOffStr);
         Relay4Off;
      }
   } else
*/
   if (n==4) {
      OutString(0,0,RelayTStr);
      if (r&(1<<3)) {
         OutString(42,0,RelayOnStr);
         RelayTOn;
      } else {
         OutString(42,0,RelayOffStr);
         RelayTOff;
      }
   }

   Redraw();

   c=WaitReadKey();
   if (c==KB_UP) {
      n++;
     if (n>4) n=0;
   }
   if (c==KB_DOWN) {
      if (n>0) n--; else n=4;
   }
   if (c==KB_RIGHT) {
      if (n==0) {
         l1++;
         if (l1>3) l1=0;
      }
      if (n) {
         if (r&(1<<(n-1))) r&=~(1<<(n-1));
         else r|=(1<<(n-1));
      }
   }
   if (c==KB_LEFT) {
      if (n==0) {
         if (l1>0) l1--;
         else l1=3;
      }
      if (n) {
         if (r&(1<<(n-1))) r&=~(1<<(n-1));
         else r|=(1<<(n-1));
      }
   }


}
AllLEDOff;
RelayAllOff;
return 1;
}


static char CALIBRDAC_TEST(void)
{ unsigned int i;
  unsigned short Cmnd[8];

SetFont(Font6x8);

ClearLCD();
Redraw();

OutString(0,0,StrDAC);
Redraw();
if (Wait2(ww)==1) return 2;

#ifndef __emulator
i=0;
while (1) {
 SetCalibrDAC(i);
 Delay(5);
// ReadADC(0x08,&Cmnd);

// acm, 2-11-11, this diag hangs, verified on 2 boards.  Not a big deal, so can't spend much time on this.
// verified hang occurs within ReadADC(), because 0xFF <> read back internal ADC status 0x7c.  Observe, lower 2 bits
// map to ADC's that are never used, those PIO pins defined as Alarm outputs.  Believe 0xFF is channel mask
// indicating which ADC's to test, so thought is for sure not to read lower 2 bits.  That leaves MSB, which is signal
// ACH4, why doesn't that conversion finish?  Could be related to eratta?  Changing mask seems to allow test to
// proceed further.  Reset already done at beginning, try waiting xus to conversion done?  ADC set for 10 bit resolution mode.
// clock sel = 0x17-> 48e6/(24)*2)=1mHz
// sample & hold time = 7/1Mhz = 7usec (spec is 1usec max rate)
// 10 clocks/conv?
// added wait of 8 usec, that fixed diag hang.  Don't have anymore time to devote researching this.
 
 // Note, there is also an external ADC, IRQ/busy pin routed to AT91C_PIO_PA21 which is configured input but not used.
 // My guess is AT91SAM7 ADC only used for calibrate/diag.
 
 // ReadADC(0xFF,&Cmnd[0]);
 ReadADC(0xFC,&Cmnd[0]);  // acm, don't check unused ADC's

 OutInt(0,8,i,4);
 OutInt(60,8,Cmnd[3],4);
 Redraw();
 Delay(40);
 i++;
 if (i>1023) break;
 if (ReadKey()!=KB_NO){
    ClearLCD();
    Redraw();
    return 2;
 }
}

#endif

OutString(16*6,0,StrOK);
Redraw();
Wait2(ok);
ClearLCD();
Redraw();
return 1;
}

extern float CalcAmpl(s16 *a, int ToX); //acm, resolves Understand
extern float RunCalibrAmpl(char Side, float *AmplCoeff);
extern void DoRead(char Mesurement, char SetCurrent);

extern ROM float GainValue[MainADCChannels];
extern unsigned int IsCurrentGain;
extern unsigned int CurrentGain;

static char ANALOG_TEST(void)		// acm, refactor input current diagnostic into Set1 input current as current logic has been depopulated on recent builds, so this test was pointless.
{ float f;
  KEY ch;

  //char i,Result=1;
  //float CoeffB,CoeffC;  //2.06 ag commented, not used
  struct TComp tmpComplex3,tmpComplex4; //2.06 ag //struct TComp tmpComplex1,tmpComplex2,tmp,tmpComplex3,tmpComplex4,ResultComplex;
  //float ACShift,ABShift;  //2.06 ag commented, not used
  float *Freq, float_temp; 
  char TrSide=0;  
// for reference, float GainValue[MainADCChannels]={1.0,2.0,4.0,8.0}
//float GValue[MainADCChannels]={1.0,2.0,2.0,2.0};  //2.06 ag commented, not used // acm, debug, seem to have a CPLD or amp problem...
  
  Freq=&float_temp; // acm, do this to use ImPhaseCalc()

	  //tmpComplex1.im=tmpComplex1.re=tmpComplex2.im=tmpComplex2.re=tmpComplex3.im=tmpComplex3.re=0;
	  //CoeffB=0; //2.06 ag
	  //CoeffC=0; //2.06 ag
	
SetFont(Font6x5);

ClearLCD();
Redraw();

OutString(0,0,StrANALOGTEST);
Redraw();
if (Wait2(ww)==1) return 2;

AddrSig[0]=(short*)BaseAddrSig1[0];
AddrSig[1]=(short*)BaseAddrSig2[0];
AddrSig[2]=(short*)BaseAddrSig3[0];
AddrSig[3]=(short*)BaseAddrSig4[0];

RunCalibrAmpl(TrSide, AmplCoeff[TrSide]);  // acm, needs to be here, messes up if in while(1) (changes switch input?)
SetInputChannel(0,chSourcePhases);		// acm, modify test, seems useful to measure input currents, presuming input signal/simulator connected

//if (IsCurrentGain) {					// acm, Main() early reads CPLD, sets bit???
   OutString(72,0,StrANALOGTEST1);
//   SetCurrentGain2;  // acm, this pertained to current input
   CurrentGain=1;
//}

*Freq=0;//60; // acm, >5 bypasses CalcFreq()
PrepareRead(rdMeasure);  // acm, move up here, only needs to be done once
//PrepareRead(rdTest); // noworky

while (1) {
//      PrepareRead(rdMeasure);
/* acm, this fills sample input array with ADC current input channels 
      Ch[0]=5;
      Ch[1]=6;
      Ch[2]=7;
*/
 //     StartRead();
 //     while (EndRead);

 //     ReCalcNullLines(UpLoadCount,0,0);

	  
AmplifIndex[TrSide][BushingCh]=CurrentGain;
//ReadData(0,1,chReadAllChannels,0,SetNullLine,0);  // acm, 1 param sets gain, otherwise it is left alone
SetGain(1,AmplifIndex[TrSide][BushingCh]);			// acm, do this manually, so to cycle through gains 1,2,4,8 (x4 and x8 don't work, why?)
//why?Delay(100);
Delay(1);
DoRead(rdMeasure,0);
//DoRead(rdTest,0); noworky
ReSetNullLines(ReadCount,TrSide);

tmpComplex3.re=0;// acm, force recalc of amplitude

ImpPhaseCalc((s16 *)(AddrSig[0]),
			 (s16 *)(AddrSig[1]),
			 UpLoadCount,
			 Freq,
			 &tmpComplex4.re,&tmpComplex4.im,&tmpComplex3.re,GammaPage0,GammaPage1); 
//      SetPage(GammaPage0);
      f = tmpComplex3.re;
      //f=f*ADCStep/1000.0*0.707;						// acm, sin wave, peak to RMS: mult by 1/sqrt(2)=.707 ( pp to RMS, mult by 1/(2*sqrt(2))=.353)  
      f*= ADCStep * 0.7071 / 28.8*AmplCoeff[TrSide][0];  // 28.8 is nominal input resistance, higher if any jumpers cut
      f/=GainValue[CurrentGain]; 
      OutFloat(0,8,f,4,1);								// acm, this displays mA*10

//      SetPage(GammaPage1);
      f = tmpComplex4.re;
      f*= ADCStep * 0.7071 / 28.8*AmplCoeff[TrSide][1];
      f/=GainValue[CurrentGain];
      OutFloat(30,8,f,4,1);

	  //      SetPage(GammaPage2);
	  tmpComplex3.re=0;// acm, force recalc of amplitude
	  ImpPhaseCalc((s16 *)(AddrSig[0]),
				   (s16 *)(AddrSig[2]),
				   UpLoadCount,
				   Freq,
				   &tmpComplex4.re,&tmpComplex4.im,&tmpComplex3.re,GammaPage0,GammaPage1); 
      f = tmpComplex4.re;
      f*= ADCStep * 0.7071 / 28.8*AmplCoeff[TrSide][2];
      f/=GainValue[CurrentGain];
      OutFloat(60,8,f,4,2);
	  OutFloat(90,8,*Freq,4,1);

 //     if (IsCurrentGain) {
         OutInt(100,0,GainValue[CurrentGain],2);
 //     }
      Redraw();
      ch=ReadKey();
      if (ch==KB_ESC) break;
//      if (IsCurrentGain) {
          if ((ch==KB_LEFT)||(ch==KB_RIGHT)||(ch==KB_UP)) {
  //            if (CurrentGain==10) {
                 //SetCurrentGain2;
 //                CurrentGain=2;
 //             } else {
                 //SetCurrentGain10;
                 CurrentGain++;CurrentGain%=4;					// acm, modify, cycle through all 4 gains, 1,2,4,8.  ReadData() sets gain.
  //            }
   //       }
      }

}
return 0;
}



static char TEST_COEFF(void)
{ char i,c;
  float A[4],G[6];
  char Ok[4],GOk[6];


SetFont(Font6x8);
ClearLCD();
OutString(0,0,StrTESTCOEFF);
Redraw();
if (Wait2(ww)==1) return 2;
OutString(0,8,StrTESTCOEFFRead);
Redraw();

SaveCalibrSin=1;												// acm, enable DAC to generate calibration input waveform

OutString(0,0,StrTESTCOEFF);

SetInputChannel(0,chSourcePhasesCalibr);						// acm, configure inputs to read calibration DAC
for (i=0;i<4;i++) {
    SetGain(1,i);
    ReadData(0,0,chReadAllChannels,0,SetNullLine,0);
    A[i]=CalcAmpl((short*)AddrSig[0], UpLoadCount);
    A[i]+=CalcAmpl((short*)AddrSig[1], UpLoadCount);
    A[i]+=CalcAmpl((short*)AddrSig[2], UpLoadCount);
    A[i]/=3;
    if ((A[i]>5000)&&(A[i]<5800)) Ok[i]=1; else Ok[i]=0;		// acm, check if avg of 3 amplified channel inputs reading calibration DAC within "range" (null line adj.)
}
ClearLCD();
//OutString(0,0,StrTESTCOEFF);
SetFont(Font6x5);
//OutInt(0,8,(int)A[0],5);
//OutInt(29,8,(int)A[1],5);
//OutInt(58,8,(int)A[2],5);
//OutInt(87,8,(int)A[3],5);
//Redraw();
SetGain(1,0);
//SetInputChannel(0,chGamma);

SaveCalibrSin=1;
SetInputChannel(0,chGammaCalibr);								// acm, "" chA fed by cal DAC into Unn circuit
SetGain(1,0);
for (i=0;i<6;i++) {
    SetGain(4,i);
    if (i) CurShift=i*2;
    if (i==4) CurShift=1;
    if (i==5) CurShift=3;
    ReadData(0,0,chReadAllChannels,0,SetNullLine,0);
    G[i]=CalcAmpl((s16*)AddrSig[3], UpLoadCount);
    if ((G[i]>5000)&&(G[i]<5800)) GOk[i]=1; else GOk[i]=0;
//    if ((G[i]>3300)&&(G[i]<3800)) GOk[i]=1; else GOk[i]=0;

}
SetGain(1,0);
SetGain(4,0);
SetInputChannel(0,chGamma);

c=0;
for (i=0;i<4;i++) {											// acm, "", phA/phB/phC drive Unn 
    OutInt(0+(i*29),0,(1<<i),1);
    if (!Ok[i]) {
       if (c==0) {
           SetGain(1,i);
//           CurShift=i;
           SetInputChannel(0,chSourcePhasesCalibr);				// cross phase calibration?
           c=1;
       }
       OutString(5+(i*29),0,StrTESTCOEFFFail);
    } else
       OutString(5+(i*29),0,StrTESTCOEFFOk);
}

for (i=0;i<4;i++) {
    if ((i>0)&&(i<4)) OutInt(0+(i*29)-6,6,1<<(i*2),2);
    if (i==0) OutInt(0,6,1,1);
    if (!GOk[i]) {
       OutString(5+(i*29),6,StrTESTCOEFFFail);
       if (c==0) {
           SetGain(4,i);
           SetInputChannel(0,chGammaCalibr);
           c=1;
       }
    } else
       OutString(5+(i*29),6,StrTESTCOEFFOk);
}

for (i=4;i<6;i++) {
    if (i==4) {
       OutInt(0,11,2,1);
       if (!GOk[i]) {
          OutString(6,11,StrTESTCOEFFFail);
          if (c==0) {
             SetGain(4,i);
             SetInputChannel(0,chGammaCalibr);
             c=1;
         }
       } else
          OutString(6,11,StrTESTCOEFFOk);
    }
    if (i==5) {
        OutInt(29,11,8,1);
        if (!GOk[i]) {
           OutString(35,11,StrTESTCOEFFFail);
           if (c==0) {
              SetGain(4,i);
              SetInputChannel(0,chGammaCalibr);
              c=1;
          }
        } else
           OutString(35,11,StrTESTCOEFFOk);
    }
}

Redraw();
while (ReadKey()!=KB_ESC) {
    ReadData(0,0,chReadAllChannels,0,SetNullLine,0);
}
SaveCalibrSin=0;
SetFont(Font6x8);
return 0;
}

static int TST(int Addr, short *Bf)
{
 int i,k=0;

 memset((void *)Bf[0], 0, 1024);
 ReadFram(Addr,Bf,1024);
 for( i=0;i<512;i++)
    if (Bf[i]!=i+2){
     k++;
    }
 return k;
}

static void TestFRAM(void){

 short *Bf;
 int i,j,k;

SetFont(Font6x8);
ClearLCD();
OutString(0,0,StrTESTFRAM1);
Redraw();
if (Wait2(ww)==1) return ;
OutString(0,8,StrTESTCOEFFRead);
Redraw();

 Bf=AddrSig[0];

 for(j=0;j<32;j++){
   for(i=0;i<512;i++)
     Bf[i]=i+2;
   WriteFram(j*1024,Bf,1024);
   k=TST(j*1024,Bf);
 }

 ClearLCD();
 if (k==0){
    OutString(0,0,StrTESTFRAM2);
 }
  else {
    OutString(0,0,StrTESTFRAM3);
    OutInt(0,8,k,4);
  }
  Redraw();

  WaitReadKey();
}


void GAMMACH_TEST(void)
{ char chn=0,gn=0,Side=0;
  KEY c=KB_NO;

SetFont(Font6x8);

ClearLCD();
OutString(0,0,GammaChStr);
Redraw();
if (Wait2(ww)==1) return ;

OutString(0,8,GammaChStr1);
OutString(68,8,GammaChStr6);

while (1) {
    switch (Side) {
      case 0:OutString(100,0,GammaChStr11);break;
      case 1:OutString(100,0,GammaChStr12);break;
    }
    switch (chn) {
      case 0:OutString(24,8,GammaChStr2);SetInputChannel(Side,chPhaseA);break;
      case 1:OutString(24,8,GammaChStr3);SetInputChannel(Side,chPhaseB);break;
      case 2:OutString(24,8,GammaChStr4);SetInputChannel(Side,chPhaseC);break;
      case 3:OutString(24,8,GammaChStr5);SetInputChannel(Side,chGamma);break;
    }
    switch (gn) {
      case 0:OutString(100,8,GammaChStr7);SetGain(4,0);break;
      case 1:OutString(100,8,GammaChStr8);SetGain(4,1);break;
      case 2:OutString(100,8,GammaChStr9);SetGain(4,2);break;
      case 3:OutString(100,8,GammaChStr10);SetGain(4,3);break;
    }
    Redraw();

    c=WaitReadKey();
    if (c==KB_UP) {
        if (gn<3) gn++; else gn=0;
    }
    if (c==KB_DOWN) {
        if (gn) gn--; else gn=3;
    }

    if (c==KB_RIGHT) {
        if (chn<3) chn++; else chn=0;
    }
    if (c==KB_LEFT) {
        if (chn) chn--; else chn=3;
    }
    if (c==KB_MOD) {
        if (Side) Side=0; else Side=1;
    }
    if (c==KB_ESC) break;
}
}

void RunTest(void)
{
LCD_TEST();
RunKeyBoardTest();
RTC_TEST();
LED_RELAY_TEST();
CALIBRDAC_TEST();
TEST_COEFF();		// acm, ???
GAMMACH_TEST();
//INTEMP_TEST();
//TEMP_TEST();
//HUM_TEST();
ANALOG_TEST();
TestFRAM();
TestFlash();

}

//void TSTFRAM(void)  //2.06 test
//{
  //TestFRAM();
//}

#endif
