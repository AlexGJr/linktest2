#include "ADCDevice.h"
#include "cListBox.h"
#include "TypesDef.h"
#include "USB.h"

#ifndef __emulator
__no_init s16 Sig[4][ReadCount];
#else
s16 Sig[4][ReadCount<<1];  // 5120
#endif

#ifndef __arm
u16 *MeasList=(u16 *)&Sig[0][0];
#endif
TCheckListBox *LB=(TCheckListBox *)&Sig[2][0];

s16* BaseAddrSig1[2]={Sig[0],Sig[0]};
s16* BaseAddrSig2[2]={Sig[1],Sig[1]};
s16* BaseAddrSig3[2]={Sig[2],Sig[2]};
s16* BaseAddrSig4[2]={Sig[3],Sig[3]};

void InitRAM()
{
#ifndef __arm
MeasList=(u16*)&Sig[0][0];
#endif
LB=(TCheckListBox*)&Sig[2][0];
AddrSig[0]=(s16*)BaseAddrSig1[0];	// 5120 elements
AddrSig[1]=(s16*)BaseAddrSig2[0];
AddrSig[2]=(s16*)BaseAddrSig3[0];
AddrSig[3]=(s16*)BaseAddrSig4[0];

}
