#ifndef _tgDelta_h
#define _tgDelta_h

#include "Defs.h"
#include "MeasurementDefs.h"

/*
//��������� ��������� tg Delta
struct stTgParameters {
       //C*100
       short CA,CB,CC;
       //tg*100
};
//12 ����
#define szTgParameters sizeof(struct stTgParameters)
*/

#pragma pack(1)

//��������� ��������� tg Delta
struct stTgParameters {
       //C*10
       unsigned short C[MaxBushingCountOnSide];
       //tg*100
       short tg[MaxBushingCountOnSide];
};
//12 ����
#define szTgParameters sizeof(struct stTgParameters)

#pragma pack()

typedef struct stTgParameters TTgParameters;

//extern __no_init __eeprom struct stTgParameters InitialTgParameters;
/*
//����������� ��������� �� tg Delta
struct stAddTgParameters {
       //����������� ������ Off-Line �������� (-70 - +185 0C) with step 1 0C
       char Temperature0;

       //C*100 ,�������
       unsigned short InputC[MaxBushingCountOnSide];

       //������������ �������� �������. ����� ���� ���������� ��� ������� �� �������� ��������
       float InputCoeff[MaxBushingCountOnSide];

       //tg*100 ,%
       short Tg0[MaxBushingCountOnSide];

       float RatedVoltage;

       //C*10 ,������
       unsigned short C0[MaxBushingCountOnSide];

       //Input Impedance *100
       unsigned short InputImpedance[MaxBushingCountOnSide];

       //����������� ������ ����� �� ������������ ��������� ��������� � �������� (-70 - +185 0C) with step 1 0C
       //Min
       char MinTemperature1;
       //Avg
       char AvgTemperature1;
       //Max
       char MaxTemperature1;

       //��������� ������� � �������� ��� ������� � ������������ (k*X+b).
       float B[MaxBushingCountOnSide];
       float K[MaxBushingCountOnSide];

       //������� ���������� �� ����� ��� ������� ������������� �������� �������. ����� ���� ���������� ��� ������� �� �������� ��������
       float InputVoltage[MaxBushingCountOnSide];

       //Delta tg*100 ,%
       unsigned short STABLEDeltaTg[MaxBushingCountOnSide];

       //Delta C*100 ,%
       unsigned short STABLEDeltaC[MaxBushingCountOnSide];

       //date STABLE test
       TDateTime STABLEDate;

       //date heat test
       TDateTime HeatDate;

       //����������� ������ STABLE �������� (-70 - +185 0C) with step 1 0C
       char STABLETemperature;

       //tg*100 ,% ��� STABLE
       short STABLETg[MaxBushingCountOnSide];

       //C*10 ,������
       unsigned short STABLEC[MaxBushingCountOnSide];

       char STABLESaved;

       char NEGtg;

       char Reserved[31];
};
//48 ����
#define szstAddTgParameters sizeof(struct stAddTgParameters)

extern __no_init __eeprom struct stAddTgParameters AddTgParameters;
*/
//void ClearInitialTgDelta(void);
//void SaveInitialTgDelta(void);
void CalcTg(float AmplA,float AmplB,float AmplC,float Gamma,float Phase,struct stTgParameters *tg,char FirstCalc);
char CalcNewTg(struct stInsulationParameters *Parameters,struct stTgParameters *OutTg, float *OutF1, float *OutF2, char StableRegime );

#endif
