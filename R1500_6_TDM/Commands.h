
#define cmndGetMeasurementCount      (unsigned int)0x5001  //�������� ���������� ������� � �������
#define cmndMeasurementHeader        (unsigned int)0x5002  //�������� ��������� ������
#define cmndMeasurementDiag          (unsigned int)0x5004  //�������� ������� �� ������
#define cmndGetDeviceSetup           (unsigned int)0x5008  //�������� ��������� �������
#define cmndSetDeviceSetup           (unsigned int)0x5010  //�������� ��������� �������
#define cmndGetDateTime              (unsigned int)0x5020  //�������� ���� � ����� �� �������
#define cmndSetDateTime              (unsigned int)0x5040  //�������� ���� � ����� � ������
#define cmndDeviceBusy               (unsigned int)0x5050  //������ �����. ���� ������� ���������
#define cmndMeasurementDataLV        (unsigned int)0x5080  //�������� ������ ���������� ������ LV
#define cmndMeasurementDataHV        (unsigned int)0x5100  //�������� ������ ���������� ������ HV
#define cmndGetInitialParam          (unsigned int)0x5200  //�������� ������ �������� ������
#define cmndLastMeasurementHeader    (unsigned int)0x5400  //�������� ��������� Online ������
#define cmndLastMeasurementDiag      (unsigned int)0x5800  //�������� Online ������� ������

#define cmndPartGetDeviceSetup       (unsigned int)0x5108  //�������� ��������� ������� (�����)
#define cmndPartSetDeviceSetup       (unsigned int)0x5110  //�������� ��������� ������� (�����)

#define cmndErrorCommand             (unsigned int)0x1001  //������. �� ������ �������.
#define cmndErrorParameter           (unsigned int)0x1002  //������. �� ������ ���������.

#define MaxPart 200

