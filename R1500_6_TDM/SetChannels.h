#ifndef _SetChannels_h
#define _SetChannels_h


#ifndef __emulator

#include "Defs.h"
#include "LCD.h"

#define RefAddr(v)            {OutToAltera(0x10,v);}
#define MuxAddr(v)            {OutToAltera(0x30,v);}
#define BalanceSwitchAddr(v)  {OutToAltera(0x20,v);}
#define GainAddr(v)           {OutToAltera(0x40,v);}

#define ExtBushing(v)
//#define ExtBushing         (*(volatile unsigned char *)0x5300)

#else
#define RefAddr(v)
#define MuxAddr(v)
#define BalanceSwitchAddr(v)
#define GainAddr(v)

#define ExtBushing(v)

#endif

#define chGamma   0
#define chPhaseA  1
#define chPhaseB  2
#define chPhaseC  3

#define chSourcePhaseA 4
#define chSourcePhases 5

//#define chCrossPhases   8
#define chAaCrossPhases 6
#define chBbCrossPhases 7
#define chCcCrossPhases 8

#define chSourcePhasesCalibr 9
#define chSourcePhaseACalibr 10

#define chGammaCalibr 11

void InitInputChannels(void);
void SetInputChannel(char TrSide, char ChannelType);
void SetGain(char Channel,char AmplifIndex);

#endif

