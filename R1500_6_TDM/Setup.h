#ifndef SetupH
#define SetupH

#include "Defs.h"
#include "MeasurementDefs.h"

#define MaxMeasurementsPerDay 50

#ifndef NoCross
#define MaxShowValues 10
#else
#define MaxShowValues 9
#endif

#pragma pack(1)

struct stMeasurementsInfo {
        char  Hour,Min;             //Date and time of a mesurement
};
#define szMeasurementsInfo sizeof(struct stMeasurementsInfo)

struct stAutoBalans {
  char Month, Year, Day, Hour;
};


#pragma pack(1)
struct stSetupInfo {
        //Device Number
        char DeviceNumber;
        //Serial baud rate
        //0-9600
        //1-38400
        //2-57600
        char BaudRate;
        //Display Scroll time
        char OutTime;
        //Variables to show on display
//ROM char FillPauseStr4[]   = LANG("Date,Time","Date,Time");
//ROM char FillPauseStr5[]   = LANG("Read time","Read time");
//ROM char FillPauseStr6[]   = LANG("Gamma value","Gamma value");
//ROM char FillPauseStr7[]   = LANG("Temperature","Temperature");
//ROM char FillPauseStr8[]   = LANG("Trend","Trend");
//ROM char FillPauseStr9[]   = LANG("Temp.coeff","Temp.coeff");
//ROM char FillPauseStr10[]  = LANG("Alarm status","Alarm status");
//ROM char FillPauseStr11[]  = LANG("Tg,Capacity","Tg,Capacity");
//ROM char FillPauseStr12[]  = LANG("Zk","Zk");
        unsigned short DisplayFlag;
        //Days to save data. ag not used 
        char SaveDays;

        //Measurements are stopped
        char Stopped;
        //0-Relay Off
        //1-Relay On
        //2-by time
        char Relay;
        //Time of Relay test output ,sec
        unsigned short TimeOfRelayAlarm;

        //rated voltage (kV)
//        float RatedVoltage;

        //type of measurement schedule :  1 sched, 0 interval
        char   ScheduleType;
        //The hours and minutes for interval schedule
        struct stMeasurementsInfo dTime;
        //The hours and minutes for schedule. 50 entries.
        struct stMeasurementsInfo MeasurementsInfo[MaxMeasurementsPerDay];

        //Synchronization type 0-internal, 1-external. Not used in the code
        //char  SyncType; redefine in 2.10 to configure allowed variation of calibration from defaults
        char  CalibTreshould; //2.10 allowed calibration variation in %. Default - 15%
        
        //Synchronization frequency *100
        unsigned short  InternalSyncFrequency;

        //ModBus protocol version
        //0-RTU
        //1-TCP
        char ModBusProtocol;

        //Read analog from registers as opposite to use on board hardware. Hardware depopulated
        char ReadAnalogFromRegister;
        //Related to turning a heater
        //short HeaterOnTemperature;
        u16 ResetCount;  //2.06 renamed from HeaterOnTemperature. Number of resets from upgrade time

        // autobalance start date and time
        struct stAutoBalans AutoBalans;

        // Enabling autobalance
        char AutoBalansActive;

        // Enabling auto test XX
        char NoLoadTestActive;

        // days to be online for autobalance to be done
        char WorkingDays;
        
        unsigned short Job1; //2 bytes of Job #
        unsigned short Job2; //2 bytes of Job #
        unsigned short LoadCurrentThresholdLevel; //2 bytes of load current threshold level
        unsigned short LoadCurrentThresholdSettings; //2 byte of load current threshold settings bit coded bit0=enable
        unsigned short ConfigSaveCount; //Config saved counter
        unsigned short SN; //Serial Number
        char reserved[9]; //DE 1-1-16 reduced by 8 bytes from 21 to 13 //DE 2-1-16 reduced by 4 bytes from 13 to 9 
//        char reserved[21];
        
};
#define szSetupInfo sizeof(struct stSetupInfo)

#define InputChannelsCount 4

#pragma pack(1)
struct stCalibrationCoeff {
   float
   TemperatureK[InputChannelsCount],
   TemperatureB[InputChannelsCount],
   CurrentK[InputChannelsCount],
   HumidityOffset[InputChannelsCount],
   HumiditySlope[InputChannelsCount],
   VoltageK[MaxTransformerSide][MaxBushingCountOnSide],
   CurrentB[InputChannelsCount],
   I4_20K,
   I4_20B;
   char CurrentChannelOnPhase[InputChannelsCount];
   char Reserved[12];
};
#define szCalibrationCoeff sizeof(struct stCalibrationCoeff)


#pragma pack(1)
struct stGammaSideSetupInfo {
// acm, actual (note doesn't correspond to Alarm Status)
		//0 bit is 1 - unused
		//1 bit is 1 - Trend to Alarm ON
		//2 bit is 1 - Temperature coefficient to Alarm ON
	
		// original comment
		//On/Off Alarm
        //0 bit is 1 - Gamma to Alarm
        //1 bit is 1 - Temperature coefficient to Alarm ON
        //2 bit is 1 - Trend to Alarm ON
        //3 bit is 1 - Tg to Alarm ON
        //4 bit is 1 - speed Tg to Alarm ON
        //5 bit is 1 - Zk to Alarm ON
        //6 bit is 1 - speed Zk to Alarm ON
        char AlarmEnable;

        //Imbalance alarm thresholds in % (Byte*0.05), i.e. 100==5%
        char GammaYellowThresholld,GammaRedThresholld;
        //Gamma Temperature Coefficient Alarm Threshold (Byte*0.002)
        char TCoefficient;
        //Gamma Trend Alarm Threshold (Byte*0.2	  Max- 5%/Yr) //acm, 4-27 change from .2 to 1
        char TrendAlarm;

       //Temperature at OffLine tests (-70 - +185 0C) with step 1 0C
       char Temperature0;

       //C*100 , sensor capacitance plus bushing C2 in microFaradas. Available via display and registers but not used at any routines.
       unsigned short InputC[MaxBushingCountOnSide];

       //This is a coefficient that can be set or read by text command, but not used in routines. 3.5 for Russia and 3.3 for US
       float InputCoeff[MaxBushingCountOnSide];

       //tg*100 ,% 
       short Tg0[MaxBushingCountOnSide];

       //C*10 ,pF
       unsigned short C0[MaxBushingCountOnSide];

       //Input Impedance *100
       unsigned short InputImpedance[MaxBushingCountOnSide];

       //The temperatures for Stable routine (-70 - +185 0C) with step 1 0C
       //Min
       char MinTemperature1;
       //Avg
       char AvgTemperature1;
       //Max
       char MaxTemperature1;

       //A change in capaciatnce and power factor at startup specific HeatTest (k*X+b). test only can be called from text command. Never used.
       float B[MaxBushingCountOnSide];
       float K[MaxBushingCountOnSide];

       //Input voltages to calculate InputCieff using real signals. No rutines associated with this functionality.
       float InputVoltage[MaxBushingCountOnSide];

       //Delta tg*100 ,%
       unsigned short STABLEDeltaTg[MaxBushingCountOnSide];

       //Delta C*100 ,%
       unsigned short STABLEDeltaC[MaxBushingCountOnSide];

       //date STABLE test
       _TDateTime STABLEDate;

       //date heat test
       _TDateTime HeatDate;

       //Temperature @ STABLE routing time (-70 - +185 0C) with step 1 0C
       char STABLETemperature;

       //tg*100 ,% at STABLE
       short STABLETg[MaxBushingCountOnSide];

       //C*10 ,pF
       unsigned short STABLEC[MaxBushingCountOnSide];

       char STABLESaved;

       //Rated voltage (kV)
       float RatedVoltage;

       //Rated current (A)
       float RatedCurrent;

       //Phase amplitude , mV @ STABLE
       unsigned short StablePhaseAmplitude[MaxBushingCountOnSide];
       //Phase source current amplitude , mA @ STABLE
       unsigned short StableSourceAmplitude[MaxBushingCountOnSide];

       //Stable Signal phase, Deg*100
       unsigned short StableSignalPhase[MaxBushingCountOnSide];
       //Stable Sorse phase, Deg*100
       unsigned short StableSourcePhase[MaxBushingCountOnSide];

       //Average Load Active %
       char STABLEAvgCurrent;

       //Alarm thresholds Tg *100
       short TgYellowThresholld,TgRedThresholld,TgVariationThresholld;
       
       // ag looks like bapancing pot settings
       char ImpedanseValue[MaxBushingCountOnSide];

       //temperature channel select, always off. Hardware does not exist
       //0 - temperature OFF
       //1 - temperature from channel 1
       //2 - temperature from channel 2
       //3 - temperature from channel 3
       char TemperatureConfig;
       
       //looks not used at any place
       char ExtImpedanseValue[MaxBushingCountOnSide];

       //Short circuit impedance

       //ag External reference for Synch. Not used anywhere
       char ExternalSync;

       char reserved[17];

};
#define szGammaSideSetupInfo sizeof(struct stGammaSideSetupInfo)

#pragma pack(1)
struct stGammaSetupInfo {

        //0 bit is 1 - Read on HV side
        //1 bit is 1 - Read on LV side
        //2 bit is 1 - Read on HV2 side
        //3 bit is 1 - Read on LV2 side
        char ReadOnSide;


        //Parameter Change (Allows New data storage, if G has changed since the last measurement on more then G*CH%)
       // char MaxParameterChange;
		// acm, comment out unused MaxParamterChange, replace with AveragingFor Gamma_X
		// created a big long note in Setup.cpp explaining subtle reason why not to just unreserve char.
		char AveragingForGamma_X; // acm, v2.00, this coefficient/100=X, used to weight Unn average.  X==.5 is normal, .33 weighted, 1 off.
		
        //Alarm Hysteresis (Releases an Alarm, if an alarming parameter drops below (PA - PA*AH%) of alarm threshold PA)
        char AlarmHysteresis;

        //allowed phase angle difference from ideal 120
        char AllowedPhaseDispersion;

        //Days to Calculate Trend (Can not be less then 10 days)
        char DaysToCalculateTrend;
        //Days to Calculate Temperature Coefficient
        char DaysToCalculateTCoefficient;
        //Averaging for Gamma
        char AveragingForGamma;

        //Time in minutes to recheck new alarm status
        char ReReadOnAlarm;

        //Min Diagnosis Gamma, days
        char MinDiagGamma;
//AddTgParam
       char NEGtg;

       struct stGammaSideSetupInfo GammaSideSetup[MaxTransformerSide];

       //Days to Calculate BASELINE
       char DaysToCalculateBASELINE;

       //load channel number to use
       char LoadChannel;

       char ReduceCoeff;  // acm, 2-13-09 Boltov fix

       char Reserved[30];
       
};
//74 Bytes
#define szGammaSetupInfo sizeof(struct stGammaSetupInfo)

struct stZkSideToSideSetupInfo {
        //Thesholds
       char ZkYellowThresholld,ZkRedThresholld;
        //Zk Trend Alarm Threshold
       char ZkTrendAlarm;

       char reserved[32];

};
#define szZkSideToSideSetupInfo sizeof(struct stZkSideToSideSetupInfo)

struct stZkSetupInfo {

        //0 bit is 1 - Calc on LV side
        //1 bit is 1 - Calc on HV2 side
        //2 bit is 1 - Calc on LV2 side
        char CalcZkOnSide;


        //Parameter Change (Allows New data storage, if G has changed since the last measurement on more then G*CH%)
        char MaxParameterChange;
        //Alarm Hysteresis (Releases an Alarm, if an alarming parameter drops below (PA - PA*AH%) of alarm threshold PA)
        char AlarmHysteresis;

        //Days to Calculate Trend (Can not be less then 10 days)
        char DaysToCalculateTrend;
        //Averaging for Zk
        char AveragingForZk;

        //Time in minutes to recheck alarm status
        char ReReadOnAlarm;
/*
       //0- �������� ��� ���� �
       //1- ��� ���� �
       //2- ��� ���� �
       char CurrentType;
       //0- �������� ��� c HVSide
       //1- ��� ���� c LVSide
       //2- ��� ���� c HV2Side
       //3- ��� ���� c LV2Side
       char CurrentSide;
*/
       //Number of days to calck and analyze Zk
       char ZkDays;

       char Empty;

       struct stZkSideToSideSetupInfo ZkSideToSideSetup[MaxCorrelationSide];

       long FaultDate;

       char Reserved[28];
};
#define szZkSetupInfo sizeof(struct stZkSetupInfo)

//Initial (balancing data) parameters
struct stInitialSideParameters {
       //Pahse shift between measuring channels Deg*100. Likely phase A and Imbalance channels
       short ChPhaseShift;

       //The amplitude of the phase signal, mV	*10 (as def. in MODBUS), but believe this is mv/100k resistor, i.e. mA
       unsigned short PhaseAmplitude[MaxBushingCountOnSide];

       //Source amplitude, mA
       unsigned short SourceAmplitude[MaxBushingCountOnSide];

       //Temperature ~(-70 - +185 0C) with step 1 0C
       char Temperature;

       //Gamma, %*0.01
       unsigned short GammaAmplitude;
       //Phase, Byte*1.50
       unsigned short GammaPhase;

       //Initial Gamma Temperature Coefficient, *0.002
       char KT;

       //Gamma Temperature Coefficient Phase, *1.5
       char KTPhase;

       //Defect Code at Balancing
       char DefectCode;		// acm, v1.79 start using this.  Bit 0, phaseB/C swap, bit 1=hi balance encountered, one char/side.  Same values stored in data record, confusing?

       //KT Saved in inishial =0; not saved any other value;
       char KTSaved;

       //phase
       unsigned short SignalPhase[MaxBushingCountOnSide];

       //phase
       unsigned short SourcePhase[MaxBushingCountOnSide];

       //LTC position
       unsigned short RPN;
       //Load
       unsigned short Current;

       char Reserved[4];
};
//27 bytes
#define szInitialSideParameters sizeof(struct stInitialSideParameters)

//Initial (balancing) parameters
struct stInitialParameters {
       //Date of measurement
       char Day;
       char Month;
       char Year;  //-2000
       //Time of measurement
       char Hour;
       char Min;

       //parameters for each set
       struct stInitialSideParameters TrSideParam[MaxTransformerSide];

       char Reserved[10];

};
//30 bytes
#define szInitialParameters sizeof(struct stInitialParameters)

//initial parameters Zk
struct stInitialZkParameters {
       //Date of measurement
       char Day;
       char Month;
       char Year;  //-2000
       //Time of measurement
       char Hour;
       char Min;

       //Side to Side parameters
       struct stSideToSideData SideToSideData[MaxCorrelationSide];

       char Reserved[8];

};
//30 Bytes
#define szInitialZkParameters sizeof(struct stInitialZkParameters)

//Base parameters to calc Zk with 10% load steps
struct stBaseLineZkTable {
    float ZkP[MaxCorrelationSide][MaxLoads][MaxBushingCountOnSide];
    u32   ZkPCount[MaxCorrelationSide][MaxLoads];
    _TDate ZkPOnDate [MaxCorrelationSide][MaxLoads];
};
#define szBaseLineZkTable sizeof(struct stBaseLineZkTable)


struct stSetup {
       struct stSetupInfo SetupInfo;
       struct stGammaSetupInfo GammaSetupInfo;
       struct stInitialParameters InitialParameters;
       struct stCalibrationCoeff CalibrationCoeff;
       struct stZkSetupInfo ZkSetupInfo;
       struct stInitialZkParameters InitialZkParameters;
       struct stBaseLineZkTable BaseLineZk;
};
#define szSetup (sizeof(struct stSetup)-szBaseLineZkTable)

#define szFullSetup sizeof(struct stSetup)

#pragma pack()

extern unsigned short SetupValid;
extern struct stSetup Setup;

void LoadSetup(char SetParam, char SetTime);
char RunSetup(void);
char SelectSide(char *CurSide);
void SetSetupParam(void);
void SaveSetup(void);
void TestSetup(void);
int GetRand(int min, int max);

void CalibrTemp(void);
void DoCalibrHumidity(void);

int TestSetupCRC(void);  // acm, 2-13-09 Boltov fix

/*
void LoadSetup(char SetParam);
void SaveSetup(void);
char RunSetup(void);
void SavePartOfSetup(unsigned int Offset, unsigned int Len);
void SetSetupParam(void);
//void LineApr(float *X,float *Y,int ArrLen,float *K,float *B);
void SaveCalibrationCoeff(void);
void CheckSetup(void);
*/

//Default values of parameter
#define stchOnDefault          0
#define stchSensDefault        10.0
#define stchPDWDefault         1
#define stchPDIntDefault       2
#define stchP0Default          5
#define stchP1Default          30
#define stchP2Default          100
#define stchP3Default          50
#define stchP4Default          500
#define stchP5Default          5000
#define stchPhaseDefault       0
//#define stchNCHShiftDefault    5
#define defNoiseOff            0
#define stchNCHShiftDefault    defNoiseOff
#define stchFilterDefault      0
#define stRVoltDefault         13.8
#define stRCurrDefault         1
#define stSchedTDefault        1
#define stDefaultHour1         4
#define stDefaultHour2         10
#define stDefaultHour3         16
#define stDefaultHour4         22
#define stDefaultdHour         6
#define stSaveModeDefault      0
#define stHiAlarmDefault       10.0
#define stSaveDaysDefault      15
#define stTMinDefault          -50
#define stTMaxDefault          1000
#define stCMinDefault          0
#define stCMaxDefault          1000
#define stUMinDefault          0
#define stUMaxDefault          1000
#define stSpeedStackDefault    18
#define stPDIStackDefault      18
#define stType4_20Default      0
#define stPSpeed0Default       1000
#define stPSpeed1Default       2000
#define stchMinNoiseDefault    0
#define stLowNChLevelDefault   0

#define HeaterTemperatureMin   -20
#define HeaterTemperatureMax   50
#endif
