#ifndef __arm
#include "Protocol.h"
#endif
//---------------------------------------------------------------------------

#include "Trans.h"
#include "graph.h"
#include <string.h>
#include "trstr.h"
#include "Balans.h"
#include "Uart.h"
#include "Keyboard.h"

//---------------------------------------------------------------------------


// Input and Output buffers

// Input and Output buffers
char *OutBuf=(char*)UartWriteBuf;
//BUF  OutBuf;

char *InBuf;

#define erNoError            0
#define erEnd               -1
#define erInvalidCommand    -2
#define erInvalidParam      -3
#define erInvalidInt        -4
#define erInvalidFloat      -5
#define erInvalidDate       -6
#define erInvalidTime       -7
#define erNoPointComma      -8
RESULT RError=erNoError;

BUFLEN CurSymbol=0;
BUFLEN CurOperator=0;
BUFLEN CurOutSymbol=0;

char CurWord[64];

// ��������� ������
long ParamLong;
int Param1;
int Param2,Param3,Param4,Param5,Param6,Param7,Param8,Param9,Param10,Param11;

extern void TestSetup(); // acm, do this so Understand resolves

typedef void (*FuncDo)(void);
typedef void (*FuncDoInt)(int Param);

// �������� ���� �����, ����������� ��� � CurWord, ������� �����
static RESULT GetWord(void)
{
int i=0;

while ((CurSymbol<BufLen) && (InBuf[CurSymbol]>0) &&
      ((InBuf[CurSymbol]==' ') ||
       (InBuf[CurSymbol]==9) ||
       (InBuf[CurSymbol]==0xD) ||
       (InBuf[CurSymbol]==0xA))) {
    CurSymbol++;
}
if (CurSymbol>=BufLen)
    return erEnd;
if (InBuf[CurSymbol]==0)
    return erEnd;
while ((CurSymbol<BufLen) &&
       (InBuf[CurSymbol]>0) &&
       (i<127) &&
       (InBuf[CurSymbol]!=' ') &&
       (InBuf[CurSymbol]!=9) &&
       (InBuf[CurSymbol]!=0xD) &&
       (InBuf[CurSymbol]!=0xA)) {
  CurWord[i]=InBuf[CurSymbol];
    if ((CurWord[i]>='a') && (CurWord[i]<='z'))
        CurWord[i]-=('a'-'A');
  i++;
    CurSymbol++;
    if (CurWord[0]==';')
        break;
    if (InBuf[CurSymbol]==';')
        break;
}
CurWord[i]=0;
return i;
}


// �������� ������������ ����� � ����������
/*
static char CmpWord(char* Word)
{
int i=0;

while (Word[i]==CurWord[i]) {
    if (Word[i]==0) {
        if (CurWord[i]==0) return 1;
            else return 0;
    } else i++;
}
return 0;
}
*/

static char CmpWordConst(char ROM* Word)
{
int i=0;

while (Word[i]==CurWord[i]) {
    if (Word[i]==0) {
        if (CurWord[i]==0) return 1;
            else return 0;
    } else i++;
}
return 0;
}

// ��������� � �������� ����� ������
static void AddOut(char* Err)
{
BUFLEN i=0;

while (Err[i]>0) {
    OutBuf[CurOutSymbol]=Err[i];
    CurOutSymbol++;
    i++;
}
OutBuf[CurOutSymbol]=0;
}

static void AddOutConst(char ROM* Err)
{
BUFLEN i=0;

while (Err[i]>0) {
    OutBuf[CurOutSymbol]=Err[i];
    CurOutSymbol++;
    i++;
}
OutBuf[CurOutSymbol]=0;
}

static char Len(char *Str)
{int i;
 for (i=0;i<256;i++) if (Str[i]==0) return i;
 return 255;
}

// ��������� � �������� ����� Long
static void AddOutLong(long l)
{
AddOut(LongToStr(l));
AddOutConst(trEmptyStr);
}

// ��������� � �������� ����� Date
static void AddOutDate(long l)
{
extern char OutStr[LEN];
int tmp;
int i;

LongToStr(l);
tmp=Len(OutStr);
if (8-tmp) {
   for (i=tmp;i>=0;i--){
       OutStr[i+8-tmp]=OutStr[i];
   }
   for (i=0;i<8-tmp;i++) OutStr[i]='0';
}
/*
extern char OutStr[tmpStrLen];
char tmp;
int i;

tmp=strlen(OutStr);
if (8-tmp) {
   for (i=tmp;i>=0;i--){
       OutStr[i+8-tmp]=OutStr[i];
   }
   for (i=0;i<8-tmp;i++) OutStr[i]='0';
}
*/
/*
char tmp[2];
extern char OutStr[tmpStrLen];

LongToStr(l);
tmp[0]='0';
tmp[1]=0;
while (strlen(OutStr)<8) AddOut(tmp);
AddOut(OutStr);
AddOutConst(&trEmptyStr);
*/
//AddOut(LongToStr(l));
//AddOutConst(&trEmptyStr);
AddOut(OutStr);
AddOutConst(trEmptyStr);
}


// ��������� � �������� ����� Time
static void AddOutTime(long l)
{
extern char OutStr[LEN];
int tmp;
int i;

LongToStr(l);
tmp=Len(OutStr);
if (6-tmp) {
   for (i=tmp;i>=0;i--){
       OutStr[i+6-tmp]=OutStr[i];
   }
   for (i=0;i<6-tmp;i++) OutStr[i]='0';
}
/*
char tmp[2];
extern char OutStr[tmpStrLen];
LongToStr(l);
tmp[0]='0';
tmp[1]=0;
while (Len(OutStr)<6) AddOut(tmp);
AddOut(OutStr);
AddOutConst(&trEmptyStr);
*/
//AddOut(LongToStr(l));
AddOut(OutStr);
AddOutConst(trEmptyStr);
}

// ��������� � �������� ����� Time
static void AddOutShortTime(unsigned int l)
{
extern char OutStr[LEN];
int tmp;
int i;

LongToStr(l);
tmp=Len(OutStr);
if (4-tmp) {
   for (i=tmp;i>=0;i--){
       OutStr[i+4-tmp]=OutStr[i];
   }
   for (i=0;i<4-tmp;i++) OutStr[i]='0';
}
AddOut(OutStr);
AddOutConst(trEmptyStr);
}

// ��������� � �������� ����� Int
static void AddOutInt(int l)
{
AddOut(LongToStr(l));
AddOutConst(trEmptyStr);
}


// ��������� � �������� ����� Float XXX.X
static void AddOutFloat(float f)
{
extern char OutStr[LEN];

//AddOut(FloatToStr(f));
FloatToStr(OutStr,f,7,3);
AddOut(OutStr);
AddOutConst(trEmptyStr);
}

// ��������� � �������� ����� Float XXX.Digits
/*
static void AddOutFloatSpec(float f,char Digits)
{
AddOut(FloatToStrSpec(f,Digits));
AddOutConst(trEmptyStr);
}
*/

static void OutEndLine(void)
{
//AddOutConst(&trLF);
}


// ��������� � �������� ����� ; � ������� ������
static void AddOutEnd(void)
{

if (CurOutSymbol) {
   if (OutBuf[CurOutSymbol-1]==' ') CurOutSymbol--;
}
AddOutConst(trCP);
//AddOutConst(&trLF);
}


// ��������� � �������� ����� ��������� �� ������
static RESULT SayError(RESULT res)
{
RError=res;
if (res==erInvalidCommand) { AddOutConst(trError); AddOutConst(trInvalidCommand); }
else if (res==erInvalidParam) { AddOutConst(trError); AddOutConst(trInvalidParameter); }
else if (res==erInvalidInt) { AddOutConst(trError); AddOutConst(trInvalidIntegerValue); }
else if (res==erInvalidFloat) { AddOutConst(trError); AddOutConst(trInvalidFloatValue); }
else if (res==erInvalidDate) { AddOutConst(trError); AddOutConst(trInvalidDate); }
else if (res==erInvalidTime) { AddOutConst(trError); AddOutConst(trInvalidTime); }
else if (res==erNoPointComma) { AddOutConst(trError); AddOutConst(trCPExpected); }
else return res;
InBuf[CurSymbol]=0;
AddOut(&InBuf[CurOperator]);
OutEndLine();
return res;
}



static char CheckPointComma(void)
{
if (GetWord()<0) { SayError(erNoPointComma); return 1; }
if (!CmpWordConst(trCP)) { SayError(erNoPointComma); return 1; }
return 0;
}


static char CurWordIsInt(void)
{
char i=0;

while (CurWord[i]!=0) {
    if (((CurWord[i]>='0') && (CurWord[i]<='9'))||((CurWord[i]=='-'))) {

    } else return 0;
    i++;
}
return 1;
}


static char CurWordIsDate(void)
{
char i=0;

ParamLong=0;
while (CurWord[i]!=0) {
    if ((CurWord[i]>='0') && (CurWord[i]<='9')) {
        ParamLong*=10;
        ParamLong+=(CurWord[i]-'0');
    } else return 0;
    i++;
}
return (strlen(CurWord)==8);
}


static char CurWordIsTime(void)
{
int i=0;

ParamLong=0;
while (CurWord[i]!=0) {
    if ((CurWord[i]>='0') && (CurWord[i]<='9')) {
        ParamLong*=10;
        ParamLong+=(CurWord[i]-'0');
    } else return 0;
    i++;
}
return (strlen(CurWord)==6);
}


static char IsFloat(void)
{
int i=0;

while (CurWord[i]!=0) {
    if (((CurWord[i]>='0') && (CurWord[i]<='9')) ||
        (CurWord[i]=='.') ||
        (CurWord[i]=='-') ||
        (CurWord[i]==',')) {

    } else return 0;
    i++;
}
return 1;
}


static int GetInt(char NeedWord)
{
if (NeedWord)
    if (GetWord()<0) return erInvalidInt;
if (CurWordIsInt()) return StrToInt(CurWord);
    else return erInvalidInt;
}

#define gpNone 0
#define gpInt 1
#define gpQuestion 2
// ���������� ��� ���������, ���������� ��� � ��������
// ��� ����� - ���������� ������ int, �� char
static RESULT GetParam(void* Param1)
{
if (GetWord()<0) return SayError(erInvalidParam);
if (CurWordIsInt()) {
    if ((*((int*)Param1)=GetInt(0))<0) return SayError(erInvalidInt);
    if (CheckPointComma()) return erNoPointComma;
    return gpInt;
}
else if (CmpWordConst(trQuestion)) {
    if (CheckPointComma()) return erNoPointComma;
    return gpQuestion;
} else return SayError(erInvalidParam);
}


// �������� ������������ ����� � ���������� � ?;
static char CmpWordQuestion(char ROM* Word, FuncDo DoIt)
{
if (CmpWordConst(Word)) {
    switch (GetParam(&Param1)) {
       case gpQuestion: DoIt(); break;
    }
    return 1;
} else return 0;
}


// �������� ������������ ����� � ���������� � ;
static char CmpWordSimple(char ROM* Word, FuncDo DoIt)
{
if (CmpWordConst(Word)) {
    if (CheckPointComma()) return 1;
    DoIt();
    return 1;
} else return 0;
}


// �������� ������������ ����� � ���������� � int ��� ?;
/*
static char CmpWordQuestionOrInt(char ROM* Word, FuncDoInt DoItInt, FuncDo DoItQuestion)
{
if (CmpWordConst(Word)) {
    switch (GetParam(&Param1)) {
        case gpInt: DoItInt(Param1); break;
        case gpQuestion: DoItQuestion(); break;
    }
    return 1;
} else return 0;
}
*/


#include "DoIt.h"


signed char Translate(void)
{
//Start Balance
#ifndef OldR1500_6
extern unsigned int AutoBalance;
#endif
unsigned long Date;
RESULT c;
float ParamFloat1,ParamFloat2,ParamFloat3,ParamFloat4,ParamFloat5,ParamFloat6,ParamFloat7,ParamFloat8;

RError=erNoError;
CurSymbol=0;
CurOperator=0;
CurOutSymbol=0;
CurWord[0]=0;
OutBuf[0]=0;
if (BusyFlag) {
   GetBusy();
   OutEndLine();
   return erNoError;
}
while (RError==0) {
    CurOperator=CurSymbol;
    c=GetWord();
    if (c==erEnd)
        break;
    // RST
    else if (CmpWordConst(trCommandRST)) {
         DeviceReset();
    }
    // DEVICETYPE;
    else if (CmpWordQuestion(trCommandDEVICETYPE,GetDeviceType)) {
    }
    // VER ?;
    else if (CmpWordQuestion(trCommandVER,GetVER)) {
    }
    // TIME YYYYYYMMSS HHMMSS;
    // TIME ?;
    else if (CmpWordConst(trCommandTIME)) {
		/* acm, 1-19-11 more closely match behavior to PDM - return input time wasn't getting returned
        if (GetWord()<0) return SayError(erInvalidDate);
        if (CurWordIsDate()) {
            SetDate(ParamLong,0);
            if (GetWord()<0) return SayError(erInvalidTime);
            if (CurWordIsTime()) {
               if (CheckPointComma()) return erNoPointComma;
               SetTime(ParamLong);
            }
        }
        else if (CmpWordConst(trQuestion)) {
            if (CheckPointComma()) return erNoPointComma;
            GetTime();
        } else return SayError(erInvalidParam);
    }*/
		if (GetWord()<0) return SayError(erInvalidParam);
        if (CurWordIsDate()) {
			
            Date=ParamLong;
            if (GetWord()<0) return SayError(erInvalidParam);
            if (CurWordIsTime()) {
				if (CheckPointComma()) return erNoPointComma;
				SetTime(Date, ParamLong);
				GetTime();
            } else return SayError(erInvalidTime);
        }
        else if (CmpWordConst(trQuestion)) {
            if (CheckPointComma()) return erNoPointComma;
            GetTime();
        } else return SayError(erInvalidParam);
    }

    // TREAD MODE ArchOnDay dt TIME1 ��.. TIME8;
    // TREAD ?;
    else if (CmpWordConst(trCommandTREAD)) {
        if (GetWord()<0) return SayError(erInvalidParam);
        if (CurWordIsInt()) {
           if ((Param1=GetInt(0))<0) return SayError(erInvalidInt);
           SetTreadBegin(Param1);
           if ((Param1=GetInt(1))<0) return SayError(erInvalidInt);
           SetArchOnDay(Param1);
           if ((Param1=GetInt(1))<0) return SayError(erInvalidInt);
           SetTreadDelta(Param1);
           if (GetWord()<0) return SayError(erInvalidParam);
           while (CurWordIsInt()) {
              if ((Param1=GetInt(0))<0) return SayError(erInvalidInt);
              SetTreadAdd(Param1);
              if (GetWord()<0) return SayError(erInvalidParam);
           }
           if (!CmpWordConst(trCP)) return SayError(erNoPointComma);
           if ((MeasurementResult!=msStatusReRead)&&(MeasurementResult!=msStatusReRead1Min))
              GetNextTime(DateTimeNowNoSec());
        } else if (CmpWordConst(trQuestion)) {
           if (CheckPointComma()) return erNoPointComma;
           GetTread();
        } else return SayError(erInvalidParam);
    }
    // SHOW SHOWTIME DATE TIME ...;
    else if (CmpWordConst(trCommandSHOW)) {
            if (GetWord()<0) return SayError(erInvalidParam);
            if (CurWordIsInt()) {
               if ((Param1=GetInt(0))<0) return SayError(erInvalidInt);
               if (GetWord()<0) return SayError(erInvalidParam);
               if ((CurWordIsInt())&&(Param2=GetInt(0))<0) return SayError(erInvalidInt);
               if (GetWord()<0) return SayError(erInvalidParam);
               if ((CurWordIsInt())&&(Param3=GetInt(0))<0) return SayError(erInvalidInt);
               if (GetWord()<0) return SayError(erInvalidParam);
               if ((CurWordIsInt())&&(Param4=GetInt(0))<0) return SayError(erInvalidInt);
               if (GetWord()<0) return SayError(erInvalidParam);
               if ((CurWordIsInt())&&(Param5=GetInt(0))<0) return SayError(erInvalidInt);
               if (GetWord()<0) return SayError(erInvalidParam);
               if ((CurWordIsInt())&&(Param6=GetInt(0))<0) return SayError(erInvalidInt);
               if (GetWord()<0) return SayError(erInvalidParam);
               if ((CurWordIsInt())&&(Param7=GetInt(0))<0) return SayError(erInvalidInt);
               if (GetWord()<0) return SayError(erInvalidParam);
               if ((CurWordIsInt())&&(Param8=GetInt(0))<0) return SayError(erInvalidInt);
               if (GetWord()<0) return SayError(erInvalidParam);
               if ((CurWordIsInt())&&(Param9=GetInt(0))<0) return SayError(erInvalidInt);
               if (GetWord()<0) return SayError(erInvalidParam);
               if ((CurWordIsInt())&&(Param10=GetInt(0))<0) return SayError(erInvalidInt);
               SetShow(Param1,Param2,Param3,Param4,Param5,Param6,Param7,Param8,Param9,Param10);
            } else if (CmpWordConst(trQuestion)) {
               if (CheckPointComma()) return erNoPointComma;
               GetShow();
            } else return SayError(erInvalidParam);
        }
    // SINGLE
    else if (CmpWordSimple(trCommandSTART,SetStart)) {
    }
    // BUSY ?;
    else if (CmpWordQuestion(trCommandBUSY,GetBusy)) {
    }
    // CLEAR
    else if (CmpWordConst(trCommandCLEAR)) {
        SetClear();
    }
    // CLEARALL
    else if (CmpWordConst(trCommandCLEARALL)) {
        SetClearAll();
    }
    // PAUSE
    else if (CmpWordConst(trCommandPAUSE)) {
//        if (GetWord()<0) {SetPause();}
//        else if (CmpWordConst(trQuestion)) {
//                GetPause();
//             } else PauseOn();
// acm, replace above with PDM implementation
		// acm, 11-26-10, need to verify Pause set when IHM requests download data, indications not, possibly
		// related to changes done here, e.g. GetWord() implementations are different...  Repeated observation
		// that IHM reports CRC error during large data download "claudeRMA"
        if(GetWord()<0) return SayError(erInvalidParam);
        if(CmpWordConst(trQuestion)){
          GetPause();
        } else
        if(CmpWordConst(trCP)){ // acm, if string terminated by semi-colon
          SetPause();
          GetPause();
        } else return SayError(erInvalidParam);
}
     // RESUME
    else if (CmpWordSimple(trCommandRESUME,SetResume)) {
		SetResume();
    }
    // STOP
//    else if (CmpWordSimple(&trCommandSTOP,SetStop)) {
    else if (CmpWordConst(trCommandSTOP)) {
        if (GetWord()<0) {SetStop();}
        else if (CmpWordConst(trQuestion)) {
                GetStop();
             } else SetStop();
    }
    // MONDATE DATE TIME ?;
    else if (CmpWordConst(trCommandMONDATE)) {
        if (GetWord()<0) return SayError(erInvalidDate);
        if (CurWordIsDate()) {
           Date=ParamLong;
           if (GetWord()<0) return SayError(erInvalidTime);
           if (CurWordIsTime()) {
              GetMeasurementOnDate(Date,ParamLong);
           } else return SayError(erInvalidTime);
        } else return SayError(erInvalidDate);
    }
    // MISC GammaTrendDays GammaAveMeas TempCoefCalc UnbalLim InpImped1 InpImped2 InpImped3 RatedVoltage;
    // MISC ?;
    else if (CmpWordConst(trCommandMISC)) {
        if (GetWord()<0) return SayError(erInvalidInt);
        if (CurWordIsInt()) {
           if ((Param1=GetInt(0))<0) return SayError(erInvalidInt);
           if ((Param2=GetInt(1))<0) return SayError(erInvalidInt);
           if ((Param3=GetInt(1))<0) return SayError(erInvalidInt);
           if ((Param4=GetInt(1))<0) return SayError(erInvalidInt);
           if ((Param5=GetInt(1))<0) return SayError(erInvalidInt);
           if ((Param6=GetInt(1))<0) return SayError(erInvalidInt);
/*
           if (GetWord()<0) return SayError(erInvalidParam);
           if (!IsFloat()) return SayError(erInvalidFloat);
           if ((ParamFloat1=StrToFloat(CurWord))<0) return SayError(erInvalidFloat);
           if (GetWord()<0) return SayError(erInvalidParam);
           if (!IsFloat()) return SayError(erInvalidFloat);
           if ((ParamFloat2=StrToFloat(CurWord))<0) return SayError(erInvalidFloat);
           if (GetWord()<0) return SayError(erInvalidParam);
           if (!IsFloat()) return SayError(erInvalidFloat);
           if ((ParamFloat3=StrToFloat(CurWord))<0) return SayError(erInvalidFloat);
           if (GetWord()<0) return SayError(erInvalidParam);
           if (!IsFloat()) return SayError(erInvalidFloat);
           if ((ParamFloat4=StrToFloat(CurWord))<0) return SayError(erInvalidFloat);
*/
           SetMisc(Param1,Param2,Param3,Param4,Param5,Param6,Param7);
        } else if (CmpWordConst(trQuestion)) {
           if (CheckPointComma()) return erNoPointComma;
           GetMisc();
        } else return SayError(erInvalidParam);
    }
    // THRESH GROUP GammaRed GammaYellow TempCoefThresh GammaTrendThresh Hysteresis ;
    // THRESH GROUP ?;
    else if (CmpWordConst(trCommandTHRESH)) {
        if (GetWord()<0) return SayError(erInvalidParam);
        if ((Param1=GetInt(0))<0) return SayError(erInvalidInt);
        if (GetWord()<0) return SayError(erInvalidParam);
        if (IsFloat()) {
           if ((ParamFloat1=StrToFloat(CurWord))<0) return SayError(erInvalidFloat);
           if (GetWord()<0) return SayError(erInvalidParam);
           if (!IsFloat()) return SayError(erInvalidFloat);
           if ((ParamFloat2=StrToFloat(CurWord))<0) return SayError(erInvalidFloat);
           if (GetWord()<0) return SayError(erInvalidParam);
           if (!IsFloat()) return SayError(erInvalidFloat);
           if ((ParamFloat3=StrToFloat(CurWord))<0) return SayError(erInvalidFloat);
           if (GetWord()<0) return SayError(erInvalidParam);
           if (!IsFloat()) return SayError(erInvalidFloat);
           if ((ParamFloat4=StrToFloat(CurWord))<0) return SayError(erInvalidFloat);
           if ((Param2=GetInt(1))<0) return SayError(erInvalidInt);
           if (GetWord()<0) return SayError(erInvalidParam);
           if (!IsFloat()) return SayError(erInvalidFloat);
           if ((ParamFloat5=StrToFloat(CurWord))<0) return SayError(erInvalidFloat);
           if (GetWord()<0) return SayError(erInvalidParam);
           if (!IsFloat()) return SayError(erInvalidFloat);
           if ((ParamFloat6=StrToFloat(CurWord))<0) return SayError(erInvalidFloat);
           if (GetWord()<0) return SayError(erInvalidParam);
           if (!IsFloat()) return SayError(erInvalidFloat);
           if ((ParamFloat7=StrToFloat(CurWord))<0) return SayError(erInvalidFloat);
           if (CheckPointComma()) return erNoPointComma;
           SetThresh(Param1-1,ParamFloat1,ParamFloat2,ParamFloat3,ParamFloat4,Param2,ParamFloat5,ParamFloat6,ParamFloat7);
        } else if (CmpWordConst(trQuestion)) {
           if (CheckPointComma()) return erNoPointComma;
           GetThresh(Param1-1);
        } else return SayError(erInvalidParam);
    }
    // ALARM RelayON RelayOnTimeOn RelayOnTime GammaAlarmON TempCoefAlarmON TrendAlarmON ;
    // ALARM ?;
    else if (CmpWordConst(trCommandALARM)) {
        if (GetWord()<0) return SayError(erInvalidParam);
        if (CurWordIsInt()) {
           if ((Param1=GetInt(0))<0) return SayError(erInvalidInt);
           if (GetWord()<0) return SayError(erInvalidParam);
           if (CmpWordConst(trQuestion)) {
              if (CheckPointComma()) return erNoPointComma;
              GetAlarm(Param1-1);
           } else {
             if ((Param2=GetInt(0))<0) return SayError(erInvalidInt);
             if ((Param3=GetInt(1))<0) return SayError(erInvalidInt);
             if ((Param4=GetInt(1))<0) return SayError(erInvalidInt);
             if ((Param5=GetInt(1))<0) return SayError(erInvalidInt);
             if ((Param6=GetInt(1))<0) return SayError(erInvalidInt);
             if ((Param7=GetInt(1))<0) return SayError(erInvalidInt);
             if ((Param8=GetInt(1))<0) return SayError(erInvalidInt);
             if (CheckPointComma()) return erNoPointComma;
             SetAlarm(Param1-1,Param2,Param3,Param4,Param5,Param6,Param7,Param8);
           }

        } else return SayError(erInvalidParam);
    }
    //SETPARAM GROUP InpImped1 InpImped2 InpImped3 RatedVoltageKV RatedCurrent;
    else if (CmpWordConst(trCommandSETPARAM)) {
        if (GetWord()<0) return SayError(erInvalidParam);
        if ((Param1=GetInt(0))<0) return SayError(erInvalidInt);
        if (GetWord()<0) return SayError(erInvalidParam);
        if (IsFloat()) {
           if ((ParamFloat1=StrToFloat(CurWord))<0) return SayError(erInvalidFloat);
           if (GetWord()<0) return SayError(erInvalidParam);
           if (!IsFloat()) return SayError(erInvalidFloat);
           if ((ParamFloat2=StrToFloat(CurWord))<0) return SayError(erInvalidFloat);
           if (GetWord()<0) return SayError(erInvalidParam);
           if (!IsFloat()) return SayError(erInvalidFloat);
           if ((ParamFloat3=StrToFloat(CurWord))<0) return SayError(erInvalidFloat);
           if (GetWord()<0) return SayError(erInvalidParam);
           if (!IsFloat()) return SayError(erInvalidFloat);
           if ((ParamFloat4=StrToFloat(CurWord))<0) return SayError(erInvalidFloat);
           if (GetWord()<0) return SayError(erInvalidParam);
           if (!IsFloat()) return SayError(erInvalidFloat);
           if ((ParamFloat5=StrToFloat(CurWord))<0) return SayError(erInvalidFloat);
           if (CheckPointComma()) return erNoPointComma;
           SetSetParam(Param1-1,ParamFloat1,ParamFloat2,ParamFloat3,ParamFloat4,ParamFloat5);
        } else if (CmpWordConst(trQuestion)) {
           if (CheckPointComma()) return erNoPointComma;
           GetSetParam(Param1-1);
        } else return SayError(erInvalidParam);

    }
    // LOG NUMBER ?;
    else if (CmpWordConst(trCommandLOG)) {
        if (GetWord()<0) return SayError(erInvalidParam);
        if (CurWordIsInt()) {
           if ((Param1=GetInt(0))<1) return SayError(erInvalidParam);
           if (Param1>MaxLogData) return SayError(erInvalidParam);
           if (GetWord()<0) return SayError(erInvalidParam);
           if (CmpWordConst(trQuestion)) {
              if (CheckPointComma()) return erNoPointComma;
              GetLog(Param1);
           }

        } else return SayError(erInvalidParam);
    }
    // DEFAULT
    else if (CmpWordSimple(trCommandDEFAULT,SetDefault)) {
    }
    // MEM ?;
    else if (CmpWordQuestion(trCommandMEM,GetMem)) {
    }
    // CALIBRT T1K T1B T2K T2B T3K T3B T4K T4B;
    else if (CmpWordConst(trCommandCALIBRT)) {
        if (GetWord()<0) return SayError(erInvalidParam);
        if (IsFloat()) {
           ParamFloat1=StrToFloat(CurWord);
           if (GetWord()<0) return SayError(erInvalidParam);
           if (!IsFloat()) return SayError(erInvalidFloat);
           ParamFloat2=StrToFloat(CurWord);
           if (GetWord()<0) return SayError(erInvalidParam);
           if (!IsFloat()) return SayError(erInvalidFloat);
           ParamFloat3=StrToFloat(CurWord);
           if (GetWord()<0) return SayError(erInvalidParam);
           if (!IsFloat()) return SayError(erInvalidFloat);
           ParamFloat4=StrToFloat(CurWord);
           if (GetWord()<0) return SayError(erInvalidParam);
           if (!IsFloat()) return SayError(erInvalidFloat);
           ParamFloat5=StrToFloat(CurWord);
           if (GetWord()<0) return SayError(erInvalidParam);
           if (!IsFloat()) return SayError(erInvalidFloat);
           ParamFloat6=StrToFloat(CurWord);
           if (GetWord()<0) return SayError(erInvalidParam);
           if (!IsFloat()) return SayError(erInvalidFloat);
           ParamFloat7=StrToFloat(CurWord);
           if (GetWord()<0) return SayError(erInvalidParam);
           if (!IsFloat()) return SayError(erInvalidFloat);
           ParamFloat8=StrToFloat(CurWord);
            SetCalibrT(ParamFloat1,ParamFloat2,ParamFloat3,ParamFloat4,ParamFloat5,ParamFloat6,ParamFloat7,ParamFloat8);
        } else if (CmpWordConst(trQuestion)) {
           if (CheckPointComma()) return erNoPointComma;
           GetCalibrT();
        } else return SayError(erInvalidParam);
    }
    // CALIBRA A1K A1B A2K A2B A3K A3B A4K A4B;
    else if (CmpWordConst(trCommandCALIBRA)) {
        if (GetWord()<0) return SayError(erInvalidParam);
        if (IsFloat()) {
           ParamFloat1=StrToFloat(CurWord);
           if (GetWord()<0) return SayError(erInvalidParam);
           if (!IsFloat()) return SayError(erInvalidFloat);
           ParamFloat2=StrToFloat(CurWord);
           if (GetWord()<0) return SayError(erInvalidParam);
           if (!IsFloat()) return SayError(erInvalidFloat);
           ParamFloat3=StrToFloat(CurWord);
           if (GetWord()<0) return SayError(erInvalidParam);
           if (!IsFloat()) return SayError(erInvalidFloat);
           ParamFloat4=StrToFloat(CurWord);
           if (GetWord()<0) return SayError(erInvalidParam);
           if (!IsFloat()) return SayError(erInvalidFloat);
           ParamFloat5=StrToFloat(CurWord);
           if (GetWord()<0) return SayError(erInvalidParam);
           if (!IsFloat()) return SayError(erInvalidFloat);
           ParamFloat6=StrToFloat(CurWord);
           if (GetWord()<0) return SayError(erInvalidParam);
           if (!IsFloat()) return SayError(erInvalidFloat);
           ParamFloat7=StrToFloat(CurWord);
           if (GetWord()<0) return SayError(erInvalidParam);
           if (!IsFloat()) return SayError(erInvalidFloat);
           ParamFloat8=StrToFloat(CurWord);
           SetCalibrA(ParamFloat1,ParamFloat2,ParamFloat3,ParamFloat4,ParamFloat5,ParamFloat6,ParamFloat7,ParamFloat8);
        } else if (CmpWordConst(trQuestion)) {
           if (CheckPointComma()) return erNoPointComma;
           GetCalibrA();
        } else return SayError(erInvalidParam);
    }
    // CALIBRH Offset Slope;
    else if (CmpWordConst(trCommandCALIBRH)) {
        if (GetWord()<0) return SayError(erInvalidParam);
        if (IsFloat()) {
           ParamFloat1=StrToFloat(CurWord);
           if (GetWord()<0) return SayError(erInvalidParam);
           if (!IsFloat()) return SayError(erInvalidFloat);
           ParamFloat2=StrToFloat(CurWord);
           SetCalibrH(ParamFloat1,ParamFloat2);
        } else if (CmpWordConst(trQuestion)) {
           if (CheckPointComma()) return erNoPointComma;
           GetCalibrH();
        } else return SayError(erInvalidParam);
    }
    // READINIT GROUP ?;
    else if (CmpWordConst(trCommandREADINIT)) {
        if (GetWord()<0) return SayError(erInvalidParam);
        if (CurWordIsInt()) {
           if ((Param1=GetInt(0))<0) return SayError(erInvalidInt);
           if (GetWord()<0) return SayError(erInvalidParam);
           if (CmpWordConst(trQuestion)) {
              if (CheckPointComma()) return erNoPointComma;
              GetInit(Param1-1);
           } else return SayError(erInvalidParam);
        } else return SayError(erInvalidParam);
    }
    // READREC REC GROUP ?;
    else if (CmpWordConst(trCommandREADREC)) {
        if (GetWord()<0) return SayError(erInvalidParam);
        if (CurWordIsInt()) {
           if ((Param1=GetInt(0))<=0) return SayError(erInvalidInt);
           if ((unsigned int)Param1>MeasurementsInArchive) SayError(erInvalidParam);
           if ((Param2=GetInt(1))<0) return SayError(erInvalidInt);
           if (GetWord()<0) return SayError(erInvalidParam);
           if (CmpWordConst(trQuestion)) {
              if (CheckPointComma()) return erNoPointComma;
              GetRec(Param1,Param2-1);
           } else return SayError(erInvalidParam);
        } else return SayError(erInvalidParam);
    }
    // DIAG Meas Side ?;
    else if (CmpWordConst(trCommandDIAG)) {
        if (GetWord()<0) return SayError(erInvalidParam);
        if (CurWordIsInt()) {
           if ((Param1=GetInt(0))<=0) return SayError(erInvalidInt);
           if ((unsigned int)Param1>MeasurementsInArchive) SayError(erInvalidParam);
           if ((Param2=GetInt(1))<0) return SayError(erInvalidInt);
           if (GetWord()<0) return SayError(erInvalidParam);
           if (CmpWordConst(trQuestion)) {
              if (CheckPointComma()) return erNoPointComma;
              GetDiagnosis(Param1,Param2-1);
           } else return SayError(erInvalidParam);
        } else return SayError(erInvalidParam);
    }
    // ERROR ?;
    else if (CmpWordConst(trCommandERROR)) {
        if (GetWord()<0) return SayError(erInvalidParam);
        if (CmpWordConst(trQuestion)) {
           if (CheckPointComma()) return erNoPointComma;
           GetError();
        } else return SayError(erInvalidParam);
    }
    // ERRORBIT ?;
    else if (CmpWordConst(trCommandERRORBIT)) {
        if (GetWord()<0) return SayError(erInvalidParam);
        if (CmpWordConst(trQuestion)) {
           if (CheckPointComma()) return erNoPointComma;
           GetErrorBit();
        } else return SayError(erInvalidParam);
    }
    // BASELINE GROUP ?;
    else if (CmpWordConst(trCommandSTABLE)) {
        if (GetWord()<0) return SayError(erInvalidParam);
        if (CurWordIsInt()) {
           if ((Param1=GetInt(0))<=0) return SayError(erInvalidInt);
           if (GetWord()<0) return SayError(erInvalidParam);
           if (CmpWordConst(trQuestion)) {
              if (CheckPointComma()) return erNoPointComma;
              GetStable(Param1-1,0);
           } else
              if (CurWordIsDate()) {
                 if (GetWord()<0) return SayError(erInvalidTime);
                 if (CmpWordConst(trQuestion)) {
                    if (CheckPointComma()) return erNoPointComma;
                    GetStable(Param1-1,ParamLong);
                 }
             } else return SayError(erInvalidParam);
        } else return SayError(erInvalidParam);
    }
    // HEATTEST ?;
    else if (CmpWordConst(trCommandHEATTEST)) {
        if (GetWord()<0) return SayError(erInvalidParam);
        if (CmpWordConst(trQuestion)) {
           if (CheckPointComma()) return erNoPointComma;
           GetHEATTEST();
        } else return SayError(erInvalidParam);

    }
    // OFFLINEPARAM GROUP ?;
    else if (CmpWordConst(trCommandOFFLINEPARAM)) {
        if (GetWord()<0) return SayError(erInvalidParam);
        if (CurWordIsInt()) {
           if ((Param1=GetInt(0))<=0) return SayError(erInvalidInt);
           if (GetWord()<0) return SayError(erInvalidTime);
           if (CmpWordConst(trQuestion)) {
              if (CheckPointComma()) return erNoPointComma;
              GetOFFLINEPARAM(Param1-1);
           } else {
              if ((Param2=GetInt(0))<-70) return SayError(erInvalidInt);
              if (Param2>185) return SayError(erInvalidParam);
              if (GetWord()<0) return SayError(erInvalidParam);
              if (!IsFloat()) return SayError(erInvalidFloat);
              ParamFloat1=StrToFloat(CurWord);
              if (GetWord()<0) return SayError(erInvalidParam);
              if (!IsFloat()) return SayError(erInvalidFloat);
              ParamFloat2=StrToFloat(CurWord);
              if (GetWord()<0) return SayError(erInvalidParam);
              if (!IsFloat()) return SayError(erInvalidFloat);
              ParamFloat3=StrToFloat(CurWord);
              if (GetWord()<0) return SayError(erInvalidParam);
              if (!IsFloat()) return SayError(erInvalidFloat);
              ParamFloat4=StrToFloat(CurWord);
              if (GetWord()<0) return SayError(erInvalidParam);
              if (!IsFloat()) return SayError(erInvalidFloat);
              ParamFloat5=StrToFloat(CurWord);
              if (GetWord()<0) return SayError(erInvalidParam);
              if (!IsFloat()) return SayError(erInvalidFloat);
              ParamFloat6=StrToFloat(CurWord);
              if (CheckPointComma()) return erNoPointComma;
              SetOFFLINEPARAM(Param1-1,Param2,ParamFloat1,ParamFloat2,ParamFloat3,ParamFloat4,ParamFloat5,ParamFloat6);
           }
        }  else return SayError(erInvalidParam);
    }
    // CHDIV GROUP ?;
    else if (CmpWordConst(trCommandCHDIV)) {
        if (GetWord()<0) return SayError(erInvalidParam);
        if (CurWordIsInt()) {
           if ((Param1=GetInt(0))<=0) return SayError(erInvalidInt);
           if (GetWord()<0) return SayError(erInvalidTime);
           if (CmpWordConst(trQuestion)) {
              if (CheckPointComma()) return erNoPointComma;
              GetCHDIV(Param1-1);
           } else {
              if (!IsFloat()) return SayError(erInvalidFloat);
              ParamFloat1=StrToFloat(CurWord);
              if (GetWord()<0) return SayError(erInvalidParam);
              if (!IsFloat()) return SayError(erInvalidFloat);
              ParamFloat2=StrToFloat(CurWord);
              if (GetWord()<0) return SayError(erInvalidParam);
              if (!IsFloat()) return SayError(erInvalidFloat);
              ParamFloat3=StrToFloat(CurWord);
              SetCHDIV(Param1-1,ParamFloat1,ParamFloat2,ParamFloat3);
          }
       } else return SayError(erInvalidParam);
    }
    // NEGTG ?;
    else if (CmpWordConst(trCommandNEGTG)) {
        if (GetWord()<0) return SayError(erInvalidParam);
        if (CmpWordConst(trQuestion)) {
           if (CheckPointComma()) return erNoPointComma;
           GetNEGTG();
        } else {
           if (CurWordIsInt()) {
              Param1=GetInt(0);
              if ((Param1<0)||(Param1>1)) return SayError(erInvalidInt);
              SetNEGTG(Param1);
           } else return SayError(erInvalidParam);
        }
    }

    //BALANCE Side;
#ifndef OldR1500_6
    else if (CmpWordConst(trCommandBALANCE)) {
        if (GetWord()<0) return SayError(erInvalidParam);
        if (CurWordIsInt()) {
           if ((Param1=GetInt(0))<=0) return SayError(erInvalidInt);
           if (Param1>MaxTransformerSide) return SayError(erInvalidInt);
           if (CheckPointComma()) return erNoPointComma;
//           AutoBalansing(Param1-1);
        } else {
          AutoBalance=1;
        }
    }
#endif
    //REGDATA ReadFromReg Temperature LoadActivePercent LoadReactivePercent Humidity LTCPosition;
    //REGDATA ?;
    else if (CmpWordConst(trCommandREGDATA)) {
        if (GetWord()<0) return SayError(erInvalidParam);
        if (CurWordIsInt()) {
           if ((Param1=GetInt(0))<0) return SayError(erInvalidInt);
           if ((Param2=GetInt(1))<-70) return SayError(erInvalidInt);
           if ((Param3=GetInt(1))<0) return SayError(erInvalidInt);
           if ((Param4=GetInt(1))<-127) return SayError(erInvalidInt);
           if ((Param5=GetInt(1))<0) return SayError(erInvalidInt);
           if ((Param6=GetInt(1))<0) return SayError(erInvalidInt);
           if (CheckPointComma()) return erNoPointComma;
           SetRegData(Param1,Param2,Param3,Param4,Param5,Param6);
        } else if (CmpWordConst(trQuestion)) {
           if (CheckPointComma()) return erNoPointComma;
           GetRegData();
        } else return SayError(erInvalidParam);
/*
        if (GetWord()<0) return SayError(erInvalidParam);
        if (CmpWordConst(&trQuestion)) {
           if (CheckPointComma()) return erNoPointComma;
           GetRegData();
        } else {
          if (GetWord()<0) return SayError(erInvalidParam);
          if (CurWordIsInt()) {
             if ((Param1=GetInt(0))<0) return SayError(erInvalidInt);
             if ((Param2=GetInt(1))<0) return SayError(erInvalidInt);
             if ((Param3=GetInt(1))<0) return SayError(erInvalidInt);
             if ((Param4=GetInt(1))<0) return SayError(erInvalidInt);
             if ((Param5=GetInt(1))<0) return SayError(erInvalidInt);
             if ((Param6=GetInt(1))<0) return SayError(erInvalidInt);
             if (CheckPointComma()) return erNoPointComma;
             SetRegData(Param1,Param2,Param3,Param4,Param5,Param6);
         } else return SayError(erInvalidParam);
        }
*/

    }
    //AUTOBALANCE ?;
    //AUTOBALANCE Date Hour WorkDays BalanceRunning NoLoadTestRunning;
    else if (CmpWordConst(trCommandAUTOBALANCE)) {
        if (GetWord()<0) return SayError(erInvalidParam);
        if (CmpWordConst(trQuestion)) {
           if (CheckPointComma()) return erNoPointComma;
           GetAutoBalance();
        } else {
           if (CurWordIsDate()) {
              SetDate(ParamLong,0);
              Param1=GetInt(1);
              if ((Param1<0)||(Param1>23)) return SayError(erInvalidInt);
              Param2=GetInt(1);
              if (Param2<0) return SayError(erInvalidInt);
              if (Param2>9) Param2=9;
              Param3=GetInt(1);
              if ((Param3<0)||(Param3>1)) return SayError(erInvalidInt);
              Param4=GetInt(1);
              if ((Param4<0)||(Param4>1)) return SayError(erInvalidInt);
              if (CheckPointComma()) return erNoPointComma;
              SetAutoBalance(ParamLong,Param1,Param2,Param3,Param4);
           } else return SayError(erInvalidTime);
        }
    }
    // CLEARLOG;
    else if (CmpWordConst(trCommandCLEARLOG)) {
         if (CheckPointComma()) return erNoPointComma;
         ClearLog();
    }
    // PROCTYPE ?;
    else if (CmpWordConst(trCommandPROCTYPE)) {
        if (GetWord()<0) return SayError(erInvalidParam);
        if (CmpWordConst(trQuestion)) {
           if (CheckPointComma()) return erNoPointComma;
           GetProcRype();
        } else return SayError(erInvalidParam);
    }
   // ;
    else if (CmpWordConst(trCP)) {
    }
    else return SayError(erInvalidCommand);
}
OutEndLine();
TestSetup();
return RError;
}

