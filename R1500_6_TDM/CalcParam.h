#ifndef _CalcParam_h
#define _CalcParam_h

#include "TypesDef.h"

struct TSinParameters {
float
     Phase,
     Ampl,
     Freq;
};

//void CalcSinParameters(int *Sn, unsigned int ArrLen, struct TSinParameters *SinParam,char AmplOnly,char TestMaxSin, float ClcFreq);
//void NewCalcSinParameters(int *Sn, unsigned int ArrLen, struct TSinParameters *SinParam,char AmplOnly,float ReadFr,float ClcFreq);
void LineApr(float *X,float *Y,unsigned int ArrLen,float *K, float *B);
float CalcAmpl(short *a, int ToX);
void _ImpPhaseCalc(short*a, short *x, int NPoints, float *Freq,float *Ampl, float *Faza, float *Ampl1,char Page0,char Page1);
    //��������� ��������
void ImpPhaseCalc(short *a, short *x, int NPoints, float *Freq,float *Ampl, float *Faza, float *Ampl1,char Page0,char Page1);
void GetGarmonSvertka(short *Channel,int FromX,int ToX,float Freq,float dX,float *A1,float *F1);
float CalcFreq(s16 *a, unsigned int ToX, float ReadFr, float *Phase, float *Ampl);

#endif
