#ifndef _Error_h
#define _Error_h

#include "Defs.h"

typedef unsigned long ErrorType;

extern volatile ErrorType Error;

//Количество отслеживаемых ошибок (Max=32)
#define MaxKnownError 32
#define MsgSize       15

// acm, error bit definitions for combined MODBUS registers 9(msword) and 3(lsword)
#define erHVTestFail        	0               // dwe, Set1 DAC test fail
#define erLVTestFail        	1               // dwe, Set2 DAC test fail
#define erFlashWriteFail    	2               // dwe, flash write fail DATA ERROR
#define erFlashReadFail     	3               // dwe, flash read fail DATA ERROR
#define erSet1Off           	4		// acm, initial data present, no input signal (related, erLoSignal) DATA ERROR
#define erSet2Off           	5		// ""  Set 2 DATA ERROR
#define erPhaseShiftSet1    	6		// acm, Set1 input phase incorrect DATA ERROR
#define erPhaseShiftSet2    	7		// acm, Set2 input phase incorrect wonderful coding, set2 error define not used same as set1, it is (erPhaseShiftSet1+1)... DATA ERROR
#define erChannels          	8		// acm, Display shows "Error Calibration" DATA ERROR
#define erPhaseAOff             9		// dwe, Phase A off DATA ERROR
#define erPhaseBOff         	10		// dwe, Phase B off DATA ERROR
#define erPhaseCOff         	11		// dwe, Phase C off DATA ERROR
#define erUnitOff           	12		// acm, both erSet1Off && erSet2Off DATA ERROR
#define erFreq              	13		// acm, sync frequency out of spec
#define erLoSignal          	14		// acm, no initial data: unplugged or low input signal (check resistors?), set 1 and/or set 2 DATA ERROR
#define erHiSignal          	15		// acm, no initial data:  hi input signal level DATA ERROR
#define erStop              	16              // dwe, all measurements stopped
#define erPause             	17              // dwe, all measurements paused
#define erLoCurrent         	18              // dwe, low current DATA ERROR
#define erXXMode            	19              // ag Not used or set anywhere
#define erClock             	20		// acm, RTC set earlier than last recorded measurement timestamp
#define erClock_rd_TWI      	21              // dwe, user should never see this used for debug
#define erwarnFlashYelHiUnnBal 	22 	        // dwe, not used // acm, new bit to drive yellow LED if balancing result too hi

#define erClock2             	23 		// acm, differentiate from erClock, this means RTC osc. stop bit error flag set, e.g. bad battery
#define er_noinit_need2bal	24		// acm, v2, good idea to indicate BHM not capable of taking measurements until balance DATA ERROR
#define erPhaseShiftSetSpot1  	25		// acm, v2.00, help isolate transgrid error, differentiate that erPhaseShiftSetx happened line 823 (not 827/1164/1187)
#define erPhaseShiftSetSpot2  	26		// acm, v2.00, help isolate transgrid error, differentiate that erPhaseShiftSetx happened line 827 (not 823/1164/1187)
#define erSide1Threshold  	27		// dwe, Side1 Threshold test failed
#define erSide2Threshold  	28		// dwe, Side2 Threshold test failed
#define erSide1Critical         29              // dwe, Side 1 had a critical error
#define erSide2Critical         30              // dwe, Side 2 had a critical error
#define erRainTimer             31              // dwe, rain timer is set (ie: raining etc) //DE 4-1-16 Rain timer is set
// acm, change mind, don't make an error, as it will flash Green and cause confusion.
// acm, rather set bits in Param structure that gets stored with data, then SCADA can read offline...
// could change mind back, flash green fast, clear on first measurment (like bit22)
// 3-28, yep, changed mind again.  Just store warn codes in DefectCode, toggle Yellow LED based on it and new stop_yellow_flashing flag
#define warnsetphaserot			1		// acm, v1.79 FW
#define	warnFlashYelHiUnnBal 	2 		// acm, new bit to drive yellow LED if balancing result too hi
//erSet1Off,erSet2Off,erPhaseShiftSet1,erPhaseShiftSet2,erChannels,erPhaseAOff,
//erPhaseBOff,erPhaseCOff,erUnitOff,erLoSignal,erHiSignal,erLoCurrent,er_noinit_need2bal,
//erRainTimer  //DE 4-1-16 added to mask

#define AnyDataError 0x8104DFF0  //DE 4-1-16 changed from 0x0104DFF0 to add erRainTimer
                                           
#include "KeyBoard.h"

extern ROM char ErrorMsg[MaxKnownError][MsgSize];

KEY ShowError(unsigned int DelaySec,unsigned long CurError,KEY WaitKeyDown);

#endif
