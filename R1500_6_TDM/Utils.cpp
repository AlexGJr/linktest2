#include <math.h>
#include "DefKB.h"
#include "Defs.h"
#include "LCD.h"
#include "StrConst.h"
#include "PicRes.h"
#include "KeyBoard.h"
#include "Graph.h"

long Round(float x)
{
if (fabs(x-((long)x)) >= 0.5) {
  if (x<0) return (((long)x)-1);
  else return (((long)x)+1);
} else return ((long)x);
}

void zerointstr(char *s1)
{
unsigned int i=0;
while (s1[i]!=0)
  {
  if (s1[i]==' ') s1[i]='0';
  i++;
  }
}

unsigned int DivAndRound(float f1,float f2,unsigned int MaxValue)
{float f;
 unsigned int i;

 f=f1/f2;
 if (f>(float)MaxValue) return MaxValue;
 i=f;
// if ((f-i)>=(f2/2)) i++;
 if ((f-i)>=0.5) i++;
 return i;
}

unsigned int YesNoMessage(char ROM *Str,char Font)
{ unsigned int len,CurPos=0;
  unsigned int key;

  SetFont(Font);
  len=fStrLen(Str);
  len=(112-(CurFontWidth*len))/2;
  while (1) {
     ClearLCD();
     OutString(len,0,Str);
     OutString(16,8,YesStr);
     OutString(80,8,NoStr);
     if (CurPos==0) OutPicture(8,8,RArr);
     else  OutPicture(72,8,RArr);
     Redraw();
     key = WaitReadKeyWithDelay(BREAKDELAY, KB_TIMEOUT);
     if ((key==KB_ENTER)&&(CurPos==0)) return KB_ENTER;
     if ((key==KB_ENTER)&&(CurPos==1)) return KB_ESC;
     if (key==KB_ESC) return KB_ESC;
     if ((key==KB_UP)||(key==KB_RIGHT)||(key==KB_DOWN)||(key==KB_LEFT))
        if (CurPos) CurPos=0; else CurPos=1;
  }

}

void SetEEPROMData(unsigned int Addr,char Data)
{
}

char PLMVersion(void)
{
#ifndef __emulator
  char res;
    DisableLCD(0xFF);
    SPI_Prepare(SPIFREQ_16000,0);
    SPI_SetCS(SPI_CS_ALTERA);
    res=SPI_WriteRead(0xE0);
    SPI_ClearCS();
    EnableLCD();
    return res;
#else
return 0;
#endif
}

char BoardType(void)
{
#ifndef __emulator
 char res;
    DisableLCD(0xFF);
    SPI_Prepare(SPIFREQ_16000,0);
    SPI_SetCS(SPI_CS_ALTERA);
    res=SPI_WriteRead(0xD0);
    SPI_ClearCS();
    EnableLCD();
    return res;
#else
return 8;
#endif
}

void PhaseRound(float *Phase)
{ long l;

if (*Phase>360)
  {
  l=*Phase/360;
  *Phase=*Phase-l*360;
  }
else
  {
  if (*Phase<0)
    {
    if (*Phase<-360) {
       l=*Phase/360;
       *Phase=*Phase-l*360;
    }
    *Phase+=360;
    }
  }

}
