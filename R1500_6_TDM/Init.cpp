
#include "BoardAdd.h"
#include "keyboard.h"
#include "LCD.h"
#include "RAM.h"
#include "UART.h"
#include "DrvFLASH.h"
#include "Error.h"
#include "DACDevice.h"
#include "RAM.h"

#ifndef __emulator

#include "WatchDog.h"
#include "Fram.h"
#include "rtc.h"
#include "SPI.h"
#include "Graph.h"
#include "DAC.h"
#include "USB.h"

#endif


unsigned int ModuleCode;
extern char C50;


void InitSAM7(void)
{

#ifndef __emulator

//DisableWatchDog();
EnableWatchDog();

// First, enable the clock of the PIO
AT91F_PMC_EnablePeriphClock ( AT91C_BASE_PMC, 1 << AT91C_ID_PIOA ) ;

//��������� ��� pullup ���������
AT91F_PIO_CfgPullup(AT91C_BASE_PIOA,0);
//�������� pullup
AT91F_PIO_CfgPullup(AT91C_BASE_PIOA,AT91C_PIO_PA8 | //�����������  ����������
                                    AT91C_PIO_PA27 | AT91C_PIO_PA28 | AT91C_PIO_PA29  //����������
				    | AT91C_PIO_PA0 |AT91C_PIO_PA1 |AT91C_PIO_PA5|  // 11-13 ACM pullup all inputs, e.g. if there is only 1 board being tested
                                    AT91C_PIO_PA12|
                                    AT91C_PIO_PA19|AT91C_PIO_PA20|AT91C_PIO_PA23
									);

AT91F_PIO_CfgInput(AT91C_BASE_PIOA, AT91C_PIO_PA0 |AT91C_PIO_PA1 |AT91C_PIO_PA5|
                                    AT91C_PIO_PA12|AT91C_PIO_PA17|AT91C_PIO_PA18|
                                    AT91C_PIO_PA19|AT91C_PIO_PA20|AT91C_PIO_PA23);

//KeyBoard
AT91F_PIO_CfgInput(AT91C_BASE_PIOA, AT91C_PIO_PA24|AT91C_PIO_PA25|AT91C_PIO_PA26|
                                    AT91C_PIO_PA27|AT91C_PIO_PA28|AT91C_PIO_PA29);

//Busy LCD
//AT91F_PIO_ClearOutput(AT91C_BASE_PIOA, AT91C_PIO_PA31);  // acm, 11-5-10 comment out, this is an input!
AT91F_PIO_CfgInput(AT91C_BASE_PIOA, AT91C_PIO_PA31);	// acm, 11-5-10, fix minor bug, this is an input!
// what apparently was happening is if Display plugged in, then removed, SPI_WriteRead2() would still see BUSY from display,
// so FW thought display connected. Other than wasting 800+ microseconds, not big deal.  One of the new changes is to increase
// that timeout so must fix.

//CS SPI
AT91F_PIO_SetOutput(AT91C_BASE_PIOA, SPI_CS0 | SPI_CS1 | SPI_CS2 | SPI_CS3);
AT91F_PIO_CfgOutput(AT91C_BASE_PIOA, SPI_CS0 | SPI_CS1 | SPI_CS2 | SPI_CS3);

//ADC
AT91F_PIO_CfgInput(AT91C_BASE_PIOA, AT91C_PIO_PA21);

//Module ID
AT91F_PIO_SetOutput(AT91C_BASE_PIOA, AT91C_PIO_PA16);
AT91F_PIO_CfgOutput(AT91C_BASE_PIOA, AT91C_PIO_PA16);

//Relay
AT91F_PIO_ClearOutput(AT91C_BASE_PIOA, AT91C_PIO_PA17);
AT91F_PIO_CfgOutput(AT91C_BASE_PIOA, AT91C_PIO_PA17);
AT91F_PIO_ClearOutput(AT91C_BASE_PIOA, AT91C_PIO_PA18);
AT91F_PIO_CfgOutput(AT91C_BASE_PIOA, AT91C_PIO_PA18);

// acm, transgrid debug, redefine START1 pin scope point
AT91F_PIO_CfgOutput(AT91C_BASE_PIOA, AT91C_PIO_PA30);
AT91F_PIO_ClearOutput(AT91C_BASE_PIOA, AT91C_PIO_PA30);
// START2 partially coded, need to stub above to use
//AT91F_PIO_CfgOutput(AT91C_BASE_PIOA, AT91C_PIO_PA0);
//AT91F_PIO_ClearOutput(AT91C_BASE_PIOA, AT91C_PIO_PA0);

//USBReadAddr=(volatile u8 *)UartReadBuf[USBPort];
//USBInit();

// ��������� SPI
SPI_ClearCS();
AT91F_PMC_EnablePeriphClock ( AT91C_BASE_PMC, 1<< AT91C_ID_SPI ) ;
// MISO, MOSI, SPCK = A
AT91F_PIO_CfgPeriph(AT91C_BASE_PIOA,AT91C_PIO_PA12 | AT91C_PIO_PA13 | AT91C_PIO_PA14, 0);
AT91C_BASE_SPI->SPI_IDR=0xFFFFFFFF;

InitRTC();

// Reset
AT91F_PIO_ClearOutput(AT91C_BASE_PIOA, AT91C_PIO_PA2);
AT91F_PIO_CfgOutput(AT91C_BASE_PIOA, AT91C_PIO_PA2);
AT91F_PIO_SetOutput(AT91C_BASE_PIOA, AT91C_PIO_PA2);

InitFram();
SetFont(Font8x6);
#endif

InitKeyboard();
AutoRepeatDisable();
InitRAM();
InitFlash(0,512);
if (!InitDrvFlash()) {Error|=((unsigned long)1<<erFlashReadFail);  Error|=((unsigned long)1<<erFlashWriteFail); }

#ifndef __emulator
USB_Usart_Init(); //ag 2.10 changed from Usart_Init. The only call besides USB
InitLCD();
 if (!AT91F_PIO_IsInputSet(AT91C_BASE_PIOA,AT91C_PIO_PA0))  //low
 {
   C50=1; 
 }
#endif
 
// acm, note, additional configurations of PA15 done in PrepareTimer()/SetFilterPMW() in main().
// this used to blink LED's, more importantly output 8khz clock for switched capacitor LPF's to operate, fc=80hz
// took a while to figure this out.
}

