#ifndef _adc_h
#define _adc_h

#include "Defs.h"

//���������� �������� ������� ���
#define MainADCChannels 4

#define rdTest 0
#define rdMeasure 1
#define rdADCTest 2

//#define UpLoadCount            4800
//#define ReadCount              4800
//#define BalansingUpLoadCount   2400

//#define ReadCount              6120
//#define UpLoadCount            6000
#define UpLoadCount            4800
#define ReadCount              5120
#define BalansingUpLoadCount   1200
#define BalansingReadCount     1320

#define HumidityChannel 0
#define InTemperatureChannel 1
#define LTCChannel 2

#define ADCInMax    490.0
#define ADCInMax8   460.0
#define ADCInMax8_2 980.0

#define ADCMax    14400.0
#define ADCMax8   14400.0
#define ADCMax8_2 30700.0

extern u32 SaveCalibrSin;

//������� ������������� ������������ �������� �� �������
extern char AmplifIndex[MaxTransformerSide][3];

extern short *AddrSig[MainADCChannels];

extern ROM float GainValue[MainADCChannels];
extern ROM float GammaGainValue[MainADCChannels+2];

extern ROM char CurrentOnCh[MaxBushingCountOnSide];

extern float PhasesShift[MaxTransformerSide][MainADCChannels];
extern float ADCPhasesShift[MaxTransformerSide][MainADCChannels];

extern short NullLine[MaxTransformerSide][MainADCChannels+1];

extern char Ch[MainADCChannels];

extern volatile u32 EndRead;

extern char CurShift;
//Load Current
extern float  LoadCurrentA,LoadCurrentB,LoadCurrentC;
extern u32 LoadCurrentP;
extern u32 MinReadData;

void ReadData(char ChangeK1,char ChangeK4,char ReadChannels, char TrSide, char ReCalcNull, char SetCurrent);
float ReadTemperatureADCValue(char Channel);
int ReadTemperature(char Channel);
void ReadActiveLoad(void);
char ReadHumidity(void);
int ReadLTC(void);
float RunCalibrAmpl(char Side, float *AmplCoeff);
float ReadIntTemperature(void);
char WatchFunc(float *WAmpl);
void StartRead(void);
void PrepareRead(char Mesurement);
void ReCalcNullLines(unsigned int NReadCount, char Side, char ReSetCh1);



#endif
