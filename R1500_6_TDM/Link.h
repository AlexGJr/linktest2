#include "TypesDef.h"

//����������� �� ��D �������
extern char ExtTemperature[4];
//�������� �������� �� ��D �������
extern s16 ExtLoadActive;					// acm, v2 redefine to s16
//�������� ���������� �� ��D �������
extern s16 ExtLoadReactive;				// acm, v2 redefine s16
//��������� �� ��D �������
extern char ExtHumidity;
//��������� ��� �� ��D �������
extern char ExtRPN;

// acm,v2
extern s16 LoadCurrent2;
extern s16 LoadCurrent3;
extern s16 voltage2;
extern s16 voltage1;
extern s16 voltage;
extern s16 EventTrigger;
#define NewMeasurement 0x01 //bit 1
#define ConfigUpload 0x02   //bit 2
#define NewEventLog 0x04    //bit 3
#define NewlyBalanced 0x08  //bit 4

int SendAnswer(unsigned int Cmnd,unsigned int Info1,unsigned int Info2, unsigned int Info3,unsigned int Info4, char *BufMess);
int ModBusProcessString(char *BufMess ,char i);
int GetRegisterValue(int raddr, int rlen, char* data);
int SetRegisterValue(int raddr, int rlen, char* data);
int ScanMessages(void);
void SetDeviceBusy(void);
void SetDeviceNoBusy(void);
void SendString(void);
char IsRegAddressCorrect(int StartAddr,int Count);
char IsCoilAddressCorrect(int StartAddr,int Count);

