#ifndef _ram_h
#define _ram_h

#include "cListBox.h"
#include "ADCDevice.h"
#include "TypesDef.h"
#include "USB.h"

#define SetPage(a)
#define GetPage() 0

#define ParametersPage 0
#define TempPage 0

#define SetPage0
#define SetPage1
#define SetPage2
#define SetPage3

#define GammaPage0 0
#define GammaPage1 0
#define GammaPage2 0
#define GammaPage3 0
#define GammaPage4 0
#define GammaPage5 0
#define GammaPage6 0
#define GammaPage7 0

extern s16 Sig[4][ReadCount];

extern s16* BaseAddrSig1[2];
extern s16* BaseAddrSig2[2];
extern s16* BaseAddrSig3[2];
extern s16* BaseAddrSig4[2];

#define MeasListTempAddr ((void*)&Sig[0][0])

#ifndef __arm
extern u16 *MeasList;
#endif
extern TCheckListBox *LB;

void InitRAM();

#endif
