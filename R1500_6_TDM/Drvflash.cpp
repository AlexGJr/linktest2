/******************************************************************************************

   >>> DrvFlash ������� ��� ������ � ���� <<<

   ���� ���������� ��������� 26.01.2005

   �������� ������:

     �������������:
       InitDrvFlash - �������������
       OpenFlash - �������� ������ ������
       �������� ! ����������� ������������ ��� ������� � ����������� � ����� �������
     �������� ������:
       ClearFlash     - ������������� ��� �������� � 0xFF, ��������� ������� �����
       FreeBlockFlash - ���������� ������ ���������� ����� � ������
       FreeFlash      - ���������� ������ ���������� ����� � ������
       SizeFlash      - ���������� ������ Flash � ������
       FileDelete     - �������� ���� ��� �������
     ������ ������ ������
       FileCreate     - �������� ����� ������
       FileClose      - ��������� ���� ������ ����� ��������
       FileOpen       - �������� ����� ������
       FileRead       - ������ ������ �� �����, ������������ ����� ������� FileCreate ��� FileOpen
       FileWrite      - ������ ������ � ����, ������������ ����� ������� FileCreate ��� FileOpen
     �������
       FlashRebildFreeBlock - ������� ��� ����� ���������� ��� ��������� � ������������� �� � 0xFF
       CreateList - ������� ������������� �� ������� ������ ��������������� ������

     �������� �����������:
     26.01.2005
       - ��������� ������� FreeBlockFlash
       - �������� ������� FileCreate, ������ ����������� ��������� ����� � ���� ����������
         ���������� FlashRebildFreeBlock

******************************************************************************************/
#include "drvflash.h"
#include <stdlib.h>
#include <string.h>

#include "SysUtil.h"
#include "flash.h"

#ifndef __arm
  #include "ram.h"
#endif

#ifdef __emulator
  TFatExtraData Tf_extra_data;
  TFatExtraData *f_extra_data=&Tf_extra_data;
#endif

char f_init = 0;
char f_open = 0;
u32 f_beg_claster = 0;
u32 f_end_claster = 0;
u16 f_bad_block=0;

/*
����� ����� ���������� FAT, ���������� � ����� ������� ������ // Number of unit expansion FAT, is selected in what area write
    equFatExpandedMain     - ������� ������ // The scope of the data
    equFatExpandedProgram  - ������� �������� // The area programs
    equFatExpandedHide     - ������� ������� // Hidden area
*/
//char FatExpandedBlock        = equFatExpandedMain;

#define mask_free   0x03
#define mask_first  0x02
#define mask_middle 0x01
#define mask_null   0x00

ROM char lo_fat_mask[4]={0xFC,0xF3,0xCF,0x3F};
ROM char up_fat_mask[4]={0x03,0x0C,0x30,0xC0};
//---
char get_fat_mask(u16 index)
{
char n;
n   = FatBuf[index/4];
n  &= up_fat_mask[index%4];
n >>= ((index%4)*2);
return n;
}
//---
void set_fat_mask(u16 index, char mask)
{
char n;
n  = FatBuf[index/4];
n &= lo_fat_mask[index%4];
n |= (mask & 0x03) << ((index%4)*2);
FatBuf[index/4] = n;
}
//---
char f_check_ff(TIndexClaster *index)
{
char i;
for (i=0;i<sizeof(TIndexClaster);i++)
    if ( ((char *)index)[i]!=0xFF) return 0;
return 1;
}
//---
char f_check_index(TIndexClaster *info)
{
if (f_check_ff(info)==1) return 1;

if (info->id==0xFF) return 0;

if ((info->bad==0xFF) &&
    ((info->seek==0xFFFFFFFF) || (info->seek<FatBlockCount)) &&
    ((info->prev==0xFFFFFFFF) || (info->prev<FatBlockCount))
    ) return 1;

return 0;
}
//---
char f_get_index(u16 index, TIndexClaster *info)
{
if (FlashBadBlock(index)) return 0;

if (FlashReadBlockDop(index,(char*)info,0,sizeof(TIndexClaster))==sizeof(TIndexClaster))
   {
   return f_check_index(info);
   }
return 0;
}
//---
char f_set_index(u16 index, TIndexClaster *info)
{
if (f_check_index(info)!=1) return 0;
return (FlashWriteBlockDop(index,(char*)info,0,sizeof(TIndexClaster))==sizeof(TIndexClaster));
}
//---
void f_search_claster(u32 *free, u32 *first, u32 *middle, u32 *null)
{
#ifndef __arm
char page;
#endif
u32 i;

#ifndef __arm
page=GetPage();
SetPage(FlashMemoryPage);
#endif
*free=0; *first=0; *middle=0; *null=0;
for (i=f_beg_claster;i<f_end_claster;i++)
    {
    switch (get_fat_mask(i))
      {
      case mask_free: (*free)++; break;
      case mask_first: (*first)++; break;
      case mask_middle: (*middle)++; break;
      case mask_null:    (*null)++; break;
      }
    }
if ((*null)>=f_bad_block) (*null)-=f_bad_block;

#ifndef __arm
SetPage(page);
#endif
}
//---
u32 f_free_claster(void)
{
u32 free, first, middle, null;
f_search_claster(&free, &first, &middle, &null);
return free;
}
//---
u32 f_next_block(u32 i)
{
TIndexClaster index;
if (f_get_index(i,&index))
   if ((index.seek >= f_beg_claster) && (index.seek < f_end_claster))
      return index.seek;
return 0;
}
//---
long f_write_read(f_type_write_read func,TFile *f, char *buf, u32 offset,u32 count)
{
u32 i,j,seek,claster;
u16 RWBytes;

if (count==0) return 0;
if ( ((float)offset+(float)count)/(float)FatBlockLen > f->count )
   {
   if ( offset/FatBlockLen < f->count ) {count=f->count*FatBlockLen-offset;}
   else return 0;
   }

claster = f->id;

j=offset/FatBlockLen;
for (i=0;i<j;i++)
    {
    claster=f_next_block(claster);
    offset -= FatBlockLen;
    }

if (offset)
   {
   if (count>FatBlockLen-offset) RWBytes=FatBlockLen-offset;
   else                          RWBytes=count;
   if (func(claster,buf,offset,RWBytes)!=RWBytes) return 0;
   seek=RWBytes;
   if (seek==count) return seek;
   claster=f_next_block(claster);
   }
else
   {
   seek=0;
   }

j=(count-seek)/FatBlockLen;
for (i=0;i<j;i++)
   {
   if (func(claster,&buf[seek],0,FatBlockLen)!=FatBlockLen) return 0;
   seek+=FatBlockLen;
   if (seek==count) return seek;
   claster=f_next_block(claster);
   }

if (seek<count)
   {
   j=count-seek;
   if (func(claster,&buf[seek],0,j)!=j) return 0;
   seek+=j;
   }

return seek;
}
//---

u16 f_add_block(TFile *f,u16 count)
{
TIndexClaster index,old;
u32 i,prev,j;
#ifndef __arm
char page;
#endif
u16 res=0;

#ifndef __arm
page=GetPage();
SetPage(FlashMemoryPage);
#endif
if ((f->fm & fmResize)==0) {goto f_add_block_end;}

if ((f->count == 0) || (f->count > FatBlockCount)) {goto f_add_block_end;}

if ((f->id < f_beg_claster) || (f->id >= f_end_claster)) {goto f_add_block_end;}

if (f_free_claster()<count) {goto f_add_block_end;}

//������� ������ ���� � ��������� ��� �� �����������
prev=f->id;
if (f_get_index(prev,&index)==0) {goto f_add_block_end;}
if ((index.id!=id_block_first) && (index.id!=id_block_one)) {goto f_add_block_end;}

//����� ��������� ����
for (i=0;i<(f->count-1);i++)
    prev=f_next_block(prev);

if (f_get_index(prev,&index)==0) {goto f_add_block_end;}

old=index;

j = 0;
count += f->count;
for (i=f_beg_claster;i<f_end_claster;i++)
    {
    if (get_fat_mask(i)==mask_free)
       {
       if (index.id==id_block_end) {index.id=id_block_middle;}

       index.seek=i;
       if (f_set_index(prev,&index)==0) {goto f_add_block_end;}

       index.id = id_block_middle;
       index.seek=0xFFFFFFFF;
       index.prev=prev;

       if (f_set_index(i,&index))
          {
          f->count++;
          j++;
          prev=i;
          old=index;
          set_fat_mask(i,mask_middle);
          }
       else //���� �� ������ ��������, ����������� ������ ������
          {
          index=old;
          set_fat_mask(i,mask_null);
          }
       }
    if (f->count>=count) break;
    }

res=j;

f_add_block_end:
#ifndef __arm
SetPage(page);
#endif
return res;
}


/*

  ��������, ���������� equFatBeginClaster � equFatEndClaster ������ ����
  ����������� ��������� �� FatBlocksInClaster (����� ������ � ��������), �����
  ����� ������� �����
  Attention, variables and equFatBeginClaster equFatEndClaster must be
  aligned to the FatBlocksInClaster (number of blocks in the cluster), otherwise it will be
  a great for troll

*/
char InitDrvFlash(void)
{
f_init=InitFlash(0,FatPageSize);
if (f_init)
   {
   f_beg_claster = FatBlocksInClaster;
   f_end_claster = FatBlockCount;
   }
return f_init;
}

/*

   ������� ���, ��� ����������� �������������

*/
char ClearFlash(void)
{
u32 i,j;
#ifndef __arm
char page;
#endif
char k,mask;

#ifndef __arm
page=GetPage();
SetPage(FlashMemoryPage);
#endif
j=f_beg_claster;
for (i=f_beg_claster/FatBlocksInClaster;i<f_end_claster/FatBlocksInClaster;i++)
    {
    if (FlashClearBlock(i,0)==1) {mask=mask_free;}
    else                         {mask=mask_null;}
    for (k=0;k<FatBlocksInClaster;k++)
        {set_fat_mask(j,mask); j++;}
    }
#ifndef __arm
SetPage(page);
#endif
return 1;
}

/*

  ���������� ������ ���������� ����� � ������

*/
u32 FreeBlockFlash(void)
{
u32 free, first, middle, null;
f_search_claster(&free, &first, &middle, &null);
return (free+null);
}

/*

  ���������� ������ ���������� ����� � ������

*/
u32 FreeFlash(void)
{
return FreeBlockFlash()*FatBlockLen;
}
/*

  ���������� ������ Flash � ������

*/
u32 SizeFlash(void)
{
return (f_end_claster-f_beg_claster)*FatBlockLen;
}
/*

  ������� � ������ ������ �������� // Develop in the memory array indices

*/

void f_open_flash(u16 beg,u16 end)
{
u16 i;
TIndexClaster index;
for (i=beg;i<end;i++)
  {
  if (f_get_index(i,&index))
     {
      switch (index.id)
        {
        case id_block_free:    set_fat_mask(i,mask_free); break;
        case id_block_one:
        case id_block_first:   /*if (f_extra_data->f_end_id==0)
                                  {
                                  f_extra_data->f_end_id=i;
                                  f_extra_data->f_end_date=index.date;
                                  }
                               else
                                  {
                                  if (index.date > f_extra_data->f_end_id)
                                      {
                                      f_extra_data->f_end_id=i;
                                      f_extra_data->f_end_date=index.date;
                                      }
                                  }
                               if (f_extra_data->f_first_id==0)
                                  {
                                  f_extra_data->f_first_id=i;
                                  f_extra_data->f_first_date=index.date;
                                  }
                               else
                                  {
                                  if (index.date < f_extra_data->f_first_id)
                                      {
                                      f_extra_data->f_first_id=i;
                                      f_extra_data->f_first_date=index.date;
                                      }
                                  }*/
                               set_fat_mask(i,mask_first);
                               break;
        case id_block_middle:
        case id_block_end:     set_fat_mask(i,mask_middle); break;
        default:               set_fat_mask(i,mask_null); break;
        }
     }
  else
     {
     set_fat_mask(i,mask_null);
     f_bad_block++;
     }
  }
}

char OpenFlash(void)
{
//u16 i;
#ifndef __arm
char page;
#endif
//TIndexClaster index;

if (!f_init) return 0;

#ifndef __arm
page=GetPage();
SetPage(FlashMemoryPage);
#endif
//f_extra_data->f_end_id=0;
//f_extra_data->f_end_date=0;

//f_extra_data->f_first_id=0;
//f_extra_data->f_first_date=0;

f_bad_block=0;
f_open_flash(f_beg_claster,f_end_claster);

#ifndef __arm
SetPage(page);
#endif
f_open=1;
return f_open;
}
/*

  �������� ������ ����� // The creation of a new file

*/
int FileCreate(TFile *f,u16 Attr, u32 Size,_TDateTime *aDate)
{
TIndexClaster index;
_TDateTime date;
u32 i,j=1;
#ifndef __arm
char page;
#endif
int res=0;
u32 free, first, middle, null;


#ifndef __arm
page=GetPage();
SetPage(FlashMemoryPage);
#endif
if (aDate==0) DateTimeNow(&date);
else date =*aDate;

//���� ����� ������ - ��������� ����� ������ // If you set the size to calculate the number of blocks
if (Size)
   {
   j=Size/FatBlockLen+((Size%FatBlockLen)?1:0);
   }

//��������� ��������� ����� �� ����� // Check the free place in positions
f_search_claster(&free,&first,&middle,&null);
//���� ��������� ������ ��� ���������, ������� ��� ����� ������... // If free is less than required, look what you can do to help ...
if (free<j)
   {
   //���� ���������+��������� ������ ��� ���������, �� ������... // If free remote is less than required, the ?????? ...
   if ((free+null)<j) goto FileCreateEnd;
   //������ ������ ������ � ������� ��������� ����� // We are carrying all blocks and look free space
   FlashRebildFreeBlock();
   f_search_claster(&free,&first,&middle,&null);
   //���� ��������� ������ ��� ���������, �� ������ ������!!! // If free is less than required, the complete ??????!!!
   if(free<j) goto FileCreateEnd;
   }

f->fm    = fmRead | fmWrite | fmResize;
f->id    = 0;
f->count = 0;

index.id        = id_block_first;
index.date      = date;
index.bad       = 0xFF;
index.attr      = Attr;
index.seek      = 0xFFFFFFFF;
index.prev      = 0xFFFFFFFF;

//������� ������ ���� The first block ??????? //The first block found
for (i=f_beg_claster;i<f_end_claster;i++)
    {
    if (get_fat_mask(i)==mask_free)
       {
       if (f_set_index(i,&index))
          {
          f->id=i;
          f->count++;
          set_fat_mask(i,mask_first);
          break;
          }
       else
          {
          set_fat_mask(i,mask_null);
          }
       }
    }
if (f->count==0) {goto FileCreateEnd;}

//���� ���������� - ������� ��������� ����� // If you want to create the rest of the blocks
if (j>f->count)
   {
   f_add_block(f,j-f->count);
   if (f->count != j) {goto FileCreateEnd;}
   }

res=1;
/*
if (date > f_extra_data->f_end_date)
   {
   f_extra_data->f_end_id=f->id;
   f_extra_data->f_end_date=date;
   }
if (date < f_extra_data->f_first_date)
   {
   f_extra_data->f_first_id=f->id;
   f_extra_data->f_first_date=date;
   }
*/
FileCreateEnd:
#ifndef __arm
SetPage(page);
#endif
return res;
}
/*

  �������� ����� �� ������ ��� ������

*/
int FileOpen(TFile *f,u16 id, unsigned int Mode)
{
TIndexClaster index;

if ((Mode & (fmRead | fmWrite | fmResize))==0) return -1;

if (f_get_index(id,&index)==0) return -2;

if ((index.id!=id_block_first) && (index.id!=id_block_one)) return -3;
if ((index.seek!=0xFFFFFFFF) && (index.seek>=FatBlockCount)) return -4;
if (index.prev!=0xFFFFFFFF) return -5;

f->fm=Mode;
f->id=id;
f->count=1;
if (index.seek==0xFFFFFFFF) return 1;

while (1)
  {
  f->count++;
  if (f_get_index(index.seek,&index)==0) return -6;
  if ((index.id!=id_block_middle) && (index.id!=id_block_end)) return -7;
  if ((index.seek!=0xFFFFFFFF) && (index.seek>=FatBlockCount)) return -8;
  if (index.seek==0xFFFFFFFF) {return 1;}
  }
}
/*

  �������� �����

*/
int FileClose(TFile *f)
{
u32 i,j;
TIndexClaster index;
#ifndef __arm
char page;
#endif
int res=0;


#ifndef __arm
page=GetPage();
SetPage(FlashMemoryPage);
#endif
if (f->count==1) {res=1; goto FileCloseEnd;}

if (f_get_index(f->id,&index)!=1) {goto FileCloseEnd;}
if (index.id!=id_block_first) {goto FileCloseEnd;}

for (i=1;i<f->count;i++)
  {
  if ((index.seek==0)||(index.seek>FatBlockCount)) {goto FileCloseEnd;}
  j=index.seek;
  if (f_get_index(j,&index)!=1) {goto FileCloseEnd;}
  if (index.id==id_block_end) {res=1; goto FileCloseEnd;}
  if (index.id!=id_block_middle) {goto FileCloseEnd;}
  }
if (index.seek==0xFFFFFFFF)
   {
   index.id=id_block_end;
   if (f_set_index(j,&index)!=1) {goto FileCloseEnd;}

   res=1;;
   }

FileCloseEnd:
#ifndef __arm
SetPage(page);
#endif
return res;
}
/*

  ������ ������ �� �����

*/
long FileRead(TFile *f, char *buf, u32 offset,u32 count)
{
if ((f->fm & fmRead)==0) return 0;
return f_write_read(&FlashReadBlock, f, buf, offset, count);
}

/*

  ������ ������ � ����

*/
long FileWrite(TFile *f, char *buf, u32 offset,u32 count)
{
u32 i;
if ((f->fm & fmWrite)==0) return 0;

i=offset+count;
i=(i/FatBlockLen) + ((i%FatBlockLen)?1:0);

if (i > f->count)
   {
   if ((f->fm & fmResize)==0) return 0;
   if (f_free_claster() < (i - f->count)) return 0;
   f_add_block(f,i - f->count);
   if (i > f->count) return 0;
   }

return f_write_read(&FlashWriteBlock, f, buf, offset, count);
}

/*

    �������� �����

*/

int FileDelete(u32 id)
{
u16 i,n;
TFile f;
TIndexClaster index;
#ifndef __arm
char page;
#endif
int res=0;

#ifndef __arm
page=GetPage();
SetPage(FlashMemoryPage);
#endif
if (FileOpen(&f,id,fmRead | fmWrite | fmResize)==1)
   {
   n=f.id;
   for (i=0;i<f.count;i++)
       {
       if (f_get_index(n,&index))
          {
          index.id=id_block_del;
          f_set_index(n,&index);
          set_fat_mask(n, mask_null);
          n=index.seek;
          if ((n<f_beg_claster) || (n>=f_end_claster)) break;
          }
       else
          break;

       }
   res=1;
   }
#ifndef __arm
SetPage(page);
#endif
return res;
}

/*



*/
int FlashRebildBadBlock(void)
{
return 1;
}

/*

  ����������� ��������� �����  // Recalculate free blocks

*/
int FlashRebildFreeBlock(void)
{
#ifndef __arm
char page;
#endif
u32 i;

#ifndef __arm
page=GetPage();
SetPage(FlashMemoryPage);
#endif
f_bad_block=0;
for (i=f_beg_claster/FatBlocksInClaster;i<f_end_claster/FatBlocksInClaster;i++)
    {
    FlashRewriteBlock(i);
    }
f_open_flash(f_beg_claster,f_end_claster);

#ifndef __arm
SetPage(page);
#endif
return 1;
}


/*

    �������� �������������� �� ������� ������� ��������������� ������

*/

int FlashReadPageInline(u32 NStr,char* Buf,u16 off,u16 len);

typedef void (*f_sort_func)(_TDateTime x,s16 *i,s16 *j);

u16 *sortbuf;
f_sort_func sortfunc;

_TDateTime sortdate(int i)
{
_TDateTime date;
FlashReadPageInline(sortbuf[i]*FatPageInBlock,(char *)&date,FlashPageLen+1,sizeof(date));
return date;
}

void sort_forward(_TDateTime x,s16 *i,s16 *j)
{
u16 i1,j1;
i1=*i; j1= *j;
while (sortdate(i1)>x) {i1=i1+1;}
while (x>sortdate(j1)) {j1=j1-1;}
*i=i1; *j=j1;
}

void sort_back(_TDateTime x,s16 *i,s16 *j)
{
u16 i1,j1;
i1=*i; j1= *j;
while (sortdate(i1)<x) {i1=i1+1;}
while (x<sortdate(j1)) {j1=j1-1;}
*i=i1; *j=j1;
}



void sort(int l, int r)
{
s16 index1,index2;
_TDateTime x;
u16 tmp;

  index1=l;
  index2=r;
  x=sortdate((l+r) / 2);
  do
    {
    sortfunc(x,&index1,&index2);
    //while (sortdate(i)>x) {i=i+1;}
    //while (x>sortdate(j)) {j=j-1;}
    if (index1<=index2)
       {
       tmp=sortbuf[index1];
       sortbuf[index1]=sortbuf[index2];
       sortbuf[index2]=tmp;
       index1=index1+1;
       index2=index2-1;
       }
    }
  while (index1<=index2);
  if (l<index2) sort(l,index2);
  if (index1<r) sort(index1,r);
}


void sort_buble(u16 count)
{
u16 i,j;
u16 c;
char flag;
TIndexClaster i1,i2;


for (i=0; i<count-1; i++)
    {
    flag=1;
    for (j=count-1; j>i; j--)
        {
        f_get_index(sortbuf[j],&i1);
        f_get_index(sortbuf[j-1],&i2);
        if (i1.date>i2.date)
           {
           c=sortbuf[j];
           sortbuf[j]=sortbuf[j-1];
           sortbuf[j-1]=c;
           flag=0;
           }
         }
    if (flag) break;
    }
}

void CreateList(u16 *buf,u32 *count,char direction)
{
u32 i,j;
#ifndef __arm
char page;
#endif
u16 c;

j=0;
#ifndef __arm
page=GetPage();
SetPage(FlashMemoryPage);
#endif
for (i=f_beg_claster;i<f_end_claster;i++)
    {
    if (get_fat_mask(i)==mask_first)
       {
#ifndef __arm
       SetPage(page);
#endif
       buf[j++]=(u16)i;
#ifndef __arm
       SetPage(FlashMemoryPage);
#endif
       }
    }

#ifndef __arm
SetPage(page);
#endif
switch (j)
  {
  case 0:
  case 1: break;
  case 2:
     {
     sortfunc = (direction==stForward)?&sort_forward:&sort_back;
     sortbuf=buf;
     i=0;
     if (direction==stForward)
        if (sortdate(0)<sortdate(1)) i=1;

     if (direction==stBack)
        if (sortdate(0)>sortdate(1)) i=1;

     if (i)
        {
        c=sortbuf[0];
        sortbuf[0]=sortbuf[1];
        sortbuf[1]=c;
        }

     break;
     }
  default:
     {
     sortfunc = (direction==stForward)?&sort_forward:&sort_back;
     sortbuf=buf;
     sort(0, j-1);
     break;
     }
  }
(*count)=j;
}

#include "DrvFlashTest.h"
