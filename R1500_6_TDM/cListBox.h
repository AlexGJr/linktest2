#ifndef _clistbox_h
#define _clistbox_h

#include "keyboard.h"

#define MAXLBLINES 60
#define MAXSTRLEN  25

typedef char* (*FuncGetStr)(short Index);
typedef char  (*FuncGetCh)(short Index);


typedef struct {
  unsigned short CurPosition,Count,Width,NumStrs,X,Y,TopLine;
  char  Type;  //ListBox == 0
               //CheckListBox==1
  char  Font;
  unsigned short UpKey, DwnKey, ActnKey;
  unsigned short PgUpKey, PgDwnKey;

  char Dynamic;

  FuncGetStr GetStr;
  FuncGetCh  GetCh;

  char  StrArray[MAXLBLINES][MAXSTRLEN];
  char  Checked[MAXLBLINES];

} TCheckListBox;


#define szCheckListBox sizeof(TCheckListBox)
#define szDynamicCheckListBox sizeof(TCheckListBox)-MAXLBLINES-MAXLBLINES*MAXSTRLEN  //

void InitCheckListBox(TCheckListBox *aLB,unsigned int X,unsigned int Y,unsigned int Width,unsigned int NumStrs,char Font, char aType);
void SetDynamicCLB(TCheckListBox *aLB, int CountStr, FuncGetStr GetStr, FuncGetCh GetCh);
void SetCheckListBoxKeys(TCheckListBox *aLB,unsigned int aUpKey, unsigned int aDwnKey, unsigned int aActnKey, unsigned int aPgUpKey, unsigned int aPgDwnKey);
void InsertCLBLine(TCheckListBox *aLB,unsigned int Position, char *aLine, char aChecked);
void EditCLBLine(TCheckListBox *aLB,unsigned int Position, char *aLine, char aChecked);
void DeleteCLBLine(TCheckListBox *aLB,unsigned int Position);
void ClearCheckListBox(TCheckListBox *aLB);
void CLBScrollUp(TCheckListBox *aLB, char *NeedRedraw);
void CLBScrollDown(TCheckListBox *aLB, char *NeedRedraw);
void ReDrawCheckListBox(TCheckListBox *aLB);
void HandleCheckListBox(TCheckListBox *aLB, KEY key, char *NeedRedraw);

#endif
