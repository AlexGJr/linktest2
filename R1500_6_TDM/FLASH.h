#ifndef _flash_h
#define _flash_h

#include "flashmem.h"
#include "TypesDef.h"

extern u32 FlashPageLen;        // ������ ������ ��� �������� Flash
extern u32 FlashPageDopLen;     // �������������� ������ �� �������� Flash
extern u32 FlashPagesInBlock;   // ������� � ����� Flash
extern u32 FlashAllBlocksCount; // ����� ������ �� Flash
extern u32 FlashBlockLen;       // ����� ����� �� Flash
extern u32 FlashFullPageLen;    //(FlashPageLen+FlashPageDopLen)

typedef int (*f_type_write_read)(u32 NStr,char* Buf,u16 off,u16 len);

extern u16  FlashID;
extern u16  FatPageInBlock;      
extern u16  FatBlockLen;
extern u16  FatBlocksInClaster;
extern u32  FatBlockCount;

int  InitFlash(int ForceScan, u16 aFatBlockLen);
char FlashBadBlock(u32 NStr);
int  FlashClearBlock(u32 NStr,char test);

int  FlashReadBlockDop(u32 NStr,char* Buf,u16 off,u16 len);
int  FlashReadBlock(u32 NStr,char* Buf,u16 off,u16 len);

int  FlashWriteBlockDop(u32 NStr,char* Buf,u16 off,u16 len);
int  FlashWriteBlock(u32 NStr,char* Buf,u16 off,u16 len);

int FlashRewriteBlock(u32 NClaster);

u16 FlashBadBlockCount(void);



/*
unsigned int ReadFlash(unsigned int Block,void *buf,unsigned int offset,unsigned int count);
unsigned int WriteFlash(unsigned int Block,void *buf,unsigned int offset,unsigned int count);
unsigned int WriteFlashNoClear(unsigned int Block,void *buf,unsigned int offset,unsigned int count);
unsigned int ClearFlash(unsigned int Block);
unsigned int FlashID(void);
*/
void         TestFlashLow(void);

void Tst(void);

/*
// ��������� ������
void TestFlash1(void);
void TestFlash2(void);
void TestFlash3(void);
void TestFlash4(void);
*/

#endif

