#ifndef _Defs_h
#define _Defs_h

#ifndef __emulator
#define ROM const
#else
#define ROM
#endif

#include "TypesDef.h"

//------------------------------------------------------------------------------
//������� �����, �� ������ ��������� ����� �� ����� � MAIN � ������ � TrStr.h
#define  SoftVersionValue 0x0202;  // acm, bug, needs to correlate to TRSTR.cpp ROM char trVersionValue[]=, 


#define HeaterTemperatureTrend 3

//#define CalibrRepeat 5
#define CalibrRepeat 3

//#define Beep()

#define     Font8x8            1
#define     Font5x6            2
#define     Font6x5            2
#define     Font6x8            3
#define     Font8x6            3

#define smrReDrawScrl           254

#define ScreenHeight            16
#define ScreenWidth             112
#define ScreenWidthDiv8         14

#define LEN 15



//������� ��������� Led
extern volatile char Led;
//������� ��������� Relay
extern volatile char Relay;
//������� ��������� Coeff
extern char Gain;

//Default Frequency
//0-50��
//1-60��
#ifndef engl
  #define DefaultFrequency 0
#else
  #define DefaultFrequency 1
#endif


#ifndef __emulator
#define ADCStep (float)0.091552734375   // acm, magic constant, converts input mV to Source mA
#else
//#define ADCStep (float)0.091552734375
#define ADCStep (float)0.1
#endif

//#define MaxTransformerSide 4    //HVSide=0
//#define MaxCorrelationSide 3    //LVSide=0
#define MaxTransformerSide 2
#define MaxCorrelationSide 1
#define HVSide  0
#define LVSide  1
#define HVSide2 2
#define LVSide2 3
#define AllSides 4

#define BushingCh 0
#define GammaCh 2
#define BushingOnGammaCh 1

#define SetNullLine   1
#define NoSetNullLine 0

#define MaxBushingCountOnSide 3
//#define MaxBushingCountOnSide 1
#define BushingA 0
#define BushingB 1
#define BushingC 2

//������������ ������������ ����� �� //The maximum pause duration for
//������� PAUSE - min //Commanded PAUSE - min
//#define MaxPauseMin 180
#define MaxPauseMin 15 //DE 5-7-15 change to 15 minutes pause time

#define MaxLimit 6

//Max ���������� �������
#define MaxPDChannels 10

//����������
#ifndef __emulator

#include "spi.h"
#include "BoardAdd.h"

#define LedAdrr   0x60
#define RelayAdrr 0x50
#define DeviceAdrr 0x90

#define TDMCurrentGainAdrr 0x50

#define OutToAltera(Addr,v) { DisableLCD(0xFF); SPI_Prepare(SPIFREQ_16000,0); SPI_SetCS(SPI_CS_ALTERA); SPI_WriteRead(Addr); SPI_WriteRead(v); SPI_ClearCS(); EnableLCD(); }
#define ReadFromAltera(Addr,v) { DisableLCD(0xFF); SPI_Prepare(SPIFREQ_16000,0); SPI_SetCS(SPI_CS_ALTERA); v=SPI_WriteRead(Addr); SPI_ClearCS(); EnableLCD(); }

//#define LED2Green  { Led|=(1<<0);  OutToAltera(LedAdrr,Led);  }
//#define LED2Yellow { Led&=~(1<<0); OutToAltera(LedAdrr,Led);  }
//#define LED2Red    { Led&=~(1<<0); OutToAltera(LedAdrr,Led);  }
//#define LED2Off    { Led&=~(1<<0); OutToAltera(LedAdrr,Led);  }
#define LED2Yellow
#define LED1Green  { Led|=(1<<0); OutToAltera(LedAdrr,Led); }

#define LED1Yellow { Led|=(1<<1);  Led&=~(1<<2); OutToAltera(LedAdrr,Led); }
#define LED1Red    { Led|=(1<<2);  Led&=~(1<<1); OutToAltera(LedAdrr,Led); }

#define LED1Off    { Led&=~(1<<0); OutToAltera(LedAdrr,Led); }
#define AllLEDOff  { Led=0;OutToAltera(LedAdrr,Led);}
#define LED1YOFF 	{ Led&=~(1<<1); OutToAltera(LedAdrr,Led); }

#ifdef TDM

#define SetRelayNormal     { Relay&=~(1<<2); Relay&=~(1<<3);  AT91F_PIO_ClearOutput(AT91C_BASE_PIOA, AT91C_PIO_PA17); AT91F_PIO_ClearOutput(AT91C_BASE_PIOA, AT91C_PIO_PA18);}
#define SetRelayWarning    { Relay|=(1<<2);  Relay&=~(1<<3);  AT91F_PIO_SetOutput(AT91C_BASE_PIOA, AT91C_PIO_PA17);   AT91F_PIO_ClearOutput(AT91C_BASE_PIOA, AT91C_PIO_PA18);}
#define SetRelayAlarm      { Relay|=(1<<3);  Relay&=~(1<<2);  AT91F_PIO_ClearOutput(AT91C_BASE_PIOA, AT91C_PIO_PA17); AT91F_PIO_SetOutput(AT91C_BASE_PIOA, AT91C_PIO_PA18);}
#define SetRelayOnLine     { Relay|=(1<<1);  AT91F_PIO_ClearOutput(AT91C_BASE_PIOA, AT91C_PIO_PA16);}
#define ClearRelayOnLine   { Relay&=~(1<<1); AT91F_PIO_SetOutput(AT91C_BASE_PIOA, AT91C_PIO_PA16);}
#define RelayAllOff        { Relay=0; AT91F_PIO_ClearOutput(AT91C_BASE_PIOA, AT91C_PIO_PA16); AT91F_PIO_ClearOutput(AT91C_BASE_PIOA, AT91C_PIO_PA17); AT91F_PIO_ClearOutput(AT91C_BASE_PIOA, AT91C_PIO_PA18); }

#define ONTempRelay        { Relay|=(1<<0);   }
#define OFFTempRelay       { Relay&=~(1<<0);  }

#define SetCurrentGain2    {OutToAltera(TDMCurrentGainAdrr,0);}  // acm, mux select input current?  Depoped, but even before, seems like no-op
#define SetCurrentGain10   {OutToAltera(TDMCurrentGainAdrr,1);}

#else
#define SetRelayNormal     { Relay&=~(1<<2); Relay&=~(1<<3); OutToAltera(RelayAdrr,Relay); }
#define SetRelayWarning    { Relay|=(1<<2);  Relay&=~(1<<3); OutToAltera(RelayAdrr,Relay); }
#define SetRelayAlarm      { Relay|=(1<<3);  Relay&=~(1<<2); OutToAltera(RelayAdrr,Relay); }
#define SetRelayOnLine     { Relay|=(1<<1);  OutToAltera(RelayAdrr,Relay); }
#define ClearRelayOnLine   { Relay&=~(1<<1); OutToAltera(RelayAdrr,Relay); }
#define RelayAllOff        { Relay=0;        OutToAltera(RelayAdrr,Relay); }

#define ONTempRelay        { Relay|=(1<<0);  OutToAltera(RelayAdrr,Relay); }
#define OFFTempRelay       { Relay&=~(1<<0); OutToAltera(RelayAdrr,Relay); }
#endif

#else

#define LED2Green  { Led|=(1<<0);    }
#define LED2Yellow { Led&=~(1<<0);   }
#define LED2Red    { Led&=~(1<<0);   }
#define LED2Off    { Led&=~(1<<0);   }
#define LED1Green  { Led&=~(1<<1); Led&=~(1<<2); }

#define LED1Yellow { Led|=(1<<1);  Led&=~(1<<2);  }
#define LED1Red    { Led|=(1<<2);  Led&=~(1<<1);  }

#define LED1Off    { Led&=~(1<<1); Led&=~(1<<2);  }
#define AllLEDOff  { Led=0;}

#define SetRelayNormal     { Relay&=~(1<<2); Relay&=~(1<<3);  }
#define SetRelayWarning    { Relay|=(1<<2);  Relay&=~(1<<3);  }
#define SetRelayAlarm      { Relay|=(1<<3);  Relay&=~(1<<2);  }
#define SetRelayOnLine     { Relay|=(1<<1);   }
#define ClearRelayOnLine   { Relay&=~(1<<1);  }
#define RelayAllOff        { Relay=0;         }

#define ONTempRelay        { Relay|=(1<<0);   }
#define OFFTempRelay       { Relay&=~(1<<0);  }

#endif

//�������� � �������� �� �����
//�� Setup ��� ��������� ������
//���� �� ������ ����� �� ������ �� ���� �������
#define NoKeyDelayInSec (unsigned int)7200

//����� ����������
#define MaxNote 1

//���������� ����� � ������
#define MaxMinPerDay 1440
//���������� ����� � 1/2 �����
#define HalfMaxMinPerDay 720

#define Max4_20Ma 14
#define Min4_20Ma 4

#define StartLimit1 0.62
#define StartLimit2 0.56

//Only Right
//#define MeasurementEnable (unsigned int)0x0838

//Only Left
#define MeasurementEnable (unsigned int)0x1607

//Only Right A-A
//#define MeasurementEnable (unsigned int)0x0008

//Left and Right
//#define MeasurementEnable (unsigned int)0xFFFF

//#define TemperatureMaxChannels 1
//#define AirMoistureMaxChannels 2
//#undef AirMoistureMaxChannels
//#define GammaMaxChannels 1
//#define OtherMaxChannels 4
//#undef OtherMaxChannels
//#define I4_20MaxChannels 1

//#define SwitchRelay(a,b) *((char*)a)=b; Delay(250)

#define stAlarm 3
#define stWarning 2
#define stNormal 1
#define stUnknown 0

//#define OutVoltageShim 1
//#define Out420Shim     2
//#define OutFullShim    3

//#define CalibrationShim 1
//#define WorkShim        0

#define svNoSave       0
#define svSaveCurrent  1
#define svSaveAllways  2
#define svSaveOnlyRED  3

#define tkFirst 0
#define tkCurrent 1

#define chRead2Channel 0
#define chRead3Channel 1

#ifndef __emulator

    #define ReadFreq 6000.0
//    #define ReadFreq 6001.50037509377344336084021
//     #define ReadFreq 8000.0

//  #ifdef Read8kHz
//     #define ReadFreq 8000.0
//    #define ReadFreq 4000.0
//  #else
//    #define ReadFreq 6001.50037509377344336084021
//  #endif

//#define ReadFreq (float)6250.0
#else
  #ifdef Read8kHz
    #define ReadFreq 8000.0
//    #define ReadFreq 4000.0
  #else
    #define ReadFreq 6000.0
  #endif
#endif

#define ReadRate (float)1.0/ReadFreq
//#define ReadFreq3 (float)4000.0
//#define ReadRate3 (float)1.0/ReadFreq3

//#define ReadFreq3 4799.04019196160767846430713857229
#define ReadRate3 (float)1.0/ReadFreq3

#define PiValue 3.14159265358979
#define PiValue2 6.2831853
//#define AmplCalcMul 1.56985871271585557299843014128728
//#define AmplCalcMul 1.57334596036252713816417565313404

//#define AmplCalcMul 1.5712153350616702019011705554246
//#define AmplCalcMul 1.571119

//#define AmplCalcMul 1.5701237755508813824449750042591
//#define AmplCalcMul 1.57150641557628068915396482616046
//#define AmplCalcMul 1.5715370//4596898769310012961027799
//#define AmplCalcMul   1.571483793
//pi/2
//#define AmplCalcMul   1.5707963
#define AmplCalcMul   1.5710754535

#define STABLEDiagRegime 1
//#define NORMALDiagRegime 0
#define NORMALDiagRegime 1

#define LoSignalLevel 150
#define HiSignalLevel 1400

//#define Delay1ms __delay_cycles(8000);
#define Delay1ms

#define chReadAllChannels 0x0F

#define msStatusReRead       (signed char)1
#define msStatusReRead1Min   (signed char)2
#define msStatusErrorCalibr  (signed char)-1
#define msStatusErrorABShift (signed char)-2
#define msStatusErrorACShift (signed char)-3
#define msStatusErrorABCAmpl (signed char)-4
#define msStatusErrorZk      (signed char)-5

#define SBIT(PORT,BIT) PORT |= 1<<BIT
#define CBIT(PORT,BIT) PORT &= ~(1<<BIT)

#ifndef  __debug
#define BREAKDELAY (unsigned int)NoKeyDelayInSec
#else
#define BREAKDELAY (unsigned int)100
#endif

//��� ������
#define ftStandart 0

#define DiagOnInput 1
#define DiagOnGamma 0
#define CurDiagRegime DiagOnGamma

#define OnLineDiag 1
#define OnSumDiag  0
#define CurDiagType  OnSumDiag
//#define CurDiagType  OnLineDiag

#define AddOnLine 1
#define NoOnLine  0

#define min(a, b) (((a) < (b)) ? (a) : (b))

#define shNoShowErrors 0
#define shShowErrors 1

#endif

