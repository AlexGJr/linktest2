#ifndef _modbusapp_h
#define _modbusapp_h

//#include "ModBus.h"
#include "TypesDef.h"

TCRC AddToCRCCalc(TCRC CRC,char b);

//---------------------- Modbus --------------------------------

int ReadHoldingRegs(int raddr, int rlen, char* data);
int WriteHoldingRegs(int raddr, int rlen, char* data);
int WriteSingleReg(int raddr, int rval);
static int GetBit(int i);
int ReadCoilStatus(int raddr, int rlen, char* data);
int WriteSingleCoil(int raddr, int rval);
int WriteMultipleCoils(int raddr, int rlen, char* data);

// ��᫠�� �१ TCP-���� 502. 1-Ok 0-Error
int SendModbusTCP(char *tel, int len);
// ����� ����, �� �� �모�뢠�� �����. return Len
int ReadModbusTCP(char *tel, int len);
// �������� �����
void ShiftModbusTCP(int len);
// ��᫠�� �१ RS-485. 1-Ok 0-Error
int SendModbusCOM(char *tel, int len);
// ����� ����, �� �� �모�뢠�� �����. return Len
int ReadModbusCOM(char *tel, int len);
// �������� �����
void ShiftModbusCOM(int len);

int ModBusProcessData(char *Buf, int l);

//---------------------- Modbus --------------------------------

#endif
