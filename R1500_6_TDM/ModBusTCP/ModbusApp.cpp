
#include "ModBus.h"
#include "ModBusApp.h"
#include "Uart.h"
#include <string.h>
#include "Link.h"
#include "USB.h"

extern unsigned char Silenced;

extern void Shift(int aPort, unsigned int Len);  // acm, do this to resolve function for Understand.  Why Understand confused if external added to Uart.h strange.

//#include "..\Graph.h"
//#include "..\LCD.h"
//#include "..\KeyBoard.h"



TCRC AddToCRCCalc(TCRC CRC,char b)
{
  char ca,n;
  CRC^=b;
  for (n=0;n<8;n++) {
    ca=(CRC & 1);
    CRC>>=1;
    if (ca) CRC^=0xA001;
  }
  return CRC;
}


//---------------------- Modbus --------------------------------

int ReadHoldingRegs(int raddr, int rlen, char* data)
{
  return GetRegisterValue(raddr,rlen,data);
}


int WriteHoldingRegs(int raddr, int rlen, char* data)
{
  // saves registers starting from tel[5] - data
  return SetRegisterValue(raddr,rlen,data);
}


int WriteSingleReg(int raddr, int rval)
{
  // Save register
  //return SetRegisterValue(raddr,1,(char *)&rval);
  SetRegisterValue(raddr,1,(char *)&rval);
  
  return 4;
}


static int GetBit(int i)
{
  //Current state of Relay
  extern volatile char Relay;
  int Result=0;
  
  //Relay status
  switch (i) {
  case 0:if (Relay&(1<<2)) Result=1; else Result=0; break;
  case 1:if (Relay&(1<<3)) Result=1; else Result=0; break;
  case 2:if (Relay&(1<<1)) Result=0; else Result=1; break;
  }
  
  return Result;
}


int ReadCoilStatus(int raddr, int rlen, char* data)
{
  int i,l,b;
  
  
  l=1;
  b=0;
  //      00000000 11111100
  // bits 76543210 54321098 ...
  data[1]=0;
  for (i=raddr;i<raddr+rlen;i++) {
    
    if (GetBit(i)) data[l]|=(1<<b);
    
    b++;
    if (b>7) {
      l++;
      b=0;
      data[l]=0;
    }
  }
  l=rlen/8;
  if (rlen%8) l++;
  data[0]=l;
  return l+1;
}



void SetBit(int i,char Value)
{
  //Current state of Relay
  extern volatile char Relay;
  
  /*
  OutInt(0,0,i,3);
  OutInt(0,8,Value,3);
  Redraw();
  WaitReadKey();
  */
  
  //Relay status
  switch (i) {
  case 0:if (Value) Relay|=(1<<2);  else Relay&=~(1<<2); break;
  case 1:if (Value) Relay|=(1<<3);  else Relay&=~(1<<3); break;
  case 2:if (Value) Relay&=~(1<<1); else Relay|=(1<<1);  break;
  }
#ifndef __emulator
  (*(volatile char *)0x2300)=Relay;  // acm, bad, writing to addr 0x2300 is in flash
#endif
  
}



int WriteMultipleCoils(int raddr, int rlen, char* data)
{
  // saves relay starting from tel[5] - bits with values
  //      00000000 11111100
  // bits 76543210 54321098 ...
  
  int i,l,b;
  
  l=1;
  b=0;
  //      00000000 11111100
  // bits 76543210 54321098 ...
  data[1]=0;
  for (i=raddr;i<raddr+rlen;i++) {
    //    SetBit(i,data[l]&(1<<b));
    b++;
    if (b>7) {
      l++;
      b=0;
      data[l]=0;
    }
  }
  l=rlen/8;
  if (rlen%8) l++;
  data[0]=l;
  return l+1;
}


int WriteSingleCoil(int raddr, int rval)
{
  // Saves relay state rval = 1 or 0
  //SetBit(raddr,rval);  // acm, uncomment to allow user to write, but no reason to
  
  return 4;
}

// ---------- Particular implimentation - shall be coded somwhere else ----------


// Send via TCP-port 502. 1-Ok 0-Error
int SendModbusTCP(char *tel, int len)
{
  //UartStarted[UartComputerPort]=1;
  return UartWriteBlock(CurUartPort, tel, len);
}


// Read buffer, but keep data. return Len
int ReadModbusTCP(char *tel, int len)
{
  UartCheckReadBlock(CurUartPort);
  
  if (UartCurRead[CurUartPort]<(unsigned int)len)
    return 0;
  
  memcpy(tel,&(UartReadBuf[CurUartPort]),len);
  return len;
}


// Shift data
void ShiftModbusTCP(int len)
{
  Shift(CurUartPort,len);
}


// Send via RS-485. 1-Ok 0-Error
int SendModbusCOM(char *tel, int len)
{
  if (Silenced==1 && CurUartPort==0) return len;  //2.10 Do not send over backplain, if programming another slave
  else if (Silenced>1) Silenced=1;
  return UartWriteBlock(CurUartPort, tel, len);
}


// Read buffer, but keep data. return Len
int ReadModbusCOM(char *tel, int len)
{
  extern u8 USBReadData[USBDataBlock];
  
  //extern __no_init volatile u8* USBReadAddr;
  
  UartCheckReadBlock(CurUartPort);
  if (UartCurRead[CurUartPort]<(unsigned int)len)
    return 0;
  if (CurUartPort==USBPort)
    memcpy(tel,USBReadData,len);
  else
    memcpy(tel,&(UartReadBuf[CurUartPort]),len);
  
  return len;
}


// Shift data
void ShiftModbusCOM(int len)
{
  Shift(CurUartPort,len);
}


//Our 72 function
int ModBusProcessData(char *Buf, int l)
{
  unsigned int Cmnd,CmndInfo1,CmndInfo2,CmndInfo3,CmndInfo4;
  
  Cmnd=(unsigned int)Buf[1]*256+(unsigned int)Buf[0];
  CmndInfo1=(unsigned int)Buf[3]*256+(unsigned int)Buf[2];
  CmndInfo2=(unsigned int)Buf[5]*256+(unsigned int)Buf[4];
  CmndInfo3=(unsigned int)Buf[7]*256+(unsigned int)Buf[6];
  CmndInfo4=(unsigned int)Buf[9]*256+(unsigned int)Buf[8];
  return SendAnswer(Cmnd,CmndInfo1,CmndInfo2,CmndInfo3,CmndInfo4,Buf);
  
}

//---------------------- Modbus --------------------------------

