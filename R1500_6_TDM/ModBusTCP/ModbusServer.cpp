
#include "Link.h"
#include "ModBusApp.h"

#ifdef __r500
#include "..\AComp.h"
#include "..\Ind.h"
#endif

#ifdef __R1500_6
#include "RAM.h"
#include "LCD.h"
#include "Defs.h"
#include "Graph.h"
//#include "..\StrConst.h"
#include "PicRes.h"
#endif



#include "uart.h"
#include <string.h>
#include "ModBusServer.h"
#include "KeyBoard.h"

int ServerModbusMode=MODBUS_RTU;  //2.10 ag fixes removing the beow call and sets always to modbus tcp

//ag commented out. Not used.
/*int ServerModbusSetMode(int aModbusMode) @ "USBCODE"
{
  if (aModbusMode==MODBUS_RTU) {
    ServerModbusMode=MODBUS_RTU;
    return MODBUS_SUCCESS;
  }
  if (aModbusMode==MODBUS_TCP) {
    ServerModbusMode=MODBUS_TCP;
    return MODBUS_SUCCESS;
  }
  return MODBUS_ERROR;
}*/


int ServerModbusIsEnabledFunctionCode(int aModbusFunctionCode)
{
  if (aModbusFunctionCode==MF_ReadCoilStatus) return 1; else
    if (aModbusFunctionCode==MF_ForceSingleCoil) return 1; else
      if (aModbusFunctionCode==MF_PresetSingleRegister) return 1; else
        if (aModbusFunctionCode==MF_ForceMultipleCoils) return 1; else
          if (aModbusFunctionCode==MF_PresetMultipleRegs) return 1; else
            if (aModbusFunctionCode==MF_UserStrFunctionCode) return 1; else
              if (aModbusFunctionCode==MF_ReadHoldingRegisters) return 1; else
                if (aModbusFunctionCode==MF_UserBinFunctionCode) return 1; else
                  return 0;
}


char ID1, ID2=0;

int ServerModbusWrite(int  slave, int  function, char *data, int datalen, char in_port_is_USB)
{
  int len,i;
  char *tel=(char *)UartWriteBuf;
  TCRC CRC;
  
  if (slave < 0 || slave > 255) return MODBUS_ERROR;
  if (datalen == 0) return MODBUS_ERROR;
  if (((function & 0x80)==0) &&
      (!ServerModbusIsEnabledFunctionCode(function))) return MODBUS_ERROR;
  
  len = 0;
  
  if ((ServerModbusMode == MODBUS_TCP)&& !in_port_is_USB)  {
    //if ((ServerModbusMode == MODBUS_TCP))  {      // acm, 11-6-11
    
    if (datalen>255) return MODBUS_ERROR;
    
#ifdef MEGA
    SetPage(InOutBufferPage);
#endif
    
    // bytes 0,1 Transaction ID. Not important. Usually zero when making a request, the server will copy them faithfully into the response.
    tel[len++] = ID1;
    tel[len++] = ID2;
    tel[len++] = 0;         // bytes 2,3 Protocol number. Must be zero.
    tel[len++] = 0;
    tel[len++] = 0;         // byte 4 Length (upper byte). Since all requests will be less than 256 bytes in length (!), this will always be zero.
    tel[len++] = 2+datalen; // byte 5 Length (lower byte). Equal to the number of bytes which follow
    
    tel[len++] = (char) slave;
    tel[len++] = (char) function;
    
    memcpy(&(tel[len]),data,datalen);
    
    // Send via TCP
    if (SendModbusTCP(tel,datalen+8)) return MODBUS_SUCCESS;
    else return MODBUS_ERROR;
  }
  else if ((ServerModbusMode == MODBUS_RTU) || in_port_is_USB)  {
    //else if ((ServerModbusMode == MODBUS_RTU))  {
    if (datalen>MaxDataLen)
      return MODBUS_ERROR;
    
#ifdef MEGA
    SetPage(InOutBufferPage);
#endif
    tel[len++] = (char) slave;
    tel[len++] = (char) function;
    memcpy(&(tel[len]),data,datalen);
    
    CRC=0xFFFF;
    for (i=0;i<datalen+2;i++)
      CRC=AddToCRCCalc(CRC,tel[i]);
    tel[datalen+2]=CRC&0xFF;
    tel[datalen+3]=CRC>>8;
    
    // Send via RS-485
    if (SendModbusCOM(tel,datalen+4)) return MODBUS_SUCCESS;
    else return MODBUS_ERROR;
  }
  else return MODBUS_ERROR;
}



static void PutModbusException(int slave, int function, int ModbusExceptoionCode)
{
  char tel[1];
  
#ifdef MEGA
  SetPage(InOutBufferPage);
#endif
  
  tel[0]=ModbusExceptoionCode;
  ServerModbusWrite(slave,function | 0x80, tel, 1, CurUartPort==USBPort);
}


int ServerModbusListening(int slave, char in_port_is_USB)  // acm, 1-18-11, add parameter to allow disable TCP over USB, so user cannot kill communication
{
  int i,datalen,function,raddr,rlen,rval,broadcast;
  
  //#ifndef __emulator
  //  char *tel=(char*)TempUARTBuffer;
  //#else
  char tel[UartDataBlockOut];
  //#endif
  
  TCRC CRC;
  
  datalen=0;
  if ((ServerModbusMode == MODBUS_TCP) && !in_port_is_USB) {
    
#ifdef MEGA
    SetPage(InOutBufferPage);
#endif
    
    if (ReadModbusTCP(tel, 8)<=0) return MODBUS_ERROR;
    // bytes 0,1 Transaction ID faithfully copied from the request message
    // bytes 2,3 Protocol number always zero
    // byte 4 Response length (upper byte) Always zero
    // byte 5 Response length (lower byte). Equal to the number of bytes which follow
    if (tel[4] != 0) { ShiftModbusTCP(5); return MODBUS_ERROR; }
    if (tel[3] != 0) { ShiftModbusTCP(4); return MODBUS_ERROR; }
    if (tel[2] != 0) { ShiftModbusTCP(3); return MODBUS_ERROR; }
    
    ID1=tel[0];
    ID2=tel[1];
    datalen    = tel[5]-2;
    function  = tel[7];
    
    broadcast=(tel[6]==0);
    if ((!broadcast)&&(slave != tel[6])) {
      ShiftModbusTCP(datalen+8);
      return MODBUS_ERROR;
    }
    
    if (!ServerModbusIsEnabledFunctionCode(function)) {
      ShiftModbusTCP(datalen+8);
      PutModbusException(slave,function,ME_ILLEGAL_FUNCTION);
      return MODBUS_ERROR;
    }
    
    if (ReadModbusTCP(tel, datalen+8)<=0) {
      return MODBUS_ERROR;
    }
    ShiftModbusTCP(datalen+8);
    
    memmove(tel,&(tel[8]),datalen);
  }
  else if ((ServerModbusMode == MODBUS_RTU) || in_port_is_USB) {
    
#ifdef MEGA
    SetPage(InOutBufferPage);
#endif
    
    if (ReadModbusCOM(tel, 8)<=0) return MODBUS_ERROR;
    
    function  = tel[1];
    
    broadcast=(tel[0]==0);
    
    
    if ((function==MF_ReadCoilStatus) ||
        (function==MF_ReadHoldingRegisters) ||
          (function==MF_ForceSingleCoil) ||
            (function==MF_PresetSingleRegister))
      datalen    = 4;
    else
      if ((function==MF_ForceMultipleCoils) ||
          (function==MF_PresetMultipleRegs)) {
            datalen    = tel[6]+5;
          } else
            if ((function==MF_UserStrFunctionCode) ||
                (function==MF_UserBinFunctionCode))
              datalen    = 2+(int)(((int)tel[3]<<8)+tel[2]);
            else
              datalen    = 0;
            //      if ((function==0)&&(broadcast)) {
            //         ShiftModbusCOM(2);
            //         return MODBUS_SUCCESS;
            //      }
            
            if (datalen==0) {
              ShiftModbusCOM(1);
              return MODBUS_ERROR;
            }
            if (datalen>MaxDataLen) {
              ShiftModbusCOM(1);
              return MODBUS_ERROR;
            }
            
            if (ReadModbusCOM((char *) tel, datalen+4)<=0) {
              return MODBUS_ERROR;
            }
            
            // acm, 1.75 bug, commenting this out to achieve "don't require MODBUS address match over USB, just causes issues"
            // doesn't work, causes BHM to respond to PDM request on RS485, causing CRC errors.
            //      if ((!broadcast)&&(slave != tel[0])) {
            
            // acm, 11-16-12 code review, two ReadModbusCOM() with no ShiftModbusCOM() tel[0] still points addr., not a problem...
            if (!in_port_is_USB && (!broadcast)&&(slave != tel[0])) {
              ShiftModbusCOM(datalen+4);
              return MODBUS_ERROR;
            }
            
            slave = tel[0];  // acm, save this, need to pass back this address to host app to avoid CRC mis-match
            
            if (!ServerModbusIsEnabledFunctionCode(function)) {
              ShiftModbusCOM(datalen+4);
              PutModbusException(slave,function,ME_ILLEGAL_FUNCTION);
              return MODBUS_ERROR;
            }
            
            ShiftModbusCOM(datalen+4);
            
            
            CRC=0xFFFF;
            for (i=0;i<datalen+2;i++)
              CRC=AddToCRCCalc(CRC,tel[i]);
            //      if ((tel[datalen+2] | (tel[datalen+3]<<8)) != CRC) {
            if (((unsigned int)tel[datalen+3]<<8)+tel[datalen+2] != CRC) {
              return MODBUS_ERROR;
            }
            memmove(tel,&(tel[2]),datalen);
            
            //Shift
            if (USBFlag&&UartCurRead[0]) {			// acm, remnant DMA code?
              ShiftModbusCOM(UartCurRead[0]);
            }
            
  } else
    return MODBUS_ERROR;
  
  
  // Analyzing request
  if (datalen<=0) return MODBUS_ERROR;
  
#ifdef __r500
#ifdef MEGA
  if (!BusyFlag) {
    ind_string[0]=chS;
    ind_string[1]=chE;
    ind_string[2]=chn;
    ind_string[3]=chd;
    ind_string[4]=chi;
    ind_string[5]=chn;
    ind_string[6]=chG;
    ind_string[7]=chEmpty;
    Redraw();
  }
#endif
#endif
  
#ifdef __R1500_6
  if (!BusyFlag) {
    //     ClearLCD();
    //     SetFont(Font8x8);
    //     OutString(16,4,SendDataStr);
    OutPicture(104,0,LinkIco);
    Redraw();
  }
#endif
  
  // Busy
  if ((BusyFlag)&&(!broadcast)) {
    PutModbusException(slave,function,ME_SLAVE_DEVICE_BUSY);
    return MODBUS_ERROR;
  }
  
  
  if (function==MF_ReadHoldingRegisters) {
    raddr=(tel[0]<<8) | tel[1];
    rlen=(tel[2]<<8) | tel[3];
    if ((rlen<1)||(rlen>0x7D)) {
      if (!broadcast) PutModbusException(slave,function,ME_ILLEGAL_DATA_VALUE);
      return MODBUS_ERROR;
    }
    if ((unsigned int)raddr+rlen>0x7FFF) {
      if (!broadcast) PutModbusException(slave,function,ME_ILLEGAL_DATA_ADDRESS);
      return MODBUS_ERROR;
    }
    if (!IsRegAddressCorrect(raddr,rlen)) {
      if (!broadcast) PutModbusException(slave,function,ME_ILLEGAL_DATA_ADDRESS);
      return MODBUS_ERROR;
    }
    
    datalen=ReadHoldingRegs(raddr,rlen,tel);
    
  } else if (function==MF_PresetSingleRegister) {
    raddr=(tel[0]<<8) | tel[1];
    rval=(tel[3]<<8) | tel[2];
    datalen=WriteSingleReg(raddr,rval);
    
  } else if (function==MF_PresetMultipleRegs) {
    raddr=((int)tel[0]<<8) | tel[1];
    rlen=((int)tel[2]<<8) | tel[3];
    if ((rlen<1)||(rlen>0x7D)||(rlen*2!=tel[4])) {
      if (!broadcast) PutModbusException(slave,function,ME_ILLEGAL_DATA_VALUE);
      return MODBUS_ERROR;
    }
    if ((unsigned int)raddr+rlen>0x7FFF) {
      if (!broadcast) PutModbusException(slave,function,ME_ILLEGAL_DATA_ADDRESS);
      return MODBUS_ERROR;
    }
    if (!IsRegAddressCorrect(raddr,rlen)) {
      if (!broadcast) PutModbusException(slave,function,ME_ILLEGAL_DATA_ADDRESS);
      return MODBUS_ERROR;
    }
    datalen=WriteHoldingRegs(raddr,rlen,&tel[5]);
    datalen=4;
    
  } else if (function==MF_ReadCoilStatus) {
    raddr=(tel[0]<<8) | tel[1];
    rlen=(tel[2]<<8) | tel[3];
    if ((rlen<1)||(rlen>0x7D0)) {
      if (!broadcast) PutModbusException(slave,function,ME_ILLEGAL_DATA_VALUE);
      return MODBUS_ERROR;
    }
    if ((unsigned int)raddr+rlen>0x7FFF) {
      if (!broadcast) PutModbusException(slave,function,ME_ILLEGAL_DATA_ADDRESS);
      return MODBUS_ERROR;
    }
    if (!IsCoilAddressCorrect(raddr,rlen)) {
      if (!broadcast) PutModbusException(slave,function,ME_ILLEGAL_DATA_ADDRESS);
      return MODBUS_ERROR;
    }
    
    datalen=ReadCoilStatus(raddr,rlen,tel);
    
  } else if (function==MF_ForceSingleCoil) {
    raddr=(tel[0]<<8) | tel[1];
    rval=(tel[2]<<8) | tel[3];
    if ((rval!=0x0000)&&(rval!=0xFF00)) {
      if (!broadcast) PutModbusException(slave,function,ME_ILLEGAL_DATA_VALUE);
      return MODBUS_ERROR;
    }
    datalen=WriteSingleCoil(raddr,(rval==0xFF00));
    
  } else if (function==MF_ForceMultipleCoils) {
    raddr=(tel[0]<<8) | tel[1];
    rlen=(tel[2]<<8) | tel[3];
    i=rlen/8;
    if (rlen%8) i++;
    if ((rlen<1)||(rlen>0x7B0)||(i!=tel[4])) {
      if (!broadcast) PutModbusException(slave,function,ME_ILLEGAL_DATA_VALUE);
      return MODBUS_ERROR;
    }
    if ((unsigned int)raddr+rlen>0x7FFF) {
      if (!broadcast) PutModbusException(slave,function,ME_ILLEGAL_DATA_ADDRESS);
      return MODBUS_ERROR;
    }
    if (!IsCoilAddressCorrect(raddr,rlen)) {
      if (!broadcast) PutModbusException(slave,function,ME_ILLEGAL_DATA_ADDRESS);
      return MODBUS_ERROR;
    }
    
    datalen=WriteMultipleCoils(raddr,rlen,tel);
    
  } else if (function==MF_UserBinFunctionCode) {
    // All is our. DE 4-24-15 This is the secret binary command 72=0x48 which is used
    // to transfer large blocks of data such as configuration or data records
    memmove(tel,&(tel[2]),datalen-2);
    datalen=ModBusProcessData((char *)tel,datalen-2);
    
  } else if (function==MF_UserStrFunctionCode) {
    // All is our 71
    memmove(tel,&(tel[2]),datalen-2);
    datalen=ModBusProcessString((char *)tel,datalen-2);
    if (datalen==0) return MODBUS_SUCCESS;
  } else datalen=0;
  
  
  if (broadcast)
    return MODBUS_SUCCESS;
  
  // failure to analyze and recognize
  if (datalen<=0) {
    PutModbusException(slave,function,ME_SLAVE_DEVICE_FAILURE);
    return MODBUS_ERROR;
  }
  
  // Send this
  if ((function==MF_UserBinFunctionCode)||
      (function==MF_UserStrFunctionCode)) {
        // All our
        memmove(&(tel[2]),tel,datalen);
        tel[0] = datalen & 0xFF;
        tel[1] = (datalen >> 8);
        datalen+=2;
      }
  
  return ServerModbusWrite(slave,function,tel,datalen, CurUartPort==USBPort);
  
}

