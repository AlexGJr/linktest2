#ifndef _uart_h
#define _uart_h

extern int CurUartPort;
#define UartPorts    2
#define USBPort      UartPorts-1

extern int ID_Slave;
extern int BusyFlag;
extern unsigned int AT91_BAUD_RATE;

//����� �� USB
extern int USBFlag;

//#define UartDataBlock 1024
#define UartDataBlockIn  1024
#define UartDataBlockOut 1024


extern volatile char UartStarted[UartPorts];
extern volatile unsigned int UartReadCount[UartPorts], UartCurRead[UartPorts];
extern volatile unsigned int UartWriteCount[UartPorts], UartCurWrite[UartPorts];

typedef char TUartReadBuf[UartPorts-1][UartDataBlockIn];
typedef char TUartWriteBuf[UartDataBlockOut];
extern TUartReadBuf UartReadBuf;
extern TUartWriteBuf UartWriteBuf;

extern volatile unsigned int UartTic[UartPorts];

void Usart_init(void);
void Usart_done(void);
void Usart_reset(void);
void Usart_c_irq_handler(void);

int UartWriteBlock(int aPort, void* Buf, int Len);
void UartCheckReadBlock(int aPort);
void Shift(int aPort, unsigned int Len);

#endif

