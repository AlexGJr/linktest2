//*----------------------------------------------------------------------------
//*      ATMEL Microcontroller Software Support  -  ROUSSET  -
//*----------------------------------------------------------------------------
//* The software is delivered "AS IS" without warranty or condition of any
//* kind, either express, implied or statutory. This includes without
//* limitation any warranty or condition with respect to merchantability or
//* fitness for any particular purpose, or against the infringements of
//* intellectual property rights of others.
//*----------------------------------------------------------------------------
//* File Name           : interrupt_Usart.c
//* Object              : USART Interrupt Management
//*
//* 1.0 24/Jun/04 JPP   : Creation
//*----------------------------------------------------------------------------

// Include Standard LIB  files
#include "Board.h"
#include "Uart.h"
#include "BoardAdd.h"
#include "Modbus.h"
#include "ModBusApp.h"
#include "ModbusServer.h"
#include "Link.h"
#include "KeyBoard.h"
#include "USB.h"
#include <string.h>

#include "Defs.h"
#include "LCD.h"

unsigned int SCAL;
int ID_Slave=1;
int CurUartPort=0;

//unsigned long BufSend;

//������ ����� � ������� BusyCommand
int  BusyFlag = 1;
unsigned int AT91_BAUD_RATE;

//����� �� USB
int USBFlag=0;

// ��������� 0, ����� ����������, 1 - ����� ��������
volatile char UartStarted[UartPorts];
TUartReadBuf UartReadBuf;
TUartWriteBuf UartWriteBuf;
TUartWriteBuf TempUARTBuffer;

volatile unsigned int UartCurRead[UartPorts]={0};
volatile unsigned int UartWriteCount[UartPorts]={0}, UartCurWrite[UartPorts]={0};
volatile unsigned int UartTic[UartPorts]={0};

char *BufSend=(char *)&UartWriteBuf;


//����, ��� ��������� ��� �� ����������, ���� ��������
volatile char FlagSendMess=0;


//* \fn    AT91F_US_Baudrate
//* \brief Calculate the baudrate
//* Standard Asynchronous Mode : 8 bits , 1 stop , no parity
#define AT91C_US_ASYNC_MODE ( AT91C_US_USMODE_NORMAL + \
                        AT91C_US_NBSTOP_1_BIT + \
                        AT91C_US_PAR_NONE + \
                        AT91C_US_CHRL_8_BITS + \
                        AT91C_US_CLKS_CLOCK )

//*------------------------- Internal Function --------------------------------

//*----------------------------------------------------------------------------
//* Function Name       : Usart_c_irq_handler
//* Object              : C handler interrupt function called by the interrupts
//*                       assembling routine
//* Input Parameters    : <RTC_pt> time rtc descriptor
//* Output Parameters   : increment count_timer0_interrupt
//*----------------------------------------------------------------------------


void Usart_c_irq_handler(void) @ "USBCODE"
{
unsigned int status;
unsigned int Tic;

	//get Usart status register

	status = AT91C_BASE_US0->US_CSR;

	if ( status & AT91C_US_RXRDY)
        {
             Tic=GetTic32();
             if (GetTic32()-UartTic[0]>1) {
//             if (GetTic32()-UartTic[0]>15) {
                 // ����� ������ �� ����-���� 1/32 ���
                  UartCurRead[0]=0;
             }
             UartTic[0]=Tic;
             if (UartCurRead[0]<UartDataBlockIn) UartReadBuf[0][UartCurRead[0]++]=AT91F_US_GetChar(AT91C_BASE_US0);
                 else AT91F_US_GetChar(AT91C_BASE_US0);  // acm, 2-19-12 missing else forever, side effects->underrun error?  Correct in Main & PDM
//				 if (AT91F_US_Error(AT91C_BASE_US0)&AT91C_US_FRAME) LED1Yellow;  // acm, debug code, see if framing error detect with DRMCC master 8N1
// note - we didn't normally see FRAME errors connected to 8N1 DRMCC, because I believe FRAME bit not valid at this point
//        we did see FRAME error when unit starts measurement.  Also see rxovrn, indicating high IRQ latency, not surprising.
		}

	// Reset the satus bit
	 AT91C_BASE_US0->US_CR = AT91C_US_RSTSTA;
}
//*-------------------------- External Function -------------------------------

//*----------------------------------------------------------------------------
//* Function Name       : Usart_init
//* Object              : USART initialization
//* Input Parameters    : none
//* Output Parameters   : TRUE
//*----------------------------------------------------------------------------
//************ag 2.10 This function rewriteen inside USBCODE as USB_Usart_Init
//void Usart_init ( void ) @ "USBCODE"
////* Begin
//{
//#ifndef __emulator
//        //SD - 1
//        AT91F_PIO_ClearOutput(AT91C_BASE_PIOA, AT91C_PIO_PA8);
//        AT91F_PIO_CfgOutput(AT91C_BASE_PIOA, AT91C_PIO_PA8);
//
//        //ServerModbusSetMode(MODBUS_RTU);
//
//	AT91PS_USART COM0 = AT91C_BASE_US0;
//
// 	//* Configure PIO controllers to periph mode
// 	AT91F_PIO_CfgPeriph( AT91C_BASE_PIOA,
// 		((unsigned int) AT91C_PA5_RXD0    ) |
// 		((unsigned int) AT91C_PA6_TXD0    ) |
// 		((unsigned int) AT91C_PA7_RTS0    ), // Peripheral A
// 		0); // Peripheral B
//
//
//   	// First, enable the clock of the PIOB
//    	AT91F_PMC_EnablePeriphClock ( AT91C_BASE_PMC, 1 << AT91C_ID_US0 ) ;
//	// Usart Configure
//    AT91F_US_Configure (COM0, MCK, AT91C_US_USMODE_RS485 | AT91C_US_NBSTOP_1_BIT | AT91C_US_PAR_NONE | AT91C_US_CHRL_8_BITS, AT91_BAUD_RATE, 0);
//
//	// Enable usart
//	COM0->US_CR = AT91C_US_RXEN | AT91C_US_TXEN ;
//
//    //* Enable USART IT error and RXRDY
//    	AT91F_US_EnableIt(COM0,AT91C_US_RXRDY);
//
//    	//* open Usart 1 interrupt
//	AT91F_AIC_ConfigureIt ( AT91C_BASE_AIC, AT91C_ID_US0, USART_INTERRUPT_LEVEL,AT91C_AIC_SRCTYPE_INT_LEVEL_SENSITIVE, Usart_c_irq_handler);
//	AT91F_AIC_EnableIt (AT91C_BASE_AIC, AT91C_ID_US0);
//
//        UartStarted[0]=1;
//#endif
////* End
//}

void Usart_reset ( void )
//* Begin
{
#ifndef __emulator

	AT91PS_USART COM0 = AT91C_BASE_US0;

 	//* Configure PIO controllers to periph mode
/*
 	AT91F_PIO_CfgPeriph( AT91C_BASE_PIOA,
 		((unsigned int) AT91C_PA5_RXD0    ) |
 		((unsigned int) AT91C_PA6_TXD0    ) |
                ((unsigned int) AT91C_PA7_RTS0  ), // Peripheral A
 		0); // Peripheral B
*/
 	AT91F_PIO_CfgPeriph( AT91C_BASE_PIOA,
 		((unsigned int) AT91C_PA5_RXD0    ) |
 		((unsigned int) AT91C_PA6_TXD0    ), // Peripheral A
 		0); // Peripheral B
        //485 In-Out �������
        AT91F_PIO_CfgOutput(AT91C_BASE_PIOA, AT91C_PA7_RTS0);
        AT91F_PIO_ClearOutput(AT91C_BASE_PIOA, AT91C_PA7_RTS0);

//        COM0->US_TTGR=8;

   	// First, enable the clock of the PIOB
    	AT91F_PMC_EnablePeriphClock ( AT91C_BASE_PMC, 1 << AT91C_ID_US0 ) ;
	// Usart Configure
    AT91F_US_Configure (COM0, MCK, AT91C_US_USMODE_RS485 | AT91C_US_NBSTOP_1_BIT | AT91C_US_PAR_NONE | AT91C_US_CHRL_8_BITS, AT91_BAUD_RATE, 0);

	// Enable usart
	COM0->US_CR = AT91C_US_RXEN | AT91C_US_TXEN ;

    //* Enable USART IT error and RXRDY
    	AT91F_US_EnableIt(COM0,AT91C_US_RXRDY);

    	//* open Usart 1 interrupt
	AT91F_AIC_ConfigureIt ( AT91C_BASE_AIC, AT91C_ID_US0, USART_INTERRUPT_LEVEL,AT91C_AIC_SRCTYPE_INT_LEVEL_SENSITIVE, Usart_c_irq_handler);
	AT91F_AIC_EnableIt (AT91C_BASE_AIC, AT91C_ID_US0);

        UartStarted[0]=1;
#endif
//* End
}

void Usart_done ( void )
{
        UartStarted[0]=0;
}

int UartWriteBlock(int aPort, void* Buf, int Len) @ "USBCODE"
{
unsigned int c,i;
extern int ServerModbusMode;

if (Len) {

//  UartCurWrite[aPort]=1;
//  UartWriteCount[aPort]=Len;
//  BufSend=(char *)Buf;

#ifndef __emulator
   if (CurUartPort==USBPort) {
      //USB
      USBWrite((u8*)Buf,Len);
   } else {
      //Com
#ifndef TDM
      if (ServerModbusMode == MODBUS_RTU) Delay(180);
#endif
      AT91F_PIO_SetOutput(AT91C_BASE_PIOA, AT91C_PA7_RTS0);
      Delay(1);

      AT91C_BASE_US0->US_CR = AT91C_US_RXDIS | AT91C_US_TXEN ;

      for (i=0;i<Len;i++){
          c=*((char*)Buf+i);
          while (!AT91F_US_TxReady(AT91C_BASE_US0));
          AT91F_US_PutChar(AT91C_BASE_US0,c);
          // wait for the end of transmition
          while ( !(AT91C_BASE_US0->US_CSR & AT91C_US_TXEMPTY) );
      }
      Delay(1);
      AT91F_PIO_ClearOutput(AT91C_BASE_PIOA, AT91C_PA7_RTS0);
      AT91C_BASE_US0->US_CR = AT91C_US_RXEN | AT91C_US_TXEN ;
   }

#endif

}

return Len;
}

void UartCheckReadBlock(int aPort)
{

}

void Shift(int aPort, unsigned int Len)
{

  if (Len>0) {
    if (UartCurRead[aPort]<=Len) {
       UartCurRead[aPort]=0;
    } else {
       if (aPort==USBPort)
          USBShift(Len);
       else
          memmove(UartReadBuf[aPort],&(UartReadBuf[aPort][Len]),(UartCurRead[aPort]-=Len));
}
}
}






