#include "Defs.h"
#include "DAC.h"
#include "LCD.h"
#include "spi.h"
#include "BoardAdd.h"
#include "Setup.h"

extern struct stSetup Setup;  //acm, make known to Understand
extern void LoadSetup(char SetParam, char SetTime);

char DACValue[MaxTransformerSide][MaxBushingCountOnSide];
char ExtDACValue[MaxTransformerSide][MaxBushingCountOnSide];

//���� ��������� ���������
volatile char ChangeDACValues=0;

extern char swap_phaseBC[]; 	// acm, v1.79 FW, set in beginning of Main()
extern char setx_state;			// acm, swap_state defined Main(), drivenin SetInputChannel(), 0==don't swap, 1==set1 selected, 2=set 2 selected

static void NLDAC(void)
{
#ifndef __emulator
DisableLCD(0xFF);
SPI_SetCS(SPI_CS_LDAC);
SPI_ClearCS();
EnableLCD();
#endif
}

void SetDACDevice(char Side, char Value,char DACNum)
{
 //���� ��������� ���������
 extern volatile char ChangeDACValues;
 char Addr;

#ifndef __emulator
 DisableLCD(0xFF);
 
DACValue[Side][DACNum]=Value; // acm, v1.79, starting to get confusing, store here before DACNum swap.  Moved from below, doing it there does double swap. 
 if(swap_phaseBC[Side]) 	// acm, v1.79, swap phase BC also need to swap POT address...
 {
	 if(DACNum==BushingB) DACNum=BushingC;
	 else
		 if(DACNum==BushingC) DACNum=BushingB;  
 }
 
 if (Side) {SPI_SetCS(SPI_CS_R2);} else {SPI_SetCS(SPI_CS_R1); }
 Addr=DACNum;

 SPI_Prepare(SPIFREQ_8000,0);
 SPI_WriteRead(Addr);
 SPI_WriteRead(Value);

 SPI_ClearCS();
 EnableLCD();
#endif
 //acm DACValue[Side][DACNum]=Value;
 //���� ��������� ���������  Flag Change Settings
 ChangeDACValues=1;
}

void SetCalibrDAC(unsigned int Value)
{
#ifndef __emulator
SetDAC(1,Value<<2);
NLDAC();
#endif
}

#ifdef __emulator  //acm, ifdef this out because I keep on staring at this, we use SetDACDevice()...
void SetExtDAC(char Side, char Value,char DACNum)  // acm, 3-29-10, this function isn't for our hardware - SPI_CS_R3 is not implemented...
{char Addr;

#ifndef __emulator
 DisableLCD(0xFF);

 switch (DACNum){
    case 0: {if (Side) {SPI_SetCS(SPI_CS_R2); Addr=3;} else {SPI_SetCS(SPI_CS_R1); Addr=2;} break;}  // acm, 2-18-09 select POT chip select here
    case 1: {if (Side) {SPI_SetCS(SPI_CS_R3); Addr=2;} else {SPI_SetCS(SPI_CS_R1); Addr=3;} break;}
    case 2: {if (Side) {SPI_SetCS(SPI_CS_R3); Addr=3;} else {SPI_SetCS(SPI_CS_R2); Addr=2;} break;}

 }
 SPI_Prepare(SPIFREQ_8000,0);
 SPI_WriteRead(Addr);
 SPI_WriteRead(Value);

 SPI_ClearCS();
 EnableLCD();
#endif
 ExtDACValue[Side][DACNum]=Value;  // acm, 2-19-09, FYI - doesn't seem to be used anymore

}
#endif

// acm, 3-9, correction, POT can be read back, datasheet wrong.  Readback algorithm as stated by Analog FAE: "You can use SDO with SPI mode to read back
// the SPI data, in this case the SPI data is wrttten twice, the second time the data will appear at SDO. SDO can be connected to MISO pin of the SPI uC."
// acm, 2-19-09 abandon, turns out POT cannot be read back in SPI mode (but can in I2C mode, odd).
// acm, 2-18-09 added to read back POT values, check for corruption (note, component DA21 an A5312 DAC is write only)
//char GetExtDAC(char Side, char DACNum)
//{char Addr; Value;

//#ifndef __emulator
//DisableLCD(0xFF);
//
// switch (DACNum){
//    case 0: {if (Side) {SPI_SetCS(SPI_CS_R2); Addr=3;} else {SPI_SetCS(SPI_CS_R1); Addr=2;} break;}  // acm, 2-18-09 select POT chip select here
//    case 1: {if (Side) {SPI_SetCS(SPI_CS_R3); Addr=2;} else {SPI_SetCS(SPI_CS_R1); Addr=3;} break;}
//    case 2: {if (Side) {SPI_SetCS(SPI_CS_R3); Addr=3;} else {SPI_SetCS(SPI_CS_R2); Addr=2;} break;}
//
// }
// SPI_Prepare(SPIFREQ_8000,0);
// SPI_WriteRead(Addr);
// Value=SPI_Read();
//
// SPI_ClearCS();
// EnableLCD();
//#endif
//// ExtDACValue[Side][DACNum]=Value;
//	return (Value);
//}

void InitDAC(void)
{int i,j;

// acm, 2-6-12.  Discovered if balance attempted with current imbalance set too great (25mA/15mA/25mA) resulted in FRAM CRC mismatch.
// Apparently balance routine set pot to 0 or 255.  HERE where Setup in RAM was getting written (Setup...=127).  StartMeasure() continues
// taking a measurement, but retries because magnitude difference too great.  2nd iteration through StartMeasure(), TestCRC() displays 
// error msg.  Resolution:  to be consistant with noise harden philosophy, re-read FRAM if we think there is a noise related error.
// In balancing example here, it will just re-read previously written values, measurement should complete (rather than infinite loop).
			 
			 //LoadSetup(1,1);  // acm, add call, reload setup and invoke SetSetupParam.  Implicit assumption is FRAM is more robust than RAM from noise event
			 // acm, v1.79, spot where AG noticed FullStatus getting clobbered - don't want to change Modbus reg 4/5/6 to SCADA inbetween measurements
			 // upon further thought, changed first parameter from 1 to 0, disabling call to SetSetupParm, no reason for this call upon closer inspection.. 
			 LoadSetup(0,1);
	
 for (i=0;i<MaxTransformerSide;i++) {
     for (j=0;j<MaxBushingCountOnSide;j++) {
//         if ((Setup.GammaSetupInfo.GammaSideSetup[i].ImpedanseValue[j]==255)||// acm, v2.00 4-5-12, comment out.  Really should have done this v1.77 instead of just commenting out assignment (=127). 
//             (Setup.GammaSetupInfo.GammaSideSetup[i].ImpedanseValue[j]==0))
//            Setup.GammaSetupInfo.GammaSideSetup[i].ImpedanseValue[j]=127;     // acm, this was commented out v1.77 (see comment above), but keeping if() above caused next line, conditional call to loadSetup(), oops
			 
			 // acm, v2.00 4-5-12, code review, realized calling LoadSetup() here done multiple times (? - note above comment, or not at all), only need to do this once, so moved to beginning.

			 SetDACDevice(i,Setup.GammaSetupInfo.GammaSideSetup[i].ImpedanseValue[j],j);  // acm, these are the initial values

// acm, 3-29-10, this function is for a future hardware version, e.g. refers to non-existant CS3, and improperly writes pot3.
/*         if ((Setup.GammaSetupInfo.GammaSideSetup[i].ExtImpedanseValue[j]>63)||
             (Setup.GammaSetupInfo.GammaSideSetup[i].ExtImpedanseValue[j]==0))
            Setup.GammaSetupInfo.GammaSideSetup[i].ExtImpedanseValue[j]=32;
         SetExtDAC(i,Setup.GammaSetupInfo.GammaSideSetup[i].ExtImpedanseValue[j],j);  
*/
     }
 }
 
 SetCalibrDAC(512);

}




