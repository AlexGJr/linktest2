#include "defkb.h"
#include "defs.h"
#include "clistbox.h"
#include "graph.h"
#include "picres.h"
#include "keyboard.h"
#include "DefKB.h"
#include "string.h"
#include "LCD.h"
#include <stdlib.h>

/*
typedef struct {
  short CurPosition,Count,Width,NumStrs,X,Y,TopLine;
  char  StrArray[MAXLBLINES][40];
  char  Checked[MAXLBLINES];
  int   Tags[MAXLBLINES];
  char  Font;
  unsigned int UpKey, DwnKey, ActnKey;
#ifdef __arm9
  unsigned int PgUpKey, PgDwnKey;
#endif
} TCheckListBox;
*/


static void DrawLBLine(TCheckListBox *LB, int i){
 char Str[MAXSTRLEN];
 char Delta=(LB->NumStrs<3)?0:2;

  SetFont(LB->Font);

  if((i>=LB->TopLine)&&(i<LB->Count)&&(i<LB->TopLine+LB->NumStrs)) {
    //checked
    if (LB->Type) {
      if (LB->Dynamic) {
        if (LB->GetCh(i)) OutPicture(LB->X+Delta, LB->Y+Delta+(i-LB->TopLine)*CurFontHeight + (CurFontHeight - CheckOn[0])/2, CheckOn); else
          OutPicture(LB->X+Delta, LB->Y+Delta+(i-LB->TopLine)*CurFontHeight + (CurFontHeight - CheckOff[0])/2, CheckOff);
      } else {
        if (LB->Checked[i]) OutPicture(LB->X+Delta, LB->Y+Delta+(i-LB->TopLine)*CurFontHeight + (CurFontHeight - CheckOn[0])/2, CheckOn); else
          OutPicture(LB->X+Delta, LB->Y+Delta+(i-LB->TopLine)*CurFontHeight + (CurFontHeight - CheckOff[0])/2, CheckOff);
      }
    }
    //string
    if (LB->Dynamic) strcpy(Str, LB->GetStr(i));
      else strcpy(Str, LB->StrArray[i]);
    if(i==LB->CurPosition)OutString1Inv(LB->X+Delta+15*LB->Type,LB->Y+Delta+(i-LB->TopLine)*CurFontHeight,Str);else
      OutString1(LB->X+Delta+15*LB->Type,LB->Y+Delta+(i-LB->TopLine)*CurFontHeight,Str);

  }
}

void InitCheckListBox(TCheckListBox *LB,unsigned int X,unsigned int Y,unsigned int Width,unsigned int NumStrs,char Font, char aType)
{
  LB->CurPosition=0;
  LB->TopLine=0;
  LB->Count=0;
  LB->Width=Width;
  LB->NumStrs=NumStrs;
  LB->X=X;
  LB->Y=Y;
  LB->Font=Font;
  LB->Type=aType;
  LB->UpKey=KB_UP;
  LB->DwnKey=KB_DOWN;
  LB->ActnKey=KB_ENTER;
  LB->PgUpKey=KB_MOD_UP;
  LB->PgDwnKey=KB_MOD_DOWN;
  LB->Dynamic=0;
}

void SetDynamicCLB(TCheckListBox *LB, int CountStr, FuncGetStr GetStr, FuncGetCh GetCh){

  LB->Dynamic=1;
  LB->GetStr=GetStr;
  LB->GetCh=GetCh;
  LB->Count=CountStr;
}

void SetCheckListBoxKeys(TCheckListBox *LB, unsigned int aUpKey, unsigned int aDwnKey, unsigned int aActnKey,
                         unsigned int aPgUpKey, unsigned int aPgDwnKey
                         ){
  LB->UpKey=aUpKey;
  LB->DwnKey=aDwnKey;
  LB->ActnKey=aActnKey;
  LB->PgUpKey=aPgUpKey;
  LB->PgDwnKey=aPgDwnKey;
}

void InsertCLBLine(TCheckListBox *LB,unsigned int Position, char *aLine, char aChecked)
{ int i;
  if ((LB->Count<MAXLBLINES)&&(LB->Count>=Position)){
    for(i=LB->Count;i>Position;i--) {
      strcpy(LB->StrArray[i], LB->StrArray[i-1]);
      LB->Checked[i]=LB->Checked[i-1];
    }

    LB->Count++;
    strcpy(LB->StrArray[Position],aLine);
    LB->Checked[Position]=aChecked;
#ifdef __arm9
    LB->Tags[Position]=0;
#endif
  }
}

void EditCLBLine(TCheckListBox *LB,unsigned int Position, char *aLine, char aChecked)
{
  if ((LB->Count<=MAXLBLINES)&&(LB->Count>Position)){
    strcpy(LB->StrArray[Position], aLine);
    LB->Checked[Position]=aChecked;
  }
}

void DeleteCLBLine(TCheckListBox *LB,unsigned int Position)
{ int i;
  if ((LB->Count>0)&&(Position<LB->Count)){
    for(i=Position;i<LB->Count;i++)
      strcpy(LB->StrArray[i], LB->StrArray[i+1]);
    LB->Count--;
    if(LB->Count>0)
      if(LB->CurPosition>=LB->Count)LB->CurPosition=LB->Count-1;
    if(LB->Count>0)
      if(LB->TopLine>=LB->Count)LB->TopLine=LB->Count-1;
  }
}

void ClearCheckListBox(TCheckListBox *LB)
{
  if (LB->Count>0)
  {
   LB->Count=0;
  }
}

void DrawScroller(TCheckListBox *LB, int Position, char Draw){
float curprocent, cury;
unsigned int d1=CurFontWidth%2, y, w2 = CurFontWidth/2;

  if (LB->Count<2) curprocent=0; else
    curprocent=(float)(Position)/(float)(LB->Count-1);

  if(LB->NumStrs<3)
    cury=(float)(LB->Y)+(float)(LB->NumStrs-1)*(float)CurFontHeight*curprocent;
  else
    cury=(float)(LB->Y)+((float)(LB->NumStrs*CurFontHeight+4-CurFontHeight-CurFontHeight))*curprocent+(float)CurFontHeight;

  if(cury-(unsigned int)cury>0.5)y=(unsigned int)cury+1;
    else y=(unsigned int)cury;

  if (Draw) {
    if(LB->NumStrs<3)
      DrawRect(LB->X+LB->Width-w2-1,y,LB->X+LB->Width-w2+1,y+CurFontHeight-1);
    else
      DrawRect(LB->X+LB->Width-CurFontWidth-d1-1,y,LB->X+LB->Width-1,y+CurFontHeight-1);
  } else {
    if(LB->NumStrs<3){
      ClearRect(LB->X+LB->Width-w2-1, y, CurFontHeight, 3);
      DrawLine(LB->X+LB->Width-w2,LB->Y,LB->X+LB->Width-w2,LB->Y+LB->NumStrs*CurFontHeight-1);
    } else {
      ClearRect(LB->X+LB->Width-CurFontWidth-d1, y, CurFontHeight+1, (CurFontWidth+d1));
      DrawRect(LB->X+LB->Width-CurFontWidth-d1-1,LB->Y+CurFontHeight,LB->X+LB->Width-1,LB->Y+(LB->NumStrs-1)*CurFontHeight+3);
    }
  }

#ifdef MEGA
Redraw();
#endif
}

void CLBScrollUp(TCheckListBox *LB, char *NeedRedraw)
{
   if (LB->CurPosition>0)
   {LB->CurPosition--;
    if(LB->CurPosition<LB->TopLine){
      LB->TopLine=LB->CurPosition;
      *NeedRedraw=1;
    } else {
      DrawLBLine(LB, LB->CurPosition+1);
      DrawLBLine(LB, LB->CurPosition);

      //перерисовка скроллера
      DrawScroller(LB, LB->CurPosition+1, 0);
      DrawScroller(LB, LB->CurPosition, 1);
    }
   }
}

void CLBScrollDown(TCheckListBox *LB, char *NeedRedraw)
{
  if (LB->CurPosition<LB->Count-1)
   {LB->CurPosition++;
    if(LB->CurPosition>=(LB->TopLine+LB->NumStrs)) {
      LB->TopLine=LB->CurPosition-LB->NumStrs+1;
      *NeedRedraw=1;
    } else {
      DrawLBLine(LB, LB->CurPosition-1);
      DrawLBLine(LB, LB->CurPosition);

      //перерисовка скроллера
      DrawScroller(LB, LB->CurPosition-1, 0);
      DrawScroller(LB, LB->CurPosition, 1);
    }
   }
}


void ReDrawCheckListBox(TCheckListBox *LB)
{ int i;
  char w2, d1, d2, d3;

   SetFont(LB->Font);

   d1=CurFontWidth%2;
   w2=CurFontWidth/2;
   if(CurFontWidth>=8) d2=2;else d2=1;
   if (LB->NumStrs>2) d3=4; else d3=0;

   ClearRect(LB->X,LB->Y,LB->NumStrs*CurFontHeight+d3,LB->Width);

   for(i=LB->TopLine;i<min((int)(LB->TopLine+LB->NumStrs),(int)LB->Count);i++) {
     DrawLBLine(LB, i);
   }


   if (LB->NumStrs>2){
     DrawRect(LB->X,LB->Y,LB->X+LB->Width-1,LB->Y+LB->NumStrs*CurFontHeight+3);
     DrawRect(LB->X+LB->Width-CurFontWidth-d1-1,LB->Y,LB->X+LB->Width-1,LB->Y+LB->NumStrs*CurFontHeight-1+d3);
     DrawRect(LB->X+LB->Width-CurFontWidth-d1-1,LB->Y+CurFontHeight,LB->X+LB->Width-1,LB->Y+(LB->NumStrs-1)*CurFontHeight-1+d3);

     DrawLine(LB->X+LB->Width-CurFontWidth+d2-1,LB->Y+CurFontHeight-d2,LB->X+LB->Width-w2-d1-1,LB->Y+d2);
     DrawLine(LB->X+LB->Width-w2-d1-1,LB->Y+d2,LB->X+LB->Width-d2-1,LB->Y+CurFontHeight-d2);
     DrawLine(LB->X+LB->Width-d2-1,LB->Y+CurFontHeight-d2,LB->X+LB->Width-CurFontWidth+d2-d1-1,LB->Y+CurFontHeight-d2);

     DrawLine(LB->X+LB->Width-CurFontWidth+d2-1,LB->Y+(LB->NumStrs-1)*CurFontHeight-1+d2+d3,LB->X+LB->Width-w2-d1-1,LB->Y+LB->NumStrs*CurFontHeight-1-d2+d3);
     DrawLine(LB->X+LB->Width-w2-d1-1,LB->Y+LB->NumStrs*CurFontHeight-1-d2+d3,LB->X+LB->Width-d2-1,LB->Y+(LB->NumStrs-1)*CurFontHeight-1+d2+d3);
     DrawLine(LB->X+LB->Width-d2-1,LB->Y+(LB->NumStrs-1)*CurFontHeight-1+d2+d3,LB->X+LB->Width-CurFontWidth+d2-d1-1,LB->Y+(LB->NumStrs-1)*CurFontHeight-1+d2+d3);

     DrawScroller(LB, LB->CurPosition, 1);
   } else {
     DrawLine(LB->X+LB->Width-w2,LB->Y,LB->X+LB->Width-w2,LB->Y+LB->NumStrs*CurFontHeight-1);
     DrawScroller(LB, LB->CurPosition, 1);
   }
}

void HandleCheckListBox(TCheckListBox *LB, KEY key, char *NeedRedraw){

if(key==LB->UpKey)CLBScrollUp(LB, NeedRedraw);
else if (key==LB->DwnKey)CLBScrollDown(LB, NeedRedraw);
else if (key==LB->ActnKey){
  if (LB->Type) {
    (LB->Checked[LB->CurPosition])?(LB->Checked[LB->CurPosition]=0):(LB->Checked[LB->CurPosition]=1);
    *NeedRedraw=1;
  }
}else if (key==LB->PgUpKey){
if(LB->CurPosition) {
  if(LB->TopLine>=LB->NumStrs){
    LB->TopLine-=LB->NumStrs;
    LB->CurPosition-=LB->NumStrs;
  } else {
    LB->TopLine=0;
    LB->CurPosition=0;
  }
  *NeedRedraw=1;
}
}else if (key==LB->PgDwnKey){
if(LB->CurPosition<LB->Count-1){
  if(LB->TopLine+LB->NumStrs*2<LB->Count-1) {
    LB->TopLine+=LB->NumStrs;
    LB->CurPosition+=LB->NumStrs;
  } else {
//    LB->TopLine=max(LB->Count-LB->NumStrs, 0);
    if (LB->NumStrs>LB->Count) LB->TopLine=0; else LB->TopLine=LB->Count-LB->NumStrs;
    LB->CurPosition=LB->Count-1;
  }
  *NeedRedraw=1;
}
}

}
