
#include "Defs.h"

// 11-6-08 define english as it wasn't anywhere else (referenced in protocol.h too)
#define engl 1

#ifdef engl
  #define LANG(E,R) E
#else
  #define LANG(E,R) R
#endif

#ifdef TDM
ROM char DeviceStr[]=LANG("DRI BHM", "TDM R1500 ");
#else
ROM char DeviceStr[]=LANG("R1500/6", "R1500/6");
#endif

ROM char msResetProcess[]  = "Clearing settings...";  // acm, 7-14-10, MODBUS default command change

ROM char EmptyStr[]=LANG("", "");
ROM char VersionStr[]=LANG("Version", " ������");
//ROM char VersionNStr[]=LANG("1.6","2.02");  // acm 10-15-09 gotta change this too to display right ver, use version in TRSTR.cpp for consistancy
ROM char CalibrStr[]=LANG(".Calibrating.",".Calibrating.");
ROM char UnnStr[]=LANG("Unn=","Unn=");
ROM char PhaseAStr[]=LANG("PhA=","PhA=");
ROM char PhaseBStr[]=LANG("PhB=","PhB=");
ROM char PhaseCStr[]=LANG("PhC=","PhC=");
ROM char ErrorStr[]=LANG("Error","Error");
ROM char SendDataStr[]=LANG("Connection","Connection");
ROM char AmplEdStr[]=LANG("mV","mV");
ROM char AmplGainStr[]=LANG("Gain(  )","Gain(  )");
ROM char DegStr[]=LANG("Deg","Deg");
ROM char PhaseStr[]=LANG("Phase=","Phase=");
ROM char AmplEdPrcStr[]=LANG("%","%");

ROM char DateSeparator[]=LANG("/","/");
ROM char TimeSeparator[]=LANG(":",":");

//Setup
ROM char SecStr[]    = LANG("sec.","sec.");
ROM char MinStr[]  = LANG("min.","min.");
ROM char PercentStr[]  = LANG("%","%");
ROM char DegrStr[]  = LANG("dgr.","dgr.");
ROM char CStr[]  = LANG("C","C");

ROM char InpPwdStr[]       = LANG("Password:", "  ������:");
ROM char StBalansStr[]     = LANG("Balancing", "Balancing");

ROM char SelSideStr1[]     = LANG("Select input", "Select input");
//ROM char SelSideStr2[]     = LANG("Side", "Side");
//ROM char SelSideStr3[]     = LANG("Left ", "Left ");
//ROM char SelSideStr4[]     = LANG("Right", "Right");
ROM char SelSideStr2[]     = LANG("", "");
ROM char SelSideStr3[]     = LANG("Set 1", "Set 1");
ROM char SelSideStr4[]     = LANG("Set 2", "Set 2");

ROM char DateStr[]         = LANG("Date", "Date");

ROM char TimeStr[]         = LANG("Time", "Time");
ROM char ReadTimeStr1[]    = LANG("Reading", "Reading");
ROM char ReadTimeStr2[]    = LANG("Schedule", "Schedule");
ROM char BaudRateStr1[]    = LANG("  Set", "  Set");
ROM char BaudRateStr2[]    = LANG("Baud rate", "Baud rate");
ROM char BaudRateStr3[]    = "9600  ";
ROM char BaudRateStr4[]    = "38400 ";
ROM char BaudRateStr5[]    = "57600 ";
ROM char BaudRateStr6[]    = "115200";
ROM char BaudRateStr7[]    = "230400";
ROM char BaudRateStr8[]    = "500000";
ROM char BaudRateStr9[]    = "1000000";
ROM char ReadFromSideStr1[]= LANG("Read data", "Read data");
ROM char ReadFromSideStr2[]= LANG(" from ..." ," from ...");
ROM char ProtocolStr1[]    = LANG(" ModBus" ,"ModBus");
ROM char ProtocolStr2[]    = LANG("protocol" ,"protocol");
ROM char ProtocolStr3[]    = LANG("RTU" ,"RTU");
ROM char ProtocolStr4[]    = LANG("TCP" ,"TCP");

ROM char RelayStr1[]    = LANG("  Select",   "  Select");
ROM char RelayStr2[]    = LANG("Relay mode", "Relay mode");
ROM char RelayStr3[]    = LANG("OFF    ","OFF    ");
ROM char RelayStr4[]    = LANG("ON     ","ON     ");
ROM char RelayStr5[]    = LANG("by time","by time");
ROM char RelayStr6[]    = LANG("Time of relay","Time of relay");

//ROM char OnOffSideStr1[]   = LANG("Side left", "Side left");
//ROM char OnOffSideStr2[]   = LANG("Side right","Side right");
ROM char OnOffSideStr1[]   = LANG("Set 1","Set 1");
ROM char OnOffSideStr2[]   = LANG("Set 2","Set 2");
ROM char OnOffSideStr3[]   = LANG("OFF", "OFF");
ROM char OnOffSideStr4[]   = LANG("ON ", "ON ");

ROM char MeasuringStr[]    = LANG("..Measuring..", "..Measuring..");
ROM char LoadingStr[]      = LANG("...Loading...", "...Loading...");

ROM char ReadTimeStr3[]    = LANG("Schedule", "Schedule");
ROM char ReadTimeStr4[]    = LANG("by interval", "by interval");
ROM char ReadTimeStr5[]    = LANG("by time    ", "by time    ");
ROM char ReadTimeStr6[]    = LANG("dt-", "dt-");

ROM char DeviceNumberStr1[]= LANG("Device", "Device");
ROM char DeviceNumberStr2[]= LANG("number", "number");
ROM char DeviceNumberStr3[]= LANG("#", "#");

ROM char StoppedStr1[]     = LANG("Stop", "Stop");
ROM char StoppedStr2[]     = LANG("Monitoring", "Monitoring");
ROM char StoppedStr3[]     = LANG("stopped", "stopped");
ROM char StoppedStr4[]     = LANG(" work  ", " work  ");

ROM char FillPauseStr1[]   = LANG("Fill" , "Fill");
ROM char FillPauseStr2[]   = LANG("pause", "pause");
ROM char FillPauseStr3[]   = LANG("Show", "Show");
ROM char FillPauseStr4[]   = LANG(" Date,Time "," Date,Time ");
ROM char FillPauseStr5[]   = LANG(" Read time "," Read time ");
ROM char FillPauseStr6[]   = LANG("    Unn    ","    Unn    ");
ROM char FillPauseStr7[]   = LANG("Temperature","Temperature");
ROM char FillPauseStr8[]   = LANG("   Trend   ","   Trend   ");
ROM char FillPauseStr9[]   = LANG("Temp. coeff","Temp.coeff ");
ROM char FillPauseStr10[]  = LANG("   Alarm   ","   Alarm   ");
ROM char FillPauseStr11[]  = LANG("Tg,Capacity","Tg,Capacity");
ROM char FillPauseStr12[]  = LANG("   Zk      ","   Zk      ");
ROM char FillPauseStr13[]  = LANG("Humidity    %","Humidity    %");
ROM char FillPauseStr14[]  = LANG("Current    ","Current    ");
ROM char FillPauseStr15[]  = LANG("Humidity   ","Humidity   ");

ROM char ShowPauseStr6[]   = LANG("Unn=","Unn=");
ROM char ShowPauseStr10[]  = LANG("Alarm","Alarm");
ROM char ShowPauseStr11[]  = LANG("Tg       Cap.","Tg       Cap.");

ROM char ShowTimeStr1[]    = LANG("Show time","Show time");
//ROM char ShowTimeStr2[]    = LANG("sec.","sec.");

ROM char AverageNumStr1[]  = LANG("Average","Average");
ROM char AverageNumStr2[]  = LANG("number","number");

ROM char ReReadAlarmStr1[]  = LANG("Re-Reading","Re-Reading");
ROM char ReReadAlarmStr2[]  = LANG("  Alarm   ","  Alarm   ");

ROM char AlarmHysteresisStr1[]  = LANG("  Alarm   ","  Alarm   ");
ROM char AlarmHysteresisStr2[]  = LANG("Hysteresis","Hysteresis");

ROM char DaysTrendStr1[]  = LANG(" Days to  "," Days to  ");
ROM char DaysTrendStr2[]  = LANG("Calc trend","Calc trend");
ROM char DaysStr[]  = LANG("days","days");

ROM char DaysTKStr1[]  = LANG(" Days to  "," Days to  ");
ROM char DaysTKStr2[]  = LANG("Calc TempK","Calc TempK");

ROM char PhaseDispStr1[]  = LANG("  Phase   ","  Phase   ");
ROM char PhaseDispStr2[]  = LANG("Dispersion","Dispersion");

ROM char AlarmEnableStr1[]   = LANG("  Setup   " ,"  Setup   ");
ROM char AlarmEnableStr2[]   = LANG("Alarm type", "Alarm type");
//ROM char AlarmEnableStr3[]   = LANG("HV side ","HV side ");
//ROM char AlarmEnableStr4[]   = LANG("LV side ","LV side ");
//ROM char AlarmEnableStr5[]   = LANG("HV2 side","HV2 side");
//ROM char AlarmEnableStr6[]   = LANG("LV2 side","LV2 side");
ROM char AlarmEnableStr3[]   = LANG("Set 1","Set 1");
ROM char AlarmEnableStr4[]   = LANG("Set 2","Set 2");
ROM char AlarmEnableStr5[]   = LANG("Set 3","Set 3");
ROM char AlarmEnableStr6[]   = LANG("Set 4","Set 4");
ROM char AlarmEnableStr7[]   = LANG("Unn Alarm  ","Unn Alarm  ");
ROM char AlarmEnableStr8[]   = LANG("TempK Alarm","TempK Alarm");
ROM char AlarmEnableStr9[]   = LANG("Trend Alarm","Trend Alarm");
ROM char AlarmEnableStr10[]  = LANG("Tg Alarm   ","Tg Alarm   ");
ROM char AlarmEnableStr11[]  = LANG("SpdTg Alarm","SpdTg Alarm");
ROM char AlarmEnableStr12[]  = LANG("Zk Alarm   ","Zk Alarm   ");
ROM char AlarmEnableStr13[]  = LANG("SpdZk Alarm","SpdZk Alarm");
ROM char AlarmEnableStr14[]  = LANG("HV-LV side ","HV-LV side ");
ROM char AlarmEnableStr15[]  = LANG("Zk","Zk");

ROM char AlarmThreshStr1[]   = LANG("Unn Alarm" ,"Unn Alarm");
ROM char AlarmThreshStr2[]   = LANG("Thresholds", "Thresholds");
ROM char AlarmThreshStr3[]   = LANG("Yellow, %" ,"Yellow, %");
ROM char AlarmThreshStr4[]   = LANG("Red   , %", "Red   , %");
ROM char AlarmThreshStr5[]   = LANG(" Tangent" ," Tangent");
ROM char AlarmThreshStr6[]   = LANG("Yellow   " ,"Yellow   ");
ROM char AlarmThreshStr7[]   = LANG("Red      ", "Red      ");
ROM char AlarmThreshStr8[]   = LANG("Variation", "Variation");

ROM char TKAlarmStr1[]   = LANG("Temp. koef." ,"Temp. koef.");
ROM char TKAlarmStr2[]   = LANG(" Threshold ", " Threshold ");
ROM char TKAlarmStr3[]   = LANG("TK, %Unn/C" ,"TK, %Unn/C");

ROM char TrendAlarmStr1[]   = LANG("Unn trend" ,"Unn trend");
ROM char TrendAlarmStr2[]   = LANG("Threshold ", "Threshold ");
ROM char TrendAlarmStr3[]   = LANG("Trend" ,"Trend");

ROM char RVoltageStr1[]   = LANG("  Rated " ,"  Rated ");
ROM char RVoltageStr2[]   = LANG(" Voltage", " Voltage");
ROM char RVoltageStr3[]   = LANG("Volt.,kV" ,"Volt.,kV");

ROM char RCurrentStr1[]   = LANG("  Rated " ,"  Rated ");
ROM char RCurrentStr2[]   = LANG(" Current", " Current");
ROM char RCurrentStr3[]   = LANG("Curr.,A" ,"Curr.,A");

ROM char Tg0Str1[]   = LANG("Off-line Tg" ,"Off-line Tg");
ROM char Tg0Str2[]   = LANG("  value  ", "  value  ");
ROM char Tg0Str3[]   = LANG("Tg A,%","Tg A,%");
ROM char Tg0Str4[]   = LANG("Tg B,%","Tg B,%");
ROM char Tg0Str5[]   = LANG("Tg C,%","Tg C,%");

ROM char C0Str1[]   = LANG("Off-line C" ,"Off-line C");
ROM char C0Str2[]   = LANG("  value  ", "  value  ");
ROM char C0Str3[]   = LANG("C A,pF","C A,pF");
ROM char C0Str4[]   = LANG("C B,pF","C B,pF");
ROM char C0Str5[]   = LANG("C C,pF","C C,pF");

ROM char TgTempStr1[]   = LANG("Temperature" ,"Temperature");
ROM char TgTempStr2[]   = LANG("measur. Tg", "measur. Tg");
ROM char TgTempStr3[]   = LANG("Temp. ,C" ,"Temp. ,C");

ROM char InputCStr1[]   = LANG("  Sensor" ,"  Sensor");
ROM char InputCStr2[]   = LANG(" capacity", " capacity");
ROM char InputCStr3[]   = LANG("Cap.A,mkF","Cap.A,mkF");
ROM char InputCStr4[]   = LANG("Cap.B,mkF","Cap.B,mkF");
ROM char InputCStr5[]   = LANG("Cap.C,mkF","Cap.C,mkF");

ROM char ImpedanceStr1[]   = LANG("  Input" , "  Input");
ROM char ImpedanceStr2[]   = LANG("Impedance","Impedance");
ROM char ImpedanceStr3[]   = LANG("Imp.A,Ohm","Imp.A,Ohm");
ROM char ImpedanceStr4[]   = LANG("Imp.B,Ohm","Imp.B,Ohm");
ROM char ImpedanceStr5[]   = LANG("Imp.C,Ohm","Imp.C,Ohm");

ROM char CalcZkStr1[]   = LANG("Calculate " ,"Calculate ");
ROM char CalcZkStr2[]   = LANG("Zk for ...", "Zk for ...");
ROM char CalcZkStr3[]   = LANG("Calc. Zk", "Calc. Zk");

ROM char CurrentTypeStr1[]    = LANG("Calculate", "Calculate");
ROM char CurrentTypeStr2[]    = LANG("Load","Load");
ROM char CurrentTypeStr03[]   = LANG(" - "," - ");
ROM char CurrentTypeStr3[]    = LANG(" 1 "," 1 ");
ROM char CurrentTypeStr4[]    = LANG(" 2 "," 2 ");
ROM char CurrentTypeStr5[]    = LANG(" 3 "," 3 ");
ROM char CurrentTypeStr6[]    = LANG("Calculate load", "Calculate load");
ROM char CurrentTypeStr7[]    = LANG(" on channel", " on channel");
ROM char CurrentTypeStr8[]    = LANG("A-a","A-a");
ROM char CurrentTypeStr9[]    = LANG("B-b","B-b");
ROM char CurrentTypeStr10[]   = LANG("C-c","C-c");
ROM char CurrentTypeStr11[]   = LANG("Avg","Avg");

ROM char OutPhA[]    = LANG("Phase A","Phase A");
ROM char OutPhB[]    = LANG("Phase B","Phase B");
ROM char OutPhC[]    = LANG("Phase C","Phase C");

ROM char CalibrVStr1[]    = LANG("Calibration", "Calibration");
ROM char CalibrVStr2[]    = LANG(" voltages  ", " voltages  ");
ROM char CalibrVStr3[]    = LANG("ADC:", "ADC:");
ROM char CalibrVStr4[]    = LANG(",V:", ",V:");

ROM char CalibrIStr1[]    = LANG("Calibration", "Calibration");
ROM char CalibrIStr2[]    = LANG(" Current   ", " Current   ");
ROM char CalibrIStr3[]    = LANG("Slope","Slope");
ROM char CalibrIStr4[]    = LANG("Offset","Offset");

ROM char XXModeStr1[]    = LANG("Open-circuit", "Open-circuit");
ROM char XXModeStr2[]    = LANG(" conditions ", " conditions ");
ROM char XXModeStr3[]    = LANG("Balansing needed!", "Balansing needed!");
ROM char XXModeStr4[]    = LANG("Save initial Zk?", "Save initial Zk?");

ROM char ArchViewCaption[]      = LANG("Measurements","Measurements");
ROM char ArchLoading[]          = LANG("Load measurements...","");
ROM char NoArchiveError[]       = LANG("No measurements!", "��� ����������� �������!");

ROM char DelArchiveStr1[]       = LANG("Delete", "Delete");
ROM char DelArchiveStr2[]       = LANG("All measurements?", "All measurements?");
ROM char DelArchiveStr3[]       = LANG("Yes    No", "Yes    No");
ROM char DelArchiveStr4[]       = LANG("Deleting...", "Deleting...");
ROM char DelArchiveStr5[]       = LANG("Archive", "Archive");
ROM char DelArchiveStr6[]       = LANG("Cleared!", "Cleared!");

ROM char AlarmStatusStr0[]      = LANG("NO ALARMS", "NO ALARMS");
ROM char AlarmStatusStr1[]      = LANG("Unn", "Unn");
ROM char AlarmStatusStr2[]      = LANG(" Trend", " Trend");
ROM char AlarmStatusStr3[]      = LANG(" T.Coeff", " T.Coeff");

ROM char DelArchStr1[]          = LANG("Delete", ",Delete");
ROM char DelArchStr2[]          = LANG("Archive", ",Archive");
ROM char DelArchStr3[]          = LANG("Clear all", ",Clear all");
ROM char DelArchStr4[]          = LANG("device data", ",device data");

ROM char SingleStr1[]           = LANG("Single", ",Single");
ROM char SingleStr2[]           = LANG("Measurement", ",Measurement");

ROM char DelArchiveConfirm[]    = LANG("Clear archive?", ",Clear archive?");
ROM char DelMeasConfirm[]       = LANG("Delete?", ",Delete?");
ROM char YesStr[]               = LANG("Yes", ",Yes");
ROM char NoStr[]                = LANG("No", ",No");

ROM char SaveInitStr[]          = LANG("Save data?", ",Save data?");

ROM char BaseLineStr1[]         = LANG("  Run", "  Run");
ROM char BaseLineStr2[]         = LANG("Base Line", "Base Line");
ROM char BaseLineStr3[]         = LANG("Recalc OK", "Recalc OK");
ROM char BaseLineStr4[]         = LANG("No recalc", "No recalc");
ROM char BaseLineStr5[]         = LANG("on days", "on days");

ROM char ShortPhaseA[]          = LANG("A", "A");
ROM char ShortPhaseB[]          = LANG("B", "B");
ROM char ShortPhaseC[]          = LANG("C", "C");

ROM char EmptyTime[]            = LANG("--:--", "--:--");

ROM char StrTEMP[]              = LANG("Temperature","Temperature");
//ROM char StrTEMP1[]             = LANG("measure, C   ","measure, C   ";
ROM char StrTEMP2[]             = LANG(" 0C ->      C"," 0C ->      C");
ROM char StrTEMP3[]             = LANG("100C->      C","100C->      C");

ROM char CalibrTStr1[]          = LANG("Calibration", "Calibration");
ROM char CalibrTStr2[]          = LANG("temperature", "temperature");

ROM char CalibrTStr3[]          = LANG("Channel", "Channel");
ROM char CalibrTStr4[]          = LANG("1", "1");
ROM char CalibrTStr5[]          = LANG("2", "2");
ROM char CalibrTStr6[]          = LANG("3", "3");
ROM char CalibrTStr7[]          = LANG("4", "4");

ROM char CalibrHStr1[]          = LANG("Calibration", "Calibration");
ROM char CalibrHStr2[]          = LANG(" Humidity",   " Humidity");
ROM char CalibrHStr3[]          = LANG("Offset", "Offset");
ROM char CalibrHStr4[]          = LANG("Slope ", "Slope ");

ROM char TOnSetStr1[]           = LANG("Temperature", "Temperature");
ROM char TOnSetStr2[]           = LANG(" channels"  , " channels");
ROM char TOnSetStr3[]           = LANG("Not connect ", "Not connect ");
ROM char TOnSetStr4[]           = LANG("On channel 1", "On channel 1");
ROM char TOnSetStr5[]           = LANG("On channel 2", "On channel 2");
ROM char TOnSetStr6[]           = LANG("On channel 3", "On channel 3");
ROM char TOnSetStr7[]           = LANG("On channel 4", "On channel 4");

ROM char HeaterTStr1[]          = LANG(" Heater on", " Heater on");
ROM char HeaterTStr2[]          = LANG("temperature", "temperature");
ROM char HeaterTStr3[]          = LANG("Temp <   C", "Temp <   C");

ROM char XXTestStr1[]           = LANG(" Data","������");
ROM char XXTestStr2[]           = LANG("  saved.","���������.");
ROM char XXTestStr3[]           = LANG("  not saved.","�� ���������.");
ROM char XXTestStr4[]           = LANG("No load","  ���� ");
ROM char XXTestStr5[]           = LANG(" Test  ","   ��  ");

ROM char StTimeBalansStr[]      = LANG("On Time", "�� �������");
ROM char StTimeBalansStr1[]     = LANG("Since date", "Since date");
ROM char StTimeBalansStr2[]     = LANG("Since time", "Since time");
ROM char StTimeBalansStr3[]     = LANG("hour", "�����");
ROM char StTimeBalansStr4[]     = LANG("Working", "Working");
ROM char StTimeBalansStr5[]     = LANG("days", "days");
ROM char StTimeBalansStr6[]     = LANG("Start supervision?", "Start supervision?");
ROM char StTimeBalansStr7[]     = LANG("Run no load test?", "Run no load test?");

ROM char BalancingStr[]     = LANG("..Balancing..", "..Balancing..");


