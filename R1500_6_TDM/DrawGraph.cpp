#include "LCD.h"
#include "Defs.h"
#include "KeyBoard.h"
#include "DefKB.h"
#include "Graph.h"
#include "PicRes.h"
#include "DrawGraph.h"
#include "RAM.h"


void InitGraph(struct StOneGraph *Graph,
                           char StartPage,
                           short *Addr,
                           int Count
                           )
{
 unsigned int i;
 int Y ;

   (*Graph).BaseAddress=Addr;
   (*Graph).StartPage=StartPage;
   (*Graph).XLeft=0;
   (*Graph).MaxXLeft=0;
   (*Graph).XRight=Count-1;
   (*Graph).MaxXRight=Count-1;


   (*Graph).CountX=Count;

   (*Graph).YMax=-100000;
   (*Graph).YMin=100000;
   SetPage((*Graph).StartPage);

   for (i=((*Graph).XLeft); i<=((*Graph).XRight); i++){
//           Y=GetYValue5(Graph,i);
           Y=((short *)(*Graph).BaseAddress)[i];
           if (Y>(*Graph).YMax) (*Graph).YMax=Y;
           if (Y<(*Graph).YMin) (*Graph).YMin=Y;
   }


   if (((*Graph).YMax==0)&&((*Graph).YMin==0)){
      (*Graph).YMax++;
      (*Graph).YMin--;
   }
}




void DrawGraph(struct StOneGraph *Graph,
                      char X1,char Y1,
                      char X2,char Y2)
{
 unsigned int i;
 int Y ;
 int YY1,YY2,XX1,XX2;

//   DrawLine(X1,Y1,X2,Y1);
//   DrawLine(X2,Y1,X2,Y2);
//   DrawLine(X2,Y2,X1,Y2);
   DrawLine(X1,Y2,X1,Y1);

   DrawLine(X1,Y1+(Y2-Y1+1)/2,X2,Y1+(Y2-Y1+1)/2);
   (*Graph).dX1=X2-X1-1;
   (*Graph).dX2=((*Graph).XRight-(*Graph).XLeft);
   (*Graph).dY1=Y2-Y1/*-2*/;
   (*Graph).dY2=(*Graph).YMax-(*Graph).YMin;
   XX1=X1+1;
//   XX2=XX1;
   SetPage((*Graph).StartPage);
   Y=((short*)(*Graph).BaseAddress)[(*Graph).XLeft];
   YY1=Y2/*-2*/-(((long)Y-(long)(*Graph).YMin)*(long)(*Graph).dY1/(long)(*Graph).dY2);
   for (i=(*Graph).XLeft+1; i<=((*Graph).XRight); i++){
       Y=((short*)(*Graph).BaseAddress)[i];
       YY2=(long)Y2/*-2*/-(((long)Y-(long)(*Graph).YMin)*(long)(*Graph).dY1/(long)(*Graph).dY2);
       XX2=(long)X1+2+((long)(*Graph).dX1*(long)(i-(*Graph).XLeft)/(long)(*Graph).dX2-1);
       DrawLine(XX1,YY1,XX2,YY2);
       XX1=XX2;
       YY1=YY2;
   }
}
