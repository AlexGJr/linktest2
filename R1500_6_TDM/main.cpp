#ifndef __arm
#include "Protocol.h"
#include "ADC.h"
#include "DAC.h"
#include "ModBusTCP\UART.h"
#include "ModBusTCP\ModbusServer.h"
#else
#include "BoardAdd.h"
#include "ADCDevice.h"
#include "DACDevice.h"
#include "FRAM.h"
#include "UART.h"
#ifndef __emulator
#include "WatchDog.h"
#include "Timer.h"
#endif
#endif

#include "Init.h"
#include "LCD.h"
#include "Keyboard.h"
#include "PicRes.h"
#include "DateTime.h"
#include "MainWin.h"
#include "SysUtil.h"
#include "Defs.h"
#include "Setup.h"
#include "Archive.h"
#include "GammaMeasurement.h"
#include "CrossMeasurement.h"
#include "Link.h"
#include "RAM.h"
#include "Error.h"
#include "Graph.h"
#include "StrConst.h"
#include "Balans.h"
#include "Test.h"
#include "LogFile.h"
#include "DrvFlash.h"
#include "SetChannels.h"
#include "USB.h"
#include "DefKB.h"
#include "RTC.h"

#include "Complex.h"

#include "Diagnosis.h"  // acm, 4-1-10, need to include this so can initialize structure after boot, otherwise when trying
// to init Diagnosis-> elements, get Pe154 or Pe393 errors, incomplete type.  Compilier gets confused for some reason.

//extern struct stDiagnosis *Diagnosis;
#include "TypesDef.h"

#include "trstr.h" // acm, need for fw_build_date
#include <string.h>	// acm, v2.01
#include "graph.h"

extern int TestSetupCRC(void);  // acm, 3-10-10 explicitly do this even though not doing works
extern void TestSetup(); // acm, do this so Understand resolves
extern void clr_RTC_STATUS();
extern char RTC_STATUS();
extern void LoadSetup(char SetParam, char SetTime);
extern u32 FullAutoBalansing(void);
extern void AT91F_enable_interrupt(void);  // acm, defined extern in BoardAdd.h, but not included here, crappy code!
extern void AT91F_disable_interrupt(void);
extern void RunTest(void);
extern void Init(void);

#include "Setup.h"

#ifndef __arm
#ifdef __emulator
#include "Main.h"
#endif
#endif

//Curren state of Led
volatile char Led=0;
//Current state of Relay
volatile char Relay=0;
#ifdef __arm
unsigned int
#else
char
#endif
//Start Measuring
ReadOnTime=0,
ReadSingle=0,
//Start Balance
AutoBalance=0,
RunXX=0,
PreSetDeviceAddr=0,
IsCurrentGain=0,
CurrentGain=1
;
unsigned long RainTo=0; //DE 4-1-16 Rain timer
unsigned int cntTemp=0;
unsigned int PausedFrom=0;
unsigned int OutRelayFrom=0;
unsigned int DateOfCalcTK=0;       //Date to calculate temperature coefficient
unsigned char Silenced=0;

//Las measurement result
extern signed char MeasurementResult;

float WatchAmpl[MaxTransformerSide]={0,0};
unsigned long WatchCounter=0;

u32 RunWatch=0;

u16 OneSec=0; //2.06 1 sec counter
u16 SwWd=0;   //2.06 Software Watchdog counter

_TDateTime TimeNextMeas=0;      //Next measurement time
_TDateTime EndMeasurementTime=0;  // acm, define here to allow reference in set_time() procedure

#ifndef __arm
extern char KEYB;
#else
struct StDateTime DateTime;
#endif
char C50=0;
char fresh_RTC=0;
int stale_RTC; //acm 11-18-10

char swap_phaseBC[2]; 	// acm, v1.79, swap A/D input, array for noswap, set1 and set2.
char setx_state=0;		// acm, swap_phaseBC[] index,  0==set1 phaseA/B/C selected, 1==set2 phaseA/B/C selected.  Driven by SetInputChannel()

char Defect_SM=0;					// acm, v1.79, yellow LED state byte
extern void OutLED(char Status);	// acm, v1.79, used to resume Yellow/Red warning/alarm

static _TTime GetMinTimeInArray(_TTime CurTime)
{ _TTime Time,i,tmp;

Time=0xFFFFFFFF;
for (i=0;i<MaxMeasurementsPerDay;i++) {
  tmp=EncodeTime(Setup.SetupInfo.MeasurementsInfo[i].Hour,Setup.SetupInfo.MeasurementsInfo[i].Min,0);
  if ((tmp)&&(tmp>CurTime)&&(tmp<Time)) Time=tmp;  // ag 4-16-16 returned to original acm, 1-10-11, change tmp>=CurTime, fix skip measurment endcase?  Yes, but results in multiple measurements per schedule if measurement time <1min (customer won't see).
}
if (Time==0xFFFFFFFF) return 0;
else return Time;
}

void GetNextTime(_TDateTime CurTime)
{ _TDateTime  Time;
//  unsigned int Day,Month,Year;
_TTime tmp;

Time=0;
if (Setup.SetupInfo.ScheduleType) {
  tmp=GetMinTimeInArray(DateTimeToTime(CurTime));
  Time=TimeToDateTime(tmp);
  if (Time==0) {
    //        if (MaxMeasurementsPerDay>1) {
    Time=GetMinTimeInArray(0);
    if (Time!=0) {
      if (DateTimeToTime(CurTime)>Time)
        Time=DateToDateTime((DateTimeToDate(CurTime)+1));
      else
        Time=DateToDateTime((DateTimeToDate(CurTime)));
      Time=AddTime(Time,GetMinTimeInArray(0),0);
    }
    //        }
  }  else
    Time=AddTime(DateToDateTime(DateTimeToDate(CurTime)),DateTimeToTime(Time),0);
#ifndef __arm
  SetPage(ParametersPage);
#endif
  if (MeasurementsInArchive) {
    if (DateTimeToDate(Time)!=EncodeDate(Parameters->Year%100+2000,Parameters->Month,Parameters->Day)) {
      CurMeasurement=0;	// acm, unused
    }
  }
  
  
} else {
  Time=TimeToDateTime(EncodeTime(Setup.SetupInfo.dTime.Hour,Setup.SetupInfo.dTime.Min,0));
  if (Time) {
    Time=AddTime(CurTime,Time,1);
    
#ifndef __arm
    SetPage(ParametersPage);
#endif
    if (MeasurementsInArchive) {
      if (DateTimeToDate(Time)!=EncodeDate(Parameters->Year,Parameters->Month,Parameters->Day)) {
        CurMeasurement=0;  // acm, unused
      }
    }
  }
}
TimeNextMeas = Time;
}

#ifdef __arm
#define MaxLEDBlinkCounter 12000
#define MaxLEDBlinkCounterOnError 3000

// acm, comment out 2 lines if move LED to PIT/*acm
int LEDBlinkCounter=0;
char CurLEDBlinkState=0;//*/
char stop_yellow_flashing = 1;

#ifndef __emulator
static void FilterPWMInterrupt(void)  // acm, this used to blink LED's, more importantly output 8khz clock for switched capacitor LPF's to operate, fc=80hz
{
  int CurMaxLEDBlinkCounter;
  unsigned int dummy;
  char c;			// acm, change from int to char
  
  // Acknowledge interrupt status
  dummy = AT91C_BASE_TC1->TC_SR;
  // Suppress warning variable "dummy" was set but never used
  dummy = dummy;
  // someday move to PIT service routine in keyboard.c to reduce CPU load:  measured ~4% faster balance()... /*
  if (RedrawEnabled) {			// acm, appears RedrawEnabled (for ext display) CLEARED prior to every SPI transfer
    c=Led;							// acm, Led == variable set in LED macro that drives LED to CPLD.  bit0==green, bit1=yellow, bit2=red.  
    if ((c&(1<<0))==1) {			// acm, if green set on, blink if count reach, else exit.  Green turned on bottom main(), so if hang, green stays off.
      LEDBlinkCounter++;
      //Blinking LED quick or slow
      if ((!BusyFlag)&&(Error&~((unsigned long)1<<erPause))) CurMaxLEDBlinkCounter=MaxLEDBlinkCounterOnError;
      else CurMaxLEDBlinkCounter=MaxLEDBlinkCounter;
      if (!SPIBusy) {				// acm, !SPIBusy mutually exclusive with RedrawEnabled?
        if (LEDBlinkCounter>CurMaxLEDBlinkCounter) {
          if (CurLEDBlinkState) 	// acm, if LED's on, turn off
          {
            c&=~(1<<0);// acm stub out, back to pre-1.79? c&=~(1<<1); //acm, blink off green & yellow LED, i.e. c&=~(3)
            CurLEDBlinkState=0;
          } else	
          {
            c|=(1<<0);// acm, blink on green
            CurLEDBlinkState=1;
          }
          AT91F_disable_interrupt();
          OutToAltera(LedAdrr,(char)c);
          AT91F_enable_interrupt();
          LEDBlinkCounter=0;
        }
      }
    }
  }// acm*/
}
#endif

static void SetFilterPMW(void)			// acm, this used to blink LED's, more importantly output 8khz clock for switched capacitor LPF's to operate, fc=80hz
{
#define LED_priority 0
#ifndef __emulator
  float Freq;
  //unsigned int mask ;
  
  Freq=8000.0;
  PrepareWaveTimer(1,1,&Freq);
  //* Open Timer 1 interrupt
  // acm, comment out 3 lines to disable Timer1 IRQ, move LED code to PIT
  AT91F_AIC_ConfigureIt ( AT91C_BASE_AIC, AT91C_ID_TC1, LED_priority, AT91C_AIC_SRCTYPE_INT_HIGH_LEVEL, (void(*)(void))FilterPWMInterrupt);
  AT91F_TC_InterruptEnable(AT91C_BASE_TC1,AT91C_TC_CPAS);  //  IRQ enable CPC
  AT91F_AIC_EnableIt (AT91C_BASE_AIC, AT91C_ID_TC1);
  // acm */
  
  StartWaveTimer(1);
#endif
}
#endif


void AutoBalancePlusDay(void)
{
  _TTime Time;
  _TDate Date;
  unsigned int Year,Month,Day,Hour,Min,Sec;
  _TDateTime tmpTime;
  
  // + �����
  tmpTime = DoPackDateTime(EncodeDate(Setup.SetupInfo.AutoBalans.Year%100+2000,Setup.SetupInfo.AutoBalans.Month,Setup.SetupInfo.AutoBalans.Day),
                           EncodeTime(Setup.SetupInfo.AutoBalans.Hour,0,0));
  Time=TimeToDateTime(EncodeTime(23,59,59));
  if (Time) tmpTime=AddTime(tmpTime,Time,0);
  Time=TimeToDateTime(EncodeTime(0,0,1));
  if (Time) tmpTime=AddTime(tmpTime,Time,0);
  DoUnPackDateTime(tmpTime,&Date,&Time);
  DecodeDate(Date,&Year,&Month,&Day);
  DecodeTime(Time,&Hour,&Min,&Sec);
  Setup.SetupInfo.AutoBalans.Year=Year%100;
  Setup.SetupInfo.AutoBalans.Month=Month;
  Setup.SetupInfo.AutoBalans.Day=Day;
  Setup.SetupInfo.AutoBalans.Hour=Hour;
}


// acm, v2.01
char Str_to_month(char const * date)
{
  int x;  // compiler more efficient with int vs char...
  const char * mo_list[12]={"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
  
  for(x=0;x<12;x++)
    if(!strncmp(date, mo_list[x],3)) return x+1; //return build Month
  
  return 0;			// 0 some sort of error
}

#ifndef __emulator

int main();

#endif

#ifdef __emulator
void C_Entry(void) {
#else
  //#pragma optimize=s 2 //ag 2-24-16 change optimization in all main()
  int main() {
    //  u32 A=(int)MainAddr;
#endif
    
    KEY ch;
    char IsNeedRedraw=1;
    unsigned int Cmnd;
    _TDateTime CurTime, tmpTime;//, OldCurTime; // 12-4 ACM add OldCurTime for RTC debug below
    
    _TTime Time;
    char WatchEnabled=1;
    float PshHV=0,Freq=0;
    unsigned long tmpError;
    char tSec=60;
    int i,j;//acm,errorcount,passcount,freeze,twi_retry, twi_error;  // 12-4 ACM add errorcount for RTC debug below
    //char diag_time[7]; //acm 11-8-10
    
    float AmplA,AmplB,AmplC,ABPhaseShift,ACPhaseShift;
    //unsigned long EndMeasurementTime=0;  // acm, 11-29 change scope, define above, to ref in set_time()
    int number_records_to_delete;						//acm, 11-22
    
    extern unsigned int diag_measurement_count;			// count, init to 0 at power up, if = 10 set flag below
    
    diag_measurement_count = 0;					// ensure variables cleared after reset
    
    // acm, 7-14-10
    u32 uiTemp;
    
    // acm, v2.01
    char s[5]={'\0','\0','\0','\0','\0'}; // v2.01, int to string, for register 40-2
    s16 fwdate, fwday;
    
    /* Enable User Reset and set its minimal assertion to */
    //???
    //AT91C_BASE_RSTC->RSTC_RMR = AT91C_SYSC_URSTEN | (0xF<<8) | (unsigned int)(0xA5<<24);
#ifndef __emulator
    AT91C_BASE_RSTC->RSTC_RMR = (0xC<<8) | (unsigned int)(0xA5<<24);
#endif
    Delay(50);
    
    InitSAM7();
    //InitRTC();  //acm, do just once here //ag 2.10 already called in InitSAM7
    
    SetDeviceBusy();
    LED1Green;
    LED2Yellow;
    
#ifndef __emulator
    SetFilterPMW();		// acm, 8000hz frequency, IRQ's/sec, blink LED.  8000/100=80hz, switched cap LPF
    
    DisableLCD(0xFF);
    SPI_Prepare(SPIFREQ_16000,0);
    SPI_SetCS(SPI_CS_ALTERA);
    SPI_WriteRead(0xD0);
    IsCurrentGain=SPI_WriteRead(0);
    SPI_ClearCS();
    EnableLCD();
    IsCurrentGain=IsCurrentGain&0x80;  // acm, ?
    if (IsCurrentGain) {
      SetCurrentGain2;
      CurrentGain=2;
    }
#endif
    
#ifndef TestOFF
    Delay(150);
    if (ReadKey()==KB_ESC)
      RunTest();
#endif
    
    InitLog();
    
    RunMainWin(0);    // acm, output version number here
    
    //while (1) ReadTemperature(0);
    
#ifndef __emulator
    DisableLCD(0xFF);
    SPI_Prepare(SPIFREQ_16000,0);
    SPI_SetCS(SPI_CS_ALTERA);
    SPI_WriteRead(DeviceAdrr);
    PreSetDeviceAddr=SPI_WriteRead(0);
    SPI_ClearCS();
    EnableLCD();
#endif
    
    LoadSetup(1,1);
    
    InitDAC();
    InitInitialParameters();
    
    if (!OpenFlash()) {
      Error|=((unsigned long)1<<erFlashReadFail);
      Error|=((unsigned long)1<<erFlashWriteFail);
    } else {
      SetPage(MeasListPage);
      CreateList((u16 *)MeasListTempAddr,&MeasurementsInArchive,stForward/*����� � ������*/);
      SaveFileIndexes(MeasListTempAddr);
      LoadLastMeasurement();
    }
    
    //if (ch&0x07) {
    //   SaveLog(lcPowerOn);				// acm, this will add entry to log showing unit powered up, wow.
    //   SaveLogIfNewError(lcPowerOn,lcPowerOn);      // DE 4-24-15 do not repeat "Power turned on" in log
    SaveLogIfNewErrorAndSkipLast(lcPowerOn,lcPowerOn);      // DE 4-24-15 do not repeat "Power turned on" in log
    //}
    
    ++Setup.SetupInfo.ResetCount; //2.06 Increment reset count
    SaveSetup(); //2.06 saves new ResetCount
    SetDeviceNoBusy();
    LED1Green;
    RunMainWin(1);
    SetDeviceBusy();
    LED1Green;
    LED2Yellow;
    ClearLCD();
    SetFont(Font6x5);
    OutString(4,4,LoadingStr);  // acm, change display output message to "...Loading..."
    Redraw();
    
    for (ch=0;ch<MaxTransformerSide;ch++) 
    {
      WatchAmpl[ch]=0; // acm, level used by WatchFunc() processed in forground loop below
      // acmV1.79 auto phase rotation change starts here.  Set array if phase BC needs rotating, used by IRQ service routine to determine when to swap A/D inputs, and SetChannels.  Balancing() sets initial parameters with set 1 or set 2 swap state. 
      swap_phaseBC[ch]=Setup.InitialParameters.TrSideParam[ch].DefectCode&warnsetphaserot;  	// acm, recall phaseBC swap state from Setup prior to first input test
      if  (!(Setup.GammaSetupInfo.ReadOnSide&(1<<ch))) continue;
      if (ReadSourcePhases(&AmplA,&AmplB,&AmplC,&ABPhaseShift,&ACPhaseShift, ch, &Freq, 0)) 
      {
        if (!RunCalibration(&PshHV,&Freq,&WatchAmpl[ch],1,ch)) 
        {
          WatchAmpl[ch]=0;
        }
      }
    }
    //2.06 ag moved up here to save correct errors with reading //2.06 ag added check for ch ON//DE 8-8-16 fixed XFMR OFF
    if (Setup.GammaSetupInfo.ReadOnSide==3 && Error&((ErrorType)1<<erSet1Off) && Error&((ErrorType)1<<erSet2Off)) Error|=((ErrorType)1<<erUnitOff);
    else if (Setup.GammaSetupInfo.ReadOnSide==1 && Error&((ErrorType)1<<erSet1Off)) Error|=((ErrorType)1<<erUnitOff);
    else if (Setup.GammaSetupInfo.ReadOnSide==2 && Error&((ErrorType)1<<erSet2Off)) Error|=((ErrorType)1<<erUnitOff);
    if (Error&(ErrorType)1<<erUnitOff) Error&=~(((ErrorType)1<<erSet1Off)|((ErrorType)1<<erSet2Off)); //2.06 ag clear setoff errors if unit off
    
    if(Setup.GammaSetupInfo.ReadOnSide==0) Error|=((unsigned long)1<<erStop); //2.06 ag change to stop DE 8-9-16 fixed UNIT OFF 
    else if (!Setup.SetupInfo.Stopped) Error&=~((unsigned long)1<<erStop);	//2.06 have to do so otherwise does not go from stopped
    
    WatchCounter=0;
    RunWatch=0;
    
    if (Setup.GammaSetupInfo.ReadOnSide&(1<<HVSide))
      SetInputChannel(HVSide,chGamma);
    else
      if (Setup.GammaSetupInfo.ReadOnSide&(1<<LVSide))
        SetInputChannel(LVSide,chGamma);
      else
        SetInputChannel(HVSide,chGamma);
      
      ClaerAllSource();
      
      IsNeedRedraw=1;
      
      
      GetNextTime(DateTimeNowNoSec());
      
      tmpError=Error;
      tmpError&=~((unsigned long)1<<erPause);
      if (!tmpError) {
        SetRelayOnLine;
      } else {
        ClearRelayOnLine;
      }
      
      // acm, dead battery check -> if bad, set RTC to FW build date for now. If Main module installed, RTC will sync to that, otherwise if measurements in archive, date will be to that date in main loop.
      if (RTC_STATUS() & 0x80)
      {
	
        // v2.01
	s[0]=s[1]='0';
	s[2]=fw_build_date[4];
	s[3]=fw_build_date[5];
	fwdate=(s16)StrToInt(s);	//build date
        
	s[0]=fw_build_date[7];
	s[1]=fw_build_date[8];
	s[2]=fw_build_date[9];
	s[3]=fw_build_date[10];
	fwday=(s16)StrToInt(s);	//build year
	
        //	SET_TIME(fw_build_date[5], fw_build_date[4], fw_build_date[3], fw_build_date[2], fw_build_date[1], fw_build_date[0]);
	// v2.01
	SET_TIME(Str_to_month((char const *)fw_build_date), fwdate, fwday, 0,0,0);
	
	clr_RTC_STATUS(); // acm, clear OSF bit
	Error|=(unsigned long)(1<<erClock2);
	CurTime = DoPackDateTime(EncodeDate(fw_build_date[5]%100+2000,fw_build_date[4],fw_build_date[3]),
                                 EncodeTime(fw_build_date[2],fw_build_date[1],  0));
      }
      else
      {
	CurTime = DateTimeNowNoSec();
      }
      
      SetDeviceNoBusy();
      
#ifndef __emulator
      while (1) {
#else
        while (TryClose==0) {
#endif
          
          SwWd=0; //6.02 clear Software Watchdog;
          
          // acm, dead battery check
          if (RTC_STATUS() & 0x80) Error|=(unsigned long)(1<<erClock2);	
          
          ScanMessages();
          
          if (LCDConnected&&IsNeedRedraw) //ag 2-24-16 added check that dispaly is connected
          {
            if (MeasurementsInArchive) {
              ShowParameters(Parameters,Diagnosis,Setup.SetupInfo.DisplayFlag,ch,ParametersPage,ParametersPage,shShowErrors);
            } else
              ShowParameters(Parameters,Diagnosis,Setup.SetupInfo.DisplayFlag&0x383,ch,ParametersPage,ParametersPage,shShowErrors);
            IsNeedRedraw=0;
            Redraw();
          }
          
          if (MeasurementsInArchive) 
          {
            // acm, 12-27 comment out here, discovered with big archive this takes too long	    LoadLastMeasurement();
            // during verification witnessed case were *Parameters got cleared out, so I added this here to fix.  But now digging deeper, don't see
            // where in code this could happen.
            ResetWatchDog();  		// acm, insurance, for some reason saw reset during clear large archive
            EndMeasurementTime=DoPackDateTime(EncodeDate(2000+((*Parameters).Year%100),(*Parameters).Month,(*Parameters).Day),
                                              EncodeTime((*Parameters).Hour, (*Parameters).Min,0));
            
            if (CurTime<EndMeasurementTime) // complicated logic to choose best time if anomolous condition encountered
            {
              if (fresh_RTC && !(Error&(unsigned long)(1<<erClock)) /*&& !(Error&(unsigned long)(1<<erClock2))*/ && !(Error&(unsigned long)(1<<erClock_rd_TWI)))  // fresh_RTC implies RTC set to past intentionally
              {// delete all "bogus" records 
                //AT91F_disable_interrupt();  // acm doing this causes resets, don't know why.  Idea to hold of doing measurements, may overkill.  Observed IRQ happened in link tranlate command decode
                SetDeviceBusy();
                DisableWatchDog();
                
                number_records_to_delete = GetMOnDate(((long)DateTime.Year+(long)2000)*(long)10000+(long)DateTime.Month*(long)100+(long)DateTime.Day,
                                                      (long)DateTime.Hour*(long)10000+(long)DateTime.Min*(long)100+(long)DateTime.Sec);
                // acm, exactly how records deleted in MODBUS command
                // notes:
                // FRAM contains file index array, 2 byte index, a number representing last measurement stored, sorted highest 1st
                // fileindex(0) represents last data record
                // fileindex(MeasurmentsInArchive) is oldest data record
                for (i=number_records_to_delete;i<=MeasurementsInArchive;i++) FileDelete(LoadFileIndex(MeasurementsInArchive-i));
                // resort file index array, store back to FRAM, takes a long time
                CreateList((u16 *)MeasListTempAddr,&MeasurementsInArchive,stForward/*����� � ������*/);
                SaveFileIndexes(MeasListTempAddr);
                
                // acm, reload most recent data point
                LoadLastMeasurement();
                // acm, recalculate last measurement time
                EndMeasurementTime=DoPackDateTime(EncodeDate(2000+((*Parameters).Year%100),(*Parameters).Month,(*Parameters).Day),
                                                  EncodeTime((*Parameters).Hour, (*Parameters).Min,0));
                
                // acm, clear "baseline" if date occured in "future"
                for (j=0;j<MaxTransformerSide;j++)// acm, 3/1/11 clear each baseline independently
                {
                  if (Setup.GammaSetupInfo.GammaSideSetup[j].STABLEDate > CurTime)
                  {
                    Parameters->TrSide[j].Trend=  // acm, little bug fix in rc13, changed variable i to j
                      Parameters->TrSide[j].KTPhase=
                        Parameters->TrSide[j].KT=
                          Trend[j]=
                            KTPhase[j]=
                              KT[j]=0;
                    // baseline data
                    Setup.GammaSetupInfo.GammaSideSetup[j].STABLEDate=0;
                    Setup.GammaSetupInfo.GammaSideSetup[j].STABLETemperature=0;
                    // clearing allows baseline to be re-run after next measurment, using any remaining data in archive + required new measurments required to reach min (38)
                    Setup.GammaSetupInfo.GammaSideSetup[j].STABLESaved=0;
                    ClearInitialTgDelta(AllSides);
                    ClearInitialZk(); // AG "not used"
                    //ClearArchive();  // we've already cleared desired data, don't clear data we don't have too
                    // acm, stuff from ClearArchive() that applies to this situation:
                    //***************************************************************
                    OutRelayFrom=0;
                    CurMeasurement=0;	// acm, unused
                    //Number of confirmation measurement �����  ���������� ������������ ������
                    SaveNum=1;
                    //Current state Gamma
                    GammaStatus=stUnknown;
                    //Current state of all parameters
                    FullStatus=stUnknown;
                    MeasurementResult=0;
                    DiagResult=0;
                    
                    for (i=0;i<szInsulationParameters;i++)
                      *((char*)Parameters+i)=0;
                    
                    for (i=0;i<szDiagnosis;i++)
                      *((char*)Diagnosis+i)=0;
                    
                    ClearAvg();
                    
                    OutGammaStatus(GammaStatus=stUnknown,0);
                    
                    for (i=0;i<MaxBushingCountOnSide;i++) {
                      //Phase signal in mV 
                      Setup.GammaSetupInfo.GammaSideSetup[j].StableSourceAmplitude[i]=(unsigned int)0;
                      //Phase, deg
                      Setup.GammaSetupInfo.GammaSideSetup[j].StableSourcePhase[i]=(unsigned int)0;
                      Setup.GammaSetupInfo.GammaSideSetup[j].StablePhaseAmplitude[i]=(unsigned int)0;
                      //Phase, deg
                      Setup.GammaSetupInfo.GammaSideSetup[j].StableSignalPhase[i]=(unsigned int)0;
                      Setup.GammaSetupInfo.GammaSideSetup[j].STABLEDeltaC[i]=0;
                      Setup.GammaSetupInfo.GammaSideSetup[j].STABLEDeltaTg[i]=0;			  
                    }
                    
                    SaveLog(lcClearAll);
                    TestSetup();
                  }
                }			  
                EnableWatchDog();// AT91F_enable_interrupt();
                SetDeviceNoBusy();
                
                fresh_RTC=0; stale_RTC=0;  // reset flag/timer to re-enable RTC change filter
              }
              else
              {
                // update RTC with last stored time, as previous algorithm did
                // RTC changed, but not from MODBUS command
                DateTime.Year=(*Parameters).Year;  // acm, store last measurement time (probably loose a few minutes)
                DateTime.Month=(*Parameters).Month;
                DateTime.Day=(*Parameters).Day;
                DateTime.Hour=(*Parameters).Hour;
                DateTime.Min=(*Parameters).Min;
                SET_TIME_FROMBUF(); SET_TIME_FROMBUF(); // acm 2-18-12 double set to ensure time set sticks
                
                Error|=(unsigned long)(1<<erClock); //11-16-08 cast as long, ensure erClock bit set
              }
              // following logic applied for either case above, change RTC to last measure time or delete future data...
              
              // acm, 11-18-10 recalculate CurTime - this is new, suspect fixes some bad end case...
              GetNextTime(CurTime=DateTimeNowNoSec());
            }
          }
          
          // acm, fresh_RTC reset after 30 minute (from last time set by user)
          //      check here vs. above, to guarentee at least 1 chance to delete old records.
          //      Observed setting time to past triggers new measurement, which may take 1+ minute...
          if (fresh_RTC)
            if ((stale_RTC>GetTic32()) || (GetTic32()-stale_RTC)>(30*60*33)) {fresh_RTC=0; stale_RTC=0;};  // when rollover or timeout reset flag
          
          //pause
          if (Error&(1<<erPause)) {
            uiTemp=(GetTic32()-PausedFrom)/32/60;
            if (uiTemp>=MaxPauseMin) {
              PausedFrom=0;
              Silenced=0;
              Error&=~(1<<erPause);
            }
          }
          //2.06 ag clear erStop if ch enabled and not stopped or set if no ch enabled
          if(Setup.GammaSetupInfo.ReadOnSide==0) Error|=((unsigned long)1<<erStop); //2.06 ag change to stop
          else if (Error&(unsigned long)1<<erStop && !Setup.SetupInfo.Stopped) Error&=~((unsigned long)1<<erStop);
          
          //2.06 ag setting or clearing erLoCurrent right away not waiting for a measurement
          if ((Setup.GammaSetupInfo.ReadOnSide&(1<<0) && Setup.SetupInfo.LoadCurrentThresholdSettings&0x3) ||
              (Setup.GammaSetupInfo.ReadOnSide&(1<<1) && Setup.SetupInfo.LoadCurrentThresholdSettings&0xC)) {	//2.06 ag //load current is bad threshold is enabled for set that is enabled
                if (ExtLoadActive < Setup.GammaSetupInfo.GammaSideSetup[0].RatedCurrent * 0.05) Error|=((unsigned long)1<<erLoCurrent);	//load current is bad
                else Error&=~((unsigned long)1<<erLoCurrent); //load current threshold enabled and is good
              }
          else Error&=~((unsigned long)1<<erLoCurrent);	//load current threshold is not enabled
          
          //Rain timer  //DE 4-1-16 decrement Rain timer every minute
          if (Error&(unsigned long)1<<erRainTimer){
            if (cntTemp>GetTic32()){
              cntTemp=65535-cntTemp+GetTic32();
            }
            else{
              cntTemp=GetTic32()-cntTemp;
            }
            if (RainTo>cntTemp){
              RainTo=RainTo-cntTemp;}
            else  {
              RainTo=0;
              Error&=~((unsigned long)1<<erRainTimer); 
            }
            cntTemp=GetTic32();
          }
          
          // acm, PDM lacks this function to turn off relay based on TimeOfRelayAlarm setting.  AG says not useful for BHM too, but will not touch.
          if (OutRelayFrom>0) {
            Cmnd=(DateTime.Min*60+DateTime.Sec);
            if (Cmnd<OutRelayFrom)
              Cmnd=(MaxMinPerDay-OutRelayFrom)+Cmnd;
            else Cmnd-=OutRelayFrom;
            if (Cmnd>Setup.SetupInfo.TimeOfRelayAlarm) {
              OutRelayFrom=0;
              if (!Setup.SetupInfo.Stopped)
                SetRelayNormal;//PORTD&=~(1<<7);
            }
          }
          // 6.02 comment out HeaterOnTemperature was always 0 anyway
          //   if (ReadIntTemperature()<Setup.SetupInfo.HeaterOnTemperature) {
          //      ONTempRelay;
          //   } else {
          //      if (ReadIntTemperature()>Setup.SetupInfo.HeaterOnTemperature+HeaterTemperatureTrend) {
          //         OFFTempRelay;
          //      }
          //   }
          if (RunXX) {
            if (IsInitialParam) {
              if (RunXXTest()) {
                if (Setup.SetupInfo.NoLoadTestActive) {
                  Setup.SetupInfo.NoLoadTestActive=0;
                  TestSetup();
                }
              } else {
                if (Setup.SetupInfo.NoLoadTestActive) {
                  // + day
                  AutoBalancePlusDay();
                  TestSetup();
                }
              }
            }
            RunXX=0;
            IsNeedRedraw=1;
          }
          
          CurTime = DateTimeNowNoSec();
          
          if (LCDConnected && DateTime.Sec!=tSec) {	//2.06 add LCDConnected check
            tSec=DateTime.Sec;
            IsNeedRedraw=1;
          }
          
          if (!DateTime.Sec) {
            if (Setup.CalibrationCoeff.CurrentChannelOnPhase[0]||
                Setup.CalibrationCoeff.CurrentChannelOnPhase[1]||
                  Setup.CalibrationCoeff.CurrentChannelOnPhase[2])
              ReadActiveLoad();
          }
          
          ///////Autobalansing
          if (((Setup.SetupInfo.AutoBalansActive)||(Setup.SetupInfo.NoLoadTestActive))&&
              (IsDate(Setup.SetupInfo.AutoBalans.Year,Setup.SetupInfo.AutoBalans.Month,Setup.SetupInfo.AutoBalans.Day))&&
                (IsTime(Setup.SetupInfo.AutoBalans.Hour,0,0)))  {
                  
                  tmpTime = DoPackDateTime(EncodeDate(Setup.SetupInfo.AutoBalans.Year%100+2000,Setup.SetupInfo.AutoBalans.Month,Setup.SetupInfo.AutoBalans.Day),
                                           EncodeTime(Setup.SetupInfo.AutoBalans.Hour,0,0));
                  if ((tmpTime<=CurTime)){
                    if (Setup.SetupInfo.WorkingDays) {
                      LED2Yellow;
                      SetDeviceBusy();
                      
                      if (LCDConnected){	//2.06 ag add check
                        ClearLCD();
                        SetFont(Font8x8);
                        OutString(4,4,MeasuringStr);
                        Redraw();}
                      
                      i=0;
                      j=0;
                      for (ch=0;ch<MaxTransformerSide;ch++) {
                        if  (!(Setup.GammaSetupInfo.ReadOnSide&(1<<ch))) continue;
                        i++;
                        Freq=0;
                        if (ReadSourcePhases(&AmplA,&AmplB,&AmplC,&ABPhaseShift,&ACPhaseShift, ch, &Freq, 0)) {
                          j++;
                        }
                      }
                      
                      if ((i)&&(i==j))  Setup.SetupInfo.WorkingDays--;
                      else Setup.SetupInfo.WorkingDays=Setup.SetupInfo.AutoBalansActive-1;
                      // + day
                      AutoBalancePlusDay();
                      TestSetup();
                      SetDeviceNoBusy();
                    } else {
                      if (Setup.SetupInfo.AutoBalansActive) AutoBalance=1;
                      if (Setup.SetupInfo.NoLoadTestActive) RunXX=1;
                    }
                  }
                }
          
          if (((TimeNextMeas>0)&&(CurTime>=TimeNextMeas))||(ReadSingle))
          {
            if ((!Setup.SetupInfo.Stopped)&&(PausedFrom==0))
              ReadOnTime=1;
          }
          
          if ((ReadOnTime)||(ReadSingle)) 
          {
            // acm, doesn't exist        LED2Yellow;
            if (LCDConnected) 
            {	//2.06 ag add check
              ClearLCD();
              SetFont(Font8x8);
              OutString(4,4,MeasuringStr);
              Redraw();
            }
            
            SetDeviceBusy();
            MeasurementResult=StartMeasure(svSaveAllways,0); // acm, svSaveAllways same effect as svSaveCurrenht
            
            if (MeasurementResult==msStatusReRead) 
            {
              Time=TimeToDateTime(EncodeTime(0,Setup.GammaSetupInfo.ReReadOnAlarm,0));
              if (Time) 
                TimeNextMeas=AddTime(CurTime,Time,0);
            } 
            else 
              GetNextTime(CurTime);
            
            CurTime = DateTimeNowNoSec();
            
            ReadOnTime=0;
            ReadSingle=0;
            IsNeedRedraw=1;
            WatchCounter=0;
            RunWatch=0;
            
            SetDeviceNoBusy();
          } 
          else if(PausedFrom == 0) 
          {
            //acm, behavior here to guarentee read of DUT if in Alarm, at least once every 1/2 hour independant of set schedule
            if ((Setup.SetupInfo.ScheduleType==1)||
                ((Setup.SetupInfo.ScheduleType==0)&&(Setup.SetupInfo.dTime.Hour>0)&&(!((Setup.SetupInfo.dTime.Hour==1)&&(Setup.SetupInfo.dTime.Min==0)))))
            {
              if (((DateTime.Min==0)||(DateTime.Min==30))&&(DateTime.Sec<5)) 
              {
                LED2Yellow;
                
                if (LCDConnected)//2.06 add check
                { 
                  ClearLCD();
                  SetFont(Font8x8);
                  OutString(4,4,MeasuringStr);
                  Redraw();
                }
                
                SetDeviceBusy();
                MeasurementResult=StartMeasure(svSaveOnlyRED,0);  // acm, perform measurement but do not save unless RED alarm
                SetDeviceNoBusy();
              }  
            }
          }
          
          // acm, apparently RunWatch is source of mysterious "measuring" message and nothing stored.
          // RunWatch enabled by PIT timer, every 5 minutes.
          
          if ((RunWatch)&&(WatchEnabled)) 
          {
            if ((!Setup.SetupInfo.Stopped)&&(!PausedFrom)&&(GammaStatus<stAlarm))
            {
              LED2Yellow;
              SetDeviceBusy();
              if (LCDConnected){ //2.06 add check
                ClearLCD();
                SetFont(Font8x8);
                OutString(4,4,MeasuringStr);
                Redraw();}
              if (WatchFunc(&WatchAmpl[0])) // acm, if Gamma value exceeds red alarm threshold clear running average
              {
                // acm, v2.01, philosophy change, always keep enabled
                //WatchEnabled=0;
                ReadSingle=1; // acm, hi imbalance schedules full read (error messages and such)
                ClearAvg();
              }
              RunWatch=0;
              SetDeviceNoBusy();
            }
          }
          if (LCDConnected){ //2.06 ag moved here before key operation
            ch=ReadKey();
            if (ch==KB_MOD) {
              LED2Yellow;
              SetDeviceBusy();
              RunSetup();
              IsNeedRedraw=1;
              SetDeviceNoBusy();
              ch=KB_NO;
            }
            if (ch==KB_MEM) {
              LED2Yellow;
              SetDeviceBusy();
              WorkWithArchive(1);
              IsNeedRedraw=1;
              SetDeviceNoBusy();
              ch=KB_NO;
            }
            if ((ch==KB_UP)||(ch==KB_DOWN)||(ch==KB_RIGHT)||(ch==KB_LEFT)) IsNeedRedraw=1;
          } 	
          else ch=KB_NO;
          
          
          
          if (AutoBalance) {
            TestSetupCRC(); // acm, 2-13-09 Botov fix.  Why check prior to balance, don't POT EEPROM's get reinit'ed during balance?  What is "Autobalance" routine up at line 608??
            LED2Yellow;
#ifndef OldR1500_6
            LED2Yellow;
            ClearLCD();
            SetFont(Font8x8);
            OutString(4,4,BalancingStr);
            Redraw();
            ClearAllData();
            SetDeviceBusy();  // acm, ClearAllData() clears busy, so set here instead
            if (FullAutoBalansing()) {
              if (Setup.SetupInfo.AutoBalansActive) {
                Setup.SetupInfo.AutoBalansActive=0;
                if (Setup.SetupInfo.NoLoadTestActive) {
                  if (RunXXTest()) {								// acm, winding distortion feature?
                    Setup.SetupInfo.NoLoadTestActive=0;
                    RunXX=0;
                  } else {
                    // + day
                    AutoBalancePlusDay();
                  }
                }
                TestSetup();
              }
            } else {
              // AutoBalans did not pass
              if (Setup.SetupInfo.AutoBalansActive) {
                tmpTime = DoPackDateTime(EncodeDate(Setup.SetupInfo.AutoBalans.Year%100+2000,Setup.SetupInfo.AutoBalans.Month,Setup.SetupInfo.AutoBalans.Day),
                                         EncodeTime(Setup.SetupInfo.AutoBalans.Hour,0,0));
                if ((tmpTime<=CurTime)){
                  //  +day
                  AutoBalancePlusDay();
                  TestSetup();
                }
              }
            }
            AutoBalance=0;
            SetDeviceNoBusy();
#endif
          }
          
#ifdef __emulator
          if (ch==KB_ENTER) StartMeasure(svSaveAllways,0);
#endif
          
#ifndef __emulator
          tmpError=Error;
          tmpError&=~((unsigned long)1<<erPause);
          tmpError&=~((unsigned long)1<<erUnitOff);
          tmpError&=~((unsigned long)1<<erClock2);  //ag2-24-16 masking erClock2 
          tmpError&=~((unsigned long)1<<erClock);  //ag2-24-16 masking erClock
          tmpError&=~((unsigned long)1<<erClock_rd_TWI);  //ag2-24-16 masking erClock_rd_TWI
          //tmpError&=~((unsigned long)1<<erRainTimer);  //2.06 ag enable rain timer DE 4-6-16 clear erRainTimer
          tmpError&=~((unsigned long)1<<erSide1Critical|(unsigned long)1<<erSide2Critical);	//2.06 ag clear critical errors, if non other exists. Case: clear rain timer or low load current caused critical error
          
          // acm, v1.79, blink yellow LED every 5 min: On (even seconds)/Off (odd seconds), 6 sec duration.  6 SPI calls, i.e. minimal CPU overhead.
          // also blink yellow if just balanced and before next measurement (stop_yellow_flashing)
          if (!Defect_SM && ((DateTime.Sec==0) || !stop_yellow_flashing) && (
                                                                             Setup.InitialParameters.TrSideParam[0].DefectCode ||
                                                                               Setup.InitialParameters.TrSideParam[1].DefectCode))
          {
            Defect_SM=1;  												// enable state machine below
            LED1Yellow;
          }
          
          switch(Defect_SM)											// state machine
          {
          case 0: break;
          case 1:
          case 3:
          case 5:
            { 
              if (DateTime.Sec%2==0) {LED1YOFF; Defect_SM++;} 	// note, Red LED if set turns off...
              break;
            }
          case 2:
          case 4:
          case 6:
            { 
              if (DateTime.Sec%2==1) {LED1Yellow; Defect_SM++;}
              break;
            }
            
          case 7:
          default:
            {
              LED1YOFF;
              if (stop_yellow_flashing)  	// (no more continue flashing until 1st measurement)
                OutLED(FullStatus); 	// set Alarm/Warning LED's back to original state
              Defect_SM=0;
            } 
          }
          
          if (tmpError)
          {
            //      if (Led&(1<<0)) LED1Off;
            ClearRelayOnLine;					// acm, if error, do not drive LED on, PWM IRQ routine will blink fast
          } else {LED1Green; SetRelayOnLine;} //acm, drive Green LED on, negates PWM IRQ routine above blinking off...
          
#endif
          
        }
        
      }
