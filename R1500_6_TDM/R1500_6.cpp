//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
USEFORM("EmulWindow.cpp", FormEmul);
USEUNIT("main.cpp");
USEUNIT("..\COMMON\keyboard.cpp");
USEUNIT("..\COMMON\debug.cpp");
USEUNIT("Init.cpp");
USEUNIT("..\COMMON\LCD112x16.cpp");
USEUNIT("..\COMMON\rtc.cpp");
USEUNIT("..\COMMON\BoardAdd.cpp");
USEUNIT("..\COMMON\dac.cpp");
USEUNIT("..\COMMON\spi.cpp");
USEUNIT("..\COMMON\Graph.cpp");
USEUNIT("PicRes.cpp");
USEUNIT("MainWin.cpp");
USEUNIT("StrConst.cpp");
USEUNIT("Setup.cpp");
USEUNIT("Error.cpp");
USEUNIT("Archive.cpp");
USEUNIT("GammaMeasurement.cpp");
USEUNIT("ScrlMngr.cpp");
USEUNIT("SetChannels.cpp");
USEUNIT("CalcParam.cpp");
USEUNIT("Utils.cpp");
USEUNIT("Diagnosis.cpp");
USEUNIT("balans.cpp");
USEUNIT("..\COMMON\SysUtil.cpp");
USEUNIT("Link.cpp");
USEUNIT("cListBox.cpp");
USEUNIT("CrossMeasurement.cpp");
USEUNIT("Drvflash.cpp");
USEUNIT("flash.cpp");
USEUNIT("LogFile.cpp");
USEUNIT("RAM.cpp");
USEUNIT("DrawGraph.cpp");
USEUNIT("ModBusTCP\ModbusApp.cpp");
USEUNIT("ModBusTCP\ModbusServer.cpp");
USEUNIT("COMPLEX.CPP");
USEUNIT("DACdevice.cpp");
USEUNIT("Test.cpp");
USEUNIT("ADCDevice.cpp");
USEUNIT("ModBusTCP\Usart.cpp");
USEUNIT("TRSTR.cpp");
USEUNIT("..\COMMON\Fram.cpp");
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
    try
    {
         Application->Initialize();
         Application->CreateForm(__classid(TFormEmul), &FormEmul);
                 Application->Run();
    }
    catch (Exception &exception)
    {
         Application->ShowException(&exception);
    }
    return 0;
}
//---------------------------------------------------------------------------
