#ifndef DiagnosisH
#define DiagnosisH

#include "Defs.h"
#include "tgDelta.h"

#define MaxBaseLineMeasurements 700

//#define MinDaysToCalcTrend 10
#define MinDaysToCalcTCoeff 10

/*
   #define MaxBaseCalcMeasurements 3
   #define MaxTrendCalcMeasurements 3
   #define MaxDiagCalcMeasurements  10
   #define MinDaysForDiagnosis 10
   #define MinDaysForTrend 5
   #define MinDaysForBaseLine 5
*/

//   #define MaxBaseCalcMeasurements 100
//   #define MaxTrendCalcMeasurements 100
   #define MaxBaseCalcMeasurements  38
   #define MaxTrendCalcMeasurements 38
   #define MaxDiagCalcMeasurements  38
//   #define MaxDiagCalcMeasurements  4

//   #define MaxZkCalcMeasurements    38
   #define MaxZkCalcMeasurements    5

   #define MinDaysForDiagnosis      10
//   #define MinDaysForDiagnosis      1
   #define MinDaysForTrend          15
   #define MinDaysForBaseLine       15
   #define MinDaysForZk             15


struct stCorrelationData {
       float ccr[MaxBushingCountOnSide][3];
};

struct stSourceForCorrelation {
       float data[MaxBushingCountOnSide+1][MaxBaseLineMeasurements]; //'(ar() - array the includes k-1 what to correlate and the last to what correlate
};  // acm, 3-16-10, two large arrays in diagnosis.cpp are addressed in region defined in RAM.cpp (&Sig[4][5120]), no bounds checking done?

#pragma pack(1)

//������� �����������
struct stDiagnosis {
       struct stTgParameters TgParam[MaxTransformerSide];
       char Reserved[16];
};
#define szDiagnosis sizeof(struct stDiagnosis)

#pragma pack()

extern struct stDiagnosis *Diagnosis;

char DoSumDiag(_TDateTime DiagDate,                    //End Data for Diagnosis
               struct stDiagnosis *DiagData,          //Results of diagnostics
               char FlagBase,                         //1 - on Base Line data, 0 - on offline data
               char InputData,                        //1-  Input, 0-Gamma
               unsigned int MesurementNum,            //����� ������ ��� 0 ���� �� ��������
               char DoOnLineDiag,                     //1- On-Line diagnosis, 0-Sum diagnosis
               char DiagDataPage,
               char UseOnLineData                     //Add On-Line data from Parameters structure
              );

char RunBaseLine(_TDateTime DaseLineDate,
                 char InputData  //1-  Input, 0-Gamma       ''Selecting Gamma channel or inputs for analysis
                );
char GammaTrend(_TDateTime DiagDate,
                char *GammaTempPh,
                char *GammaTemp,
                char *GammaTrend,
                unsigned int MesurementNum,             //����� ������ ��� 0 ���� �� ��������
                char UseOnLineData
);

int DoCalcAvgZk(_TDateTime DiagDate,
                 struct stZkTable *ZkTbl,
                 unsigned int *ZkPercent,
                 unsigned int MesurementNum,             //����� ������ ��� 0 ���� �� ��������
                 char UseOnLineData);


//Gamma Temperature Coefficient, *0.002
extern char KT[MaxTransformerSide];
//Gamma Temperature Coefficient Phase, *1.5
extern char KTPhase[MaxTransformerSide];

//�������� ��������� Gamma (���� � ���*0.2) //acm, 4-27 change from .2 to 1
extern char Trend[MaxTransformerSide];

//char DoDiagnosis(struct stInsulationParameters *Param,struct stDiagnosis *Diag, char StableRegime);
//char RunStabilityTest(void);
//char RunTemperatureTest(void);
#endif
